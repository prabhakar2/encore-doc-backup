({
	doInit : function(component, event, helper) {
		
	},
    
    doSave:function(component, event, helper){
        var action=component.get("c.saveAccount");
        action.setParams({
            "acc":component.get("v.acc")
        });
        action.setCallback(this,function(result){
            var res=result.getReturnValue();
            //alert(res);
            if(res=='Success'){
                var showToast=$A.get("e.force:showToast");
                showToast.setParams({
                    "type":"Success",
                    "title":"Success!",
                    "message":"Record Inserted Successfully",
                    "duration":1000
                });
                showToast.fire();
                $A.get("e.force:refreshView").fire();
            }else{
                var showToast=$A.get("e.force:showToast");
                showToast.setParams({
                    "type":"Error",
                    "title":"Error!",
                    "message":res,
                    "duration":1000
                });
                showToast.fire();
            }
        });
        $A.enqueueAction(action);
        //alert(JSON.stringify((component.get("v.acc"))));
    }
})