({
    /*doInit : function(component, event, helper) {
        var action = component.get("c.getAccounts");
        action.setCallback(this,function(result){
            component.set("v.accounts",result.getReturnValue());
            
        });
        $A.enqueueAction(action);
        
        
    },*/
    
    generateChart : function(component, event, helper) {
        var chartdata = {labels: 
                         ['Sunday','Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                         datasets: [
                             {
                                 label:'Day',
                                 data: [110, 290, 150, 250, 500, 420, 100],
                                 borderColor:'rgba(100, 159, 222, 150)',
                                 fill: true,
                                 pointBackgroundColor: "#FFFFFF",
                                 pointBorderWidth: 4,
                                 pointHoverRadius: 5,
                                 pointRadius: 3,
                                 //bezierCurve: true,
                                 pointHitRadius: 10,
                                 strokeColor:'rgba(111,187,205,0.2)'
                             }
                         ]
                        }
        //Get the context of the canvas element we want to select
        var ctx = component.find("linechart").getElement();
        var lineChart = new Chart(ctx ,{
            type: 'line',
            data: chartdata,
            options: {	
                legend: {
                    position: 'top',
                    padding: 10,
                },
                responsive: true
            }
        });
    }
})