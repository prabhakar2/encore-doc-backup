({
    doInit : function(component, event, helper) {
		console.log('searchAccounts get called in doInit');
        helper.callToServer(
            component,
            "c.findAccounts",
            function(response) {
                console.log(' Response in SK_AccountList doInit:'+ JSON.stringify(response));
                var apexResponse = response;
                component.set("v.returnedRecords", apexResponse);
            }, 
            {
            	acname: component.get("v.searchValue")
            }
        ); 
	},
	searchAccounts : function(component, event, helper) {
		console.log('searchAccounts get called');
        helper.callToServer(
            component,
            "c.findAccounts",
            function(response) {
                console.log(' Response in SK_AccountList searchAccounts:'+ JSON.stringify(response));
                var apexResponse = response;
                component.set("v.returnedRecords", apexResponse);
            }, 
            {
            	acname: component.get("v.searchValue")
            }
        ); 
	}
})