({
	doInit : function(component, event, helper) {
        
		var action = component.get("c.fetchMap");
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            var arr = [];
            for(var key in res){
                arr.push({'key':key,'value':res[key]});
            }
            component.set("v.SampleMap",arr);
            
        });
        $A.enqueueAction(action);
	}
})