({
	doInit : function(component, event, helper) {
        var action=component.get("c.showAccList");
        action.setCallback(this,function(result){
            //alert(result.getReturnValue());
            if(result.getState()=='SUCCESS'){
                component.set("v.listAccount", result.getReturnValue());
            }
        });
        
        $A.enqueueAction(action);
        
	},
    
    navigateToRecord:function(component, event, helper) {
        var idx=event.target.getAttribute('data-index');
        var acc=component.get("v.listAccount")[idx];
        var navEvt=$A.get("e.force:navigateToSObject");
        
        if(navEvt){
            navEvt.setParams({
                recordId: acc.id
                
            });
            navEvt.fire();
        }
        else{
            window.location.href = '/one/one.app#/sObject/'+acc.Id+'/view';
        }
    }
})