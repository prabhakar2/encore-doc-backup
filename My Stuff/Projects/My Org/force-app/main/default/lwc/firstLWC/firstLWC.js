import { LightningElement,api } from 'lwc';

import account_field from '@salesforce/schema/contact.AccountId';
import contName from '@salesforce/schema/Contact.Name';
import title from '@salesforce/schema/Contact.Phone';
import email from '@salesforce/schema/Contact.Email';

export default class FirstLWC extends LightningElement {
    @api RecordId;
    @api objectApiName;
    
    fields = [account_field,contName,title,email];
}