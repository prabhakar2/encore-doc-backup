global class DailyLeadProcessor implements Schedulable{
    global void execute(SchedulableContext sc){
        List<Lead> leadList = new List<Lead>();
        for(Lead ld : [SELECT Id,LastName,LeadSource FROM Lead WHERE LeadSource = NULL]){
            ld.LeadSource = 'Dreamforce';
            leadList.add(ld);
        }
        
        if(leadList.size() > 0){
            update leadList;
        }
    }

}