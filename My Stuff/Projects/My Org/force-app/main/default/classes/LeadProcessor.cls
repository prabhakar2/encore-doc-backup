global class LeadProcessor implements Database.Batchable<sObject> {
    global database.QueryLocator start(database.BatchableContext bc){
        return database.getQueryLocator('select firstname,lastname,company,Status from Lead');
    }
    global void execute(database.BatchableContext bc, List<Lead> scope){
        for(Lead ld : scope){
            ld.LeadSource = 'Dreamforce';
        }
        update scope;
    }
    global void finish(database.BatchableContext bc){
        System.debug('batch Updation successfull');	
    }
}