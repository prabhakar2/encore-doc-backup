@isTest
public class LeadProcessorTest {
    @isTest
    static void test1(){
        List<Lead> leadList = new List<Lead>();
        for(Integer i=1;i<=200;i++){
            Lead ld = new Lead();
            ld.LastName = 'test '+i;
            ld.Company = 'TMC';
            leadList.add(ld);
        }
        insert leadList;
        test.startTest();
        Database.executeBatch(new LeadProcessor());
        test.stopTest();
    }
}