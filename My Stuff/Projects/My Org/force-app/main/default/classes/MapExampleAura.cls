public class MapExampleAura {
    @auraEnabled
    public static Map<Id,List<Contact>> fetchMap(){
        Map<Id,List<Contact>> sampleMap = new Map<Id,List<Contact>>();
        for(Account ac : [SELECT Id,Name,(SELECT ID,LastName,MobilePhone FROM Contacts) FROM Account LIMIT 5]){
            sampleMap.put(ac.Id,ac.Contacts);
        }
        return sampleMap; 
    }
}