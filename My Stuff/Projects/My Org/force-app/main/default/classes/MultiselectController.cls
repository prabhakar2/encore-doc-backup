public class MultiselectController {
    // SelectOption lists for public consumption
    public  Contact cont;
    
    public SelectOption[] leftOptions { get; set; }
    public SelectOption[] rightOptions { get; set; }
    
    public SelectOption[] selectedCards { get; set; }
    public SelectOption[] allCards { get; set; }
    
    public boolean flg { get; set; }
    public id contId;
    
    public MultiselectController(ApexPages.StandardController controller) {
        selectedCards = new List<SelectOption>();
        flg=false;
        List<string> DocList = new List<string>();
        contId=ApexPages.currentPage().getParameters().get('id');
        List<Contact> contlis=[select Aadhar_Card__c,BankAccount__c,Pan_Card__c, VoterId__c from contact where id=:contId];
        if(contlis.size()>0){
            cont=contlis[0];
            if(cont.Aadhar_Card__c==false)
            	DocList.add('Aadhar Card');
            else
                selectedCards.add(new SelectOption('Aadhar Card','Aadhar Card'));
            if(cont.Pan_Card__c==false)
            	DocList.add('Pan Card');
            else
                selectedCards.add(new SelectOption('Pan Card','Pan Card'));

            if(cont.VoterId__c==false)
            	DocList.add('Voter Id');
            else
                selectedCards.add(new SelectOption('Voter Id','Voter Id'));

            if(cont.BankAccount__c==false)
            	DocList.add('Bank Account');
            else
                selectedCards.add(new SelectOption('Bank Account','Bank Account'));

        	flg=false;	
        }
        else{
             cont=new Contact();
             DocList =   new  List<string>{'Pan Card','Aadhar Card','Bank Account','Voter Id'};
            flg=true;
        }
         
       
            allCards = new List<SelectOption>();
        for(string c:DocList)
            allCards.add(new SelectOption(c, c));
        //getCards();
        
    }
    public MultiselectController(){
        
    }
    // Parse &-separated values and labels from value and 
    // put them in option
    private void setOptions(SelectOption[] options, String value) {
        options.clear();
        String[] parts = value.split('&');
        for (Integer i=0; i<parts.size()/2; i++) {
            options.add(new SelectOption(EncodingUtil.urlDecode(parts[i*2], 'UTF-8'), 
                                         EncodingUtil.urlDecode(parts[(i*2)+1], 'UTF-8')));
        }
    }
    
    // Backing for hidden text field containing the options from the
    // left list
    public String leftOptionsHidden { get; set {
        leftOptionsHidden = value;
        setOptions(leftOptions, value);
    }
                                    }
    
    // Backing for hidden text field containing the options from the
    // right list
    public String rightOptionsHidden { get; set {
        rightOptionsHidden = value;
        setOptions(rightOptions, value);
    }
                                     }
    
    public PageReference save() {
        // 
        for(SelectOption so : selectedCards){
            if(so.getLabel()=='Pan Card')
                cont.Pan_Card__c=true;
            if(so.getLabel()=='Aadhar Card')
                cont.Aadhar_Card__c=true;
            if(so.getLabel()=='Bank Account')
                cont.BankAccount__c=true;
            if(so.getLabel()=='Voter Id')
                cont.VoterId__c=true;
        }
        
        upsert cont;
        return new PageReference('/'+contId);       
    }
}