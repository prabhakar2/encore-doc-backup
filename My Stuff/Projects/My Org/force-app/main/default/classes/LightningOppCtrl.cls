public class LightningOppCtrl {
    
	@AuraEnabled
    
    public Static list<Opportunity> getOppList(String OppName){
        
        List<Opportunity> listOpp=new List<Opportunity>();
        //String searchStr=string.escapeSingleQuotes(OppName);
        //searchStr='%' + searchStr + '%';
        string queryString = '';
        /*if(OppName!=null || OppName!=''){
            queryString+='select name,StageName from Opportunity where name like :OppName';
        }
        else*/
            queryString+='select name,StageName from Opportunity';
        
        listOpp=database.query(queryString);
        
        return listOpp;
        
    }
}