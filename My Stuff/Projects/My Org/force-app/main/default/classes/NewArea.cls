public abstract class NewArea 
{     
      public double radius;
      public double length;
      public double area;
    public NewArea()
    {
        
    }
    public NewArea(double r)
    {
        radius=r;
    }
    public double circleArea()
    {
        return(3.14*radius*radius);
    }
    public abstract double rectangleArea(double l,double b);
     public abstract double hexagonArea(double l);
}