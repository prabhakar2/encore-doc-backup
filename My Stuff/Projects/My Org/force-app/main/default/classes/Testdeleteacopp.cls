@isTest
public class Testdeleteacopp {
@isTest
    static void oppTest(){
        Account ac=new Account(name='bbbbb');
        insert ac;
        Opportunity opp=new Opportunity(name='opopo',accountId=ac.Id,CloseDate=System.today(),StageName='Closed Won');
        insert opp;
        
        Database.DeleteResult result=database.delete(ac,false);
        
        System.assertEquals('cant delete the account with opportunity', result.getErrors()[0].getMessage());
    }
}