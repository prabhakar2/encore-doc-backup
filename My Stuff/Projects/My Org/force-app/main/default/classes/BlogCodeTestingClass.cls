public class BlogCodeTestingClass {
    public static Map<String,String> getAllFieldLabel(String objectAPIName){
        
        Map<String,String> fieldAPIToLabelMap = new Map<String,String>();
        Map<String,Schema.SObjectField> mapOfField = Schema.getGlobalDescribe().get(objectAPIName).getDescribe().fields.getMap();
        
        for(String fieldsAPIName : mapOfField.keySet())
            fieldAPIToLabelMap.put(fieldsAPIName,mapOfField.get(fieldsAPIName).getDescribe().getLabel());
        
        return fieldAPIToLabelMap;
    }
}