public with sharing class AccountTriggerHandler {
    public static void CreateAccounts(List<account> acc){
        for(account a: acc){
        	if( a.ShippingState!=a.BillingState)
           a.ShippingState=a.BillingState;
        }
    } 
}