public class distinguish_search {
    string st;
    public void setSt(String n){
        st=n;
    }
    public string getSt(){
        return st;
    }
    public list<Account> account{get;set;}
    public list<Lead> lead{get;set;}
    public list<Opportunity> opportunity{get;set;}
    public list<Contact> contact{get;set;}
    
    public distinguish_search(){
    }
    
    public pageReference search_string(){
        account=[select name,industry from account where name=:st];
        lead=[select name, leadSource from lead where name=:st];
        opportunity=[select name,description from opportunity where name=:st];
        contact=[select firstName,lastName,phone from contact where lastName=:st or firstName=:st];
        
        return null;
    }
}