public class AccountProcessor {
    @future
    public static void countContacts(List<Id> accIds){
        Map<Id,Integer> accIdToCountMap = new Map<Id,Integer>();
        for(AggregateResult agg : [SELECT COUNT(Id) conts,AccountId accId FROM Contact 
                                   WHERE AccountId IN :accIds GROUP BY AccountId]){
            accIdToCountMap.put((Id)agg.get('accId'),(Integer)agg.get('conts'));
        }
        
        if(accIdToCountMap.keySet().size() > 0){
            List<Account> accListToUpdate = new List<Account>();
            for(Id accId : accIdToCountMap.keySet()){
                Account acc = new Account();
                acc.Id = accId;
                acc.Number_of_Contacts__c = accIdToCountMap.get(accId);
                accListToUpdate.add(acc);
            }
            if(accListToUpdate.size() > 0){
                update accListToUpdate;
            }
        }
    }
}