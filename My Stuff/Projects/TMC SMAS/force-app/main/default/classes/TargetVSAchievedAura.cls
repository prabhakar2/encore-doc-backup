public class TargetVSAchievedAura {
    // fetchTargets method work to show initial level of records
    @auraEnabled
    public static String fetchTargets(String userId,String Year){
        try{
            Map<Id,Target_Achieved__c> userToTargetMap = new Map<Id,Target_Achieved__c>();
            List<Target_Achieved__c> targetList = new List<Target_Achieved__c>();
            Map<Id,User> usrsIdToUsrMap = new Map<Id,User>();
            String currentUserId = '';
            
            currentUserId = userinfo.getUserId();
            
            for(User usr : HierarchyFinder.getSubordiatesOf(currentUserId)){
                
                if(String.isNotBlank(userId)){
                    if(usr.Id == userId){
                        userToTargetMap.put(usr.Id,null);
                        usrsIdToUsrMap.put(usr.Id, usr);
                    }
                }else{
                    userToTargetMap.put(usr.Id,null);
                    usrsIdToUsrMap.put(usr.Id, usr);
                }
                
            }
            
            Set<Id> usrIdSet = userToTargetMap.keySet();
            
            String targetQry = 'SELECT Id,Name,User__c,User__r.Name,Total_Number__c,Total_Amount__c,Target_Year__c FROM Target_Achieved__c WHERE User__c IN :usrIdSet';
            
            if(Year != null)
                targetQry+=' AND Target_Year__c =:Year';
            
            for(Target_Achieved__c tar :Database.query(targetQry)){
                if(tar.User__c != null && userToTargetMap.containsKey(tar.User__c)){
                    userToTargetMap.put(tar.User__c,tar);
                }
            }
            
            Decimal totalAmt = 0;
            Decimal totalNumber = 0;
            for(Id usrId : userToTargetMap.keySet()){
                Target_Achieved__c tarAch = new Target_Achieved__c();
                if(userToTargetMap.get(usrId) != null)
                    tarAch = userToTargetMap.get(usrId);
                else{
                    tarAch.User__r = usrsIdToUsrMap.get(usrId);
                    tarAch.Date__c = Date.valueOf(Year+'-'+System.today().month()+'-'+'1');
                    tarAch.User__c = usrId;
                }
                totalAmt +=tarAch.Total_Amount__c != null ? tarAch.Total_Amount__c : 0.0;
                totalNumber +=tarAch.Total_Number__c != null ? tarAch.Total_Number__c : 0;
                targetList.add(tarAch);
            }
            return JSON.serialize(new Wrapper(targetList,totalAmt,totalNumber)); 
        }catch(Exception e){
            return 'Error : '+e.getMessage();
        }
    }
    
    
    
    @auraEnabled
    public static List<monthlyTargetWrapper> getMonthlyTargets(String targetStr,String year){
        Map<String,List<Monthly_Target__c>> monthTargMap = new Map<String,List<Monthly_Target__c>>();
        
        Target_Achieved__c targetObj = (Target_Achieved__c)json.deserialize(targetStr, Target_Achieved__c.class);
        
        if(targetObj.Id != null){
            String targetId = targetObj.Id;
            
            String allFieldsForQuery = getAllFieldsForQuery('Monthly_Target__c');
            String qry = 'SELECT '+allFieldsForQuery+' FROM Monthly_Target__c WHERE Target_Achieved__c =:targetId ';
            qry+=' ORDER BY createdDate ';
            if(Database.query(qry).size() > 0){
                
                for(Monthly_Target__c mt : Database.query(qry)){
                    if(mt.Month__c != null){
                        if(!monthTargMap.containsKey(mt.Month__c )){
                            monthTargMap.put(mt.Month__c , new List<Monthly_Target__c>());
                        }
                        monthTargMap.get(mt.Month__c).add(mt);
                    }
                }
            }else{
                List<String> allRecordList = new List<string>();
                allRecordList =  getPicklistValue() ;
                for(Integer i=1;i<=12;i++){
                    for(String str : allRecordList){
                        Monthly_Target__c monTar = new Monthly_Target__c();
                        monTar.Month__c = getMonthInText(i);
                        monTar.Type__c = str;
                        
                        monTar.target_date__c = date.newInstance(Integer.valueOf(Year), getMonthInInt(monTar.Month__c), 1);
                        monTar.Target_Number__c = 0;
                        monTar.Target_Amount__c = 0;
                        if(!monthTargMap.containsKey(monTar.Month__c )){
                            monthTargMap.put(monTar.Month__c , new List<Monthly_Target__c>());
                        }
                        monthTargMap.get(monTar.Month__c ).add(monTar);
                    }
                }
            }
        }else{
            List<String> allRecordList = new List<string>();
            allRecordList =  getPicklistValue() ;
            for(Integer i=1;i<=12;i++){
                for(String str : allRecordList){
                    Monthly_Target__c monTar = new Monthly_Target__c();
                    monTar.Month__c = getMonthInText(i);
                    monTar.Type__c = str;
                    monTar.target_date__c = date.newInstance(Integer.valueOf(Year), getMonthInInt(monTar.Month__c), 1);
                    monTar.Target_Number__c = 0;
                    monTar.Target_Amount__c = 0;
                    if(!monthTargMap.containsKey(monTar.Month__c )){
                        monthTargMap.put(monTar.Month__c , new List<Monthly_Target__c>());
                    }
                    monthTargMap.get(monTar.Month__c ).add(monTar);
                }
            }
        }
        List<monthlyTargetWrapper> monthlyTargetWrapperList = new List<monthlyTargetWrapper>();
        //getMonthInText
        if(monthTargMap.size() > 0){
            for(Integer ix=1;ix<=12;ix++){
                String str = getMonthInText(ix);
                monthlyTargetWrapper mtw = new monthlyTargetWrapper();
                mtw.Month = str;
                mtw.monthlyTargetList = monthTargMap.get(str);
                monthlyTargetWrapperList.add(mtw);
            }
            
        }
        return monthlyTargetWrapperList;
    }
    
    @auraEnabled
    public static String UpdateMonthlyTarget(String targStr,String targList){
        try{
            Target_Achieved__c targetAch=(Target_Achieved__c)JSON.deserializeStrict (targStr,Target_Achieved__c.class);
            System.debug('targetAch>>>>>>'+targetAch.Id);
            List<monthlyTargetWrapper> monthTargList= (List<monthlyTargetWrapper>)JSON.deserialize(targList,List<monthlyTargetWrapper>.class);
            
            upsert targetAch;
            List<Monthly_Target__c> monthlyTargetList = new List<Monthly_Target__c>();
            
            for(monthlyTargetWrapper mtw : monthTargList){
                for(Monthly_Target__c mt : mtw.monthlyTargetList){
                    mt.Target_Achieved__c = targetAch.Id;
                    mt.User__c = targetAch.User__c;
                    monthlyTargetList.add(mt);
                }
            }
            upsert monthlyTargetList;
            return 'Success';
        }catch(Exception e){
            return 'Error :'+e.getMessage();
        }
        
    }
    
    // Method to fetch the picklist value from monthly target object here 
    
    public static List<String> getPicklistValue(){
        List<String> allRecordList = new List<String>();
        Schema.DescribeFieldResult fieldResult =
            Monthly_Target__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        if(ple != null && ple.size() >0){
            
            for(Schema.PicklistEntry p : ple){
                allRecordList.add(p.getValue());
            }
            
        }
        return allRecordList;
    }
    
    private static String getAllFieldsForQuery(String objAPIName){
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objAPIName).getDescribe().fields.getMap();
        String allFields = '';
        
        for(String field : objectFields.keySet()){
            allFields+=field+',';
        }
        allFields = allFields.removeEnd(',');
        return allFields;
    }
    
    private static String getMonthInText(Integer monthInNumber){
        Map<Integer,String> monthMap = new Map<Integer,String>{1=>'Apr',2=>'May',3=>'Jun',4=>'Jul',5=>'Aug',6=>'Sep',7=>'Oct',8=>'Nov',9=>'Dec',10=>'Jan',11=>'Feb',12=>'Mar'};
            return monthMap.get(monthInNumber); 
    }
    
    private static Integer getMonthInInt(String str){
        Map<String,Integer> monthMap = new Map<String,Integer>{'Apr'=>4,'May'=>5,'Jun'=>6,'Jul'=>7,'Aug'=>8,'Sep'=>9,
            'Oct'=>10,'Nov'=>11,'Dec'=>12,'Jan'=>1,'Feb'=>2,'Mar'=>3};
                return monthMap.get(str); 
    }
    public class Wrapper{
        @auraEnabled public List<Target_Achieved__c> targList   {get;set;}
        @auraEnabled public Decimal totalAmount                     {get;set;}
        @auraEnabled public Decimal totalNumber                     {get;set;}
        
        public Wrapper(List<Target_Achieved__c> targList,Decimal totalAmount,Decimal totalNumber){
            this.targList = targList;
            this.totalAmount = totalAmount;
            this.totalNumber = totalNumber;
        }
        
    }
    
    public class monthlyTargetWrapper{
        @auraEnabled public String Month;
        @auraEnabled public List<Monthly_Target__c> monthlyTargetList;
    }
    
    
    
}