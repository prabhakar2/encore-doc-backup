@isTest
public class AccountYearStageUpdateTest {
    public static testmethod void mytest(){
        
        Account acc=new Account();
        acc.Year__c='2019';
        acc.name='test';
        acc.Insurance_Amount_Consumed__c=500;
        acc.SubIndustry__c='Agent';
        acc.Ocean__c='Red';
        acc.No_Of_Employees__c='100';
        acc.Turn_Over__c='45687588';
        acc.CAM_Stage__c='Finance Accepted';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        insert acc;
        
        Cam__c c = new Cam__c();
        c.Stage__c='CAM Approved';
        c.Year__c='2019';
        c.Account__c=acc.Id;
        insert c;
        delete c;
    }
    
   
}