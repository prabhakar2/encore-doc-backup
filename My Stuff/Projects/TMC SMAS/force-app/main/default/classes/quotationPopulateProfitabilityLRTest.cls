@isTest
public class quotationPopulateProfitabilityLRTest {
    public static testMethod void test(){
        Target_Achieved__c trget=new Target_Achieved__c();
        trget.Achieved_Amount__c=6700;
        insert trget;
            
        Monthly_Target__c mnthlyTrgetObj=new Monthly_Target__c();
        mnthlyTrgetObj.User__c=UserInfo.getUserId();
        mnthlyTrgetObj.Target_Achieved__c=trget.Id;
        insert mnthlyTrgetObj;
        
       Quotation__c quote=new Quotation__c();
        quote.Monthly_Target__c=mnthlyTrgetObj.Id;
        quote.Client_Type__c='New';
        quote.RV_Type__c='HRV';
        quote.Profitability__c=40000;
        insert quote;
    }
        
        public static testMethod void test2(){
        Target_Achieved__c trget=new Target_Achieved__c();
        trget.Achieved_Amount__c=6700;
        insert trget;
            
        Monthly_Target__c mnthlyTrgetObj=new Monthly_Target__c();
        mnthlyTrgetObj.User__c=UserInfo.getUserId();
        mnthlyTrgetObj.Target_Achieved__c=trget.Id;
        insert mnthlyTrgetObj;
        
       Quotation__c quote=new Quotation__c();
        quote.Monthly_Target__c=mnthlyTrgetObj.Id;
        quote.Client_Type__c='Existing';
        quote.RV_Type__c='HRV';
        quote.Profitability__c=40000;
        insert quote;
    }
 
     public static testMethod void test3(){
        Target_Achieved__c trget=new Target_Achieved__c();
        trget.Achieved_Amount__c=6700;
        insert trget;
            
        Monthly_Target__c mnthlyTrgetObj=new Monthly_Target__c();
        mnthlyTrgetObj.User__c=UserInfo.getUserId();
        mnthlyTrgetObj.Target_Achieved__c=trget.Id;
        insert mnthlyTrgetObj;
        
       Quotation__c quote=new Quotation__c();
        quote.Monthly_Target__c=mnthlyTrgetObj.Id;
        quote.Client_Type__c='New';
        quote.RV_Type__c='LRV';
        quote.Profitability__c=40000;
        insert quote;
    }
    
     public static testMethod void test4(){
        Target_Achieved__c trget=new Target_Achieved__c();
        trget.Achieved_Amount__c=6700;
        insert trget;
            
        Monthly_Target__c mnthlyTrgetObj=new Monthly_Target__c();
        mnthlyTrgetObj.User__c=UserInfo.getUserId();
        mnthlyTrgetObj.Target_Achieved__c=trget.Id;
        insert mnthlyTrgetObj;
        
       Quotation__c quote=new Quotation__c();
        quote.Monthly_Target__c=mnthlyTrgetObj.Id;
        quote.Client_Type__c='Existing';
        quote.RV_Type__c='LRV';
        quote.Profitability__c=40000;
        insert quote;
    }
    
     public static testMethod void test5(){
        Target_Achieved__c trget=new Target_Achieved__c();
        trget.Achieved_Amount__c=6700;
        insert trget;
            
        Monthly_Target__c mnthlyTrgetObj=new Monthly_Target__c();
        mnthlyTrgetObj.User__c=UserInfo.getUserId();
        mnthlyTrgetObj.Target_Achieved__c=trget.Id;
        insert mnthlyTrgetObj;
        
       Quotation__c quote=new Quotation__c();
        quote.Monthly_Target__c=mnthlyTrgetObj.Id;
        quote.Client_Type__c='New';
        quote.RV_Type__c='FL-Bus';
        quote.Profitability__c=40000;
        insert quote;
    }
      public static testMethod void test6(){
        Target_Achieved__c trget=new Target_Achieved__c();
        trget.Achieved_Amount__c=6700;
        insert trget;
            
        Monthly_Target__c mnthlyTrgetObj=new Monthly_Target__c();
        mnthlyTrgetObj.User__c=UserInfo.getUserId();
        mnthlyTrgetObj.Target_Achieved__c=trget.Id;
        insert mnthlyTrgetObj;
        
       Quotation__c quote=new Quotation__c();
        quote.Monthly_Target__c=mnthlyTrgetObj.Id;
        quote.Client_Type__c='Existing';
        quote.RV_Type__c='FL-Bus';
        quote.Profitability__c=40000;
        insert quote;
    }
      public static testMethod void test7(){
        Target_Achieved__c trget=new Target_Achieved__c();
        trget.Achieved_Amount__c=6700;
        insert trget;
            
        Monthly_Target__c mnthlyTrgetObj=new Monthly_Target__c();
        mnthlyTrgetObj.User__c=UserInfo.getUserId();
        mnthlyTrgetObj.Target_Achieved__c=trget.Id;
        insert mnthlyTrgetObj;
        
       Quotation__c quote=new Quotation__c();
        quote.Monthly_Target__c=mnthlyTrgetObj.Id;
        quote.Client_Type__c='New';
        quote.RV_Type__c='FL-Passanger';
        quote.Profitability__c=40000;
        insert quote;
    }
     public static testMethod void test8(){
        Target_Achieved__c trget=new Target_Achieved__c();
        trget.Achieved_Amount__c=6700;
        insert trget;
            
        Monthly_Target__c mnthlyTrgetObj=new Monthly_Target__c();
        mnthlyTrgetObj.User__c=UserInfo.getUserId();
        mnthlyTrgetObj.Target_Achieved__c=trget.Id;
        insert mnthlyTrgetObj;
        
       Quotation__c quote=new Quotation__c();
        quote.Monthly_Target__c=mnthlyTrgetObj.Id;
        quote.Client_Type__c='Existing';
        quote.RV_Type__c='FL-Passanger';
        quote.Profitability__c=40000;
        insert quote;
    }
}