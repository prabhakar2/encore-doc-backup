@isTest
public class populateDueDateOnTaskTest {
    public static testmethod void mytest(){
        
        Branch__c br= new Branch__c();
        br.Branch__c='Mumbai';
        br.Working_Days__c='Monday;Tuesday;Wednesday;Thursday;Friday';
        DateTime dt = System.now();
        br.Start_Time__c= Time.newInstance(dt.hour(), dt.minute(), dt.second(), dt.millisecond());
        br.End_Time__c = Time.newInstance(dt.hour(), dt.minute(), dt.second(), dt.millisecond()); 
        insert br;
        
        User uu=new User();
        uu.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        uu.CommunityNickname='test';
        uu.Username='qazzxswedc@azxsweda.com';
        uu.LastName='bhatt';
        uu.Email='xyz@gmail.com';
        uu.EmailEncodingKey='UTF-8';
        uu.TimeZoneSidKey = 'America/Los_Angeles';
        uu.LocaleSidKey = 'en_US';
        uu.LanguageLocaleKey = 'en_US';
        uu.Alias='standt';
        uu.AboutMe='tmc';
        uu.Region__c='West';
        uu.Branch__c='Mumbai';
        insert uu;
        
        Holiday__c hh=new Holiday__c();
        hh.Name='holi';
        hh.Branch__c=br.Id;
        hh.Start_Date__c = System.today() + 1;
        hh.End_Date__c = System.today() + 5 ;
        insert hh;
        
        Step__c s = new Step__c();    
        s.TAT__c=12345;
        insert s;
        
        Task t = new Task();
        t.OwnerId = uu.Id;
        t.Subject='Donni';
        t.Status='Not Started';
        t.Step__c=s.Id;
        t.Priority='Normal';
        t.Start_Date__c=datetime.newInstance(2018, 12, 12, 10, 30, 0);
        insert t;
    }
    
    public static testmethod void mytest1(){
        
        Branch__c br= new Branch__c();
        br.Branch__c='Mumbai';
        br.Working_Days__c='Monday;Tuesday;Wednesday;Thursday;Friday;Saturday;Sunday';
        DateTime dt = System.now();
        br.Start_Time__c= Time.newInstance(dt.hour(), dt.minute(), dt.second(), dt.millisecond());
        br.End_Time__c = Time.newInstance(dt.hour(), dt.minute(), dt.second(), dt.millisecond()); 
        insert br;       
        
        User uu=new User();
        uu.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        uu.CommunityNickname='test';
        uu.Username='oplikjuy@rtvghswe.com';
        uu.LastName='bhatt';
        uu.Email='xyz@gmail.com';
        uu.EmailEncodingKey='UTF-8';
        uu.TimeZoneSidKey = 'America/Los_Angeles';
        uu.LocaleSidKey = 'en_US';
        uu.LanguageLocaleKey = 'en_US';
        uu.Alias='standt';
        uu.AboutMe='tmc';
        uu.Region__c='West';
        uu.Branch__c='Mumbai';
        insert uu;
        
        Step__c s = new Step__c();    
        s.TAT__c=12345;
        insert s;
        
        Holiday__c hh=new Holiday__c();
        hh.Name='holi';
        hh.Branch__c=br.Id;
        hh.Start_Date__c = System.today() - 1;
        hh.End_Date__c = System.today() + 5 ;
        insert hh;
        
        Task t = new Task();
        t.OwnerId = uu.Id;
        t.Subject='Donni';
        t.Status='Not Started';
        t.Step__c=s.Id;
        t.Priority='Normal';
        t.Start_Date__c=datetime.newInstance(2019, 12, 12, 23, 30, 0);
        insert t;
    }
    
    public static testmethod void mytest2(){
        Branch__c br= new Branch__c();
        br.Branch__c='Mumbai';
        br.Working_Days__c='Monday;Tuesday;Wednesday;Thursday;Friday;Saturday;Sunday';
        DateTime dt = System.now();
        br.Start_Time__c= Time.newInstance(23, 25, 00, 00);
        br.End_Time__c = Time.newInstance(23, 45, 00, 00); 
        insert br;       
        
        User uu=new User();
        uu.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        uu.CommunityNickname='test';
        uu.Username='oplikjuy@rtvghswe.com';
        uu.LastName='bhatt';
        uu.Email='xyz@gmail.com';
        uu.EmailEncodingKey='UTF-8';
        uu.TimeZoneSidKey = 'America/Los_Angeles';
        uu.LocaleSidKey = 'en_US';
        uu.LanguageLocaleKey = 'en_US';
        uu.Alias='standt';
        uu.AboutMe='tmc';
        uu.Region__c='West';
        uu.Branch__c='Mumbai';
        insert uu;
        
        Step__c s = new Step__c();    
        s.TAT__c=16;
        insert s;
        
        Holiday__c hh=new Holiday__c();
        hh.Name='holi';
        hh.Branch__c=br.Id;
        hh.Start_Date__c = System.today() - 1;
        hh.End_Date__c = System.today() + 5 ;
        insert hh;
        
        Task t = new Task();
        t.OwnerId = uu.Id;
        t.Subject='Donni';
        t.Status='Not Started';
        t.Step__c=s.Id;
        t.Priority='Normal';
        t.Start_Date__c=datetime.newInstance(2019, 12, 12, 23, 30, 0);
        insert t;
        
    }
    
   
}