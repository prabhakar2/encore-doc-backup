@istest
public class CreateInvoiceTriggerTest {
    Public static testmethod void mytest(){
        Sales_Register__c sr = new Sales_Register__c();
        sr.Invoice_Type__c='FMS';
        sr.Invoice_Amount__c=5000;
        sr.Invoice_Date__c=system.today();
        sr.Invoice_Number__c='in12444';
        
        sr.Customer_Number__c='679411';
        insert sr;
    }
}