public class DODocumentUploadAura {
    
    @auraEnabled
    public static List<DO_Document__c> getDoDocument(String DOId){
        List<DO_Document__c> doDocList =[SELECT id,name,Document_Master__r.Name,Delivery_Order__c 
                                         FROM DO_Document__c 
                                         WHERE Delivery_Order__c =:DOId];
        if(doDocList.size() > 0)
            return doDocList;
        else
            return null;
        
    }
    
    @auraEnabled
    public static void updateNewDoc(String docId,String docTypeId){
        try{
            List<ContentDocumentLink> attListToDelete = new List<ContentDocumentLink>();
            for(ContentDocumentLink cd : [SELECT ContentDocumentId 
                                          FROM ContentDocumentLink 
                                          WHERE LinkedEntityId = :docTypeId
                                          AND  ContentDocumentId != :docId]){
                                              attListToDelete.add(cd);
                                          }
            if(attListToDelete.size() > 0){
                delete attListToDelete;    
            }
            
            ContentDistribution cd = new ContentDistribution();
            cd.contentVersionId  = [SELECT id FROM ContentVersion WHERE contentDocumentId = :docId LIMIT 1].Id;
            cd.PreferencesAllowViewInBrowser = True;
            cd.Name = [SELECT Id,Name,Document_Master__r.Name FROM DO_Document__c WHERE Id =:docTypeId LIMIT 1].Document_Master__r.Name;
            insert cd;
            
            string url = [SELECT DistributionPublicUrl 
                          FROM ContentDistribution 
                          WHERE id = :cd.Id].DistributionPublicUrl;
            
            Do_document__c dod = new Do_Document__c(Id = docTypeId, document_Url__c = url);
            update dod;
        }catch(Exception e){
            System.debug('Error>>>'+e.getMessage());
        }
    }
}