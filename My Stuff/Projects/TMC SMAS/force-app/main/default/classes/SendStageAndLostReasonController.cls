@RestResource(urlMapping='/getOpportunityStageReason/*')
global class SendStageAndLostReasonController {
     @HTTPPOST
    global static List<Opportunity> sendOpportunityStageResn(){
        RestRequest req = RestContext.request;
        List<Opportunity> oppList = new List<Opportunity>();
        try{
            
            ClientWrapper wrap = (ClientWrapper)JSON.deserialize(req.requestBody.toString(),ClientWrapper.class);
            oppList = [SELECT Name, StageName,Opportunity_ID__c, Loss_Reason__c FROM Opportunity WHERE Account.Salesforce_Account_Id__c = :wrap.ClientId];
            system.debug('====oppList'+oppList);
            if(oppList.size() > 0){
                            system.debug('====oppList1111');

                SmasConstants.createLog(true,'','SendStageAndLostReasonController','Opportunity',System.now(),req.requestBody.toString());
                return oppList;
            }
            else{
                                            system.debug('====oppLfgffghfgist1111');

                SmasConstants.createLog(false,'No Opportunity found for SF Account Id : '+wrap.ClientId+'.','SendStageAndLostReasonController','Opportunity',System.now(),req.requestBody.toString());
                return oppList;
            } 
        }
        catch(Exception e){
            SmasConstants.createLog(false,e.getMessage()+' '+e.getLineNumber(),'SendStageAndLostReasonController','Opportunity',System.now(),req.requestBody.toString());
            return oppList;
        }
    }
    
    /*  Wrapper class to deserialize the JSON body.  */
    public class ClientWrapper{
        public String ClientId;
    }
}