public class GenerateRatingAura {
    
    @auraEnabled
    public static String populateRating(String camRatingId){
        try{
            String allFields = getAllFieldsForQuery('CAM_Rating__c');
            String qry = 'SELECT '+allFields+' FROM CAM_Rating__c WHERE Id =:camRatingId';
            List<CAM_Rating__c> camRatingList = Database.query(qry);
            if(camRatingList.size() > 0){
                CAM_Rating__c camRateObj = camRatingList[0];
                
                Map<String,string> filterMap = new Map<string,string>();
                Map<String,Decimal> rangeFilterMap = new Map<string,Decimal>();
                Map<string,Schema.SObjectField> objFieldMap = Schema.getGlobalDescribe().get('CAM_Rating__c').getDescribe().fields.getMap();
                
                for(string fieldApi : SmasConstants.camRatingFilter)
                    if(string.valueOf(camRateObj.get(fieldApi)) != null || string.valueOf(camRateObj.get(fieldApi))!=''){
                        filterMap.put(objFieldMap.get(fieldApi).getDescribe().getLabel(),(string)camRateObj.get(fieldApi));
                    }
                
                
                for(string fieldApi : SmasConstants.camRatingRangeFilter)
                    if((Decimal)camRateObj.get(fieldApi) != null){
                        rangeFilterMap.put(objFieldMap.get(fieldApi).getDescribe().getLabel(),(Decimal)camRateObj.get(fieldApi));
                    }

                System.debug('rangeFilterMap>>>>>>>>>>>>>>>>>>>>>>>>>>'+rangeFilterMap);
                List<CAM_Rating_Master_Type__c> camRatingMasterTypeList = new List<CAM_Rating_Master_Type__c>();
                if(filterMap.keyset().size()>0 && rangeFilterMap.keyset().size()>0){
                    camRatingMasterTypeList = [SELECT Id,Type__c,Sub_Type__c,Rating__c,Minimum__c,Maximum__c
                                               FROM CAM_Rating_Master_Type__c 
                                               WHERE (Type__c IN :filterMap.keyset() AND Sub_Type__c IN :filterMap.values() ) 
                                               OR Type__c IN :rangeFilterMap.keyset()
                                               ORDER BY Type__c,Sub_Type__c,Rating__c];
                }else if(filterMap.keyset().size()>0){
                    camRatingMasterTypeList = [SELECT Id,Type__c,Sub_Type__c,Rating__c,Minimum__c,Maximum__c
                                               FROM CAM_Rating_Master_Type__c 
                                               WHERE Type__c IN :filterMap.keyset() 
                                               AND Sub_Type__c IN :filterMap.values() 
                                               ORDER BY Type__c,Sub_Type__c,Rating__c];
                    
                }else if(rangeFilterMap.keyset().size()>0){
                    camRatingMasterTypeList = [SELECT Id,Type__c,Sub_Type__c,Rating__c,Minimum__c,Maximum__c
                                               FROM CAM_Rating_Master_Type__c 
                                               WHERE Type__c IN :rangeFilterMap.keyset() 
                                               ORDER BY Type__c,Sub_Type__c,Rating__c];
                }
                
                
                if(camRatingMasterTypeList.size()>0){
                    set<string> doneForSet = new Set<string>();
                    Map<string,string> CAMRatingfieldMap = SmasConstants.CAMRatingfieldMap;
                    for(CAM_Rating_Master_Type__c crmt : camRatingMasterTypeList){
                        
                        if(filterMap.containsKey(crmt.Type__c) && filterMap.get(crmt.Type__c) == crmt.sub_type__c){
                            camRateObj.put(CAMRatingfieldMap.get(crmt.Type__c),crmt.Rating__c);
                        }
                        
                        if(rangeFilterMap.containsKey(crmt.Type__c) && !doneForSet.contains(crmt.Type__c)){
                            decimal value = rangeFilterMap.get(crmt.Type__c);
                            if(filterTypeInMaster(crmt,value)){
                                camRateObj.put(CAMRatingfieldMap.get(crmt.Type__c),crmt.Rating__c);
                                doneForSet.add(crmt.Type__c);
                            } 
                        } 
                        
                    }
                }
                
                update camRateObj; 
                return 'Success';
            }
            
        }catch(Exception e){
            return 'Error : '+e.getMessage();
        } 
        return null;
    }
    
    //-----------------------------------------------------------------------------------------------------
    private static boolean filterTypeInMaster(CAM_Rating_Master_Type__c master,Decimal fieldValue){
        
        if(master.Minimum__c == NULL){
            decimal max = decimal.valueOf(master.Maximum__c);
            if(max>=fieldValue) 
                return true;
            else 
                return false;
        }
        
        if(master.Maximum__c == NULL){
            decimal min = decimal.valueOf(master.Minimum__c);
            if(min<=fieldValue)
                return true;
            else
                return false;
        }
        decimal min = decimal.valueOf(master.Minimum__c),max = decimal.valueOf(master.Maximum__c);
        if(min<=fieldValue && max>=fieldValue) 
            return true;
        else
            return false;
    }
    
    //------------------------------------------- Getting all fields -----------------------------------------------------------
    private static String getAllFieldsForQuery(String objAPIName){
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objAPIName).getDescribe().fields.getMap();
        String allFields = '';
        
        for(String field : objectFields.keySet()) allFields+=field+',';
        
        allFields = allFields.removeEnd(',');
        return allFields;
    }
        
}