@istest
public class GenerateRatingAuraTest {
    Public Static Testmethod void mytest(){
        Cam__c c = new Cam__c();
        c.Stage__c='CAM Approved';
        c.Year__c='2017';
        insert c;
        
        CAM_Rating__c cr = new CAM_Rating__c();
        cr.CAM__c=c.id;
        cr.Product_Service_Type__c = 7;
        insert cr;
        
        CAM_Rating_Master_Type__c crmt = new CAM_Rating_Master_Type__c();
        crmt.Rating__c=8.00;
        crmt.Type__c='Product / Service Type';
        crmt.Maximum__c='7';
        insert crmt;
        
        GenerateRatingAura.populateRating(cr.Id);
        CAM_Rating_Master_Type__c crmt1 = new CAM_Rating_Master_Type__c();
        crmt1.Rating__c=1;
        crmt1.Type__c='Years in Business';
        crmt1.minimum__c='5';
        insert crmt1;
        GenerateRatingAura.populateRating(cr.Id);
    }
    Public Static Testmethod void mytest2(){
        Cam__c c = new Cam__c();
        c.Stage__c='CAM Approved';
        c.Year__c='2017';
        insert c;
        
        CAM_Rating__c cr = new CAM_Rating__c();
        cr.CAM__c=c.id;
        cr.Product_Service_Type__c = 7;
        cr.Years_in_Business__c = 7;
        insert cr;
        
        CAM_Rating_Master_Type__c crmt1 = new CAM_Rating_Master_Type__c();
        crmt1.Rating__c=1;
        crmt1.Type__c='Years in Business';
        crmt1.minimum__c='5';
        insert crmt1;
        GenerateRatingAura.populateRating(cr.Id);
    }
    Public Static Testmethod void mytest3(){
        Cam__c c = new Cam__c();
        c.Stage__c='CAM Approved';
        c.Year__c='2017';
        insert c;
        
        CAM_Rating__c cr = new CAM_Rating__c();
        cr.CAM__c=c.id;
        cr.Product_Service_Type__c = 7;
        cr.Years_in_Business__c = 8;
        insert cr;
        
        CAM_Rating_Master_Type__c crmt1 = new CAM_Rating_Master_Type__c();
        crmt1.Rating__c=1;
        crmt1.Type__c='Years in Business';
        crmt1.minimum__c='7';
        crmt1.Maximum__c='10';
        insert crmt1;
        GenerateRatingAura.populateRating(cr.Id);
    }
}