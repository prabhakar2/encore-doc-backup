/* Class Name : UpdateCreditInfo
 * CreatedDate : 13-03-2019
 * purpose : This class is used to update the credit details on Client record on the basis of received clientID. 
 * */
public class UpdateCreditInfo {
	public static Account updateCredit(Account accObj){
        
        String endPointURL = System.label.UpdateCreditInfoURL+accObj.Unique_Id__c;
        Http httpObj = new Http();
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint(endPointURL);
        req.setMethod('GET');
        req.setTimeout(60000);
        req.setHeader('Content-Type','application/json');
        
        String username = Label.API_Callout_Username;
        String password = Label.API_Callout_password;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        
        req.setHeader('Authorization', authorizationHeader);
        
        HttpResponse res = new HttpResponse();
        if(!test.isRunningTest())
            res = httpObj.send(req);
        else{
            String str = '[{"LimitValidEndDate":"31 Mar 2018","LimitApprovedAmount":"60000000","ToBeRecoveredAmount":"27005199","LimitBalance":"32994801","LimitRemarks":"VALIDITY DATE EXPIRED!!"}]';
            res.setBody(str);
        }
        System.debug('>>>>Limit Info body>>>>>'+res.getBody());    
        if(res.getBody() != '[]'){
            
            List<Object> dataObjList = (List<Object>)JSON.deserializeUntyped(res.getBody());
            
            Map<String,Object> dataObjMap = (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(dataObjList[0]));
            System.debug('dataObjMap>>>>>>>'+dataObjMap);
            accObj.To_be_Recovered_Amt__c  = Decimal.valueOf((String)dataObjMap.get('ToBeRecoveredAmount'));
			accObj.Limit_Balance__c = Decimal.valueOf((String)dataObjMap.get('LimitBalance'));
            accObj.Limit_Valid_End_Date__c = (String)dataObjMap.get('LimitValidEndDate');
            accObj.Limit_Remarks__c = (String)dataObjMap.get('LimitRemarks');
            accObj.Limit_Approved_Amount__c = Decimal.valueOf((String)dataObjMap.get('LimitApprovedAmount'));
            
        }
        return accObj;
     }
    
}