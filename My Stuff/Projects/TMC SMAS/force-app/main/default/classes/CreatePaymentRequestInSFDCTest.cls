@istest
public class CreatePaymentRequestInSFDCTest {
    public static testmethod void mytest(){
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        
        User usr = new User(LastName = 'LIVESTON',
                            FirstName='JASON',
                            Alias = 'jliv',
                            Email = 'jason.liveston@asdf.com',
                            Username = 'jason.liveston@asdf.com',
                            ProfileId = profileId.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        
        Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account acc=new Account();
        acc.RecordTypeId=RecordTypeId;
        acc.name='bhatt';
        acc.Industry='Airlines';
        acc.Unique_Id__c='TMC123';
        insert acc;
        
        Quotation__c quot = new Quotation__c();
        quot.Account__c=acc.Id;
        quot.Lease_Rent__c=10000;
        quot.LeaseSoftQuoteId__c='701719';
        insert quot;
       List<DO_Line_Item__c> listdli = new List<DO_Line_Item__c>();
        DO_Line_Item__c dli = new DO_Line_Item__c();
        dli.Name='test';
        dli.Amount__c=50000;
        dli.Dealer__c=acc.id;
        dli.Quotation__c=quot.id;
        dli.LeaseSoftId__c='leas123';
        dli.Payment_Days__c=1;
        listdli.add(dli);
        insert listdli;
        
        List<Payment_Request__c> listpay = new List<Payment_Request__c>();
        Payment_Request__c pr = new Payment_Request__c();
        pr.DO_Line_Item__c=dli.id;
        pr.Quotation__c=quot.Id;
        pr.Initiated_by__c=usr.Id;
        pr.TAX__c=2000;
        //listpay.add(pr);
        insert listpay;
        
        CreatePaymentRequestInSFDC.PaymentWrap cprs= new CreatePaymentRequestInSFDC.PaymentWrap();
        cprs.Rowid='12344';
        cprs.amount='50000';
        cprs.vendor='1';
        cprs.TRDATE='2';
        cprs.TRCODE='4';
        cprs.SUBCODE='12389';
        cprs.QNO='23443';
        cprs.DESCRIPT='des23';
        cprs.PaymentDays='1';
        cprs.IsApprovedDo='45';
        cprs.Created_On='admin';
        cprs.DealerName='Test';
        cprs.CgstAmount='4000';
        cprs.IgstAmount='3000';
        cprs.Email='jason.liveston@asdf.com';
        
        CreatePaymentRequestInSFDC reqst=new CreatePaymentRequestInSFDC();
        String jsonStr=JSON.serialize(listdli);
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'CreatePaymentRequest';
        req.httpMethod = 'GET';
        req.requestBody = Blob.valueof(jsonStr);

        RestContext.request = req;
        RestContext.response= res;
        CreatePaymentRequestInSFDC.insertPaymentRequest();
        Test.stopTest();
    }
        public static testmethod void mytest1(){
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        
        User usr = new User(LastName = 'LIVESTON',
                            FirstName='JASON',
                            Alias = 'jliv',
                            Email = 'jason.liveston@asdf.com',
                            Username = 'jason.liveston@asdf.com',
                            ProfileId = profileId.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        
        Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        Account acc=new Account();
        acc.RecordTypeId=RecordTypeId;
        acc.name='bhatt';
        acc.Industry='Airlines';
        acc.Unique_Id__c='TMC123';
        insert acc;
        
        Quotation__c quot = new Quotation__c();
        quot.Account__c=acc.Id;
        quot.Lease_Rent__c=10000;
        quot.LeaseSoftQuoteId__c='701719';
        insert quot;
       List<DO_Line_Item__c> listdli = new List<DO_Line_Item__c>();
        DO_Line_Item__c dli = new DO_Line_Item__c();
        dli.Name='test';
        dli.Amount__c=50000;
        dli.Dealer__c=acc.id;
        dli.Quotation__c=quot.id;
        dli.LeaseSoftId__c='701719';
        dli.Payment_Days__c=1;
        listdli.add(dli);
        insert listdli;
        
        List<Payment_Request__c> listpay = new List<Payment_Request__c>();
        Payment_Request__c pr = new Payment_Request__c();
        pr.DO_Line_Item__c=dli.id;
        pr.Quotation__c=quot.Id;
        pr.Initiated_by__c=usr.Id;
        pr.TAX__c=2000;
        //listpay.add(pr);
        insert listpay;
        
        CreatePaymentRequestInSFDC.PaymentWrap cprs= new CreatePaymentRequestInSFDC.PaymentWrap();
        cprs.Rowid='12344';
        cprs.amount='50000';
        cprs.vendor='1';
        cprs.TRDATE='2';
        cprs.TRCODE='4';
        cprs.SUBCODE='12389';
        cprs.QNO='23443';
        cprs.DESCRIPT='des23';
        cprs.PaymentDays='1';
        cprs.IsApprovedDo='45';
        cprs.Created_On='admin';
        cprs.DealerName='Test';
        cprs.CgstAmount='4000';
        cprs.IgstAmount='3000';
        cprs.Email='jason.liveston@asdf.com';
        
        CreatePaymentRequestInSFDC reqst=new CreatePaymentRequestInSFDC();
        String jsonStr=JSON.serialize(listpay);
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'CreatePaymentRequest';
        req.httpMethod = 'GET';
        req.requestBody = Blob.valueof(jsonStr);

        RestContext.request = req;
        RestContext.response= res;
        CreatePaymentRequestInSFDC.insertPaymentRequest();
        Test.stopTest();
    }
}