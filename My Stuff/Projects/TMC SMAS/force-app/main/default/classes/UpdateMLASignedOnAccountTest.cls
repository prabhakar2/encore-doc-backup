@istest
public class UpdateMLASignedOnAccountTest {
   
    
    Public Static Testmethod void mytest(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User usr = new User(LastName = 'LIVESTON',
                            FirstName='JASON',
                            Alias = 'jliv',
                            Email = 'jason.liveston@asdf.com',
                            Username = 'pj161993@gmai.com',
                            ProfileId = profileId.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        Account acc=new Account();
        acc.name='bhatt';
        acc.Industry='Airlines';
        acc.Unique_Id__c='TMC123';
        acc.MLA_Signed__c=false;
        acc.BillingCity = 'Noida';
        insert acc;
        
        MLA__c mla = new MLA__c();
        mla.Account__c=acc.id;
        mla.Vatted_by__c =usr.id; 
        mla.SMAS_Signing_Authority__c = 'test';
        mla.Client_Signing_Authority__c = 'TEST';
        insert mla;
        
        mla.Client_Signing_Authority__c = 'test';
        update mla;
        try{
            delete mla;
        }catch(Exception e){}
        
        
    }
}