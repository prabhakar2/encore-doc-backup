/*
Name	    :		WorkflowPDDTaskUpdateTest
Date	    :	    24 JULY 2019	
Modified By	:		Shalini Singh
*/
@isTest
public class WorkflowPDDTaskUpdateTest {
    Static Document_Master__c docMas;
    Static Quotation__c quoteObj;
    Static PDD__c pddObj;
    
    
    public static testmethod void test(){
        test.startTest();
        Quotation__c qtest=new Quotation__c();
        qtest.Name='XYZ';
        insert qtest;
        
        Document_Master__c docMaster = new Document_Master__c();
        docMaster.RecordTypeId = Schema.SObjectType.Document_Master__c.getRecordTypeInfosByName().get('PDD').getRecordTypeId();
        docMaster.Active__c = true;
        docMaster.Name='Original RC';
        // docMaster.Required_Stage__c = 'To Process DO';
        insert docMaster;
        
        
        
        
        Document_Master__c docMaster2 = new Document_Master__c();
        docMaster2.RecordTypeId = Schema.SObjectType.Document_Master__c.getRecordTypeInfosByName().get('PDD').getRecordTypeId();
        docMaster2.Active__c = true;
        docMaster2.Name='Original Tax Invoice';
        // docMaster.Required_Stage__c = 'To Process DO';
        insert docMaster2;
        
        PDD__c pddObj= new PDD__c();
        pddObj.Status__c='Pending';
        pddObj.Quotation__c=qtest.Id;
        pddObj.Document_Master__c=docMaster.Id;
        insert pddObj;
        pddObj.Status__c='Received by CMT';
        update pddObj;
        
        PDD__c pddObjTest= pddObj.clone(false,false,false,false);
        pddObjTest.Document_Master__c=docMaster2.Id;
        pddObjTest.Status__c='Received by CMT';
        pddObjTest.Quotation__c=qtest.Id;
        insert pddObjTest;
        update pddObjTest;
        
        PDD__c pddObj1= new PDD__c();
        pddObj1.Status__c='Pending';
        pddObj1.Quotation__c=qtest.Id;
        pddObj1.Document_Master__c=docMaster.Id;
        insert pddObj1;
        update pddObj1;   
        pddObj1.Status__c='Received by Finance';
        update pddObj1;
        pddObj1.Status__c='Submitted by Fleet';
        update pddObj1;
        pddObj.Status__c='Rejected by CMT';
        update pddObj1;
        pddObj.Status__c='Rejected by Finance';
        update pddObj1;
        
        PDD__c pddObj2= new PDD__c();
        pddObj2.Status__c='Submitted by Fleet';
        pddObj2.Quotation__c=qtest.Id;
        
        pddObj2.Document_Master__c=docMaster.Id;
        insert pddObj2;
        update pddObj2;
        pddObj2.Status__c='Rejected by CMT';
        update pddObj2;
        pddObj2.Status__c='Rejected by Finance';
        update pddObj2;
        update new List<PDD__c>{pddObj1,pddObj2,pddObjTest};
            test.stopTest();
        System.debug('Total Number of SOQL Queries allowed in this apex code context: ' + Limits.getLimitQueries());
    }


  
    
    
}