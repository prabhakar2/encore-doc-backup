@isTest
public class UpdateCAMInLeaseSoftTest {
	@isTest
    static void test1(){
        Account acc = new Account();
        acc.BillingCity = 'Noida';
        acc.Name = 'Test Account';
        acc.Unique_Id__c = '1234';
        insert acc;
        
        Cam__c cm = new Cam__c();
        cm.Account__c = acc.Id;
        cm.Limit_Applied_For__c = 200000;
        cm.Next_Review_Date__c = System.today() + 5;
        insert cm;
        
        Cam_Rating__c cmRat = new Cam_Rating__c();
        cmRat.CAM__c = cm.Id;
        cmRat.Special_Condition__c = 'Test Conditions';
        insert cmRat;
        UpdateCAMInLeaseSoft.doCallOut(new List<Id>{cm.Id});
    }
}