public class CreateQuoteInSFDC {
    @future(Callout=true)
    public static void fetchQuote(){
        boolean isSuccess = false;
        String errorMsg = '';
        String endPointURL = System.label.QuoteCreationCalloutURL; 
        Http httpObj = new Http();
        HttpRequest req = new HttpRequest();
        req.setTimeout(60000);
        req.setEndpoint(endPointURL);
        req.setMethod('GET');
        req.setHeader('Content-Type','application/json');
        String username = Label.API_Callout_Username;
        String password = Label.API_Callout_password;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        HttpResponse res = new HttpResponse();
        
       if(!test.isRunningTest())
            res = httpObj.send(req);
       else{ 
            //String str = '[{"QNo":21,"QuotationDate":"2019-01-04T11:40:33.877","Client":"","Registeredcity":"NEW DELHI","Purchasedcity":"NEW DELHI","Manufacturer":"Toyota Kirloskar Motors Ltd","Model":"Innova Diesel","Variant":"Toyota Innova Crysta GX-7Str","Tenure":"60","KMS":"0","LeaseType":"OL","RvType":"LRV","CType":"Non-Commercial","AppliedInterestRate":"11.500","TotalMonthlyEMI":"47333.730","FMS":"17826.53","Profitability":"230401.36","AccessoriesTotal":"1212.48","isApproved":1,"ApprovedOn":"","isPrintApproved":1,"PrintApprovedDate":"","PrintApproved_By":"","OpportunityID":""},{"QNo":22,"QuotationDate":"2019-01-04T11:40:33.877","Client":"","Registeredcity":"NEW DELHI","Purchasedcity":"NEW DELHI","Manufacturer":"Toyota Kirloskar Motors Ltd","Model":"Innova Diesel","Variant":"Toyota Innova Crysta GX-7Str","Tenure":"60","KMS":"0","LeaseType":"OL","RvType":"LRV","CType":"Non-Commercial","AppliedInterestRate":"11.500","TotalMonthlyEMI":"47333.730","FMS":"17826.53","Profitability":"230401.36","AccessoriesTotal":"1212.48","isApproved":1,"ApprovedOn":"","isPrintApproved":1,"PrintApprovedDate":"","PrintApproved_By":"","OpportunityID":""},{"QNo":23,"QuotationDate":"2019-01-04T11:40:33.877","Client":"","Registeredcity":"NEW DELHI","Purchasedcity":"NEW DELHI","Manufacturer":"Toyota Kirloskar Motors Ltd","Model":"Innova Diesel","Variant":"Toyota Innova Crysta GX-7Str","Tenure":"60","KMS":"0","LeaseType":"OL","RvType":"LRV","CType":"Non-Commercial","AppliedInterestRate":"11.500","TotalMonthlyEMI":"47333.730","FMS":"17826.53","Profitability":"230401.36","AccessoriesTotal":"1212.48","isApproved":1,"ApprovedOn":"","isPrintApproved":1,"PrintApprovedDate":"","PrintApproved_By":"","OpportunityID":""},{"QNo":24,"QuotationDate":"2019-01-04T11:40:33.877","Client":"","Registeredcity":"NEW DELHI","Purchasedcity":"NEW DELHI","Manufacturer":"Toyota Kirloskar Motors Ltd","Model":"Innova Diesel","Variant":"Toyota Innova Crysta GX-7Str","Tenure":"60","KMS":"0","LeaseType":"OL","RvType":"LRV","CType":"Non-Commercial","AppliedInterestRate":"11.500","TotalMonthlyEMI":"47333.730","FMS":"17826.53","Profitability":"230401.36","AccessoriesTotal":"1212.48","isApproved":1,"ApprovedOn":"","isPrintApproved":0,"PrintApprovedDate":"","PrintApproved_By":"","OpportunityID":""},{"QNo":25,"QuotationDate":"2019-01-04T11:40:33.877","Client":"","Registeredcity":"NEW DELHI","Purchasedcity":"NEW DELHI","Manufacturer":"Toyota Kirloskar Motors Ltd","Model":"Innova Diesel","Variant":"Toyota Innova Crysta GX-7Str","Tenure":"60","KMS":"0","LeaseType":"OL","RvType":"LRV","CType":"Non-Commercial","AppliedInterestRate":"11.500","TotalMonthlyEMI":"47333.730","FMS":"17826.53","Profitability":"230401.36","AccessoriesTotal":"1212.48","isApproved":1,"ApprovedOn":"","isPrintApproved":0,"PrintApprovedDate":"","PrintApproved_By":"","OpportunityID":""},{"QNo":26,"QuotationDate":"2019-01-04T11:40:33.877","Client":"","Registeredcity":"NEW DELHI","Purchasedcity":"NEW DELHI","Manufacturer":"Toyota Kirloskar Motors Ltd","Model":"Innova Diesel","Variant":"Toyota Innova Crysta GX-7Str","Tenure":"60","KMS":"0","LeaseType":"OL","RvType":"LRV","CType":"Non-Commercial","AppliedInterestRate":"11.500","TotalMonthlyEMI":"47333.730","FMS":"17826.53","Profitability":"230401.36","AccessoriesTotal":"1212.48","isApproved":1,"ApprovedOn":"","isPrintApproved":0,"PrintApprovedDate":"","PrintApproved_By":"","OpportunityID":""},{"QNo":27,"QuotationDate":"2019-01-04T11:40:33.877","Client":"","Registeredcity":"NEW DELHI","Purchasedcity":"NEW DELHI","Manufacturer":"Toyota Kirloskar Motors Ltd","Model":"Innova Diesel","Variant":"Toyota Innova Crysta GX-7Str","Tenure":"60","KMS":"0","LeaseType":"OL","RvType":"LRV","CType":"Non-Commercial","AppliedInterestRate":"11.500","TotalMonthlyEMI":"47333.730","FMS":"17826.53","Profitability":"230401.36","AccessoriesTotal":"1212.48","isApproved":1,"ApprovedOn":"","isPrintApproved":0,"PrintApprovedDate":"","PrintApproved_By":"","OpportunityID":""},{"QNo":28,"QuotationDate":"2019-01-04T11:40:33.877","Client":"","Registeredcity":"NEW DELHI","Purchasedcity":"NEW DELHI","Manufacturer":"Toyota Kirloskar Motors Ltd","Model":"Innova Diesel","Variant":"Toyota Innova Crysta GX-7Str","Tenure":"60","KMS":"0","LeaseType":"OL","RvType":"LRV","CType":"Non-Commercial","AppliedInterestRate":"11.500","TotalMonthlyEMI":"47333.730","FMS":"17826.53","Profitability":"230401.36","AccessoriesTotal":"1212.48","isApproved":1,"ApprovedOn":"","isPrintApproved":0,"PrintApprovedDate":"","PrintApproved_By":"","OpportunityID":""},{"QNo":29,"QuotationDate":"2019-01-04T11:40:33.877","Client":"","Registeredcity":"NEW DELHI","Purchasedcity":"NEW DELHI","Manufacturer":"Toyota Kirloskar Motors Ltd","Model":"Innova Diesel","Variant":"Toyota Innova Crysta GX-7Str","Tenure":"60","KMS":"0","LeaseType":"OL","RvType":"LRV","CType":"Non-Commercial","AppliedInterestRate":"11.500","TotalMonthlyEMI":"47333.730","FMS":"17826.53","Profitability":"230401.36","AccessoriesTotal":"1212.48","isApproved":1,"ApprovedOn":"","isPrintApproved":0,"PrintApprovedDate":"","PrintApproved_By":"","OpportunityID":""},{"QNo":30,"QuotationDate":"2019-01-04T11:40:33.877","Client":"","Registeredcity":"NEW DELHI","Purchasedcity":"NEW DELHI","Manufacturer":"Toyota Kirloskar Motors Ltd","Model":"Innova Diesel","Variant":"Toyota Innova Crysta GX-7Str","Tenure":"60","KMS":"0","LeaseType":"OL","RvType":"LRV","CType":"Non-Commercial","AppliedInterestRate":"11.500","TotalMonthlyEMI":"47333.730","FMS":"17826.53","Profitability":"230401.36","AccessoriesTotal":"1212.48","isApproved":1,"ApprovedOn":"","isPrintApproved":0,"PrintApprovedDate":"","PrintApproved_By":"","OpportunityID":""}]';
            //String str = '[{"QNo":220405,"QuotationDate":"2019-01-04T11:40:33.877","Client":"","Registeredcity":"NEW DELHI","Purchasedcity":"NEW DELHI","Manufacturer":"Toyota Kirloskar Motors Ltd","Model":"Innova Diesel","Variant":"Toyota Innova Crysta GX-7Str","Tenure":"60","KMS":"0","LeaseType":"OL","RvType":"LRV","CType":"Non-Commercial","AppliedInterestRate":"11.500","TotalMonthlyEMI":"47333.730","FMS":"17826.53","Profitability":"230401.36","AccessoriesTotal":"1212.48","isApproved":1,"ApprovedOn":"","isPrintApproved":0,"PrintApprovedDate":"","PrintApproved_By":"","OpportunityID":""}]' ;        
            String str = '[{"QNo":230351,"QuotationDate":"2019-07-22 10:52:11 AM","Client":"PRIMEMOVER MOBILITY TECHNOLOGIES PVT. LTD.","Registeredcity":"HYDERABAD/SECUNDERABAD","Purchasedcity":"HYDERABAD/SECUNDERABAD","BranchCity":"HYDERABAD/SECUNDERABAD","BranchCityID":"9","CityID":"9","Manufacturer":"Hyundai Motors India Ltd","Model":"Santro Petrol","Variant":"NEW SANTRO MAGNA AT (P)","Tenure":"48","KMS":"0","LeaseType":"OL","RvType":"LRV","totalRV":"121160.470","CType":"Commercial","AppliedInterestRate":"10.500","TotalMonthlyEMI":"19096.310","FMS":"7139.730","Profitability":"66704.28","AccessoriesTotal":"13275.00","isApproved":1,"ApprovedOn":"2019-07-22 02:42:7 PM","isPrintApproved":1,"PrintApprovedDate":"2019-07-22 12:30:40 pm","printapproved_by":"koya.takahashi","opportunityid":"1341","email":"inderjeet.singh@smasindia.com","CreatedOn":"2019-07-22 10:52:11 AM","CreatedBy":"inderjeet.singh","clientID":"2031","BasePrice":"403868.220","EndRv":"30.000","EndRVval":"121160.470","LR":"8638.314","FinalProfitPerc":"9.04","INSTYPE":"All Year Financed","RTTYPE":"All Year Financed","ClientCategory":"Existing","ProfitLoss":"1389.67","AppliedValue":"15378.05","Contracted":"671442.24","LandingIntRate":"8.96","DisposalProfit":"0.00"}]';
            res.setBody(str);
            
      }
        
        System.debug('>>>>'+res.getBody());
        if(res.getBody() != '[]'){
            try{
                List<Quotation__c> quoteListToUpdate = new List<Quotation__c>();
                List<QuoteWrapper> quoteWrapList = (List<QuoteWrapper>)JSON.deserialize(res.getBody(), List<QuoteWrapper>.class);
                
                Map<String,Quotation__c> quoteNoToQuoteMap = new Map<String,Quotation__c>();
                Map<String,Opportunity> oppIdToOppMap = new Map<String,Opportunity>();
                Map<String,Id> createdByMap = new Map<String,Id>();
                Map<String,Account> clientMap = new Map<String,Account>();
                 Map<String,Account> clientUpdateMap = new Map<String,Account>();
                Map<String,Id> branchMap = new Map<String,Id>();
                for(QuoteWrapper qtWrap : quoteWrapList){
                    if(qtWrap.QNo != null)
                        quoteNoToQuoteMap.put(String.valueOf(qtWrap.QNo),null);
                    
                    if(qtWrap.OpportunityID != ''){
                        System.debug('oppId>>>>>'+qtWrap.OpportunityID);
                        oppIdToOppMap.put(String.valueOf(qtWrap.OpportunityID),null);
                    }
                    
                    if(qtWrap.CityID != null)
                        branchMap.put(String.valueOf(qtWrap.CityID), null);
                    
                    if(String.isNotBlank(qtWrap.Email))
                        createdByMap.put(String.valueOf(qtWrap.Email),null);
                    if(String.isNotBlank(qtWrap.clientID))
                        clientMap.put(String.valueOf(qtWrap.clientID),null);
                    
                    
                }
                
                for(Opportunity opp : [SELECT Id,Opportunity_ID__c,Account.Name
                                       FROM Opportunity
                                       WHERE Opportunity_ID__c IN :oppIdToOppMap.keySet()]){
                                           
                                           if(oppIdToOppMap.containsKey(opp.Opportunity_ID__c))
                                               oppIdToOppMap.put(opp.Opportunity_ID__c,opp);
                                           
                                       }
                
                for(Account acc : [SELECT Id,Unique_Id__c
                                       FROM Account
                                       WHERE Unique_Id__c IN :clientMap.keySet()]){
                                           
                                           if(clientMap.containsKey(acc.Unique_Id__c))
                                               clientMap.put(acc.Unique_Id__c,acc);
                                           
                                       }
                
                for(Quotation__c qt : [SELECT Id,Name,LeaseSoftQuoteId__c,Status__c
                                       FROM Quotation__c
                                       WHERE LeaseSoftQuoteId__c IN :quoteNoToQuoteMap.keySet()]){
                                           
                                           if(quoteNoToQuoteMap.containsKey(qt.LeaseSoftQuoteId__c))
                                               quoteNoToQuoteMap.put(qt.LeaseSoftQuoteId__c,qt);
                                           
                                       }
                
                for(Branch__c branch : [SELECT Id,LeaseSoft_Id__c,Branch__c FROM Branch__c WHERE LeaseSoft_Id__c IN :branchMap.keySet()]){
                    if(branchMap.containsKey(branch.LeaseSoft_Id__c)){
                        branchMap.put(branch.LeaseSoft_Id__c,branch.Id);
                    }
                }
                for(User usr : [SELECT Id,Email FROM User WHERE Email IN :createdByMap.keySet() AND isActive = true]){
                    createdByMap.put(String.valueOf(usr.Email),usr.Id);
                }
                System.debug('oppIdToOppMap>>>>'+oppIdToOppMap);
                for(QuoteWrapper qtWrap : quoteWrapList){
                    Quotation__c qt = new Quotation__c();
                    qt.Status__c = 'Draft';
                    if(quoteNoToQuoteMap.get(String.valueOf(qtWrap.QNo)) != null)
                        qt = quoteNoToQuoteMap.get(String.valueOf(qtWrap.QNo));
                    if(String.isNotBlank(qtWrap.OpportunityID)){
                        qt.Opportunity_Name__c = oppIdToOppMap.get(qtWrap.OpportunityID) !=null ? oppIdToOppMap.get(qtWrap.OpportunityID).Id : null;
                        //qt.Account__c = oppIdToOppMap.get(qtWrap.OpportunityID) !=null ? oppIdToOppMap.get(qtWrap.OpportunityID).AccountId : '';
                    }
                    if(String.isNotBlank(qtWrap.ClientId) && clientMap.get(String.valueOf(qtWrap.ClientId)) != null ){
                        qt.Account__c = clientMap.get(String.valueOf(qtWrap.ClientId)).Id;
                        System.debug('>>>>>>>Calling'+qt.Account__c);
                    }
                    qt.Client_Name__c = String.isNotBlank(qtWrap.Client) ? qtWrap.Client : '';
                    qt.Name	= String.valueOf(qtWrap.QNo);
                    qt.Quote_Name__c = (oppIdToOppMap.get(qtWrap.OpportunityID) !=null ?  oppIdToOppMap.get(qtWrap.OpportunityID).Account.Name :'' )+' '+(String.valueOf(qtWrap.Variant) != '' ? qtWrap.Variant : '')+' '+(String.valueOf(qtWrap.QNo));
                    qt.LeaseSoftQuoteId__c = String.valueOf(qtWrap.QNo);
                    qt.Registered_City__c = String.isNotBlank(qtWrap.Registeredcity) ? qtWrap.Registeredcity : '';
                    qt.Purchase_City__c = String.isNotBlank(qtWrap.Purchasedcity) ? qtWrap.Purchasedcity : '';
                    qt.Manufacturer__c = String.isNotBlank(qtWrap.Manufacturer) ? qtWrap.Manufacturer : '';
                    qt.Model__c = String.isNotBlank(qtWrap.Model) ? qtWrap.Model : '';
                    qt.Variant__c = String.isNotBlank(qtWrap.Variant) ? qtWrap.Variant : '';
                    qt.Tenure__c = String.isNotBlank(qtWrap.Tenure) ? Decimal.valueOf(qtWrap.Tenure) : 0.0 ;
                    qt.Kms__c = String.isNotBlank(qtWrap.KMS) ? Decimal.valueOf(qtWrap.KMS) : 0.0;
                    qt.Print_Approved_by__c = String.isNotBlank(qtWrap.PrintApproved_By) ? qtWrap.PrintApproved_By : '';
                    qt.Interest_Rate__c = String.isNotBlank(qtWrap.AppliedInterestRate) ? Decimal.valueOf(qtWrap.AppliedInterestRate) : 0.0;
                    qt.revisedQNo__c =String.isNotBlank(qtWrap.revisedQNo) ? qtWrap.revisedQNo : '';
                    qt.isRevisedQuote__c= String.isNotBlank(qtWrap.isRevisedQuote) ? qtWrap.isRevisedQuote : '';
                    if(String.isNotBlank(qtWrap.PrintApprovedDate)){
                        String[] dtStr = (qtWrap.PrintApprovedDate).split(' ')[0].split('-');
                        String dateStr = dtStr[2]+'-'+dtStr[1]+'-'+dtStr[0];
                        qt.Print_Approved_Date__c = date.newInstance(integer.valueof(dtStr[0]), integer.valueof(dtStr[1]), integer.valueof(dtStr[2]));
                    }
                    System.debug('clientMap>>>>'+clientMap);
                    if(!Label.Status_values_for_no_update_in_status.contains(qt.Status__c)){
                        if(qtWrap.isPrintApproved != null && qtWrap.isPrintApproved == 1 && qtWrap.isApproved != 1){
                            qt.Status__c = 'Print Approved';
                        }
                        if(qtWrap.isApproved != null && qtWrap.isApproved == 1 ){
                            qt.Status__c = 'Approved';
                            
                            //*********************** Added to update the credit Info on client(26-03-2019) *****************************// 
                            if(label.Update_Client_Credit_Info_From_Quote_API == 'YES' && clientMap.get(String.valueOf(qtWrap.ClientId)) != null){
                                if(!clientUpdateMap.containskey(String.valueOf(qtWrap.ClientId))){
                                    Account acc = UpdateCreditInfo.updateCredit(clientMap.get(String.valueOf(qtWrap.ClientId)));
                                    clientUpdateMap.put(String.valueOf(qtWrap.ClientId),acc);
                                }
                            }
                        }
                    }
                    qt.Branch__c = branchMap.get(String.valueOf(qtWrap.CityID));
                    qt.Branch_City__c = String.isNotBlank(qtWrap.BranchCity) ? qtWrap.BranchCity : '';
                    qt.Base_Price__c = String.isNotBlank(qtWrap.BasePrice) ? Decimal.valueOf(qtWrap.BasePrice) : 0.0;
                    qt.End_RV__c = String.isNotBlank(qtWrap.EndRv) ? Decimal.valueOf(qtWrap.EndRv) : 0.0;
                    qt.End_RV_Value__c = String.isNotBlank(qtWrap.EndRVval) ? Decimal.valueOf(qtWrap.EndRVval) : 0.0;
                    qt.Lease_Rent__c = String.isNotBlank(qtWrap.LR) ? Decimal.valueOf(qtWrap.LR) : 0.0;
                    qt.Final_Profit__c = String.isNotBlank(qtWrap.FinalProfitPerc) ? Decimal.valueOf(qtWrap.FinalProfitPerc) : 0.0;
                    qt.Road_Tax_Type__c = String.isNotBlank(qtWrap.RTTYPE) ? String.valueOf(qtWrap.RTTYPE) : '';
                    qt.Insurance_Type__c = String.isNotBlank(qtWrap.INSTYPE) ? String.valueOf(qtWrap.INSTYPE) : '';
                    qt.LS_Created_Date__c = String.isNotBlank(qtWrap.CreatedOn) ? Date.valueOf(qtWrap.CreatedOn) : null;
                    qt.LS_Created_by_Email__c = String.isNotBlank(qtWrap.Email) ? qtWrap.Email : '';
                    qt.LS_Created_by__c =(String.isNotBlank(qtWrap.Email) &&  createdByMap.get(qtWrap.Email) != null)? createdByMap.get(qtWrap.Email):System.label.API_User_Id;
                    qt.OwnerId = (String.isNotBlank(qtWrap.Email) && createdByMap.get(qtWrap.Email) != null) ? createdByMap.get(qtWrap.Email): System.label.API_User_Id;
                    qt.Accessories_total_value__c = String.isNotBlank(qtWrap.AccessoriesTotal)?Decimal.valueOf(qtWrap.AccessoriesTotal) : 0.0;
                    qt.RV_Type__c = String.isNotBlank(qtWrap.RvType) ? qtWrap.RvType : '';
                    qt.Lease_Type__c = String.isNotBlank(qtWrap.LeaseType) ? qtWrap.LeaseType : '';
                    qt.Total_Monthly_EMI__c = String.isNotBlank(qtWrap.TotalMonthlyEMI)?Decimal.valueOf(qtWrap.TotalMonthlyEMI) : 0.0;
                    qt.Profitability__c = String.isNotBlank(qtWrap.Profitability)?Decimal.valueOf(qtWrap.Profitability) : 0.0;
                    qt.Quotation_Date__c = String.isNotBlank(qtWrap.QuotationDate) ? Date.valueOf(qtWrap.QuotationDate)  : null;
                    qt.Usage_Type__c = String.isNotBlank(qtWrap.CType) ? qtWrap.CType : '';
                    qt.FMS__c = String.isNotBlank(qtWrap.FMS)?Decimal.valueOf(qtWrap.FMS) : 0.0;
                    qt.Client_Type__c = String.isNotBlank(qtWrap.ClientCategory) ? qtWrap.ClientCategory : '';
                    
                    /******************************************Added on 9-5-2019**************************************/
                    
                    qt.ProfitLoss__c = String.isNotBlank(qtWrap.ProfitLoss) ? Decimal.valueOf(qtWrap.ProfitLoss) : 0.0;
                    qt.Applied_Value__c = String.isNotBlank(qtWrap.AppliedValue) ? Decimal.valueOf(qtWrap.AppliedValue) : 0.0;
                    qt.Contracted__c = String.isNotBlank(qtWrap.Contracted) ? Decimal.valueOf(qtWrap.Contracted) : 0.0;
                    qt.LandingIntRate__c = String.isNotBlank(qtWrap.LandingIntRate) ? Decimal.valueOf(qtWrap.LandingIntRate) : 0.0;
                    qt.Disposal_Profit__c = String.isNotBlank(qtWrap.DisposalProfit) ? Decimal.valueOf(qtWrap.DisposalProfit) : 0.0;
                     /************************************************************************************************/
                    quoteListToUpdate.add(qt);
                }
                if(quoteListToUpdate.size() > 0){
                    upsert quoteListToUpdate;
                    if(clientUpdateMap.values().size() > 0)
                        update clientUpdateMap.values();
                    System.debug('quoteListToUpdate>>>'+quoteListToUpdate);
                    isSuccess = true;
                }
                    
            }
            catch(Exception e){
                isSuccess = false;
                errorMsg = e.getMessage() + ' '+e.getMessage();
            }
            finally{
                SmasConstants.createLog(isSuccess,errorMsg,'CreateQuoteInSFDC','Quotation__c',System.now(),res.getBody());
            }
        }else
            SmasConstants.createLog(false,'Blank Body','CreateQuoteInSFDC','Quotation__c',System.now(),res.getBody());
    }
}