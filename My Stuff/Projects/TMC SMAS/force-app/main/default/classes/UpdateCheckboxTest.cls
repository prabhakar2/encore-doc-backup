@istest
public class UpdateCheckboxTest {
    public static testmethod void mytest(){
        
        Opportunity opp = new Opportunity();
        opp.Name='testopp';
        opp.CloseDate=system.today();
        opp.StageName='Closed Won';
        insert opp;
        
        Quotation__c qu = new Quotation__c();
        qu.Opportunity_Name__c=opp.id;
        
        //qu.Name='testQuote';
        insert qu;
        
        DO_Line_Item__c dli = new DO_Line_Item__c();
        dli.Name='test';
        dli.TR_Code__c='FIN';
        dli.Subcode__c='CAR';
        dli.Quotation__c=qu.Id;
        dli.DoCancel__c=True;
        insert dli;
        
       /* DO_Line_Item__c dli1 = new DO_Line_Item__c();
        dli1.Name='test';
        dli1.TR_Code__c='INS';
        dli1.Subcode__c='YR1';
        dli1.Quotation__c=qu.Id;
        dli1.DoCancel__c=true;
        insert dli1;
      */
    

    }
    
    
    public static testmethod void mytest1(){
        Opportunity opp = new Opportunity();
        opp.Name='testopp';
        opp.CloseDate=system.today();
        opp.StageName='Closed Won';
        insert opp;
        
        Quotation__c qu = new Quotation__c();
        qu.Opportunity_Name__c=opp.id;
        qu.Car_DO__c=false;
        //qu.Name='testQuote';
        insert qu;
     
        
        DO_Line_Item__c dli = new DO_Line_Item__c();
        dli.Name='test';
        dli.TR_Code__c='RTA';
        dli.Subcode__c='RTA';
        dli.Quotation__c=qu.Id;
        insert dli;
        
        DO_Line_Item__c dli1 = new DO_Line_Item__c();
        dli1.Name='test';
        dli1.TR_Code__c='FIN';
        dli1.Subcode__c='CAR';
        dli1.Quotation__c=qu.Id;
        dli1.DoCancel__c=False;
        insert dli1;
        
        qu.Car_DO__c=true;
        qu.Insurance_DO__c=False;
        update qu;
        
        DO_Line_Item__c dli2 = new DO_Line_Item__c();
        dli2.Name='test';
        dli2.TR_Code__c='FIN';
        dli2.Subcode__c='CAR';
        dli2.Quotation__c=qu.Id;
        insert dli2;
    }
}