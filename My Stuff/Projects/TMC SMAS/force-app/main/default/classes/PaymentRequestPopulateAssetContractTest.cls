@isTest
public class PaymentRequestPopulateAssetContractTest {
	@isTest
    static void test1(){
        Quotation__c qt = new Quotation__c();
        qt.Name = 'test';
        insert qt;
        
        Asset_Contract__c ac = new Asset_Contract__c();
        ac.Quotation__c = qt.Id;
        insert ac;
        
        Payment_Request__c pr = new Payment_Request__c();
        pr.Quotation__c = qt.Id;
        pr.Initiated_by__c = UserInfo.getUserId();
        insert pr;
    }
}