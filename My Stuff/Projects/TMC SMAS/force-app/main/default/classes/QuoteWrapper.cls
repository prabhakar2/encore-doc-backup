public class QuoteWrapper {

	public Integer QNo {get;set;} 
	public String QuotationDate {get;set;} 
	public String Client {get;set;} 
    public String ClientId {get;set;} 
	public String Registeredcity {get;set;} 
	public String Purchasedcity {get;set;} 
	public String Manufacturer {get;set;} 
	public String Model {get;set;} 
	public String Variant {get;set;} 
	public String Tenure {get;set;} 
	public String KMS {get;set;} 
	public String LeaseType {get;set;} 
	public String RvType {get;set;} 
	public String CType {get;set;} 
	public String AppliedInterestRate {get;set;} 
	public String TotalMonthlyEMI {get;set;} 
	public String FMS {get;set;} 
	public String Profitability {get;set;} 
	public String AccessoriesTotal {get;set;} 
	public String ApprovedOn {get;set;} 
	public String OpportunityID {get;set;} 
    public String Status {get;set;}
    public Integer isApproved{get;set;}
    public Integer isPrintApproved{get;set;}
	public String PrintApprovedDate{get;set;}
    public String PrintApproved_By{get;set;}
    public String Email{get;set;}
    public String CreatedOn{get;set;}
    public String CreatedBy{get;set;}
    public String BasePrice{get;set;}
    public String EndRv{get;set;}
    public String EndRVval{get;set;}
    public String LR{get;set;}
    public String FinalProfitPerc{get;set;}
    public String INSTYPE{get;set;}
    public String RTTYPE{get;set;}
    public String BranchCity{get;set;}   
    public String CityID{get;set;}
    public String ClientCategory{get;set;}
    public String ProfitLoss{get;set;}
    public String AppliedValue{get;set;}
    public String Contracted{get;set;}
    public String LandingIntRate{get;set;}
    public String DisposalProfit{get;set;}
    public string revisedQNo{get;set;}
    public string isRevisedQuote{get;set;}
    public QuoteWrapper(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'QNo') {
						QNo = parser.getIntegerValue();
					} else if (text == 'QuotationDate') {
						QuotationDate = parser.getText();
					} else if (text == 'Client') {
						Client = parser.getText();
					} else if (text == 'Registeredcity') {
						Registeredcity = parser.getText();
					} else if (text == 'Purchasedcity') {
						Purchasedcity = parser.getText();
					} else if (text == 'Manufacturer') {
						Manufacturer = parser.getText();
					} else if (text == 'Model') {
						Model = parser.getText();
					} else if (text == 'Variant') {
						Variant = parser.getText();
					} else if (text == 'Tenure') {
						Tenure = parser.getText();
					} else if (text == 'KMS') {
						KMS = parser.getText();
					} else if (text == 'LeaseType') {
						LeaseType = parser.getText();
					} else if (text == 'RvType') {
						RvType = parser.getText();
					} else if (text == 'CType') {
						CType = parser.getText();
					} else if (text == 'AppliedInterestRate') {
						AppliedInterestRate = parser.getText();
					} else if (text == 'TotalMonthlyEMI') {
						TotalMonthlyEMI = parser.getText();
					} else if (text == 'FMS') {
						FMS = parser.getText();
					} else if (text == 'Profitability') {
						Profitability = parser.getText();
					} else if (text == 'AccessoriesTotal') {
						AccessoriesTotal = parser.getText();
					} else if (text == 'ApprovedOn') {
						ApprovedOn = parser.getText();
					} else if (text == 'PrintApprovedDate') {
						PrintApprovedDate = parser.getText();
					} else if (text == 'OpportunityID') {
						OpportunityID = parser.getText();
					}else if (text == 'Status') {
						Status = parser.getText();
					}else if (text == 'isApproved') {
						isApproved = parser.getIntegerValue();
					}else if (text == 'isPrintApproved') {
						isPrintApproved = parser.getIntegerValue();
                    }else if(text == 'revisedQNo'){
                        revisedQNo= parser.getText();
                    }else if(text == 'isRevisedQuote__c'){
                        isRevisedQuote=parser.getText();
                    }
                    else {
						System.debug(LoggingLevel.WARN, 'QuoteWrapper consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
	
	
	public static List<QuoteWrapper> parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return arrayOfJSON2Apex(parser);
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
	


    private static List<QuoteWrapper> arrayOfJSON2Apex(System.JSONParser p) {
        List<QuoteWrapper> res = new List<QuoteWrapper>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new QuoteWrapper(p));
        }
        return res;
    }



}