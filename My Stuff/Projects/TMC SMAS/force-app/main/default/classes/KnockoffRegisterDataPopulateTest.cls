@isTest
public class KnockoffRegisterDataPopulateTest {
    
    @testSetup static void initDataForTest(){
        Account acc = new Account();
        acc.Name='cbhj';
        acc.Oracle_ID__c='qwert';
        acc.BillingCity='Noida';
        insert acc;
        
        Invoice__c inv =new Invoice__c();
        inv.Invoice_Number__c='hhg278';
        insert inv;
        
        Receipt__c rfec=new Receipt__c();
        rfec.Transaction_No__c='ferrg';
        insert rfec;
      }
    
    public static testMethod void validKnckOffTest(){
        Account ac = [select id, Oracle_Id__c from Account];
        Invoice__c invc=[select Invoice_Number__c from Invoice__c];
        Receipt__c recpt=[select Transaction_No__c from Receipt__c];
        
        Knof_Off_Register__c kchkOfRef=new Knof_Off_Register__c();
        kchkOfRef.Oracle_Id__c=ac.Oracle_ID__c;
        kchkOfRef.Invoice_Number__c=invc.Invoice_Number__c;
        kchkOfRef.Transaction_Number__c=recpt.Transaction_No__c;
        kchkOfRef.Amount__c=50000;
        kchkOfRef.Line_Item_Description__c='test';
        kchkOfRef.Unique_Id__c='qer234';
        insert kchkOfRef;
        
         Test.enableChangeDataCapture(); 
        
        Test.getEventBus().deliver();
        
    }
    
   
    
}