@istest
public class ApproveVRLDocumentControllerTest {
    public static testmethod void mytest(){
        
        Account acc=new Account();
        acc.name='fanindra';
        acc.billingCity = 'Noida';
        insert acc;
        
        Opportunity opp=new Opportunity();
        opp.Name='testopp';
        opp.AccountId=acc.Id;
        opp.StageName='Closed Won';
        opp.CloseDate=Date.newInstance(2019,12,26);
        insert opp;
        
        Id rectype = Schema.SObjectType.Document_Master__c.getRecordTypeInfosByName().get('VRL').getRecordTypeId();
        Document_Master__c doc=new Document_Master__c();
        doc.name='document test';
        doc.Active__c=true;
        doc.RecordTypeId=rectype;
        insert doc;
        
        Quotation__c qu=new Quotation__c();
       // qu.name='quote123';
        qu.Opportunity_Name__c=opp.Id;
        qu.Status__c='Print Approved';
        qu.VRLDocumentsCreated__c=False;
        //qu.Active__c=true;
        insert qu;    
        
        List<String> vrllist =new List<String>();
        VRL_Documents__c vrl= new VRL_Documents__c();
        vrl.Required_Stage__c='To Process DO';
        vrl.Document__c=doc.Id;
        vrl.Quotation__c=qu.Id;        
        insert vrl;
        vrllist.add(vrl.Id);
        
        //Content version-------------------------
        ContentVersion cv=new Contentversion();
        cv.title='ABC';
        //cv.ContentDocumentId=doc.Id;
        cv.PathOnClient ='test';
        Blob b=Blob.valueOf('Unit Test Attachment Body');
        cv.versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body');
        insert cv;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = acc.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        
        ApproveVRLDocumentController.checkUserProfile();
        ApproveVRLDocumentController.getVRLDocumnets(qu.Id);
        ApproveVRLDocumentController.updateNewDoc( cdl.ContentDocumentId,vrl.Id);
        ApproveVRLDocumentController.initApprovalProcessForRecords(vrllist,JSON.serialize(documents));        
    }
}