public class InsuranceRequestComponentCtrl {
   
    @auraEnabled
    public static String getInsuranceObject(String sobjectId){
        
        String errorMsg = '';
        List<PDD__c> pddInfoList = [select id ,name from PDD__c where Quotation__c = : sobjectId];
        if(pddInfoList != null && pddInfoList.size() >0 ){
            List<PDD__c> pddInfoForNameValidate = [select id , name ,Status__c from PDD__c where Quotation__c = : sobjectId AND Document_Master__r.Name=: system.label.Original_Tax_Invoice];
            if(pddInfoForNameValidate != null && pddInfoForNameValidate.size() >0 && pddInfoForNameValidate[0].Status__c !=null && pddInfoForNameValidate[0].Status__c!='Pending'){
               return errorMsg;
            }else{
                errorMSg ='Original Tax Invoice Document Not Found In PDD';
                return errorMsg;
            }
        }else{
            errorMsg ='No PDD Document found ';
           return errorMsg;
        }
       
    }
    
    @auraEnabled
    public static void saveRecord(String engine,String chassis,String description,String recordid){
        Insurance_Request__c  ir = new Insurance_Request__c();
        ir.Engine_No__c = engine;
        ir.Chassis_No__c = chassis;
        ir.Description__c = description;
        ir.Quotation__c = recordid;
        insert ir;
    }
    
    
    
}