public class CamWorkflowsCtrl {

    @AuraEnabled
    Public Static List<QuoteWrapper> getworkflowsTasks(String CamId){
        List<QuoteWrapper> WrapList = new List<QuoteWrapper>();
        List<Task> taskList = [SELECT Id, Subject, ActivityDate,ownerId,Status,Owner.Name, Start_Date__c,Custom_Due_Date_Time__c FROM Task where WhatId=:CamId];
        if(taskList.size()>0){
            for(Task t : taskList){
                QuoteWrapper qw = new QuoteWrapper();
                qw.Ts=t;
                qw.Timer=null;
                if(t.Start_Date__c>=system.today()){
                    qw.Istime=true;
                }else{
                    qw.Istime=false;
                }
                if(t.Status=='Completed'){
                    qw.IsComplete=true;
                    Datetime sDate=t.Start_Date__c;
                    Datetime Duedate=t.Custom_Due_Date_Time__c;
                    Long dt1Long = sDate.getTime();
                    Long dt2Long = Duedate.getTime();
                    Long milliseconds = dt2Long - dt1Long;
                    Long seconds = milliseconds / 1000;
                    Long minutes = seconds / 60;
                    Long hours = minutes / 60;
                    Long days = hours / 24;
                    qw.Timer=days + ' days, ' + hours + ' hours, ' + 0+ ' mins, ' + 0 + ' seconds';
                }
                else{
                    qw.IsComplete=false;
                    
                }
                WrapList.add(qw);
            }  
             return WrapList;
        }
        return null;
        
    }
    
    Public class QuoteWrapper{
        @AuraEnabled Public Task Ts{get;set;}
        @AuraEnabled Public Boolean Istime{get;set;} 
        @AuraEnabled Public Boolean IsComplete{get;set;}
        @AuraEnabled Public String Timer{get;set;}               
    }
}