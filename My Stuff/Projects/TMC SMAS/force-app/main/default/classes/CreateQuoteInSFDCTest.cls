@isTest
public class CreateQuoteInSFDCTest {
    static testMethod void testMethod1(){
     
        
        Account acc=new Account();
        acc.name='bhatt';
        acc.Industry='Airlines';
        acc.Unique_Id__c='2285';
        acc.BillingCity = 'Noida';
        insert acc;
        
        List<opportunity> opplist = new List<opportunity>();
        opportunity  TestOpp=new opportunity ();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=system.today()+1 ;
        TestOpp.AccountId=acc.Id;
        opplist.add(TestOpp);        
        insert opplist;

        Leased_Asset__c la = new Leased_Asset__c();
        la.Account__c=acc.Id;
        la.Branch_City__c='Delhi';
        la.LeaseSoftId__c='TMC123';
        la.Asset_Number__c='300379';
        insert la;
        
        Quotation__c quot = new Quotation__c();
        quot.Account__c=acc.Id;
        quot.Lease_Rent__c=10000;
        quot.LeaseSoftQuoteId__c='117190';
        quot.Status__c='Approved';
        quot.Kms__c=45000;
        quot.Tenure__c=36;
        insert quot;
        
        Asset_Contract__c ac= new Asset_Contract__c();
        ac.Account__c=acc.id;
        ac.Leased_Asset__c=la.Id;
        ac.Quotation__c=quot.Id;
        insert ac;
        
        

        Test.startTest();

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/application/json/';
        req.httpMethod = 'GET';
      //  req.requestBody = Blob.valueof(jsonStr);
        
        RestContext.request = req;
        RestContext.response= res;
       //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
       	String str = '[{"QNo":117190,"QuotationDate":"2017-02-09T10:57:20","Status":"Approved","isApproved":123,"isPrintApproved":121,"Client":"1MG Technologies Private Limited","Registeredcity":"GURGAON","Purchasedcity":"GURGAON","Manufacturer":"Maruti Suzuki India Ltd.","Model":"Swift Petrol","Variant":"Swift Vxi","Tenure":"36","KMS":"45000","LeaseType":"OL","RvType":"LRV","CType":"Non-Commercial","AppliedInterestRate":"12.250","TotalMonthlyEMI":"19802.950","FMS":"","Profitability":"","AccessoriesTotal":"","ApprovedOn":"","PrintApprovedDate":"10-02-2018 1:01:37 PM","OpportunityID":""}]';
        JSONParser parser = JSON.createParser(str);
        QuoteWrapper obj = new QuoteWrapper(parser);
        QuoteWrapper.parse(str);
        QuoteWrapper.consumeObject(parser);
        CreateQuoteInSFDC.fetchQuote();        
        update TestOpp;
        Test.stopTest();
    }
}