@istest
public class CAMPDFControllerTest {
    Public static testmethod void mytest(){
        Account acc = new Account();
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect').getRecordTypeId();
        acc.Name = 'test';
        insert acc;
        
        CAM__c cm = new CAM__c();
        cm.Account__c = acc.Id;
        insert cm;
        
        Key_Management_Profile__c key = new Key_Management_Profile__c();
        key.CAM__c = cm.Id;
        key.Profile__c = 'test';
        key.Designation__c = 'test';
        key.Name = 'test';
        insert key;
        
        Key_Management_Profile__c key2 = new Key_Management_Profile__c();
        key2.CAM__c = cm.Id;
        key2.Profile__c = 'test';
        key2.Designation__c = 'test';
        key2.Name = 'test';
        insert key2;
        
        Key_Management_Profile__c key3 = new Key_Management_Profile__c();
        key3.CAM__c = cm.Id;
        key3.Profile__c = 'test';
        key3.Designation__c = 'test';
        key3.Name = 'test';
        insert key3;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(cm);
        CAMPDFController obj = new CAMPDFController(sc);
        obj.name1 = new List<Key_Management_Profile__c>{key};
            
            ApexPages.StandardController sc2 = new ApexPages.StandardController(cm);
        CAMPDFController obj2 = new CAMPDFController(sc2);
        obj2.name1= new List<Key_Management_Profile__c>{key,key2};
            
            ApexPages.StandardController sc3 = new ApexPages.StandardController(cm);
        CAMPDFController obj3 = new CAMPDFController(sc2);
        obj3.name1= new List<Key_Management_Profile__c>{key,key2,key3};
    }
}