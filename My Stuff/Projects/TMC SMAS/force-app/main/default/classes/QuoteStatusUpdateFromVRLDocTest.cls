@istest
public class QuoteStatusUpdateFromVRLDocTest {
    
    public static testmethod void mytest(){
        
        Account acc=new Account();
        acc.name='fanindra';
        acc.BillingCity = 'Noida';
        insert acc;
        
        Opportunity opp=new Opportunity();
        opp.Name='testopp';
        opp.AccountId=acc.Id;
        opp.StageName='Closed Won';
        opp.CloseDate=Date.newInstance(2019,12,26);
        insert opp;
        
        Branch__c br = new Branch__c();
        br.Branch__c='Chennai';
        insert br;
        
        Id rectype = Schema.SObjectType.Document_Master__c.getRecordTypeInfosByName().get('VRL').getRecordTypeId();
        Document_Master__c doc=new Document_Master__c();
        doc.name='document test';
        doc.Active__c=true;
        doc.Required_Stage__c = 'To Process DO';
        doc.RecordTypeId=rectype;
        insert doc;
        
        Quotation__c qu=new Quotation__c();
        qu.Opportunity_Name__c=opp.Id;
        qu.Status__c='Print Approved';
        qu.VRL_Document_Status__c = 'Submitted by Sales';
        qu.VRLDocumentsCreated__c=False;
        qu.Branch__c = br.Id;
        insert qu;    
        
        VRL_Documents__c vrl= new VRL_Documents__c();
        vrl.Required_Stage__c='To Process DO';
        vrl.Document__c=doc.Id;
        vrl.Quotation__c=qu.Id;
        vrl.Status__c='Pending from Sales';
        insert vrl;
        vrl.Status__c='Submitted by Sales';
        update vrl;
        
        VRL_Documents__c vrl1= new VRL_Documents__c();
        vrl1.Required_Stage__c='To Process DO';
        vrl1.Document__c=doc.Id;
        vrl1.Quotation__c=qu.Id;
        vrl1.Status__c='Rejected by Fleet';
        insert vrl1;
        vrl1.Status__c='Received by Fleet';
        update vrl1;
        
    }
}