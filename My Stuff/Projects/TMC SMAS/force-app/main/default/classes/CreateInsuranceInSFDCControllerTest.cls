@istest
public class CreateInsuranceInSFDCControllerTest {
    Public static testmethod void mytest(){
        Account acc=new Account();
        acc.name='bhatt';
        acc.Industry='Airlines';
        acc.Dealer_ID__c='TMC-123';
        insert acc;
        
        Leased_Asset__c la = new Leased_Asset__c();
        la.Account__c=acc.Id;
        la.Branch_City__c='Delhi';
        la.LeaseSoftId__c='TMC123';
        la.Asset_Number__c='1';
        insert la;
        
        Quotation__c quot = new Quotation__c();
        quot.Account__c=acc.Id;
        quot.Lease_Rent__c=10000;
        quot.LeaseSoftQuoteId__c='2';
        quot.Status__c='Approved';
        quot.Kms__c=45000;
        quot.Tenure__c=36;
        insert quot;
        
        CreateInsuranceInSFDCController.InsuranceWrapper cis = new CreateInsuranceInSFDCController.InsuranceWrapper();
        cis.AssetNumber='1';
        cis.QuoteNumber='2';
        cis.FromDate='3';
        cis.ToDate='4';
        cis.Amount='10000';
        cis.InsuranceCompany='TMC';
        cis.InsuranceCompID='TMC-123';
        cis.PolicyNo='12345';
        
        CreateInsuranceInSFDCController reqst=new CreateInsuranceInSFDCController();
        String jsonStr=JSON.serialize(cis);
        
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = 'application/json';
        req.httpMethod = 'GET';
        req.requestBody = Blob.valueof(jsonStr);
        
        RestContext.request = req;
        RestContext.response= res;
        CreateInsuranceInSFDCController.getInsurance();
        Test.stopTest();
    }
}