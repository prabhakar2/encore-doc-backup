@isTest
public class CreateVRLDocumentTriggerTest {
    
    public static testmethod void mytest(){
        
        Account acc=new Account();
        acc.name='bhatt';
        acc.Industry='Airlines';
        acc.Unique_Id__c='7618';
        acc.BillingCity= 'Noida';
        insert acc;
        
        Leased_Asset__c la = new Leased_Asset__c();
        la.Account__c=acc.Id;
        la.Branch_City__c='Delhi';
        la.LeaseSoftId__c='TMC123';
        la.Asset_Number__c='300379';
        insert la;
        
        Quotation__c quot = new Quotation__c();
        quot.Account__c=acc.Id;
        quot.Lease_Rent__c=10000;
        quot.LeaseSoftQuoteId__c='117190';
        quot.Status__c='Approved';
        quot.Kms__c=45000;
        quot.Tenure__c=36;
        insert quot;
        
        Asset_Contract__c ac= new Asset_Contract__c();
        ac.Account__c=acc.id;
        ac.Leased_Asset__c=la.Id;
        ac.Quotation__c=quot.Id;
        insert ac;
        
        Contact con = new Contact();
        con.AccountId=acc.Id;
        con.LastName='Fanindra';
        con.Primary_Contact__c='Primary Contact';
        insert con;
        
        Id rectype = Schema.SObjectType.Document_Master__c.getRecordTypeInfosByName().get('VRL').getRecordTypeId();
        
        Id rectype1 = Schema.SObjectType.Document_Master__c.getRecordTypeInfosByName().get('PDD').getRecordTypeId();
        Document_Master__c doc=new Document_Master__c();
        doc.name='document test';
        doc.Active__c=true;
        doc.RecordTypeId=rectype;
        insert doc;
        
        Document_Master__c doc1=new Document_Master__c();
        doc1.name='document test';
        doc1.Active__c=true;
        doc1.RecordTypeId=rectype1;
        insert doc1;
        
        Quotation__c qu=new Quotation__c();
        qu.Status__c='Approved';
        qu.VRLDocumentsCreated__c=False;
        insert qu;
    }
}