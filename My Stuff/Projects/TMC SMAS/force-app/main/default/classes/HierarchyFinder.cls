public class HierarchyFinder {
    
    private static Map<id,set<id>> completeRoleMap;
    public static List<User> subordinatesList;
    
    public static List<User> getSubordiatesOf(string userId){
        completeRoleMap = new Map<id,Set<id>>();
        subordinatesList =  new List<User>();
        
        User usr = [select UserRoleId FROM User WHERE id = :userId];
        
        if(usr.UserRoleId != NULL){
            fillCompleteData();
         	findSubordinates(usr.UserRoleId);
            return subordinatesList;
        }
        else
            return new List<User>();
        
    }
    
    private static void fillCompleteData(){
        for(UserRole ur : [SELECT id,ParentRoleId FROM UserRole]){
            if(ur.ParentRoleId != NULL){
                if(completeRoleMap.containsKey(ur.ParentRoleId)){
                    Set<id> tempSet = completeRoleMap.get(ur.ParentRoleId);
                    tempSet.add(ur.Id);
                    completeRoleMap.put(ur.ParentRoleId,tempSet);
                }else{
                    completeRoleMap.put(ur.ParentRoleId,new Set<id>{ur.id});
                }
                
            }
        }
    }
    
    private static void findSubordinates(Id roleId){
        System.debug('roleId>>>>'+roleId);
        System.debug('completeRoleMap>>>>'+completeRoleMap);
        if(completeRoleMap.containsKey(roleId)){
            Set<Id> roleIdSet = getSetRecursion(completeRoleMap.get(roleId));
            subordinatesList = [Select id,name,userRoleId,Username,Function__c 
                                From User 
                                where userRoleId IN :roleIdSet AND Function__c = 'Sales'];
        }
    }
    
    
    private static Set<id> getSetRecursion(Set<id> roleSet){
        Set<id> tempINSet = new Set<Id>();
        for(Id role : roleSet){
            if(completeRoleMap.containsKey(role))
                tempINSet.addAll(getSetRecursion(completeRoleMap.get(role)));
            else{
                tempINSet.add(role);
            }	
        }
        tempINSet.addAll(roleSet);
        return tempINSet;
    }
}