public class SmasConstants {
    public static set<string> camRatingFilter = new Set<string>{'Organization_Type__c','Industry_Type__c'};
        public static set<string> camRatingRangeFilter = new Set<string>{'Product_Service_Type__c',
            'Years_in_Business__c','Current_Ratio__c','Avg_Receivable_Days__c','Acid_Test_Ratio__c','Avg_Payable_Days__c',
            'Debt_Equity_Ratio__c','Debt_Asset_Ratio__c','Return_on_Equity_Ratio__c','Growth_in_EBDITA__c','Net_Profit_Margin__c'};
                
                public static Map<string,string> CAMRatingfieldMap = new Map<string,string>{'Organization Type'=>'Organization_Type_Rating__c',
                    'Industry Type'=>'Industry_Type_Rating__c',
                    'Product / Service Type'=>'Product_Service_Type_Rating__c',
                    'Years in Business'=>'Years_in_Business_Rating__c',
                    'Current Ratio'=>'Current_Ratio_Rating__c',
                    'Avg. Receivable Days'=>'Avg_Receivable_Days_Rating__c',
                    'Acid Test Ratio'=>'Acid_Test_Ratio_Rating__c',
                    'Avg. Payable Days'=>'Avg_Payable_Days_Rating__c',
                    'Debt/Equity Ratio'=>'Debt_Equity_Ratio_Rating__c',
                    'Debt/Asset Ratio'=>'Debt_Asset_Ratio_Rating__c',
                    'Return on Equity Ratio'=>'Return_on_Equity_Ratio_Rating__c',
                    'Growth in EBDITA'=>'Growth_in_EBDITA_Rating__c',
                    'Net Profit Margin'=>'Net_Profit_Margin_Rating__c'};
                        
                        
                        public static void createLog(boolean isSuccess,String errorMsg,String APIName,String objectName,DateTime callTime,String reponseBody){ //,Integer noOfRecords
                            API_Log__c log = new API_Log__c();
                            log.Is_Success__c = isSuccess;
                            log.Error__c = errorMsg;
                            log.API_Name__c = APIName;
                            log.Object_Name__c = objectName;
                            log.Call_Time__c =callTime;
                            //log.Reponse_Body__c = reponseBody;
                            //log.No_of_Records__c = noOfRecords;
                            insert log;
                            
                            
                        }
}