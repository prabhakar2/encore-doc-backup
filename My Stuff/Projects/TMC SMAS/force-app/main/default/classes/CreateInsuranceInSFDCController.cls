/*
#ClassName : CreateInsuranceInSFDCController
#Created Date : 21/11/2018
#Purpose : This class is used to make API callout to third party system to get the data of Insurance and insert them into SFDC.
#Developed By : 
*/
public class CreateInsuranceInSFDCController{
    @future(Callout=true)
    public static void getInsurance(){
        boolean isSuccess = false;
        String errorMsg = '';
        String endPointURL = System.Label.InsuranceCalloutURL;
        Http httpObj = new Http();
        HttpRequest req = new HttpRequest();
        
        /*  Creating request here  */
        req.setEndpoint(endPointURL);
        req.setMethod('GET');
        req.setTimeout(60000);
        req.setHeader('Content-Type','application/json');
        String username = Label.API_Callout_Username;
        String password = Label.API_Callout_password;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        HttpResponse res = new HttpResponse();
        
        if(!Test.isRunningTest()){
            /*  Making the callout to third party system.  */
            res = httpObj.send(req);
        }else{
            String str = '[{ "AssetNumber": 1, "QuoteNumber": 2, "FromDate": "sample string 3", "ToDate": "sample string 4", "Amount": "sample string 5", "InsuranceCompany": "sample string 6" }]';
            res.setBody(str);
            res.setStatusCode(200);
        }
        /*  Checking if we got the response ot not.  */
        if(res.getStatusCode() == 200 && String.isNotBlank(res.getBody()) && res.getBody() != '[]'){
            try{
                List<Insurance__c> insuranceToInsert = new List<Insurance__c>();
                Map<String,String> assetMap = new Map<String,String>();
                Map<String,String> quoteMap = new Map<String,String>();
                Map<String,String> accountMap = new Map<String,String>();
                
                /*  Fetching body from response.  */
                String jsonStr = res.getBody();
                /*  deserializing json body into objects.  */
                List<InsuranceWrapper> insuranceList = (List<InsuranceWrapper>)System.JSON.deserialize(jsonStr, List<InsuranceWrapper>.class);
                
                /*  Checking if we list has data ot not after  deserialize. */
                if(insuranceList != null && insuranceList.size()>0){
                    /*  Collecting QuoteNumber & assetNumber to fetch the quote and asset for record association.  */
                    for(InsuranceWrapper ins : insuranceList){
                        if(String.isNotBlank(ins.QuoteNumber))
                            quoteMap.put(ins.QuoteNumber,null);
                        
                        if(String.isNotBlank(ins.AssetNumber))
                            assetMap.put(ins.AssetNumber,null);
                        
                        if(String.isNotBlank(ins.InsuranceCompID))
                            accountMap.put(ins.InsuranceCompID,null);
                    }
                    
                    /*  Collecting quote based on the quotenumber in the response.  */
                    if(quoteMap != null && quoteMap.size()>0){
                        for(Quotation__c quote : [SELECT Id, LeaseSoftQuoteId__c FROM Quotation__c WHERE LeaseSoftQuoteId__c IN : quoteMap.keySet()]){
                            if(quoteMap.containsKey(quote.LeaseSoftQuoteId__c)){
                                quoteMap.put(quote.LeaseSoftQuoteId__c,quote.Id);
                            }
                        }
                    }
                    
                    /*  Collecting asset based on the assetnumber in the response.  */
                    if(assetMap != null && assetMap.size()>0){
                        for(Leased_Asset__c aset : [SELECT Id, Asset_Number__c FROM Leased_Asset__c WHERE Asset_Number__c IN : assetMap.keySet()]){
                            if(assetMap.containsKey(aset.Asset_Number__c)){
                                assetMap.put(aset.Asset_Number__c,aset.Id);
                            }
                        }
                    }
                    
                    /*  Collecting account based on the InsuranceCompID in the response.  */
                    if(accountMap != null && accountMap.size()>0){
                        for(Account acc : [SELECT Id, Dealer_ID__c FROM account WHERE Dealer_ID__c IN : accountMap.keySet()]){
                            if(accountMap.containsKey(acc.Dealer_ID__c)){
                                accountMap.put(acc.Dealer_ID__c,acc.Id);
                            }
                        }
                    }
                    
                    /*  Copying values from wrapper to actual object for insert.  */
                    for(InsuranceWrapper ins : insuranceList){
                        Insurance__c insObj = new Insurance__c();
                        if(String.isNotBlank(ins.rowid)){
                            insObj.LeaseSoft_Id__c = String.valueOf(ins.rowid);
                        }
                        if(assetMap.containsKey(ins.AssetNumber)){
                            insObj.Leased_Asset__c = assetMap.get(ins.AssetNumber);
                        }
                        if(quoteMap.containsKey(ins.QuoteNumber)){
                            insObj.Quotation__c = quoteMap.get(ins.QuoteNumber);
                        }
                        if(accountMap.containsKey(ins.InsuranceCompID)){
                            insObj.Insurance_Company_Account__c = accountMap.get(ins.InsuranceCompID);
                        }
                        if(String.isNotBlank(ins.FromDate)){
                            String newDate = UtilityController.convertDateFormat(ins.FromDate);
                            insObj.Start_Date__c = Date.valueOf(newDate);
                        }
                        if(String.isNotBlank(ins.ToDate)){
                            String newDate = UtilityController.convertDateFormat(ins.ToDate);
                            insObj.End_Date__c = Date.valueOf(newDate);
                        }
                        
                        insObj.Amount__c = String.isNotBlank(ins.Amount) ? Decimal.valueOf(ins.Amount) : 0;
                        insObj.Policy_Number__c = String.isNotBlank(ins.PolicyNo) ? String.valueOf(ins.PolicyNo) : '';
                        
                        insuranceToInsert.add(insObj);                    
                    }
                }
                
                if(insuranceToInsert != null && insuranceToInsert.size()>0){
                    upsert insuranceToInsert LeaseSoft_Id__c;
                    isSuccess = true;
                }
            }
            catch(Exception e){
                isSuccess = false;
                errorMsg = e.getMessage();
            }
            finally{
                SmasConstants.createLog(isSuccess,errorMsg,'CreateInsuranceInSFDCController','Insurance__c',System.now(),res.getBody());
            }
            
        }else
            SmasConstants.createLog(false,'Blank Body','CreateInsuranceInSFDCController','Insurance__c',System.now(),res.getBody());
        
    }//  METHOD END
    
    /*  Wrapper class to deserialize the response from third party  */
    public class InsuranceWrapper{
        public String AssetNumber;
        public String rowid;
        public String QuoteNumber;
        public String FromDate;
        public String ToDate;
        public String Amount;
        public String InsuranceCompany;
        public String InsuranceCompID;
        public String PolicyNo;
    }
    
}