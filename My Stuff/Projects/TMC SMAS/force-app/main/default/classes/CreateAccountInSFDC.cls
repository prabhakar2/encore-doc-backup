public class CreateAccountInSFDC {
    @future(Callout=true)
    public static void fetchAccount(){
        boolean isSuccess = false;
        String errorMsg = '';
        String endPointURL = System.label.AccountCreationCalloutURL;
        Http httpObj = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPointURL);
        req.setMethod('GET');
        req.setTimeout(60000);
        req.setHeader('Content-Type','application/json');
        String username = Label.API_Callout_Username;
        String password = Label.API_Callout_password;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        
        HttpResponse res = new HttpResponse();
        if(!test.isRunningTest())
            res = httpObj.send(req);
        else{
            
            String str = '[{"ClientID":3842,"ProspectID":0,"LimitBalance":"2000","LimitRemarks":"testing","LimitValidEndDate":"31 Mar 2018","AccountName":"Test TMC PJ","Region":"South","OracleClientID":"","Type":"Customer","Address1":"unit 14,7th floor international tech park,","Address2":"white feild , banglore","Address3":"","City":"BANGALORE","Pin":"560066","Industry":"IT","subIndustry":"Others","PrimaryContact":"kiran.n","PrimaryContactPhone":"9632144411","PrimaryContactEmail":"kiran@test.com","SecondaryContact":"kiran.n","SecondaryContactPhone":"9632144411","SecondaryContactEmail":"kiran@test.com",",Ocean":"Blue","isJapanese":true,"TurnOver":"5 to 150 Crs","NoOfEmployees":"20 to 300","SFClientID":"","BDM":"vivek.bg","BDMEmail":"vivek.bg@smasindia.com","Dormant":"0","Dormant_Remark":"","LimitApprovedAmount":"1000","ToBeRecoveredAmount":"500","officeAddress":[{"OfficeID":"1543","ClientID":"3842","ClientName":"Sivantos India Private Limited","CityID":"3","CityName":"MUMBAI","Address1":"911/A, 1st Floor, A Wing Solitaire Corporate","Address2":"Park Chakala Andheri (East),","Address3":"","Phone":"NA","ContactPerson":"NA","StateName":"MAHARASHTRA"}]}]';
            res.setBody(str);
        }
        System.debug('>>>>'+res.getBody());
        if(res.getBody() != '[]'){
            try{
                List<Account> accListToUpdate = new List<Account>();
                List<Contact> contactListToUpdate = new List<Contact>();
                List<Branch_Offfice__c> branchOfficeListToUpdate = new List<Branch_Offfice__c>();
                List<AccountWrapper> accList = (List<AccountWrapper>)JSON.deserialize(res.getBody(), List<AccountWrapper>.class);
                System.debug('accList>>>>'+accList);
                Map<String,Account> clientIdToAccMap = new Map<String,Account>();
                Map<String,Account> sfIdToAccMap = new Map<String,Account>();
                Map<String,List<Contact>> contactMap = new Map<String,List<Contact>>();
                Map<String,Id> bdmMap = new Map<String,Id>();
                
                for(AccountWrapper acc : accList){
                    if(acc.ClientID != null){
                        clientIdToAccMap.put(String.valueOf(acc.ClientID),null);
                        contactMap.put(String.valueOf(acc.ClientID),null);
                    }
                    if(acc.SFClientID != null)
                        sfIdToAccMap.put(acc.SFClientID,null);
                    
                    if(String.isNotBlank(acc.BDMEmail))
                        bdmMap.put(String.valueOf(acc.BDMEmail),null);
                }
                
                for(Account acc : [SELECT Id,Name,Unique_Id__c,Salesforce_Account_ID__c
                                   FROM Account 
                                   WHERE Unique_Id__c IN :clientIdToAccMap.keySet()
                                   OR Salesforce_Account_ID__c IN :sfIdToAccMap.keySet()]){
                                       
                                       if(clientIdToAccMap.containsKey(String.valueOf(acc.Unique_Id__c))){
                                           clientIdToAccMap.put(String.valueOf(acc.Unique_Id__c),acc);
                                       }
                                       else if(sfIdToAccMap.containsKey(acc.Salesforce_Account_ID__c)){
                                           sfIdToAccMap.put(acc.Salesforce_Account_ID__c,acc);
                                       }
                                   }
                for(Contact con : [SELECT Id,Account.Unique_Id__c,Primary_Contact__c 
                                   FROM Contact 
                                   WHERE Account.Unique_Id__c IN :contactMap.keySet()
                                   AND (Primary_Contact__c = 'Primary Contact'
                                        OR Primary_Contact__c = 'Secondary Contact') 
                                   ORDER BY LastmodifiedDate DESC,primary_Contact__c]){
                                       
                                       if(contactMap.containsKey(con.Account.Unique_Id__c) && contactMap.get(con.Account.Unique_Id__c) != null){
                                           List<Contact> conList = contactMap.get(con.Account.Unique_Id__c);
                                           conList.add(con);
                                           contactMap.put(con.Account.Unique_Id__c,conList);
                                       }else
                                           contactMap.put(con.Account.Unique_Id__c,new List<Contact>{con});
                    
                }
                System.debug('contactMap>>>'+contactMap);
                for(User usr : [SELECT Id,Email 
                                FROM User 
                                WHERE Email IN :bdmMap.keySet() 
                                AND isActive = true ]){
                    bdmMap.put(String.valueOf(usr.Email),usr.Id);
                }
                
                
                for(AccountWrapper acc : accList){
                    Account accObj = new Account();
                    if(clientIdToAccMap.get(String.valueOf(acc.ClientID)) != null)
                        accObj = clientIdToAccMap.get(String.valueOf(acc.ClientID));
                    else if(sfIdToAccMap.get(acc.SFClientID) != null){
                        accObj = sfIdToAccMap.get(acc.SFClientID);
                        accObj.Unique_Id__c = String.valueOf(acc.ClientID);
                    }
                    else
                        accObj.Unique_Id__c = String.valueOf(acc.ClientID);
                    
                    accObj.Name = acc.AccountName != null ? acc.AccountName : '';
                    accObj.Industry = acc.Industry != null ? acc.Industry : '';
                    accObj.SubIndustry__c = acc.subIndustry != null ? acc.subIndustry : '';
                    accObj.RecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
                    
                    if(String.isNotBlank(acc.isJapanese) && acc.isJapanese != null){
                        if(acc.isJapanese == '1')
                            accObj.Is_Japanese__c = 'Yes';
                        else if(acc.isJapanese == '2')
                            accObj.Is_Japanese__c = 'No';
                    }
                    
                    if(String.isNotBlank(acc.Dormant)){
                        if(acc.Dormant == '0')
                            accObj.Is_Dormant__c = 'No';
                        else if(acc.Dormant == '1')
                            accObj.Is_Dormant__c = 'Yes';
                        else if(acc.Dormant == '2')
                            accObj.Is_Dormant__c = 'Dormant';
                    }
                    if(String.isNotBlank(acc.BDMEmail)){
                        accObj.BDM_Email__c = String.valueOf(acc.BDMEmail);
                        accObj.OwnerId = bdmMap.get(acc.BDMEmail) != null ? bdmMap.get(acc.BDMEmail) : System.label.API_User_Id;
                    }else
                        accObj.OwnerId = System.label.API_User_Id;
                    
                    accObj.BillingStreet = acc.Address1+' '+acc.Address2 ;
                    accObj.BillingCity = acc.City != null ? acc.City : '';
                    accObj.BillingPostalCode = acc.Pin != null ? acc.Pin : '';
                    accObj.Oracle_ID__c = String.isNotBlank(acc.OracleClientID) ? String.valueOf(acc.OracleClientID) : '' ;
                    accObj.Region__c = String.isNotBlank(acc.Region) ? String.valueOf(acc.Region) : '' ;
                    accObj.Ocean__c = acc.Ocean != null ? acc.Ocean : '';
                    accObj.Turn_Over__c = acc.TurnOver != null ? acc.TurnOver : '';
                    accObj.No_Of_Employees__c = acc.NoOfEmployees != null ? acc.NoOfEmployees : '';
                    accObj.Phone = String.isNotBlank(acc.PrimaryContactPhone) ? String.valueOf(acc.PrimaryContactPhone) : '';
                    accObj.MLA_Signed__c = true;
                    accObj = UpdateCreditInfo.updateCredit(accObj);
                    
                    
                    accListToUpdate.add(accObj);
                    
                      /***********************Primary Contact**********************************/
                    
                    if(String.isNotBlank(acc.PrimaryContact)){
                        
                        List<String> conFullName = (acc.PrimaryContact).split(' ');
                        Contact con = new Contact();
                        con.Account = accObj;
                        if(contactMap.containsKey(String.valueOf(acc.ClientID)) 
                           && contactMap.get(String.valueOf(acc.ClientID))!= null 
                           && contactMap.get(String.valueOf(acc.ClientID)).size() > 0)
                            con.Id = contactMap.get(String.valueOf(acc.ClientID))[0].Id;
                        
                        con.Primary_Contact__c = 'Primary Contact';
                        if(conFullName.size() <=2){
                            con.FirstName = conFullName.size() > 1 ? conFullName[0] : '';
                            con.LastName = conFullName.size() > 1 ? conFullName[1] : conFullName[0];
                        }else if(conFullName.size() > 2){
                            con.FirstName = conFullName[0] + conFullName[1];
                            con.LastName = conFullName[2];
                        }
                        con.MobilePhone = String.isNotBlank(acc.PrimaryContactPhone) ? String.valueOf(acc.PrimaryContactPhone) : '';
                        con.Email__c = String.isNotBlank(acc.PrimaryContactEmail) ? String.valueOf(acc.PrimaryContactEmail) : '';
                        contactListToUpdate.add(con);
                    }
                    
                    /***********************Secondary Contact**********************************/
                    
                    if(String.isNotBlank(acc.SecondaryContact)){ 
                        List<String> conFullName = (acc.SecondaryContact).split(' ');
                        Contact con = new Contact();
                        con.Account = accObj;
                        if(contactMap.containsKey(String.valueOf(acc.ClientID)) && contactMap.get(String.valueOf(acc.ClientID)) != null
                          && contactMap.get(String.valueOf(acc.ClientID)).size() > 1)
                            con.Id = contactMap.get(String.valueOf(acc.ClientID))[1].Id;
                        
                        con.Primary_Contact__c = 'Secondary Contact';
                        if(conFullName.size() <=2){
                            con.FirstName = conFullName.size() > 1 ? conFullName[0] : '';
                            con.LastName = conFullName.size() > 1 ? conFullName[1] : conFullName[0];
                        }else if(conFullName.size() > 2){
                            con.FirstName = conFullName[0] + conFullName[1];
                            con.LastName = conFullName[2];
                        }
                        
                        con.MobilePhone = String.isNotBlank(acc.SecondaryContactPhone) ? String.valueOf(acc.SecondaryContactPhone) : '';
                        con.Email__c = String.isNotBlank(acc.SecondaryContactEmail) ? String.valueOf(acc.SecondaryContactEmail) : '';
                        contactListToUpdate.add(con);
                    }
                    
                    /*****************************************************************************************/
                    
                    for(AccountWrapper.BranchWrapper wrp : acc.officeAddress){
                        Branch_Offfice__c brOffice = new Branch_Offfice__c();
                        brOffice.Account__r = accObj;
                        brOffice.Office_ID__c = String.isNotBlank(wrp.OfficeID) ? String.valueOf(wrp.OfficeID) : '';
                        brOffice.City_ID__c = String.isNotBlank(wrp.CityID) ? String.valueOf(wrp.CityID) : '';
                        brOffice.City_Name__c = String.isNotBlank(wrp.CityName) ? String.valueOf(wrp.CityName) : '';
                        brOffice.Address1__c = String.isNotBlank(wrp.Address1) ? String.valueOf(wrp.Address1) : '';
                        brOffice.Address2__c = String.isNotBlank(wrp.Address2) ? String.valueOf(wrp.Address2) : '';
                        brOffice.Address3__c = String.isNotBlank(wrp.Address3) ? String.valueOf(wrp.Address3) : '';
                        brOffice.Phone__c = String.isNotBlank(wrp.Phone) ? String.valueOf(wrp.Phone) : '';
                        brOffice.Contact_Person__c = String.isNotBlank(wrp.ContactPerson) ? String.valueOf(wrp.ContactPerson) : '';
                        brOffice.State_Name__c = String.isNotBlank(wrp.StateName) ? String.valueOf(wrp.StateName) : '';
                        branchOfficeListToUpdate.add(brOffice);
                    }
                }
                
                if(accListToUpdate.size() > 0){
                    upsert accListToUpdate;
                    for(Contact con : contactListToUpdate){
                        con.AccountId = con.Account.Id;
                    }
                    upsert contactListToUpdate;
                    
                    for(Branch_Offfice__c branch : branchOfficeListToUpdate){
                        branch.Account__c = branch.Account__r.Id;
                    }
                    upsert branchOfficeListToUpdate Office_ID__c;
                }
                
               isSuccess = true;
            }
            catch(Exception e){
                isSuccess = false;
                errorMsg = e.getMessage()+' '+e.getLineNumber();
            }
            
            finally{
                SmasConstants.createLog(isSuccess,errorMsg,'CreateAccountInSFDC','Account',System.now(),res.getBody());
            }
        }else
            SmasConstants.createLog(false,'Blank Body','CreateAccountInSFDC','Account',System.now(),res.getBody()); 
    }
}