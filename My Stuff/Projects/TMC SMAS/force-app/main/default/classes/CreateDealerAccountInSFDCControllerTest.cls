@istest
public class CreateDealerAccountInSFDCControllerTest {
    Public static testmethod void mytest(){
        
        /*   
Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();

Account acc = new Account();
acc.RecordTypeId=RecordTypeId;
acc.Name='Testacc';
insert acc;
*/
        Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        
        Account acc=new Account();
        acc.RecordTypeId=RecordTypeId;
        acc.name='bhatt';
        acc.Industry='Airlines';
        acc.Unique_Id__c='TMC123';
        insert acc;
        
        List<opportunity> opplist = new List<opportunity>();
        opportunity  TestOpp=new opportunity ();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=system.today()+1 ;
        TestOpp.AccountId=acc.Id;
        opplist.add(TestOpp);        
        insert opplist;
        
        
        
        CreateDealerAccountInSFDCController.DealerAccountWrapper cdas= new CreateDealerAccountInSFDCController.DealerAccountWrapper();
        cdas.DealerID='546789';
        cdas.DealerName='TestDealer';
        cdas.AccountName='Testacc';
        cdas.AccountType='TestType';
        cdas.Address1='Delhi';
        cdas.Address2='Laxminagar';
        cdas.Address3='Nirman Vihar';
        cdas.City='Delhi';
        cdas.Pin='110092';
        cdas.Status='Test';
        cdas.GSTNO='456789';
        
        
        CreateDealerAccountInSFDCController reqst=new CreateDealerAccountInSFDCController();
        String jsonStr=JSON.serialize(cdas);

        
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = 'application/json';
        req.httpMethod = 'GET';
        req.requestBody = Blob.valueof(jsonStr);
        
        RestContext.request = req;
        RestContext.response= res;

        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        CreateDealerAccountInSFDCController.getDealerAccounts();
        update TestOpp;
        
        Test.stopTest();
        
    }
}