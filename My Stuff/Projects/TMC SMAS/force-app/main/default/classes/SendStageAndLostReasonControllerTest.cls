@isTest
public class SendStageAndLostReasonControllerTest {
 
     Public static testmethod void mytest2(){
        
        Account acc=new Account();
        acc.name='bhatt';
        acc.Industry='Airlines';
        acc.Unique_Id__c='TMC123';
        acc.BillingCity='agra';
        insert acc;
        
        
        System.debug('acc>>>>>'+acc.Salesforce_Account_ID__c);
        List<opportunity> opplist = new List<opportunity>();
        opportunity  TestOpp=new opportunity ();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=system.today()+1 ;
        TestOpp.AccountId=acc.Id;
        opplist.add(TestOpp);        
        insert opplist;
         System.debug('opplist-->'+opplist.size());
        SendOpportunityIdController.ClientWrapper sa = new SendOpportunityIdController.ClientWrapper();
        
        sa.ClientId= [select Salesforce_Account_ID__c from account limit 1].Salesforce_Account_ID__c;
        String jsonStr=JSON.serialize(sa);
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/getOpportunityStageReason/*';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(jsonStr);
        
        RestContext.request = req;
        RestContext.response= res;
        system.debug('res'+res);
         system.debug('RestContext.response'+RestContext.response);
        SendStageAndLostReasonController.sendOpportunityStageResn();
         
        Test.stopTest();
     }
     Public static testmethod void mytest3(){
        
        Account acc=new Account();
        acc.name='bhatt';
        acc.Industry='Airlines';
        acc.Unique_Id__c='TMC123';
        acc.BillingCity='agra';
        insert acc;
        
        
        System.debug('acc>>>>>'+acc.Salesforce_Account_ID__c);
        List<opportunity> opplist = new List<opportunity>();
        opportunity  TestOpp=new opportunity ();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=system.today()+1 ;
        TestOpp.AccountId=acc.Id;
        opplist.add(TestOpp);        
        insert opplist;
         System.debug('opplist-->'+opplist.size());
        SendOpportunityIdController.ClientWrapper sa = new SendOpportunityIdController.ClientWrapper();
        
        sa.ClientId= '[select Salesforce_Account_ID__c from account limit 1].Salesforce_Account_ID__c';
        String jsonStr=JSON.serialize(sa);
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/getOpportunityStageReason/*';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(jsonStr);
        
        RestContext.request = req;
        RestContext.response= res;
        
        SendStageAndLostReasonController.sendOpportunityStageResn();
         
        Test.stopTest();
     }
    Public static testmethod void mytest4(){
        
        Account acc=new Account();
        acc.name='bhatt';
        acc.Industry='Airlines';
        acc.Unique_Id__c='TMC123';
        acc.BillingCity='agra';
        insert acc;
        
        
        System.debug('acc>>>>>'+acc.Salesforce_Account_ID__c);
        List<opportunity> opplist = new List<opportunity>();
        opportunity  TestOpp=new opportunity ();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=system.today()+1 ;
        TestOpp.AccountId=acc.Id;
        opplist.add(TestOpp);        
        insert opplist;
         System.debug('opplist-->'+opplist.size());
        SendOpportunityIdController.ClientWrapper sa = new SendOpportunityIdController.ClientWrapper();
        
        sa.ClientId= '[select Salesforce_Account_ID__c from account limit 1].Salesforce_Account_ID__c';
        String jsonStr=JSON.serialize('sa');
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/getOpportunityStageReason/*';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(jsonStr);
        
        RestContext.request = req;
        RestContext.response= res;
        
        SendStageAndLostReasonController.sendOpportunityStageResn();
         
        Test.stopTest();
     }
}