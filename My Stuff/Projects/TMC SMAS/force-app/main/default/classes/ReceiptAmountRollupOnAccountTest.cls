@istest
public class ReceiptAmountRollupOnAccountTest {
    Public Static Testmethod Void mytest(){
        
        Account acc=new Account();
        acc.Year__c='2018';
        acc.name='Fanindra';
        acc.Insurance_Amount_Consumed__c=500;
        acc.SubIndustry__c='Agent';
        acc.Ocean__c='Red';
        acc.No_Of_Employees__c='100';
        acc.Turn_Over__c='45687588';
        acc.BillingCity = 'Noida';
        insert acc;
                
        Receipt__c rec = new Receipt__c();
        rec.Account__c=acc.id;
        insert rec;
        
    }
}