/*
    #ClassName : SendOpportunityIdController
    #Created Date : 26/11/2018
    #Purpose : This class is used send opportunity Stage and Reason based on the clientId of account to third party system.
    #Developed By : 
*/
@RestResource(urlMapping='/getOpportunityId/')
global class SendOpportunityIdController{
    
    @HTTPPOST
    global static List<Opportunity> sendOpportunityId(){
        RestRequest req = RestContext.request;
        List<Opportunity> oppList = new List<Opportunity>();
        try{
            
            ClientWrapper wrap = (ClientWrapper)JSON.deserialize(req.requestBody.toString(),ClientWrapper.class);
            oppList = [SELECT Name, Opportunity_ID__c FROM Opportunity WHERE Account.Salesforce_Account_Id__c = :wrap.ClientId];
            if(oppList.size() > 0){
                SmasConstants.createLog(true,'','SendOpportunityIdController','Opportunity',System.now(),req.requestBody.toString());
                return oppList;
            }
            else{
                SmasConstants.createLog(false,'No Opportunity found for SF Account Id : '+wrap.ClientId+'.','SendOpportunityIdController','Opportunity',System.now(),req.requestBody.toString());
                return oppList;
            } 
        }
        catch(Exception e){
            SmasConstants.createLog(false,e.getMessage()+' '+e.getLineNumber(),'SendOpportunityIdController','Opportunity',System.now(),req.requestBody.toString());
            return oppList;
        }
    }
    
    /*  Wrapper class to deserialize the JSON body.  */
    public class ClientWrapper{
        public String ClientId;
    }
}