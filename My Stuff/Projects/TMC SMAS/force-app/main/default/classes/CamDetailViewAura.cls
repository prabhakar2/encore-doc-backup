public class CamDetailViewAura {
    @auraEnabled
    public static String initialFetch(String accId,String camId){
        try{
            DataWrapper dataWrap = new DataWrapper();
            if(accId != null){
                List<Account> accList = [SELECT Id,Name,BillingAddress,ShippingAddress,Organization_Type__c,Industry,
                                         Years_in_Operation_in_India__c,Years_in_Operation_Globally__c,
                                         No_of_Employee_in_India__c,Locations_In_India__c,Primary_Contact__r.Name,
                                         Primary_Contact__c,BillingStreet,BillingCity,BillingState,BillingPostalCode,
                                         BillingCountry,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,
                                         ShippingCountry
                                         FROM Account
                                         WHERE Id =:accId LIMIT 1];
                if(accList.size() > 0){
                    dataWrap.accObj = accList[0];
                    
                }
                
                
                List<CAM__c> camlist = getRecords('CAM__c','Id',camId,null);
                if(camlist.size() > 0){
                    dataWrap.camObj = camlist[0];
                    
                    List<Key_Management_Profile__c> keyProfileList = getRecords('Key_Management_Profile__c','CAM__c',camId,null);
                    if(keyProfileList.size() > 0)
                        dataWrap.keyProfileList.addAll(keyProfileList);
                    
                    List<Balance_Sheet__c> balSheetList = getRecords('Balance_Sheet__c','Id',camlist[0].Balance_Sheet_CY__c,camlist[0].Balance_Sheet_PY__c);
                    if(balSheetList.size() > 0)
                        dataWrap.balanceSheetList.addAll(balSheetList);
                    
                    List<Profit_and_Loss__c> profitlossList = getRecords('Profit_and_Loss__c','Id',camlist[0].Profit_Loss_CY__c,camlist[0].Profit_and_Loss_PY__c);
                    if(profitlossList.size() > 0)
                        dataWrap.profitlossList.addAll(profitlossList);
                    List<CAM_Documents__c> camDocList = getRecords('CAM_Documents__c','CAM__c',camId,null);
                    if(camDocList.size() > 0)
                        dataWrap.camDocList.addAll(camDocList);
                }
                System.debug('balanceSheetList>>>>>>'+dataWrap.balanceSheetList);
                return json.serialize(dataWrap);
                
            }
            
            return 'Error : Something wrong with this record.';
        }catch(Exception e){
            return 'Error : '+e.getMessage();
        }
    }
    
    private static List<SObject> getRecords(String objAPIName,String fieldName,String filterField1,String filterField2){
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objAPIName).getDescribe().fields.getMap();
        String allFields = '';
        String qry='';
        for(String field : objectFields.keySet()){
            allFields+=field+',';
        }
        allFields = allFields.removeEnd(',');
        qry = 'SELECT '+allFields;
        
        if(objAPIName == 'CAM__c')
            qry += ',Account__r.Name,BDM__r.Name,Sales_person_name__r.Name FROM '+objAPIName+' WHERE '+fieldName+'=:filterField1 LIMIT 1';
        else if(objAPIName == 'CAM_Documents__c')
            qry += ',Document__r.Name FROM '+objAPIName+' WHERE '+fieldName+'=:filterField1';
        else{
            qry += ' FROM '+objAPIName+' WHERE ';
            if(objAPIName == 'Key_Management_Profile__c')
                qry +=  fieldName+'=:filterField1 ORDER By Name';
            else if(objAPIName == 'Balance_Sheet__c')
                qry += fieldName+'=:filterField1 OR '+fieldName+'=:filterField2 ORDER By Financial_Year__c DESC LIMIT 2';
            else if(objAPIName == 'Profit_and_Loss__c')
                qry += fieldName+'=:filterField1 OR '+fieldName+'=:filterField2 ORDER By Year_ended__c DESC LIMIT 2';
        }
        
        
        return Database.query(qry);
    }
    
    public class DataWrapper{
        @auraEnabled public Account accObj;
        @auraEnabled public CAM__c camObj;
        @auraEnabled public List<Key_Management_Profile__c> keyProfileList;
        @auraEnabled public List<Balance_Sheet__c> balanceSheetList;
        @auraEnabled public List<Profit_and_Loss__c> profitlossList;
        @auraEnabled public List<CAM_Documents__c> camDocList;
        public DataWrapper(){
            accObj = new Account();
            camObj = new CAM__c();
            keyProfileList = new List<Key_Management_Profile__c>();
            balanceSheetList = new List<Balance_Sheet__c>();
            profitlossList = new List<Profit_and_Loss__c>();
            camDocList = new List<CAM_Documents__c>();
        }
        
    }
}