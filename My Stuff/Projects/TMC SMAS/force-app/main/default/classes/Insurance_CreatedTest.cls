@istest
public class Insurance_CreatedTest {
    public static testmethod void mytest (){
        
        
        Opportunity opp = new Opportunity();
        opp.Name='testopp';
        opp.CloseDate=system.today();
        opp.StageName='Closed Won';
        insert opp;
        
        Quotation__c qu = new Quotation__c();
        qu.Opportunity_Name__c=opp.id;
        //qu.Name='testQuote';
        qu.Insurance_Created__c=False;
        insert qu;
        
        
        Insurance__c  ins = new Insurance__c();
        ins.Quotation__c=qu.Id;
        insert ins;
        
        qu.Insurance_Created__c=True;
        update qu;
        update ins;
        
    }

}