@isTest
public class QuotationMonthlyTargetPopulateTest {
	@isTest
    static void test1(){
         Account acc = new Account();
        acc.Name = 'test';
        acc.BillingCity = 'Noida';
        acc.OwnerId=UserInfo.getUserId();
        insert acc;
		Target_Achieved__c trget=new Target_Achieved__c();
        trget.Achieved_Amount__c=6700;
        insert trget;
        
          
        Monthly_Target__c mt = new Monthly_Target__c();
        mt.Month__c = 'May';
        mt.Target_Achieved__c = trget.id;
        mt.User__c = UserInfo.getUserId();
        mt.Type__c='HRV New';
        insert mt;
        
        Quotation__c quote=new Quotation__c();
        quote.Account__c = acc.Id;
        quote.Client_Type__c='New';
        quote.RV_Type__c='HRV';
        quote.Profitability__c=40000;
        insert quote;

        
        Do_Line_Item__c doi = new Do_Line_Item__c();
        doi.Created_On__c = Date.valueOf('2019-5-2');
        doi.Name = 'Car';
        doi.Subcode__c = 'Car';
        doi.Quotation__c = quote.Id;
        insert doi;
        doi.DoCancel__c=true;
        update doi;
        
       
      
        
    }
}