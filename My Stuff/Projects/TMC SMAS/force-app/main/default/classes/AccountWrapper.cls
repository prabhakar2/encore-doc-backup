public class AccountWrapper{

	public Integer ClientID {get;set;} 
	public Integer ProspectID {get;set;} 
	public String AccountName {get;set;} 
	public String Type_Z {get;set;} // in json: Type
	public String Address1 {get;set;} 
	public String Address2 {get;set;} 
	public String Address3 {get;set;} 
	public String City {get;set;} 
	public String Pin {get;set;} 
	public String Industry {get;set;} 
	public String subIndustry {get;set;} 
	public String PrimaryContact {get;set;} 
	public String PrimaryContactPhone {get;set;}
    public String PrimaryContactEmail {get;set;}
	public String SecondaryContact {get;set;} 
	public String SecondaryContactPhone {get;set;} 
    public String SecondaryContactEmail {get;set;}
	public String Ocean {get;set;} 
	public String isJapanese {get;set;} 
	public String TurnOver {get;set;} 
	public String NoOfEmployees {get;set;}
    public String SFClientID{get;set;}
    public String OracleClientID{get;set;}
    public String Region {get;set;}
    public String Dormant {get;set;}
    public String BDM {get;set;}
    public String BDMEmail {get;set;}
    public String LimitApprovedAmount {get;set;}
    public String ToBeRecoveredAmount {get;set;}
    public String LimitBalance {get;set;}
    public String LimitRemarks {get;set;}
    public String LimitValidEndDate {get;set;}
    public List<BranchWrapper> officeAddress{get;set;}
    
	
    
    public class BranchWrapper{
        public String ClientID;
        public String OfficeID;
        public String ClientName;
        public String CityID;
        public String CityName;
        public String Address1;
        public String Address2;
        public String Address3;
        public String Phone;
        public String ContactPerson;
        public String StateName;
        
    }


}