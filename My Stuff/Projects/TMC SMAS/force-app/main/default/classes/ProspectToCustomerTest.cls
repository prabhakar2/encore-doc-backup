@istest
public class ProspectToCustomerTest {
    public static testmethod void mytest(){
        
        Account acc = new Account();
        acc.Name='Fanindra';
        acc.Type='Customer';
        insert acc;
        
         Opportunity opp = new Opportunity();
        opp.Name='testopp';
        opp.AccountId=acc.Id;
        opp.CloseDate=system.today();
        opp.StageName='Closed Won';
        opp.Profitability__c=2500;
        insert opp;
    }

}