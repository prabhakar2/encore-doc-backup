public class CAMSetupController {
    @auraEnabled
    public static String initialFetch(String accId){
        try{
            if(accId != null){
                List<Account> accList = [SELECT Id,Name,BillingAddress,ShippingAddress,Organization_Type__c,Industry,
                                         Years_in_Operation_in_India__c,Years_in_Operation_Globally__c,
                                         No_of_Employee_in_India__c,Locations_In_India__c,Primary_Contact__r.Name,
                                         Primary_Contact__c,BillingStreet,BillingCity,BillingState,BillingPostalCode,
                                         BillingCountry,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,
                                         ShippingCountry
                                         FROM Account
                                         WHERE Id =:accId LIMIT 1];
                if(accList.size() > 0)
                    return JSON.serialize(accList[0]);
            }
            
            return 'Error : Something wrong with this record.';
        }catch(Exception e){
            return 'Error : '+e.getMessage();
        }
    }
    
    @auraEnabled
    public static String getCurrentCam(String camId){
        try{
            String allFields = getAllFieldsForQuery('CAM__c');
            
            String qry = 'SELECT '+allFields+',Account__r.Name,(SELECT Id,Name,Designation__c,Profile__c FROM Key_Management_Profiles__r ORDER BY Name) FROM CAM__c WHERE Id =:camId LIMIT 1';
            List<CAM__c> camlist = Database.query(qry);
            if(camlist.size() > 0){
                
                String allSheetFields = getAllFieldsForQuery('Balance_Sheet__c');
                
                Integer FY = getFY();
                String currentFY = String.valueOf(FY);
                String lastFY = String.valueOf(FY-1);
                
                List<Balance_Sheet__c> sheetList = new List<Balance_Sheet__c>();
                List<Profit_and_Loss__c> profitLossList =  new List<Profit_and_Loss__c>();
                
                if(camlist[0].Balance_Sheet_CY__c != null && camlist[0].Balance_Sheet_PY__c != null){
                    String sheetIdCY = camlist[0].Balance_Sheet_CY__c;
                    String sheetIdPY = camlist[0].Balance_Sheet_PY__c;
                    String sheetQry = 'SELECT '+allSheetFields+' FROM Balance_Sheet__c WHERE Id =:sheetIdCY OR ID =:sheetIdPY ORDER BY Financial_Year__c DESC LIMIT 2';
                    sheetList =  Database.query(sheetQry);
                }
                
                if(camlist[0].Profit_Loss_CY__c != null && camlist[0].Profit_and_Loss_PY__c != null){
                    String allProfitLossFields = getAllFieldsForQuery('Profit_and_Loss__c');
                    String profitLossCY = camlist[0].Profit_Loss_CY__c;
                    String profitLossPY = camlist[0].Profit_and_Loss_PY__c;
                    String profitLossQry = 'SELECT '+allProfitLossFields+' FROM Profit_and_Loss__c WHERE Id =:profitLossCY OR Id =:profitLossPY ORDER BY Year_ended__c DESC LIMIT 2';
                    profitLossList =  Database.query(profitLossQry);
                }
                
                
                List<DocWrap> docListWrap = getDocList(camId);
                return json.serialize(camlist[0])+'@@'+(camlist[0].Key_Management_Profiles__r.size() > 0? json.serialize(camlist[0].Key_Management_Profiles__r):json.serialize(new List<Key_Management_Profile__c>()))+'@@'+(sheetList.size() > 0 ? json.serialize(sheetList[0]):json.serialize(new Balance_Sheet__c()))+'@@'+(sheetList.size() > 0 ? json.serialize(sheetList[1]):json.serialize(new Balance_Sheet__c()))+'@@'+(profitLossList.size() > 0 ? JSON.serialize(profitLossList[0]) : JSON.serialize(new Profit_and_Loss__c()))+'@@'+(profitLossList.size() > 0 ? JSON.serialize(profitLossList[1]) : JSON.serialize(new Profit_and_Loss__c()))+'@@'+ json.serialize(docListWrap);
            }
            else{
                return 'Error : Something went wrong with this record.';
            }
        }catch(Exception e){
            return 'Error '+e.getMessage();
       }
    }
    
    @auraEnabled
    public static String getDraftCam(String accId){
        try{
            Integer FY = getFY();
            String currentFY = String.valueOf(FY);
            String lastFY = String.valueOf(FY-1);
            
            String allFields = getAllFieldsForQuery('CAM__c');
            List<Account> accList = [SELECT Id,Name FROM Account WHERE Id =:accId LIMIT 1];
            String qry = 'SELECT '+allFields+',Account__r.Name,(SELECT Id,Name,Designation__c,Profile__c FROM Key_Management_Profiles__r ORDER BY Name) FROM CAM__c WHERE Account__c =:accId AND Stage__c !=\'CAM Approved\' ORDER BY CreatedDate DESC LIMIT 1 ';
            System.debug('Database.query(qry)>>>>>'+Database.query(qry));
            List<CAM__c> camlist = Database.query(qry);
            if(camlist.size() > 0){
                List<Balance_Sheet__c> sheetList = new List<Balance_Sheet__c>();
                List<Profit_and_Loss__c> profitLossList = new List<Profit_and_Loss__c>();
                if(camlist[0].Balance_Sheet_CY__c != null && camlist[0].Balance_Sheet_PY__c != null){
                    String allSheetFields = getAllFieldsForQuery('Balance_Sheet__c');
                    
                    System.debug('>>>>>>>>sheetIdCY'+camlist[0].Balance_Sheet_CY__c);
                    String sheetIdCY = camlist[0].Balance_Sheet_CY__c;
                    String sheetIdPY = camlist[0].Balance_Sheet_PY__c;
                    String sheetQry = 'SELECT '+allSheetFields+' FROM Balance_Sheet__c WHERE Id =:sheetIdCY OR ID =:sheetIdPY ORDER BY Financial_Year__c DESC LIMIT 2';
                    sheetList =  Database.query(sheetQry);
                }
                
                if(camlist[0].Profit_Loss_CY__c != null && camlist[0].Profit_and_Loss_PY__c != null){
                    String allProfitLossFields = getAllFieldsForQuery('Profit_and_Loss__c');
                    String profitLossCY = camlist[0].Profit_Loss_CY__c;
                    String profitLossPY = camlist[0].Profit_and_Loss_PY__c;
                    String profitLossQry = 'SELECT '+allProfitLossFields+' FROM Profit_and_Loss__c WHERE Id =:profitLossCY OR Id =:profitLossPY ORDER BY Year_ended__c DESC LIMIT 2';
                    profitLossList =  Database.query(profitLossQry);
                }
                
                
                List<DocWrap> docListWrap = getDocList(camlist[0].Id);
                return json.serialize(camlist[0])+'@@'+(camlist[0].Key_Management_Profiles__r.size() > 0? json.serialize(camlist[0].Key_Management_Profiles__r):json.serialize(new List<Key_Management_Profile__c>()))+'@@'+(sheetList.size() > 0 ? json.serialize(sheetList[0]):json.serialize(new Balance_Sheet__c()))+'@@'+(sheetList.size() > 0 ? json.serialize(sheetList[1]):json.serialize(new Balance_Sheet__c()))+'@@'+(profitLossList.size() > 0 ? JSON.serialize(profitLossList[0]) : JSON.serialize(new Profit_and_Loss__c()))+'@@'+(profitLossList.size() > 0 ? JSON.serialize(profitLossList[1]) : JSON.serialize(new Profit_and_Loss__c())) +'@@'+ json.serialize(docListWrap);
            }
            else{
                
                CAM__c cm = new CAM__c();
                //cm.RecordTypeId = Schema.SoapType
                cm.Account__c = accId;
                cm.Year__c = currentFY;
                cm.Saved_as_Draft__c = true;
                insert cm;
                
                List<DocWrap> docListWrap = getDocList(cm.Id);
                return json.serialize(cm)+'@@'+json.serialize(new List<Key_Management_Profile__c>())+'@@'+json.serialize(new Balance_Sheet__c())+'@@'+json.serialize(new Balance_Sheet__c())+'@@'+json.serialize(new Profit_and_Loss__c())+'@@'+json.serialize(new Profit_and_Loss__c())+'@@'+ json.serialize(docListWrap);
            }
        }catch(Exception e){
            return 'Error '+e.getMessage();
       }
    }
    
    @auraEnabled
    public static String commitToDB(String accStr,String camStr,String blanceSheetStr,String totalAssetStr,String totalLiabilityStr, String profitLossSheetStr,String keyManageStr){ //, 
        if(accStr != null && camStr != null){
            try{
                Account accObj = (Account)JSON.deserialize(accStr, Account.class);
                upsert accObj;
                CAM__c camObj = (CAM__c)JSON.deserialize(camStr, CAM__c.class);
                
                Map <String, Schema.SObjectField> camObjMap = Schema.getGlobalDescribe().get('CAM__c').getDescribe().fields.getMap();
                for(String field : System.Label.Cam_required_fields_Screen_1.split(',')){
                    string DataType = String.valueOf(camObjMap.get(field).getDescribe().getType());
                    System.debug('DataType>>>>>>>>'+DataType+' '+field+' '+camObj.get(field));
                    if(camObj.get(field) == null || String.valueOf(camObj.get(field)) == '')
                        return 'All Fields are required.';
                }
                upsert camObj;
                
                if(blanceSheetStr != null){
                    
                    if(totalAssetStr != null && totalLiabilityStr != null){
                        Decimal totalAssetCurrent = Decimal.valueOf(totalAssetStr.split('@@')[0]);
                        Decimal totalAssetLast = Decimal.valueOf(totalAssetStr.split('@@')[1]);
                        Decimal totalLibCurrent = Decimal.valueOf(totalLiabilityStr.split('@@')[0]);
                        Decimal totalLibLast = Decimal.valueOf(totalLiabilityStr.split('@@')[1]);
                        
                        if(totalAssetCurrent != totalLibCurrent || totalAssetLast != totalLibLast){
                            return 'Total Asset and Total Liability are not equal.';
                        }
                    }
                    Balance_Sheet__c sheet = (Balance_Sheet__c)JSON.deserialize(blanceSheetStr.split('@@')[0], Balance_Sheet__c.class);
                    sheet.Account__c = accObj.Id;
                    sheet.Name = accObj.Name+'('+camObj.Name+') FY-'+sheet.Financial_Year__c;
                    upsert sheet;
                    
                    Balance_Sheet__c sheetLast = (Balance_Sheet__c)JSON.deserialize(blanceSheetStr.split('@@')[1], Balance_Sheet__c.class);
                    sheetLast.Account__c = accObj.Id;
                    sheetLast.Name = accObj.Name+'('+camObj.Name+') FY-'+sheetLast.Financial_Year__c;
                    upsert sheetLast;
                    
                    camObj.Balance_Sheet_CY__c = sheet.Id;
                    camObj.Balance_Sheet_PY__c = sheetLast.Id;
                    update camObj;
                }
                if(profitLossSheetStr != null){
                    Profit_and_Loss__c profitLoss = (Profit_and_Loss__c)JSON.deserialize(profitLossSheetStr.split('@@')[0], Profit_and_Loss__c.class);
                    profitLoss.Account__c = accObj.Id;
                    profitLoss.Name = accObj.Name+'('+camObj.Name+') FY-'+profitLoss.Year_ended__c;
                    upsert profitLoss;
                    
                    Profit_and_Loss__c profitLossLast = (Profit_and_Loss__c)JSON.deserialize(profitLossSheetStr.split('@@')[1], Profit_and_Loss__c.class);
                    profitLossLast.Account__c = accObj.Id;
                    profitLossLast.Name = accObj.Name+'('+camObj.Name+') FY-'+profitLossLast.Year_ended__c;
                    upsert profitLossLast;
                    
                    camObj.Profit_Loss_CY__c = profitLoss.Id;
                    camObj.Profit_and_Loss_PY__c = profitLossLast.Id;
                    update camObj;
                }
               
                if(keyManageStr != null ){
                    List<Key_Management_Profile__c> keyManListToUpdate = new List<Key_Management_Profile__c>();
                    List<Key_Management_Profile__c> keyMangList = (List<Key_Management_Profile__c>)JSON.deserialize(keyManageStr, List<Key_Management_Profile__c>.class);
                    for(Key_Management_Profile__c keyMan : keyMangList){
                        if(keyMan.Id == null){
                            if(((keyMan.Designation__c != null && keyMan.Designation__c !='')   
                                || (keyMan.Profile__c !='' && keyMan.Profile__c != null )) 
                               && (keyMan.Name == null || keyMan.Name == ''))
                                
                                return 'Error : Name of Key Management Profile should not be blank.';
                            
                            else if(keyMan.Name != null && keyMan.Name != '')
                                keyManListToUpdate.add(keyMan);
                        }
                    }
                    if(keyManListToUpdate.size() > 0)
                        upsert keyManListToUpdate;
                }
               
                return 'Success';
           }catch(Exception e){
              return 'Error : '+e.getMessage();
            }
        }else
            return 'Error : Something wrong with this record';
    }
    
    private static Integer getFY(){
        Date currentDate = System.today();
        Integer FY ;
        if(currentDate.month() > 3)
            FY = currentDate.year();
        else
            FY = currentDate.year()-1;
        return FY;
    }
    
    private static String getAllFieldsForQuery(String objAPIName){
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objAPIName).getDescribe().fields.getMap();
        String allFields = '';
        
        for(String field : objectFields.keySet()){
            allFields+=field+',';
        }
        allFields = allFields.removeEnd(',');
        return allFields;
    }
    
    private static List<DocWrap> getDocList(String camId){
        List<DocWrap> docWrapList = new List<DocWrap>();
        Set<String> docMasIdSet = new Set<String>();
        List<String> accOrganisationTypeList=new List<String>();
        for(CAM__c camdata:[select Account__r.Organization_Type__c from CAM__c where id=:camId AND Account__c!=NULL AND Account__r.Organization_Type__c!=NULL]){
            accOrganisationTypeList.add(camdata.Account__r.Organization_Type__c);
        }
        System.debug('accOrganisationTypeList'+accOrganisationTypeList);
         String s='';
        if(accOrganisationTypeList.size()>0){
        s=accOrganisationTypeList[0];
        }
        List<CAM_Documents__c> docListToInsert = new List<CAM_Documents__c>();
        for(CAM_Documents__c doc : [SELECT Id,Name,Document__c,Status__c,Document__r.Name 
                                    FROM CAM_Documents__c 
                                    WHERE CAM__c =:camId]){
            docWrapList.add(new DocWrap(doc.Status__c == 'Submitted'?true:false,doc));
            docMasIdSet.add(doc.Document__c);
        }
        for(Document_Master__c doc : [SELECT Id,Name 
                                      FROM Document_Master__c
                                      WHERE RecordType.Name = 'CAM'
                                      AND Active__c= True
                                      AND Organization_Type__c  INCLUDES(:s)
                                      AND Id NOT IN :docMasIdSet]){
                                          
                                          CAM_Documents__c camDoc = new CAM_Documents__c();
                                          camDoc.CAM__c = camId;
                                          camDoc.Document__c = doc.Id;
                                          camDoc.Document__r = doc;
                                          camDoc.Status__c = 'Pending';
                                          docListToInsert.add(camDoc);
                                          docWrapList.add(new DocWrap(false,camDoc));
                                      }
        if(docListToInsert.size() > 0)
            insert docListToInsert;
       
        if(docWrapList.size() > 0)
            return docWrapList;
        else
            return null;
        
    }
    
    @auraEnabled
    public static String finalCommitToDB(String camStr,String docStr,String screen){
        try{
            CAM__c camObj = (CAM__c)json.deserialize(camStr, CAM__c.class);
            if(screen != 'Final'){
                for(String field : System.Label.Cam_required_fields_Screen_6.split(',')){
                   if(camObj.get(field) == null || String.valueOf(camObj.get(field)) == '')
                        return 'Please fill required fields';
                }
            }
            if(docStr != null && screen == 'Final'){
                List<CAM_Documents__c> camDocList = new List<CAM_Documents__c>();
                List<DocWrap> docWrapList = (List<DocWrap>)json.deserialize(docStr, List<DocWrap>.class);
                for(DocWrap wrp : docWrapList){
                    
                    wrp.doc.Status__c = wrp.isSelected ? 'Submitted' : 'Pending';
                    camDocList.add(wrp.doc);
                }
                if(camDocList.size() > 0){
                    upsert camDocList;
                    camObj.Saved_as_Draft__c = false;
                }
            }
            upsert camObj;
            return 'Success';
        }catch(Exception e){
            return 'Error : '+e.getMessage();
        }
    }
    
    @auraEnabled
    public static Key_Management_Profile__c addNewRow(String camId){
        return new Key_Management_Profile__c(CAM__c = camId);
    }
    
    @auraEnabled
    public static void updateNewDoc(String docId,String docTypeId){
        try{
            List<ContentDocumentLink> attListToDelete = new List<ContentDocumentLink>();
            for(ContentDocumentLink cd : [SELECT ContentDocumentId 
                                          FROM ContentDocumentLink 
                                          WHERE LinkedEntityId = :docTypeId
                                          AND  ContentDocumentId != :docId]){
                                              attListToDelete.add(cd);
                                          }
            if(attListToDelete.size() > 0)
                delete attListToDelete;
                
            ContentDistribution cd = new ContentDistribution();
            cd.contentVersionId  = [SELECT id FROM ContentVersion WHERE contentDocumentId = :docId LIMIT 1].Id;
            cd.PreferencesAllowViewInBrowser = True;
            cd.Name = [SELECT Id,Name,Document__r.Name FROM CAM_Documents__c WHERE Id =:docTypeId LIMIT 1].Document__r.Name;
            insert cd;
            
            string url = [SELECT DistributionPublicUrl 
                          FROM ContentDistribution 
                          WHERE id = :cd.Id].DistributionPublicUrl;
            
            CAM_Documents__c camDoc = new CAM_Documents__c(Id = docTypeId, CAM_Document_URL__c = url);
            update camDoc;
        }catch(Exception e){
            System.debug('Error>>>'+e.getMessage());
        }
    }
    
    @auraEnabled
    public static PickListWrap getPicklistValues(){
        PickListWrap wrap = new PickListWrap();
        Schema.DescribeFieldResult fieldResult =Account.Organization_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for(Schema.PicklistEntry f : ple){
            wrap.orgTypeList.add(f.getLabel());
        } 
        
        Schema.DescribeFieldResult industry =Account.Industry.getDescribe();
        List<Schema.PicklistEntry> ple1 = industry.getPicklistValues();
        
        for(Schema.PicklistEntry f : ple1){
            wrap.accIndustryList.add(f.getLabel());
        } 
        
        Schema.DescribeFieldResult entitle =CAM__c.Employee_car_entitlement_policy__c.getDescribe();
        List<Schema.PicklistEntry> ple2 = entitle.getPicklistValues();
        
        for(Schema.PicklistEntry f : ple2){
            wrap.carEntitlementList.add(f.getLabel());
        } 
        
        Schema.DescribeFieldResult reputation =CAM__c.With_Customers__c.getDescribe();
        List<Schema.PicklistEntry> ple3 = reputation.getPicklistValues();
        
        for(Schema.PicklistEntry f : ple3){
            wrap.reputationValues.add(f.getLabel());
        }
        
        Schema.DescribeFieldResult prodOffer = CAM__c.Product_Offered__c.getDescribe();
        List<Schema.PicklistEntry> ple4 = prodOffer.getPicklistValues();
        
        for(Schema.PicklistEntry f : ple4){
            wrap.prodOffered.add(f.getLabel());
        }
        
        Schema.DescribeFieldResult financialYear = Balance_Sheet__c.Financial_Year__c.getDescribe();
        List<Schema.PicklistEntry> yearVals = financialYear.getPicklistValues();
        
        for(Schema.PicklistEntry f : yearVals){
            wrap.FY.add(f.getLabel());
        }
        
        Schema.DescribeFieldResult multiConditions = CAM__c.Choose_Condition__c.getDescribe();
        List<Schema.PicklistEntry> conditionVal = multiConditions.getPicklistValues();
        
        for(Schema.PicklistEntry f : conditionVal){
            wrap.multipleConditions.add(f.getLabel());
        }
        return wrap;
    }
    
    public class PickListWrap{
        @auraEnabled public List<String> orgTypeList;
        @auraEnabled public List<String> accIndustryList;
        @auraEnabled public List<String> carEntitlementList;
        @auraEnabled public List<String> reputationValues;
        @auraEnabled public List<String> prodOffered;
        @auraEnabled public List<String> FY;
        @auraEnabled public List<String> multipleConditions;
        public PickListWrap(){
            orgTypeList = new List<String>();
            accIndustryList= new List<String>();
            carEntitlementList= new List<String>();
            reputationValues = new List<String>();
            prodOffered = new List<String>();
            FY = new List<String>();
            multipleConditions = new List<String>();
        }
    }
    
    public class DocWrap{
        @auraEnabled public boolean isSelected{get;set;}
        @auraEnabled public CAM_Documents__c doc{get;set;}
        public DocWrap(boolean isSelected,CAM_Documents__c doc){
            this.isSelected = isSelected;
            this.doc = doc;
        }
    }
    
    public static string getFY_inTrigger(){
        return string.valueOf(getFY());
    }
}