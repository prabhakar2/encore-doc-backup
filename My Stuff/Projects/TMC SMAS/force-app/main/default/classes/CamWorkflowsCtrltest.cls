@istest
public class CamWorkflowsCtrltest {
    
    public static testmethod void mytest  (){
        
        Account acc=new Account();
        acc.name='bhatt';
        acc.Industry='IT';
        insert acc;
        
        CAM__c cam=new CAM__c();
        cam.Number_of_years__c=2;
        cam.Location__c='India';
        cam.Account__c=acc.Id;
        
        insert cam;
        Task t = new Task(Subject='Donni',Status='Completed',Priority='Normal',CallType='Outbound',WhatId=cam.id,Start_Date__c=system.today(),Custom_Due_Date_Time__c=system.today());
        insert t;
        CamWorkflowsCtrl.getworkflowsTasks(cam.id);

    }
    
}