@isTest
public class WorkflowTaskPicklistUpdateTest {
	@isTest
    static void test1(){
        Quotation__c qt = new Quotation__c();
        qt.Name = 'test Quote';
        insert qt;
        
        Document_Master__c docMaster = new Document_Master__c();
        docMaster.RecordTypeId = Schema.SObjectType.Document_Master__c.getRecordTypeInfosByName().get('VRL').getRecordTypeId();
        docMaster.Active__c = true;
        docMaster.Required_Stage__c = 'To Process DO';
        insert docMaster;
        
        VRL_Documents__c vrl1 = new VRL_Documents__c();
        vrl1.Quotation__c = qt.Id;
        vrl1.Document__c =  docMaster.Id;
        insert vrl1;
        vrl1.Status__c='Submitted by Sales';
        update vrl1;
        vrl1.Status__c = 'Received by Fleet';
        update vrl1;
        
        VRL_Documents__c vrl2 = new VRL_Documents__c();
        vrl2.Quotation__c = qt.Id;
        vrl2.Document__c =  docMaster.Id;
        vrl2.Status__c = 'Received by CMT';
        insert vrl2;
        
        VRL_Documents__c vrl = new VRL_Documents__c();
        vrl.Quotation__c = qt.Id;
        vrl.Document__c =  docMaster.Id;
        insert vrl;
        vrl.Status__c = 'Submitted by Fleet';
        update vrl;
        vrl.Status__c='Rejected by CMT';
        update vrl;
        
        VRL_Documents__c vrl3 = new VRL_Documents__c();
        vrl3.Quotation__c = qt.Id;
        vrl3.Document__c =  docMaster.Id;
        insert vrl3;
        vrl3.Status__c = 'Pending from Sales';
        update vrl3;
        vrl3.Status__c='Rejected by Fleet';
        update vrl3;
    }
}