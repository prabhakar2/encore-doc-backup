@istest
public class CreateDeliveryOrdersInSFDCControllerTest {
    Public static Testmethod void mytest(){
        
        Account acc=new Account();
        acc.name='fanindra';
        acc.BillingCity = 'Noida';
        insert acc;
        
        Opportunity opp=new Opportunity();
        opp.Name='testopp';
        opp.AccountId=acc.Id;
        opp.StageName='Closed Won';
        opp.CloseDate=System.today()+1;
        // opp.CloseDate=Date.newInstance(2018,12,26);
        insert opp;
        
        Id rectype = Schema.SObjectType.Document_Master__c.getRecordTypeInfosByName().get('VRL').getRecordTypeId();
        Document_Master__c doc=new Document_Master__c();
        doc.name='document test';
        doc.Active__c=true;
        doc.RecordTypeId=rectype;
        insert doc;
        
        Quotation__c qu=new Quotation__c();
        qu.Opportunity_Name__c=opp.Id;
        qu.Status__c='Print Approved';
        qu.VRLDocumentsCreated__c=False;
        insert qu; 
        
        Delivery_Order__c dor = new Delivery_Order__c();
        dor.Quotation__c=qu.id;
        insert dor;
        
        CreateDeliveryOrdersInSFDCController.getDeliveryOrders();
        
    }
}