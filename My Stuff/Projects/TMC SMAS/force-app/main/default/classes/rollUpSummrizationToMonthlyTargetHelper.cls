/*
  Description : Helper class for the rollup trigger which sum profitable value and count the quotations value
  @uthor : Avaneesh Singh Salesforce Developer TechMatrix consulting PVT LTD
  Created date : 24/05/2019
*/


public class rollUpSummrizationToMonthlyTargetHelper{

    public static void runTrigger(List<Quotation__c> QuotationInfosList){
          if(QuotationInfosList != null && QuotationInfosList.size() >0 ){
            Map<Id,Monthly_Target__c> monthlyTargetInfoMap = new Map<Id,Monthly_Target__c>();
               for(Quotation__c quote : QuotationInfosList){
                if(quote.Monthly_Target__c != null){
                   if( !monthlyTargetInfoMap.containsKey(quote.Monthly_Target__c )){
                      monthlyTargetInfoMap.put(quote.Monthly_Target__c, new Monthly_Target__c (id=quote.Monthly_Target__c ,
                                                                                          Achieved_Amount__c=0,Achieved_Number__c=0));
                   }
                }
              }
          
              if(monthlyTargetInfoMap.size() >0){
              
                 for(AggregateResult ar : [select count(id) cnt ,sum(Profitability__c) profit, Monthly_Target__c from  Quotation__c where Monthly_Target__c In : monthlyTargetInfoMap.keySet()
                                             GROUP BY Monthly_Target__c]){
                        Id MonthlyTargetId = (Id)ar.get('Monthly_Target__c');
                        if(monthlyTargetInfoMap.containsKey(MonthlyTargetId)){
                         Monthly_Target__c mt = monthlyTargetInfoMap.get(MonthlyTargetId);
                         mt.Achieved_Amount__c = (Decimal)(ar.get('profit'));
                         mt.Achieved_Number__c = (Decimal)(ar.get('cnt'));
                         monthlyTargetInfoMap.put(MonthlyTargetId , mt);
                        }                     
                 }
              } 
              
              if(monthlyTargetInfoMap.size() >0){
               update monthlyTargetInfoMap.values();
              }
          
          }
    
    }

}