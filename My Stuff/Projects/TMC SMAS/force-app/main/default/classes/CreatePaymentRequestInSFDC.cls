@RestResource(urlMapping = '/CreatePaymentRequest/*')
global class CreatePaymentRequestInSFDC {
	@httpPost
    global Static String insertPaymentRequest(){
        RestRequest req = RestContext.request;
        
        try{
            List<PaymentWrap> wrapList = (List<PaymentWrap>)JSON.deserialize(req.requestBody.toString(),List<PaymentWrap>.class);
            System.debug('wrapList>>>>>'+wrapList);
            Map<String,String> quotMap = new Map<String,String>();
            Map<String,String> doLineItemMap = new Map<String,String>();
            Map<String,String> userEmailMap = new Map<String,String>();
            for(PaymentWrap wrp : wrapList){
                if(String.isNotBlank(wrp.QNO))
                    quotMap.put(String.valueOf(wrp.QNO) , null);
                if(String.isNotBlank(String.valueOf(wrp.Rowid)))
                    doLineItemMap.put(String.valueOf(wrp.Rowid) , null);
                if(String.isNotBlank(wrp.Email))
                    userEmailMap.put(wrp.Email, null);
            }
            for(DO_Line_Item__c doLineItem : [SELECT Id,Name,Amount__c,LeaseSoftId__c 
                                              FROM DO_Line_Item__c
                                              WHERE LeaseSoftId__c != null 
                                              AND LeaseSoftId__c IN :doLineItemMap.keySet()]){
                                                  if(doLineItemMap.containsKey(doLineItem.LeaseSoftId__c)){
                                                      doLineItemMap.put(doLineItem.LeaseSoftId__c,String.valueOf(doLineItem.Id));
                                                  }
                                              }
            for(Quotation__c qt : [SELECT Id,Name,LeaseSoftQuoteId__c
                                   FROM Quotation__c
                                   WHERE LeaseSoftQuoteId__c IN :quotMap.keySet()]){
                                       
                                       if(quotMap.containsKey(qt.LeaseSoftQuoteId__c))
                                           quotMap.put(qt.LeaseSoftQuoteId__c,qt.Id);
                                   }
            for(User usr : [SELECT Id,Name,Email FROM User
                            WHERE Email IN :userEmailMap.keySet() LIMIT 1]){
                                userEmailMap.put(usr.Email,usr.Id);
                            }
            System.debug('wrapList>>>>'+wrapList);
            List<Payment_Request__c> paymentReqList = new List<Payment_Request__c>();
            for(PaymentWrap wrp : wrapList){
                Payment_Request__c pay = new Payment_Request__c();
                pay.Amount__c = String.isNotBlank(wrp.amount) ? Decimal.valueOf(wrp.amount) : 0.0;
                pay.Description__c = String.isNotBlank(wrp.DESCRIPT) ? String.valueOf(wrp.DESCRIPT) : '';
                
                if(String.isNotBlank(String.valueOf(wrp.Rowid)))
                    pay.DO_Line_Item__c = doLineItemMap.get(String.valueOf(wrp.Rowid));
                if(String.isNotBlank(wrp.QNO))
                    pay.Quotation__c = quotMap.get(wrp.QNO);
                if(String.isNotBlank(wrp.Email))
                    pay.Initiated_by__c = userEmailMap.get(wrp.Email) ;
                pay.TAX__c =(String.isNotBlank(wrp.CgstAmount) ? Decimal.valueOf(wrp.CgstAmount) : 0.0) +(String.isNotBlank(wrp.IgstAmount) ? Decimal.valueOf(wrp.IgstAmount) : 0.0);
                pay.Type__c = String.isNotBlank(wrp.paymentType) ? wrp.paymentType :'';
                pay.Payment_Days__c = String.isNotBlank(wrp.PaymentDays) ? Decimal.valueOf(wrp.PaymentDays) : 0;
                paymentReqList.add(pay);
                
            }
            if(paymentReqList.size() > 0){
                upsert paymentReqList;
                SmasConstants.createLog(true,'','CreatePaymentRequestInSFDC','Payment',System.now(),req.requestBody.toString());
                return 'Success';
            }
            SmasConstants.createLog(false,'Insertion Failed' ,'CreatePaymentRequestInSFDC','Payment',System.now(),req.requestBody.toString());
            return 'Insertion Failed';
            
        }catch(Exception e){
            SmasConstants.createLog(false,e.getMessage()+' '+e.getLineNumber() ,'CreatePaymentRequestInSFDC','Payment',System.now(),req.requestBody.toString());
            return 'Insertion Failed';
        }
        
    } 
    
    public class PaymentWrap{
        public String Rowid 		{get;set;}
        public String TRNO 		{get;set;}
        public String amount		{get;set;}
        public String vendor		{get;set;}
        public String TRDATE 		{get;set;}
        public String TRCODE 		{get;set;}
        public String SUBCODE		{get;set;}
        public String QNO   		{get;set;}
        public String DESCRIPT		{get;set;}
        public String PaymentDays	{get;set;}
        public String IsApprovedDo	{get;set;}
        public String Created_On	{get;set;}
        public String DealerName	{get;set;}
        public String CgstAmount	{get;set;}
        public String IgstAmount	{get;set;}
        public String Email			{get;set;}
        public String paymentType   {get;set;}
    }
}