global class CreateQuoteScheduler implements Schedulable{
	global void execute(SchedulableContext ctx) {
        CreateQuoteInSFDC.fetchQuote();
        start();
    }
    
    public static void start(){
        DateTime currentTime = System.now().addMinutes(15);
        String cronExp = ' '+currentTime.second()+' '+currentTime.minute()+' '+currentTime.hour()+' '+currentTime.day()+' '+currentTime.month()+' ? '+currentTime.year();
        System.schedule('fetch Quote Record From LeaseSoft '+currentTime, cronExp, new CreateQuoteScheduler()); 
    }
}