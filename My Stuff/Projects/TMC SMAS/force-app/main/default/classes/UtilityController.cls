/*
       #ClassName : UtilityController
    #Created Date : 21/11/2018
         #Purpose : This class is used to provide some common methods.
    #Developed By : 
*/
public without sharing class UtilityController{
    
    /*  Method to convert date format.  */
    public static String convertDateFormat(String inputDate){
        String formattedDate = '';
        /*  Checking if the input is blank or not.  */
        if(String.isNotBlank(inputDate)){
            /*  Checking if the input contains the dash seprator.  */
            Boolean isDashseparator = inputDate.contains('-');
            
            /*  If dash seprator found.  */
            if(isDashseparator){
                /*  Spliting input into date and time with white space.  */
                List<String> dateAndTime = inputDate.split(' ');
                if(dateAndTime != null && dateAndTime.size()>0){
                    /*  Spliting date part with dash seprator.  */
                    List<String> dateValues = dateAndTime[0].split('-');
                    
                    /*  Creating new date in YYYY-MM-DD format.  */
                    if(dateValues != null && dateValues.size()>0){
                        formattedDate = dateValues[2]+'-'+dateValues[1]+'-'+dateValues[0];
                    }
                }
            }
        }
    
        return String.isNotBlank(formattedDate) ? formattedDate : '';
    }
}