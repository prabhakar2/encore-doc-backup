/*
#ClassName : CreateDeliveryOrdersInSFDCController
#Created Date : 16/11/2018
#Purpose : This class is used to make API callout to third party system to get the data of Delivery Orders and insert them into SFDC.
#Developed By : 
*/
public class CreateDeliveryOrdersInSFDCController{
    @future(Callout=true)
    public static void getDeliveryOrders(){
        boolean isSuccess = false;
        String errorMsg = '';
        String endPointURL = System.Label.DeliveryOrderCalloutURL; //+'?quoteNO=41146';
        Http httpObj = new Http();
        HttpRequest req = new HttpRequest();
        
        /*  Creating request here  */
        req.setEndpoint(endPointURL);
        req.setMethod('GET');
        req.setTimeout(60000);
        req.setHeader('Content-Type','application/json');
        String username = Label.API_Callout_Username;
        String password = Label.API_Callout_password;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        HttpResponse res = new HttpResponse();
        
        if(!Test.isRunningTest()){
            /*  Making the callout to third party system.  */
            res = httpObj.send(req);
        }else{
            String str = '[ { "QuoteNumber": "701642","DealerID":"3080","rowNumber":"1133", "Description": "sample string 2", "TransactionNo": "1234", "Price": "10000", "CGST": "5", "CGSTPercent": "6", "SGST": "7", "SGSTPercent": "8", "UGST": "9", "UGSTPercent": "10", "Cess": "1000", "CessPercent": "5", "Discount": "13", "Amount": "10000", "PaymentDays": "15", "CreatedOn": "2018-5-28", "isDOApproved": "True" ,"DoCancel":"True","DoCncelDate":"2018-5-28","LeaseType":"LeaseType"}]';
            res.setBody(str);
            res.setStatusCode(200);
        }
        /*  Checking if we got the response ot not.  */
        system.debug('getStatusCode'+res.getStatusCode()+res.getBody());
        if(res.getStatusCode() == 200 && String.isNotBlank(res.getBody()) && res.getBody() != '[]'){ //
            System.debug('Status Code>>>>>>>'+res.getStatusCode());
        System.debug('Status>>>>>>>'+res.getStatus());
            try{
                Map<String,String> accIdMap = new Map<String,String>();
                Map<String,String> quoteIdMap = new Map<String,String>();
                Map<String,Delivery_Order__c> quoteToDOMap = new Map<String,Delivery_Order__c>();
                Map<String,String> doLineItemMap = new Map<String,String>();
                List<DO_Line_Item__c> doLineItemList = new List<DO_Line_Item__c>();
                /*  Fetching body from response.  */
                String jsonStr = res.getBody();
                /*  deserializing json body into objects.  */
                List<DeliveryOrderWrapper> deliveryOrderList = (List<DeliveryOrderWrapper>)System.JSON.deserialize(jsonStr, List<DeliveryOrderWrapper>.class);
                
                /*  Collecting Client id and QuoteNumber to fetch the actual account and quote for record association.  */
                for(DeliveryOrderWrapper deliveryOrd : deliveryOrderList){
                    /*  Collecting QuoteIds for record association.  */
                    
                    if(String.isNotBlank(deliveryOrd.rowNumber)){
                        doLineItemMap.put(String.valueOf(deliveryOrd.rowNumber),null);
                    }
                    if(String.isNotBlank(deliveryOrd.QuoteNumber))
                        quoteIdMap.put(deliveryOrd.QuoteNumber,null);
                    
                    /*  Collecting DealerIds for record association.  */
                    if(String.isNotBlank(deliveryOrd.DealerID))
                        accIdMap.put(deliveryOrd.DealerID,null);
                    if(String.isNotBlank(deliveryOrd.DealerID) && String.isNotBlank(deliveryOrd.QuoteNumber))
                        quoteToDOMap.put(String.valueOf(deliveryOrd.DealerID)+'-'+String.valueOf(deliveryOrd.QuoteNumber),null);
                        
                }
                
                for(DO_Line_Item__c doLineItem : [SELECT Id,Name,Amount__c,LeaseSoftId__c 
                                                  FROM DO_Line_Item__c
                                                  WHERE LeaseSoftId__c IN :doLineItemMap.keySet()]){
                                                      if(doLineItemMap.containsKey(doLineItem.LeaseSoftId__c)){
                                                          doLineItemMap.put(doLineItem.LeaseSoftId__c,doLineItem.Id);
                                                      }
                                                  }
                
                /*  Collecting account ids based on the client id we have in response.  */ 
                if(accIdMap != null && accIdMap.keyset().size()>0){
                    for(Account acc : [SELECT Id, Dealer_ID__c 
                                       FROM Account 
                                       WHERE Dealer_ID__c IN : accIdMap.keySet()]){
                        if(accIdMap.containsKey(acc.Dealer_ID__c)){
                            accIdMap.put(acc.Dealer_ID__c,acc.Id);
                        }
                    }
                }
                System.debug('accIdMap>>>>>'+accIdMap);
                /*  Collecting quote ids based on the quotenumber in the response.  */
                if(quoteIdMap != null && quoteIdMap.keyset().size()>0){
                    for(Quotation__c quote : [SELECT Id, LeaseSoftQuoteId__c 
                                              FROM Quotation__c 
                                              WHERE LeaseSoftQuoteId__c IN : quoteIdMap.keySet()]){
                        if(quoteIdMap.containsKey(quote.LeaseSoftQuoteId__c)){
                            quoteIdMap.put(quote.LeaseSoftQuoteId__c,quote.Id);
                        }
                    }
                }
                
                for(Delivery_Order__c dOrder : [SELECT Id,LeaseSoft_Id__c,Dealer__c,Quotation__c 
                                                FROM Delivery_Order__c 
                                                WHERE LeaseSoft_Id__c IN :quoteToDOMap.keySet()]){
                    if(quoteToDOMap.containsKey(dOrder.LeaseSoft_Id__c))
                        quoteToDOMap.put(dOrder.LeaseSoft_Id__c,dOrder);
                }
                
                
                /*  Copying values from wrapper to actual object for insert.  */
                for(DeliveryOrderWrapper deliveryOrder : deliveryOrderList){
                    
                    Delivery_Order__c deliveryOrd = new Delivery_Order__c();
                    /*  Copying Delivery Order values in actual object(Delivery Orders).  */
                    if(quoteToDOMap.containsKey(String.valueOf(deliveryOrder.DealerID)+'-'+String.valueOf(deliveryOrder.QuoteNumber)) 
                       && quoteToDOMap.get(String.valueOf(deliveryOrder.DealerID)+'-'+ String.valueOf(deliveryOrder.QuoteNumber)) != NULL)
                        deliveryOrd = quoteToDOMap.get(String.valueOf(deliveryOrder.DealerID)+'-'+ String.valueOf(deliveryOrder.QuoteNumber));
                    else
                        deliveryOrd.LeaseSoft_Id__c = String.valueOf(deliveryOrder.DealerID)+'-'+ String.valueOf(deliveryOrder.QuoteNumber);
                    System.debug('deliveryOrd>>>>'+deliveryOrd);
                    if(deliveryOrder.DealerID != null && accIdMap.containsKey(deliveryOrder.DealerID)){
                        deliveryOrd.Dealer__c = accIdMap.get(deliveryOrder.DealerID);
                    }
                    if(quoteIdMap.containsKey(deliveryOrder.QuoteNumber)){
                        deliveryOrd.Quotation__c = quoteIdMap.get(deliveryOrder.QuoteNumber);
                    } 
                    
                    quoteToDOMap.put(String.valueOf(deliveryOrder.DealerID)+'-'+ String.valueOf(deliveryOrder.QuoteNumber), deliveryOrd);
                    
                    
                    
                    
                    /*  Copying Delivery Order values in actual object(Delivery Orders Line Items).  */
                    DO_Line_Item__c doLineItem = new DO_Line_Item__c();
                    doLineItem.Delivery_Order__r = deliveryOrd;
                    if(doLineItemMap.containsKey(String.valueOf(deliveryOrder.rowNumber)) && doLineItemMap.get(String.valueOf(deliveryOrder.rowNumber)) != null){
                        doLineItem.Id = doLineItemMap.get(String.valueOf(deliveryOrder.rowNumber)) ;
                        
                    }
                    if(quoteIdMap.containsKey(deliveryOrder.QuoteNumber)){
                        doLineItem.Quotation__c = String.valueOf(quoteIdMap.get(deliveryOrder.QuoteNumber));
                    }                        
                    doLineItem.LeaseSoftId__c = String.isNotBlank(deliveryOrder.rowNumber) ? String.valueOf(deliveryOrder.rowNumber) :'';
                    doLineItem.Name = deliveryOrder.Description;
                    doLineItem.Amount__c = String.isNotBlank(deliveryOrder.Amount) ? Decimal.valueOf(deliveryOrder.Amount) : 0;
                    doLineItem.Cess__c = String.isNotBlank(deliveryOrder.Cess) ? Decimal.valueOf(deliveryOrder.Cess) : 0;
                    doLineItem.Cess_Percent__c = String.isNotBlank(deliveryOrder.CessPercent) ? Decimal.valueOf(deliveryOrder.CessPercent) : 0;
                    doLineItem.CGST__c = String.isNotBlank(deliveryOrder.CGST) ? Decimal.valueOf(deliveryOrder.CGST) : 0;
                    doLineItem.CGST_Percent__c = String.isNotBlank(deliveryOrder.CGSTPercent) ? Decimal.valueOf(deliveryOrder.CGSTPercent) : 0;
                    doLineItem.Created_On__c = String.isNotBlank(deliveryOrder.CreatedOn) ? Date.valueOf(deliveryOrder.CreatedOn) : null;
                    doLineItem.Discount__c = String.isNotBlank(deliveryOrder.Discount) ? Decimal.valueOf(deliveryOrder.Discount) : 0;
                    doLineItem.Payment_Days__c = String.isNotBlank(deliveryOrder.PaymentDays) ? Decimal.valueOf(deliveryOrder.PaymentDays) : 0;
                    doLineItem.Price__c = String.isNotBlank(deliveryOrder.Price) ? Decimal.valueOf(deliveryOrder.Price) : 0;
                    doLineItem.SGST__c = String.isNotBlank(deliveryOrder.SGST) ? Decimal.valueOf(deliveryOrder.SGST) : 0;
                    doLineItem.SGST_Percent__c = String.isNotBlank(deliveryOrder.SGSTPercent) ? Decimal.valueOf(deliveryOrder.SGSTPercent) : 0;
                    doLineItem.UGST__c = String.isNotBlank(deliveryOrder.UGST) ? Decimal.valueOf(deliveryOrder.UGST) : 0;
                    doLineItem.UGST_Percent__c = String.isNotBlank(deliveryOrder.UGSTPercent) ? Decimal.valueOf(deliveryOrder.UGSTPercent) : 0;
                    doLineItem.Is_Approved__c = (String.isNotBlank(deliveryOrder.isDOApproved) && deliveryOrder.isDOApproved == 'True') ? true : false;
                    doLineItem.DoCancel__c= (String.isNotBlank(deliveryOrder.DoCancel) && deliveryOrder.DoCancel == 'True') ? true : false; 
                    doLineItem.DoCancelDate__c = String.isNotBlank(deliveryOrder.DoCancelDate) ? Date.valueOf(deliveryOrder.DoCancelDate) : null;
                    doLineItem.LeaseType__c= String.isNotBlank(deliveryOrder.LeaseType) ? deliveryOrder.LeaseType : null;
                    doLineItem.Transaction_Number__c = deliveryOrder.TransactionNo;
                    
                    /******************************Added later(3-12-2018)*********************************/
                    doLineItem.TR_Code__c =  deliveryOrder.trCode != null && String.isNotBlank(deliveryOrder.trCode) ?  deliveryOrder.trCode : '';   
                    doLineItem.Subcode__c = deliveryOrder.subCode != null && String.isNotBlank(deliveryOrder.subCode)? deliveryOrder.subCode : '';
                    /*****************************************************************************************/
                    if(accIdMap.containsKey(deliveryOrder.DealerID)){
                        doLineItem.Dealer__c = accIdMap.get(deliveryOrder.DealerID);
                    }
                    doLineItemList.add(doLineItem);
                }
                system.debug('#### DO list '+quoteToDOMap);
                if(quoteToDOMap != null && quoteToDOMap.size()>0){
                    upsert quoteToDOMap.values() LeaseSoft_Id__c;// Transaction_No__c; //;
                }
                system.debug('#### list '+doLineItemList);
                if(doLineItemList != null && doLineItemList.size()>0){
                    for(DO_Line_Item__c doLine : doLineItemList){
                        doLine.Delivery_Order__c = doLine.Delivery_Order__r.Id;
                    }
                    
                    upsert doLineItemList LeaseSoftId__c;// Transaction_Number__c;
                    isSuccess = true;
                }
            }
            catch(Exception e){
                isSuccess = false;
                errorMsg = e.getMessage()+' '+e.getLineNumber();
            }
            finally{
                SmasConstants.createLog(isSuccess,errorMsg,'CreateDeliveryOrdersInSFDCController','Delivery_Order__c and DO_Line_Item__c',System.now(),res.getBody());
            }
        }else
            SmasConstants.createLog(false,'Blank Body','CreateDeliveryOrdersInSFDCController','Delivery_Order__c and DO_Line_Item__c',System.now(),res.getBody()); 
    }
    
    public class DeliveryOrderWrapper{
        String rowNumber;
        String QuoteNumber;
        String Description;
        String Price;
        String CGST;
        String CGSTPercent;
        String SGST;
        String SGSTPercent;
        String UGST;
        String UGSTPercent;
        String Cess;
        String CessPercent;
        String Discount;
        String Amount;
        String PaymentDays;
        String CreatedOn;
        String DealerID;
        String isDOApproved;
        String TransactionNo;
        String trCode;
        String subCode;
        String DoCancelDate ;
        string DoCancel;
        string LeaseType;
        /*String QuoteNumber;
String Client;        
String Variant;
String PurchaseCity;
String RegistrationCity;
String TransactionNumber;
String TransactionDate;
String Dealer;
List<DeliveryOrderLineItemWrapper> doDetailsList;

String Price;
String Discount;*/
    }  
    
}