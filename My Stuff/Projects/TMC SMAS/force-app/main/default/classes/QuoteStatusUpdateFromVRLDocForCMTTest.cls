@istest
public class QuoteStatusUpdateFromVRLDocForCMTTest {
    Public Static testmethod void mytest(){
        Account acc=new Account();
        acc.name='fanindra';
        acc.BillingCity = 'Noida';
        insert acc;
        
        Opportunity opp=new Opportunity();
        opp.Name='testopp';
        opp.AccountId=acc.Id;
        opp.StageName='Closed Won';
        opp.CloseDate=system.today()+5;
        insert opp;
        
        Branch__c br = new Branch__c();
        br.Branch__c='Chennai';
        insert br;
        
        Id rectype = Schema.SObjectType.Document_Master__c.getRecordTypeInfosByName().get('VRL').getRecordTypeId();
        Document_Master__c doc=new Document_Master__c();
        doc.name='document test';
        doc.Active__c=true;
        doc.Required_Stage__c = 'To Process DO';
        doc.RecordTypeId=rectype;
        insert doc;
        
        Quotation__c qu=new Quotation__c();
        qu.Opportunity_Name__c=opp.Id;
        qu.Status__c='Print Approved';
        qu.VRLDocumentsCreated__c=False;
        qu.VRL_Document_Status__c = 'Submitted by Fleet';
         qu.Branch__c = br.Id;
        insert qu;    
        
        List<String> vrllist =new List<String>();
        VRL_Documents__c vrl= new VRL_Documents__c();
        vrl.Required_Stage__c='To Process DO';
        vrl.Document__c=doc.Id;
        vrl.Quotation__c=qu.Id;        
        insert vrl;
        vrllist.add(vrl.Id);
    }
}