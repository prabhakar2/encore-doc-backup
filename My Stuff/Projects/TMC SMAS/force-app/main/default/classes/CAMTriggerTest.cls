@isTest
public class CAMTriggerTest {
	@isTest
    static void test1(){
       Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        
     	User usr = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jafud7.liveston@asdf.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                           Designation__c='RM' 
                           );
        insert usr;
        
          	User usr2 = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jamjn.liveston@asdf.com',
                           Username = 'jbb67ud7.liveston@asdf.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                           Designation__c='TL-RM' 
                           );
        insert usr2;
        CAM__c cm = new CAM__c();
        cm.OwnerId=usr.id;
        
        insert cm;
        
          CAM__c cm2 = new CAM__c();
        cm2.OwnerId=usr2.id;
        
        insert cm2;
           
        
         	User usr3 = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Email = 'jamjn.liveston@asdf.com',
                           Username = 'jbbn7b7ud7.liveston@asdf.com',
                           ProfileId = profileId.id,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                           Designation__c='TL-RM', 
                           Manager=usr2                                
                           );
        insert usr3;
        
         CAM__c cm3 = new CAM__c();
        cm3.OwnerId=usr3.id;
        
        insert cm3;
        
        usr2.ManagerId = usr.Id;
        update usr2;
         CAM__c cm4 = new CAM__c();
        cm4.OwnerId=usr2.id;
        update cm3;
    }
}