public class AssetContractTriggerHelper {
    public static void updateDeliveryOnDLitem(){
        Map<id,List<DO_Line_Item__c>> dLItemToQuoteIdMap=new Map<id,List<DO_Line_Item__c>>(); 	//quoteid to dolineitem list
        List<DO_Line_Item__c> DLIlist=new List<DO_Line_Item__c>();								//update	
        for(Asset_Contract__c objAssContract:(List<Asset_Contract__c>)trigger.new){
            if(objAssContract.Quotation__c!=NULL)
                dLItemToQuoteIdMap.put(objAssContract.Quotation__c,new List<DO_Line_Item__c>());
        }
        System.debug('dLItemToQuoteIdMap--->'+dLItemToQuoteIdMap.keySet());
        for(DO_Line_Item__c objDLItem: [select ID,Quotation__c,Expected_Delivery_Date__c,Client_Delivery_Date__c,Actual_Delivery_Date__c from DO_Line_Item__c 
                                        WHERE Quotation__c IN : dLItemToQuoteIdMap.keySet()]){
                                            System.debug('objDLItem---->'+objDLItem.Expected_Delivery_Date__c);
                                            List<DO_Line_Item__c> tempList=new List<DO_Line_Item__c>();
                                            if(dLItemToQuoteIdMap.containsKey(objDLItem.Quotation__c)){
                                                tempList=dLItemToQuoteIdMap.get(objDLItem.Quotation__c);
                                                tempList.add(objDLItem);
                                                 dLItemToQuoteIdMap.put(objDLItem.Quotation__c,tempList);
                                            }
                                           
                                        }
        if(dLItemToQuoteIdMap.keySet().size()>0){
        for(Asset_Contract__c objAssContrct:(List<Asset_Contract__c>)trigger.new){
            if(dLItemToQuoteIdMap.containsKey(objAssContrct.Quotation__c) && dLItemToQuoteIdMap.get(objAssContrct.Quotation__c).size()>0 ){
                for(DO_Line_Item__c objDlItem:dLItemToQuoteIdMap.get(objAssContrct.Quotation__c)){
                    objDlItem.Expected_Delivery_Date__c= objAssContrct.Expected_Delivery_Date__c;
                    objDlItem.Client_Delivery_Date__c= objAssContrct.Client_Expected_Delivery_Date__c;
                    objDlItem.Actual_Delivery_Date__c=objAssContrct.Actual_Delivery_Date__c;
                    objDlItem.Amount__c=0;
                    DLIlist.add(objDlItem);
                }
            }
        }   
            if(DLIlist.size()>0)
           update DLIlist;
        }
    }
}