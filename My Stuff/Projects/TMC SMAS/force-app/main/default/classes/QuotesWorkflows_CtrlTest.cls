@istest
public class QuotesWorkflows_CtrlTest {
    Public static testmethod void mytest(){
        
        Account acc=new Account();
        acc.name='fanindra';
        acc.billingcity = 'Noida';
        insert acc;
        
        Opportunity opp=new Opportunity();
        opp.Name='testopp';
        opp.AccountId=acc.Id;
        opp.StageName='Closed Won';
        opp.CloseDate=system.today()+7;
        insert opp;
        
        Id rectype = Schema.SObjectType.Document_Master__c.getRecordTypeInfosByName().get('VRL').getRecordTypeId();
        Document_Master__c doc=new Document_Master__c();
        doc.name='document test';
        doc.Active__c=true;
        doc.RecordTypeId=rectype;
        insert doc;
        
        Quotation__c qu=new Quotation__c();
        qu.Opportunity_Name__c=opp.Id;
        qu.Status__c='Print Approved';
        qu.VRLDocumentsCreated__c=False;
        insert qu; 
        
        Task t = new Task();
       // t.OwnerId = UserInfo.getUserId();
        t.Subject='Donni';
        t.Status='Completed';
        t.Priority='Normal';
        //t.Responsavel__c='Felipe';
        t.Quotation__c=qu.Id;
        t.Start_Date__c = System.today();
        t.Custom_Due_Date_Time__c=system.now();
        insert t;    
        
        QuotesWorkflows_Ctrl.getworkflowsTasks(qu.id);
    }
}