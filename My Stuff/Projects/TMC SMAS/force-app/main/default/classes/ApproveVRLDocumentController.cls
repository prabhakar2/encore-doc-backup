/*
#ClassName : ApproveVRLDocumentController
#Created Date : 12/11/2018
#Purpose : This class shows the VRL document for mass approval at once.
#Developed By : 
*/
public without sharing class ApproveVRLDocumentController{
    
    
    @AuraEnabled
    public static String checkUserProfile(){
        return [SELECT Id,Name FROM Profile WHERE Id =:UserInfo.getProfileId() LIMIT 1].Name;
    }
    
    @AuraEnabled
    public static List<DocWrapper> getVRLDocumnets(String quoteId){
        List<DocWrapper> vrlDocList = new List<DocWrapper>();
        
        for(VRL_Documents__c doc : [SELECT Name, Document__c, Document__r.Name, Quote__c,Required_Stage__c,Quotation__c, 
                                    Required_Stagee__c, VRL_Document_URL__c,Document_Link__c,Status__c, Original_Submitted__c 
                                    FROM VRL_Documents__c WHERE Quotation__c =: quoteId]){
                                        
                                        vrlDocList.add(new DocWrapper(false,doc));
                                    }
        
        return vrlDocList.size() > 0 ? vrlDocList : new List<DocWrapper>();
    }
    
    @AuraEnabled
    public static String initApprovalProcessForRecords(List<String> recordsIds, String wrapperList){
        List<VRL_Documents__c> recordsToUpdate = new List<VRL_Documents__c>();
        Map<id,VRL_Documents__c> docMap = new Map<id,VRL_Documents__c>([SELECT Name, Document__c, Document__r.Name, Quote__c,Required_Stage__c,Quotation__c, 
                                                                        Required_Stagee__c, VRL_Document_URL__c,Document_Link__c,Status__c, Original_Submitted__c 
                                                                        FROM VRL_Documents__c WHERE Id IN:recordsIds]);
        
        //  Deserializing record json for update.
        List<DocWrapper> tempList = (List<DocWrapper>)JSON.deserialize(wrapperList, List<DocWrapper>.class);
        
        String message = '';
        Integer successCount, errorCount;
        successCount = errorCount = 0;
        //  Submitting records for approval.
        if(tempList != null && tempList.size()>0){
            for(DocWrapper wrapObj : tempList){
                if(wrapObj.doc.Original_Submitted__c && docMap.containsKey(wrapObj.doc.Id)){
                    VRL_Documents__c vrl=docMap.get(wrapObj.doc.Id);
                    vrl.Original_Submitted__c = true;
                    recordsToUpdate.add(vrl);
                    System.debug(wrapObj.doc.name+'wrapObj.doc>>>>>'+wrapObj.doc.Status__c);
                }
            }
            System.debug('recordsToUpdate>>>>'+recordsToUpdate);
            if(recordsToUpdate != null && recordsToUpdate.size()>0){
                update recordsToUpdate;
            }
        }
        
        if(recordsIds != null && recordsIds.size()>0){
            List<Approval.ProcessResult> result = new List<Approval.ProcessResult>();
            for(String recId : recordsIds){
                // Create an approval request.
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setComments('Please approve the vrl document.');
                req.setObjectId(recId);
                
                // Submit the approval request.
                result.add(Approval.process(req));
            }
            
            if(result != null && result.size()>0){
                for(Approval.ProcessResult res : result){
                    if(res.isSuccess()){
                        successCount++;
                    }else{
                        errorCount++;
                    }
                }
            }
        }
        //  Updating records so we can identify the records uniquely.
        
        message = successCount+' record(s) successfully submitted for approval and '+errorCount+' record(s) has error(s) and not submitted for approval.';
        return message;
    }
    
    @auraEnabled
    public static void updateNewDoc(String docId,String docTypeId){
        try{
            List<ContentDocumentLink> attListToDelete = new List<ContentDocumentLink>();
            for(ContentDocumentLink cd : [SELECT ContentDocumentId 
                                          FROM ContentDocumentLink 
                                          WHERE LinkedEntityId = :docTypeId
                                          AND  ContentDocumentId != :docId]){
                                              attListToDelete.add(cd);
                                          }
            if(attListToDelete.size() > 0)
                delete attListToDelete;
            
            
            ContentDistribution cd = new ContentDistribution();
            cd.contentVersionId  = [SELECT id FROM ContentVersion WHERE contentDocumentId = :docId LIMIT 1].Id;
            cd.PreferencesAllowViewInBrowser = True;
            cd.Name = [SELECT Id,Name,Document__r.Name FROM VRL_Documents__c WHERE Id =:docTypeId LIMIT 1].Document__r.Name;
            insert cd;
            
            string url = [SELECT DistributionPublicUrl 
                          FROM ContentDistribution 
                          WHERE id = :cd.Id].DistributionPublicUrl;
            
            VRL_Documents__c vrlDoc = new VRL_Documents__c(Id = docTypeId, VRL_Document_URL__c = url);
            update vrlDoc;
        }catch(Exception e){
            System.debug('Error>>>'+e.getMessage());
        }
    }
    
    public class DocWrapper{
        @auraEnabled public boolean isDocUpload{get;set;}
        @auraEnabled public VRL_Documents__c doc{get;set;}     
        
        public DocWrapper(boolean isDocUpload,VRL_Documents__c doc){
            this.isDocUpload = isDocUpload;
            this.doc = doc;
        }
    }
}