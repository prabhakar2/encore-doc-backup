@istest
public class UpdateAmountTest {
    Public static testmethod void mytest(){
        Account acc= new Account();
        acc.name='test';
        acc.Insurance_Amount_Consumed__c=500;
        acc.SubIndustry__c='Agent';
        acc.Ocean__c='Red';
        acc.No_Of_Employees__c='100';
        acc.Turn_Over__c='45687588';
        insert acc;
        
        Insurance__c ins = new Insurance__c();
        ins.Amount__c=50000; 
        ins.Insurance_Company_Account__c=acc.Id;
        insert ins;
        
        Account acc1= new Account();
        acc1.name='test1';
        acc1.Insurance_Amount_Consumed__c=5000;
        acc1.SubIndustry__c='Agent';
        acc1.Ocean__c='Blue';
        acc1.No_Of_Employees__c='100';
        acc1.Turn_Over__c='4568758';
        insert acc1;
        
        ins.Insurance_Company_Account__c=acc1.Id;
        update ins;
        
        Delete Ins;
    }
}