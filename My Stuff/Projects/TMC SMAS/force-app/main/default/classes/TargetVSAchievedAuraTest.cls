@isTest
public class TargetVSAchievedAuraTest {
    public static testMethod void test(){
        List<UserRole> r1 = [Select Id,Name FROM UserRole ];
        
        
        
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'Regional Manager North ',parentRoleid = r1[0].Id);
        insert r;
        
        
        
        User u=new User();
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        u.UserRoleId=r1[0].Id;
        u.Email = 'purna@gmail.com';
        u.Username = 'shaji2354@smas.com';
        u.LastName='Saxena';
        u.Alias='standt';
        u.TimeZoneSidKey='America/Los_Angeles';
        u.LocaleSidKey='en_US';
        u.EmailEncodingKey='UTF-8';
        u.LanguageLocaleKey= 'en_US';
        u.Function__c = 'Sales';
        insert u;
        
        
        
        User u2=new User();
        u2.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        u2.UserRoleId=r.Id;
        u2.Function__c = 'Sales';
        u2.Email = 'purna2@gmail.com';
        u2.Username = 'shaji23542@smas.com';
        u2.LastName='Saxena';
        u2.Alias='standt';
        u2.TimeZoneSidKey='America/Los_Angeles';
        u2.LocaleSidKey='en_US';
        u2.EmailEncodingKey='UTF-8';
        u2.LanguageLocaleKey= 'en_US';
        insert u2;
        
        
        System.runAs(u){
            
            
            Target_Achieved__c trgtAchieved=new Target_Achieved__c();
            trgtAchieved.Achieved_Amount__c=70000;
            insert trgtAchieved;
            
            
            Monthly_Target__c trget=new Monthly_Target__c();
            trget.User__c = u2.Id;
            trget.Target_Achieved__c=trgtAchieved.id;
            insert trget;
            
            TargetVSAchievedAura.fetchTargets('', '2019');
            TargetVSAchievedAura.getMonthlyTargets(JSON.serialize(trget),'2019');
            TargetVSAchievedAura.UpdateMonthlyTarget(JSON.serialize(trgtAchieved),JSON.serialize(new List<Monthly_Target__c>{trget}));
        }
    }
}