@istest
public class CreateAssetAndContractInSFDCContrTest {
    Public Static Testmethod void mytest(){
        
        
        Account acc=new Account();
        acc.name='bhatt';
        acc.Industry='Airlines';
        acc.Unique_Id__c='TMC123';
        acc.BillingCity = 'noida';
        insert acc;
        
        List<opportunity> opplist = new List<opportunity>();
        opportunity  TestOpp=new opportunity ();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=system.today()+1 ;
        TestOpp.AccountId=acc.Id;
        opplist.add(TestOpp);        
        insert opplist;
        
        CreateAssetAndContractInSFDCController.AssetContractWrapper  cacs= new CreateAssetAndContractInSFDCController.AssetContractWrapper();
        cacs.AssetNo='300379';
        cacs.QuoteNo='701719';
        cacs.QuoteType='Text';
        cacs.RegistrationNumber='457689';
        cacs.RegistrationDate='10 January';
        cacs.CarName='BMW';
        cacs.ContractStartDate='16 January';
        cacs.ContractEndDate='18 January';
        cacs.City='Delhi';
        cacs.Months='4';
        cacs.KMs='100';
        cacs.Client='Test';
        cacs.User='Fan';
        cacs.deldate='19 January';
        cacs.Exp_Del_Date='28 January';
        cacs.ClientExpDelDate='30 January';
        cacs.BranchCity='Delhi';
        
        CreateAssetAndContractInSFDCController reqst=new CreateAssetAndContractInSFDCController();
        String jsonStr=JSON.serialize(cacs);
        
        Leased_Asset__c la = new Leased_Asset__c();
        la.Account__c=acc.Id;
        la.Branch_City__c='Delhi';
        la.LeaseSoftId__c='TMC123';
        la.Asset_Number__c='300379';
        insert la;
        
        Quotation__c quot = new Quotation__c();
        quot.Account__c=acc.Id;
        quot.Lease_Rent__c=10000;
        quot.LeaseSoftQuoteId__c='701719';
        
        insert quot;
        
        Asset_Contract__c ac= new Asset_Contract__c();
        ac.Account__c=acc.id;
        ac.Leased_Asset__c=la.Id;
        ac.Quotation__c=quot.Id;
        
        insert ac;
        
        
        
        CreateAssetAndContractInSFDCController.getAssetAndContracts();
        
        
    }
}