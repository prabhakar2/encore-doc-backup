/*
#ClassName : CreateDealerAccountInSFDCController
#Created Date : 20/11/2018
#Purpose : This class is used to make API callout to third party system to get the data of Dealer account and insert them into SFDC.
#Developed By : 
*/
public class CreateDealerAccountInSFDCController{
    @future(Callout=true)
    public static void getDealerAccounts(){
        boolean isSuccess = false;
        String errorMsg = '';
        String endPointURL = System.Label.DealerAccountCalloutURL;
        Http httpObj = new Http();
        HttpRequest req = new HttpRequest();
        
        /*  Creating request here  */
        req.setEndpoint(endPointURL);
        req.setMethod('GET');
        req.setTimeout(60000);
        req.setHeader('Content-Type','application/json');
        String username = Label.API_Callout_Username;
        String password = Label.API_Callout_password;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        HttpResponse res = new HttpResponse();
        
        if(!Test.isRunningTest()){
            /*  Making the callout to third party system.  */
            res = httpObj.send(req);
        }else{
            String str = '[{"DealerID":3070,"ContactPerson":"Prabhakar Joshi","ContactEmail":"abc@test.com","ContactMobile":"9876543212" ,"DealerName":"100% ROCK RESTAURANT","AccountName":"ADAAB HOTELS LTD.","AccountType":"USER","Address1":"E-146, Saket,","Address2":"","Address3":"New Delhi-110017","City":"NEW DELHI","Pin":"110017","Status":"","GSTNO":"07AABCA0850G1Z5"}]';
            res.setBody(str);
            res.setStatusCode(200);

        }
        /*  Checking if we got the response ot not.  */
        if(res.getStatusCode() == 200 && String.isNotBlank(res.getBody()) && res.getBody() != '[]'){
            
            try{
                /*  Fetching account client record type for query.  */
                String recordTypeId = '';
                if(String.isNotBlank(Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId())){
                    recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
                }
                List<Account> accountToInsert = new List<Account>();
                
                /*  Fetching body from response.  */
                String jsonStr = res.getBody();
                /*  deserializing json body into objects.  */
                List<DealerAccountWrapper> dealerList = (List<DealerAccountWrapper>)System.JSON.deserialize(jsonStr, List<DealerAccountWrapper>.class);
                
                /*  Copying values from wrapper to actual object for insert.  */
                if(dealerList != null && dealerList.size()>0){
                    for(DealerAccountWrapper acc : dealerList){
                        Account accObj = new Account();
                        
                        accObj.RecordTypeId = recordTypeId;
                        accObj.Dealer_ID__c = acc.DealerID;
                        accObj.Name = acc.DealerName; 
                        accObj.Account_Name__c = acc.AccountName;
                        accObj.ShippingStreet = acc.Address1+' '+acc.Address2;
                        accObj.ShippingCity = acc.City; 
                        accObj.ShippingPostalCode = acc.Pin;
                        //accObj.Status__c = acc.Status;
                        accObj.Account_Type__c = acc.AccountType; 
                        accObj.GST_No__c = acc.GSTNO;
                        accObj.Contact_Person__c = acc.ContactPerson;
                        accObj.Contact_Email__c = acc.ContactEmail;
                        accObj.Contact_Mobile__c = acc.ContactMobile;
                        accountToInsert.add(accObj);
                    }
                }
                
                if(accountToInsert != null && accountToInsert.size()>0){
                    upsert accountToInsert Dealer_ID__c;
                    isSuccess = true;
                }
            }
            catch(Exception e){
                isSuccess = false;
                errorMsg = e.getMessage();
            }
            finally{
                SmasConstants.createLog(isSuccess,errorMsg,'CreateDealerAccountInSFDCController','Account(Dealer)',System.now(),res.getBody());
            }
        }else
            SmasConstants.createLog(false,'Blank Body','CreateDealerAccountInSFDCController','Account(Dealer)',System.now(),res.getBody()); 
        
    }// METHOD END
    
    /*  Wrapper class to deserialize the response from third party  */
    public class DealerAccountWrapper{
        public String DealerID;
        public String DealerName;
        public String AccountName;
        public String AccountType;
        public String Address1;
        public String Address2;
        public String Address3;
        public String City;
        public String Pin;
        public String Status;
        public String GSTNO;
        public String ContactPerson;
        public String ContactEmail;
        public String ContactMobile;
    }
}