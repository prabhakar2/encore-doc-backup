global class CreateProspectScheduler implements Schedulable{
    global void execute(SchedulableContext ctx) {
        
        CreateProspectInSFDC.fetchProspect();
        System.debug('Calling Schduler');
        start();
    }
    
    public static void start(){
        DateTime currentTime = System.now().addMinutes(10);
        String cronExp = ' '+currentTime.second()+' '+currentTime.minute()+' '+currentTime.hour()+' '+currentTime.day()+' '+currentTime.month()+' ? '+currentTime.year();
        System.schedule('fetch Prospect Record From LeaseSoft '+currentTime, cronExp, new CreateProspectScheduler()); 
    }
}