@isTest
public class PopulateCamOnAccountIfApprovedTest {
	@isTest
    static void test1(){
        Account acc = new Account();
        acc.BillingCity = 'Noida';
        acc.Name = 'test';
        insert acc; 
        
        Cam__c cm = new Cam__c();
        cm.Account__c = acc.Id;
        cm.Stage__c = 'CAM Approved';
        insert cm;
    }
}