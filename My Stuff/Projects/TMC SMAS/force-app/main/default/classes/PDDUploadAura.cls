public class PDDUploadAura {
	@auraEnabled
    public static List<PddWrapper> getPDD(String qtId){
        List<PddWrapper> wrapList = new List<PddWrapper>();
        for(PDD__c pdd : [SELECT id,name,Document_Master__r.Name,Document_Master__c,Status__c,Original_Submitted_chk__c,Document_URL__c
                          FROM PDD__c 
                          WHERE Quotation__c =:qtId])
                              wrapList.add(new PddWrapper(false,pdd,false));
                          
        if(wrapList.size() > 0)
            return wrapList;
        else
            return new List<PddWrapper>();
    }
    
    @auraEnabled
    public static void updateNewDoc(String docId,String docTypeId){
        try{
            List<ContentDocumentLink> attListToDelete = new List<ContentDocumentLink>();
            for(ContentDocumentLink cd : [SELECT ContentDocumentId 
                                          FROM ContentDocumentLink 
                                          WHERE LinkedEntityId = :docTypeId
                                          AND  ContentDocumentId != :docId]){
                                              attListToDelete.add(cd);
                                          }
            if(attListToDelete.size() > 0){
                delete attListToDelete;    
            }
            
            ContentDistribution cd = new ContentDistribution();
            cd.contentVersionId  = [SELECT id FROM ContentVersion WHERE contentDocumentId = :docId LIMIT 1].Id;
            cd.PreferencesAllowViewInBrowser = True;
            cd.Name = [SELECT Id,Name,Document_Master__r.Name FROM PDD__c WHERE Id =:docTypeId LIMIT 1].Document_Master__r.Name;
            insert cd;
            
            string url = [SELECT DistributionPublicUrl 
                          FROM ContentDistribution 
                          WHERE id = :cd.Id].DistributionPublicUrl;
            
            PDD__c pdd = new PDD__c(Id = docTypeId, document_Url__c = url);
            update pdd;
        }catch(Exception e){
            System.debug('Error>>>'+e.getMessage());
        }
    }
    
    @auraEnabled
    public static String approvalSubmit(List<String> selectedIds,String wrapList){
        System.debug('>>>>>>'+wrapList);
        List<Pdd__c> pddList = new List<Pdd__c>();
        List<PddWrapper> wrapperList = (List<PddWrapper>)JSON.deserialize(wrapList, List<PddWrapper>.class);
        
        for(PddWrapper wrp : wrapperList){
            if(wrp.pdd.Original_Submitted_chk__c)
                pddList.add(wrp.pdd);
        }
        if(pddList.size() > 0)
                update pddList;
        
        List<Approval.ProcessResult> result = new List<Approval.ProcessResult>();
        for(String recId : selectedIds){
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Please approve the PDD.');
            req.setObjectId(recId);
            result.add(Approval.process(req));
        }
        
        if(result != null && result.size()>0){
            
            for(Approval.ProcessResult res : result){
                if(res.isSuccess()){
                    return 'Success';
                }else{
                    return 'Error :'+res.getErrors();
                }
            }
        }
        return 'Error : Something went wrong in this process.';
        
    }
    
    public class PddWrapper{
        @auraEnabled public boolean isSelected{get;set;}
        @auraEnabled public PDD__c pdd{get;set;}
        @auraEnabled public boolean docUpload{get;set;}
        public PddWrapper(boolean isSelected,PDD__c pdd,boolean docUpload){
            this.isSelected = isSelected;
            this.pdd = pdd;
            this.docUpload = docUpload;
        }
    }
}