global class CreateDOScheduler implements Schedulable{
	global void execute(SchedulableContext ctx) {
        CreateDeliveryOrdersInSFDCController.getDeliveryOrders();
        System.debug('Calling Schduler');
        start();
    }
    
    public static void start(){
        DateTime currentTime = System.now().addMinutes(15);
        String cronExp = ' '+currentTime.second()+' '+currentTime.minute()+' '+currentTime.hour()+' '+currentTime.day()+' '+currentTime.month()+' ? '+currentTime.year();
        System.schedule('fetch DO Record From LeaseSoft '+currentTime, cronExp, new CreateDOScheduler()); 
    }
}