@istest
public class PDDUploadAuraTest {
    public static testmethod void mytest(){
        
        Opportunity opp = new Opportunity();
        opp.Name='testopp';
        opp.CloseDate=system.today();
        opp.StageName='Closed Won';
        opp.Profitability__c=2500;
        insert opp;        
        
        Quotation__c qu = new Quotation__c();
        qu.Opportunity_Name__c=opp.id;
        qu.PDD_Document_Status__c = 'Submitted by Fleet';
        //qu.Name='testQuote';
        //qu.Active__c=True;
        insert qu;
        
        PDD__c pdd = new PDD__c();
        pdd.Quotation__c=qu.id;
        pdd.Original_Submitted_chk__c = true;
        insert pdd;
        
        Delivery_Order__c deo = new Delivery_Order__c();
        insert deo;
        
        Document_Master__c dm = new Document_Master__c();
        dm.Name='testdoc';
        insert dm;
        
        DO_Document__c dodo = new DO_Document__c();
        dodo.Delivery_Order__c=deo.id;
        dodo.Document_Master__c=dm.Id;
        insert dodo;
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = dodo.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        List<Id> selectedIds= new List<id>{pdd.Id};
            PDDUploadAura.getPDD(qu.Id);
        PDDUploadAura.updateNewDoc(documents[0].id, dodo.id);
        PDDUploadAura.approvalSubmit(selectedIds,JSON.serialize(new List<Pdd__c>{pdd}));
        
    }
}