@istest
public class QuoteStatusUpdateFromPDDdocTest {
    
    Public static testmethod void mytest2(){
        Opportunity opp = new Opportunity();
        opp.Name='testopp';
        opp.CloseDate=system.today();
        opp.StageName='Closed Won';
        opp.Profitability__c=2500;
        insert opp;        
        
        Quotation__c qu = new Quotation__c();
        qu.Opportunity_Name__c=opp.id;
        
        insert qu;
        
        Document_Master__c dr=new Document_Master__c();
        dr.name='Gate Pass/ Delivery Receipt';
        insert dr;
        
        PDD__c pdd = new PDD__c();
        pdd.Quotation__c=qu.id;
        pdd.Status__c = 'Pending';
        pdd.Document_Master__c=dr.id;
        insert pdd;
        
        pdd.Status__c = 'Submitted By Fleet';
        update pdd;
        
        
    }
    
}