@RestResource(urlMapping = '/AccountDetail/*')
global class AccountDetailService {
    @httpGet
    global static List<Account> getAccountRecords(){
       
        List<API_Manager__c> apiMangList = [SELECT Id,Last_Callout_Date_Time__c
                                            FROM API_Manager__c
                                            WHERE Object__c = 'Account'
                                            ORDER BY CreatedDate DESC];
        if(apiMangList.size() > 0){
            String allFieldsForQuery = getAllFieldsForQuery('Account');
            DateTime callOutTime = apiMangList[0].Last_Callout_Date_Time__c;
            String primaryCont = 'Primary Contact';
            String secondryCont = 'Secondary Contact';
            String qry = 'SELECT '+allFieldsForQuery+','+Label.Additional_field_for_query_in_AccountDetailService+',(SELECT Name,MobilePhone,Primary_Contact__c,Email__c FROM Contacts WHERE Primary_Contact__c =:primaryCont OR Primary_Contact__c =:secondryCont LIMIT 2) FROM Account WHERE LastModifiedDate > :callOutTime  AND (recordtype.name = \'Client\' OR recordtype.name = \'Prospect\')' ;
            
            List<Account> accList = DataBase.query(qry);
            if(accList.size() > 0){
                API_Manager__c apiMang = new API_Manager__c();
                apiMang.Last_Callout_Date_Time__c = System.now();
                apiMang.Object__c = 'Account';
                insert apiMang;
                return accList;
            }else
                return new List<Account>();
        }else
            return new List<Account>();
        
    }
    
    private static String getAllFieldsForQuery(String objAPIName){
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objAPIName).getDescribe().fields.getMap();
        String allFields = '';
        
        for(String field : objectFields.keySet()){
            if(field != 'BillingAddress' && field != 'ShippingAddress' && field != 'CreatedById')
                allFields+=field+',';
        }
        allFields = allFields.removeEnd(',');
        return allFields;
    } 
}