public class QuotesWorkflows_Ctrl {
    
    @AuraEnabled
    Public Static List<QuoteWrapper> getworkflowsTasks(String quoteId){
        List<QuoteWrapper> WrapList = new List<QuoteWrapper>();
        List<Task> taskList = [SELECT Id, Subject, ActivityDate,ownerId,Status,Owner.Name, Start_Date__c,Custom_Due_Date_Time__c FROM Task where Quotation__c=:quoteId];
        if(taskList.size()>0){
            for(Task t : taskList){
                QuoteWrapper qw = new QuoteWrapper();
                qw.Ts=t;
                qw.Timer=null;
                if(t.Start_Date__c>=system.today()){
                    qw.Istime=true;
                }else{
                    qw.Istime=false;
                }
                if(t.Status=='Completed'){
                    qw.IsComplete=true;
                    Datetime sDate=t.Start_Date__c;
                    system.debug('sDate@@' +sDate);
                    if(t.Custom_Due_Date_Time__c != Null){
                    Datetime Duedate=t.Custom_Due_Date_Time__c;
                    system.debug('Duedate@@' +Duedate);                                       
                    Integer days = Date.valueOf(sDate).daysBetween(date.valueof(Duedate));
                    system.debug('days@@' +days);
                    Integer hours = math.mod(Integer.valueOf((Duedate.getTime() - sDate.getTime())/(1000*60*60)),24); 
                    system.debug('hours@@' +hours);
                    Integer minutes = math.mod(Integer.valueOf((Duedate.getTime() - sDate.getTime())/(1000*60)),60);    
                    system.debug('minutes@@' +minutes);
                    qw.Timer=days + ' days, ' + hours + ' hours, ' + minutes+ ' mins, ' + minutes + ' seconds';
                    }
                }
                else{
                    qw.IsComplete=false;
                    
                }
                WrapList.add(qw);
            }
            
            return WrapList;
        }
        return null;
        
    }
    
    Public class QuoteWrapper{
        @AuraEnabled Public Task Ts{get;set;}
        @AuraEnabled Public Boolean Istime{get;set;} 
        @AuraEnabled Public Boolean IsComplete{get;set;}
        @AuraEnabled Public String Timer{get;set;}               
    }
    
}