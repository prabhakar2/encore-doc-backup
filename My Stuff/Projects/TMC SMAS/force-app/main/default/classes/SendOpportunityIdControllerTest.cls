@istest
public class SendOpportunityIdControllerTest {
    Public static testmethod void mytest(){
        
        Account acc=new Account();
        acc.name='bhatt';
        acc.Industry='Airlines';
        acc.Unique_Id__c='TMC123';
        
        insert acc;
        
        
        System.debug('acc>>>>>'+acc.Salesforce_Account_ID__c);
        List<opportunity> opplist = new List<opportunity>();
        opportunity  TestOpp=new opportunity ();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=system.today()+1 ;
        TestOpp.AccountId=acc.Id;
        opplist.add(TestOpp);        
        insert opplist;
        SendOpportunityIdController.ClientWrapper sa = new SendOpportunityIdController.ClientWrapper();
        
        sa.ClientId=[SELECT Id,Salesforce_Account_Id__c FROM Account WHERE Id =:acc.Id].Salesforce_Account_Id__c ;
        String jsonStr=JSON.serialize(sa);
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/getOpportunityId/';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(jsonStr);
        
        RestContext.request = req;
        RestContext.response= res;
        
        SendOpportunityIdController.sendOpportunityId();
        Test.stopTest();
        
    }
    
    Public static testmethod void mytest2(){
        
        Account acc=new Account();
        acc.name='bhatt';
        acc.Industry='Airlines';
        acc.Unique_Id__c='TMC123';
        
        insert acc;
        
        
        System.debug('acc>>>>>'+acc.Salesforce_Account_ID__c);
        List<opportunity> opplist = new List<opportunity>();
        opportunity  TestOpp=new opportunity ();
        TestOpp.name='TestOpp1';
        TestOpp.StageName='Test';
        TestOpp.CloseDate=system.today()+1 ;
        TestOpp.AccountId=acc.Id;
        opplist.add(TestOpp);        
        insert opplist;
        SendOpportunityIdController.ClientWrapper sa = new SendOpportunityIdController.ClientWrapper();
        
        sa.ClientId='SF-0001';
        String jsonStr=JSON.serialize(sa);
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/getOpportunityId/';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(jsonStr);
        
        RestContext.request = req;
        RestContext.response= res;
        
        SendOpportunityIdController.sendOpportunityId();
        Test.stopTest();
        
    }
}