@isTest
public class CAMSetupControllerTest {
    public static testmethod void mytest(){
        
        Account acc= new Account();
        acc.name='test';
        acc.Insurance_Amount_Consumed__c=500;
        acc.SubIndustry__c='Agent';
        acc.Ocean__c='Red';
    	acc.No_Of_Employees__c='100';
        acc.Turn_Over__c='>=500 Crs';
        acc.BillingCity='Noida';
        insert acc;
        
        Balance_Sheet__c  bs= new Balance_Sheet__c();
        bs.name='sheettest';
        bs.Financial_Year__c='2018';
        insert bs;
        Balance_Sheet__c  bs1= new Balance_Sheet__c();
        bs1.name='sheettest';
        bs1.Financial_Year__c='2019';
        insert bs1;
        
        String frt = JSON.serialize(bs)+'@@'+JSON.serialize(bs1);
        
        Profit_and_Loss__c pl = new Profit_and_Loss__c();
        pl.Name='profitandlosstet';
        insert pl;
        Profit_and_Loss__c pl1 = new Profit_and_Loss__c();
        pl1.Name='profitandlosstet1';
        insert pl1;
        
        String prt = JSON.serialize(pl)+'@@'+JSON.serialize(pl);
        
        CAM__c cam=new CAM__c();
        cam.Number_of_years__c=2;
        cam.Location__c='India';
        cam.Account__c=acc.Id;
        cam.Balance_Sheet_CY__c=bs.Id;
        cam.Balance_Sheet_PY__c=bs.Id;
        cam.Profit_Loss_CY__c=pl.Id;
        cam.Profit_and_Loss_PY__c=pl.Id;
        cam.BDM__c = UserInfo.getUserId();
        cam.Location__c = 'Noida';
        cam.Product_Offered__c = 'Operating Lease';
        cam.RMTB__c = 'Yes';
        cam.ADW__c = 'Yes';
        cam.Uninterrupted_Mobility__c = 'Yes';
        cam.X24_Hours_Assistance__c = 'Yes';
        cam.Management_Fee__c = 1000;
        cam.Total_Loss_Theft_Coverage__c = 'yes';
        cam.Interest_Rate__c = 5;
        cam.Profitability__c = 10;
        cam.Annual_Fleet_Potential__c = 1000;
        cam.Term__c = 'test';
        cam.Special_Condition__c = 'testing';
        insert cam;
        
        Document_Master__c dm = new Document_Master__c();
        dm.Name='mastertest';
        dm.RecordTypeId=Schema.SObjectType.Document_Master__c.getRecordTypeInfosByName().get('CAM').getRecordTypeId();
        dm.Required_Stage__c='To Process CAM';
        insert dm;
        
        CAM_Documents__c camd = new CAM_Documents__c();
        camd.Status__c='Submitted';
        camd.CAM__c=cam.Id;
        camd.Document__c=dm.Id;
        insert camd;
        List<Key_Management_Profile__c>  kmplist = new List<Key_Management_Profile__c>();
        Key_Management_Profile__c kmp= new Key_Management_Profile__c();
        kmp.Name='kmptest';
        kmp.Designation__c='Devloper';
        kmp.CAM__c=cam.Id;
        kmplist.add(kmp);
        insert kmplist;
        
        //Content version-------------------------
        ContentVersion cv=new Contentversion();
        cv.title='ABC';
        cv.PathOnClient ='test';
        Blob b=Blob.valueOf('Unit Test Attachment Body');
        cv.versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body');
        insert cv;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = acc.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        
        CAMSetupController.initialFetch(acc.Id);
        CAMSetupController.getCurrentCam(cam.id);
        CAMSetupController.getDraftCam(acc.Id);
        CAMSetupController.updateNewDoc(cdl.ContentDocumentId,camd.id);
        CAMSetupController.addNewRow(cam.id);
        CAMSetupController.getPicklistValues();
        CAMSetupController.commitToDB(JSON.serialize(acc),JSON.serialize(cam), frt,'1800@@2000','1800@@2000',prt, JSON.serialize(kmplist));
        CAMSetupController.finalCommitToDB(JSON.serialize(cam), JSON.serialize(camd), 'Final');
    }
}