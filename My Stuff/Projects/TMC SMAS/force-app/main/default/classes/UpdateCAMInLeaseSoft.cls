public class UpdateCAMInLeaseSoft {
    
    @InvocableMethod
    public static void doCallOut(List<id> camIdList){
        Set<Id> camIdSet = new Set<Id>();
        camIdSet.addAll(camIdList);
        if(camIdSet.size() > 0){
            List<Cam__c> Camlist=[SELECT Id,Name,Account__c,CreatedDate,CreatedBy.name,Account__r.Unique_Id__c,Limit_Applied_For__c,Next_Review_Date__c 
                                  FROM Cam__c 
                                  WHERE Account__c != NULL 
                                  AND Id IN :camIdSet LIMIT 1];
            if(camList.size() > 0){
                List<Cam_Rating__c> camRatingList = [SELECT Id,Special_Condition__c
                                                    FROM Cam_Rating__c
                                                    WHERE Cam__c =:Camlist[0].Id
                                                    LIMIT 1];
                 
                String jsonBody = '{"SFCamID":"'+Camlist[0].Name+'" ,"CamID":"00"';
               
                
                if(Camlist[0].Account__r.Unique_Id__c != null)
                    jsonBody += ',"clientID":"'+Camlist[0].Account__r.Unique_Id__c+'"';
                
                if(Camlist[0].Limit_Applied_For__c != null)
                    jsonBody += ',"ApprovedAmount":"'+Camlist[0].Limit_Applied_For__c+'"';
                
                jsonBody += ',"ApprovedOn":"'+System.now()+'"';
                
                jsonBody += ',"ValidFrom":"'+System.now()+'"';
                
                if(Camlist[0].Next_Review_Date__c != null)
                    jsonBody += ',"ValidTo":"'+Camlist[0].Next_Review_Date__c+'"';
                
                if(camRatingList.size() > 0){
                    if(camRatingList[0].Special_Condition__c != null){
                        String remarksAsString = (camRatingList[0].Special_Condition__c).replaceAll('\\<.*?\\>', '');
                        jsonBody += ',"Remark":"'+remarksAsString+'"';
                    }
                }
                
                jsonBody+=',"CreatedDate":"'+Camlist[0].CreatedDate+'"';   
                jsonBody+=',"CreatedBy":"'+Camlist[0].CreatedBy.name+'"';
                
                jsonBody +='}';
                updateCamInLeaseSoft(jsonBody);
            }
        }
    }
    
    @future(Callout=true)
    private static void updateCamInLeaseSoft(String jsonBody){
        System.debug('jsonBody>>>>'+jsonBody);
        String endPointURL = label.CAM_API_URL;
        System.debug('endPointURL>>>>>'+endPointURL);
        Http httpObj = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        req.setEndpoint(endPointURL);
        req.setBody(jsonBody);
        req.setMethod('POST');
        req.setTimeout(60000);
        req.setHeader('Content-Type','application/json');
        String username = Label.API_Callout_Username;
        String password = Label.API_Callout_password;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        if(!test.isRunningTest())
            res = httpObj.send(req);
        else{
            res.setStatusCode(200);
            res.setStatus('OK');
        }
        
        System.debug('Status Code>>>>>>>'+res.getStatusCode());
        System.debug('Status>>>>>>>'+res.getStatus());
        if(res.getStatusCode() == 200 && res.getStatus() == 'OK'){
            
        }
    }
}