@istest
public class DODocumentUploadAuraTest {
    public static testmethod void mytest(){
        
        
        Delivery_Order__c deo = new Delivery_Order__c();
        insert deo;
            
        Document_Master__c dm = new Document_Master__c();
        dm.Name='testdoc';
        insert dm;
        
        DO_Document__c dodo = new DO_Document__c();
        dodo.Delivery_Order__c=deo.id;
        dodo.Document_Master__c=dm.Id;
        insert dodo;
         ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        //create ContentDocumentLink  record 

        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = dodo.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
       
        DODocumentUploadAura.getDoDocument(dodo.id);
        DODocumentUploadAura.updateNewDoc(documents[0].id, dodo.id);
    }
    
}