global class CreateAssetContractScheduler implements Schedulable{
	global void execute(SchedulableContext ctx) {
        CreateAssetAndContractInSFDCController.getAssetAndContracts();
        
        System.debug('Calling Schduler');
        start();
    }
    
    public static void start(){
        DateTime currentTime = System.now().addMinutes(30);
        String cronExp = ' '+currentTime.second()+' '+currentTime.minute()+' '+currentTime.hour()+' '+currentTime.day()+' '+currentTime.month()+' ? '+currentTime.year();
        System.schedule('fetch Asset and Contract Record From LeaseSoft '+currentTime, cronExp, new CreateAssetContractScheduler()); 
    }
}