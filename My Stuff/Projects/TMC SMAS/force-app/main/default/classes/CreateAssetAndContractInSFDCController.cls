/*
#ClassName : CreateAssetAndContractInSFDCController
#Created Date : 19/11/2018
#Purpose : This class is used to make API callout to third party system to get the data of asset and contract and insert them into SFDC.
#Developed By : 
*/
public class CreateAssetAndContractInSFDCController{
    
    @future(Callout=true)
    public static void getAssetAndContracts(){
        boolean isSuccess = false;
        String errorMsg = '';
        String endPointURL = System.Label.AssetContractCalloutURL;
        
        Http httpObj = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPointURL);
        req.setMethod('GET');
        req.setTimeout(60000);
        req.setHeader('Content-Type','application/json');
        
        String username = Label.API_Callout_Username;
        String password = Label.API_Callout_password;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        
        HttpResponse res = new HttpResponse();
        
        if(!Test.isRunningTest()){
            res = httpObj.send(req);
        }else{
            String str = '[{"AssetNo":2233,"UniqueID":"2233-205898","QuoteNo":205898,"QuoteType":"OL","RegistrationNumber":"DL1234","RegistrationDate":"27-01-2019",';
            str+= '"CarName":"Santro GLS","ContractStartDate":"09-11-2009 00:00:00","ContractEndDate":"09-11-2013 00:00:00","City":"Dehradun","Months":"50","KMs":"80000",';
            str+= '"Client":"G4s IT Services (India) Private Limited","User":"Mr Sanjay Singh","deldate":"09-11-2009 12:00:0 AM","Exp_Del_Date":"","ClientExpDelDate":"29-01-2019",';
            str+= '"IsDormant":"0","Active":"1","Sold":null,"Status":"","BranchCity":"NEW DELHI","EndUserID":3876,"firstName":"Testing1","lastName":"Last","Address1":';
            str+= '"Ascendas Mahindra IT Park,Mahendra World City SEZ","Address2":"Plot No. TP2/1, Natham Sub Post Office.",';
            str+= '"Address3":"3rd Floor, Chengalpet, Kancheepuram District,","Address4":"","Phone":"","Mobile":"9962020418","Email1":"","Email2":"chandra.bhose@rntbci.com"},';
            
            str+= '{"AssetNo":1234,"QuoteNo":205763,"UniqueID":"1234-205763","QuoteType":"OL","RegistrationNumber":"HR1234","RegistrationDate":"","CarName":"Santro GLS","ContractStartDate"';
            str+= ':"09-11-2009 00:00:00","ContractEndDate":"09-11-2014 00:00:00","City":"NEW DELHI","Months":"60","KMs":"45000","Client":"G4S CORPORATE SERVICES (INDIA) PVT LTD",';
            str+= '"User":"Mr Sanjay Singh","deldate":"09-11-2009 12:00:0 AM","Exp_Del_Date":"","ClientExpDelDate":"","IsDormant":"0","Active":null,"Sold":null,"Status":"",';
            str+= '"BranchCity":"NEW DELHI","EndUserID":3876,"firstName":"Prabhu","lastName":"last","Address1":"Ascendas Mahindra IT Park,Mahendra World City SEZ","Address2":';
            str+= '"Plot No. TP2/1, Natham Sub Post Office.","Address3":"3rd Floor, Chengalpet, Kancheepuram District,","Address4":"","Phone":"","Mobile":"9962020418","Email1":"",';
            str+= '"Email2":"chandra.bhose@rntbci.com"}]';
            res.setBody(str);
            res.setStatusCode(200);
        }
        System.debug('res.body>>>>>'+res.getBody());
        if(res.getStatusCode() == 200 && String.isNotBlank(res.getBody()) && res.getBody() != '[]'){
            SavePoint sp = Database.setSavepoint();
            try{
                
                String jsonStr = res.getBody();
                
                List<AssetContractWrapper> assetContractList = (List<AssetContractWrapper>)System.JSON.deserialize(jsonStr, List<AssetContractWrapper>.class);
                
                
                Map<string,Account> accMap = new Map<string,Account>();
                
                List<Asset_Contract__c> contractList = new List<Asset_Contract__c>();
                string recordTypeIdClient = Schema.SObjectType.Account.getRecordTypeInfosByName().get('End User').getRecordTypeId();
                
                Set<string> quoteStringSet = new Set<String>();
                for(AssetContractWrapper acw : assetContractList){
                    if(string.isNotBlank(acw.quoteNo))
                        quoteStringSet.add(acw.quoteNo);
                }
                
                Set<string> foundQuotes = new Set<String>();
                
                for(Quotation__c quote : [SELECT Id, LeaseSoftQuoteId__c, Account__c 
                                          FROM Quotation__c 
                                          WHERE LeaseSoftQuoteId__c IN :quoteStringSet]){
                                              foundQuotes.add(quote.LeaseSoftQuoteId__c);
                                          }
                
                
                for(AssetContractWrapper acw : assetContractList){
                    Account acc = new Account();
                    if(acw.EndUserID != NULL){
                        if(!accMap.containsKey(string.valueOf(acw.EndUserID))){
                            acc = new Account(
                                firstName = acw.firstName != null ? acw.firstName : '',
                                lastName = acw.lastName,
                                End_User_Id__c = string.valueOf(acw.EndUserID),
                                Phone = acw.Phone != null ? acw.Phone : '',
                                recordTypeId = recordTypeIdClient,
                                PersonMobilePhone = acw.Mobile != null ? acw.Mobile: '',
                                PersonEmail = acw.Email1 != null ? acw.Email1 : '',
                                Email2__c = acw.Email2 != null ? acw.Email2 : '',
                                PersonMailingStreet = acw.Address1 +' '+ acw.Address2+' '+acw.Address3+' '+acw.Address4
                            );
                            
                        }else{
                            acc = accMap.get(string.valueOf(acw.EndUserID));
                            acc.firstName = acw.firstName;
                            acc.lastName = acw.lastName;
                        }
                        
                        accMap.put(string.valueOf(acw.EndUserID),acc);
                    }
                    
                    
                    if(acw.UniqueID != NULL && acw.UniqueID.length()>0){
                        Asset_Contract__c assetCont = new Asset_Contract__c(
                            name = acw.AssetNo,
                            Active__c = String.valueOf(acw.Active) == '1' ? true : false,
                            Contract_Term_months__c = acw.Months != null ? Decimal.valueOf(acw.Months) : 0.0,
                            Contract_Start_Date__c = String.isNotBlank(acw.ContractStartDate) ? date.valueOf(UtilityController.convertDateFormat(acw.ContractStartDate)) : null,
                            KMs__c = acw.KMs != null ? Decimal.valueOf(acw.KMs) : 0.0,
                            Branch_City__c = acw.BranchCity != null ? acw.BranchCity: '',
                            leasesoft_Id__c = acw.UniqueID,
                            Registered_City__c = acw.City,
                            Status__c = acw.Status,
                            ExShowRoom_Price__c = (acw.ExShowRoom != null && String.isNotBlank(acw.ExShowRoom)) ?  Decimal.valueOf(acw.ExShowRoom):0.0,
                            Cancel__c = String.valueOf(acw.Cancel) == '1' ? true : false,
                            Registration_Number__c = acw.RegistrationNumber != null ? acw.RegistrationNumber : '',
                            Quote_Type__c = acw.QuoteType != null ? acw.QuoteType : '',
                            Price__c = acw.price != null ? Decimal.valueOf(acw.price) : 0.0,
                            registration_date__c = String.isNotBlank(acw.RegistrationDate)? date.valueOf(UtilityController.convertDateFormat(acw.RegistrationDate)):null,
                            Actual_Delivery_Date__c = String.isNotBlank(acw.deldate ) ? date.valueOf(UtilityController.convertDateFormat(acw.deldate)) :null,
                            Expected_Delivery_Date__c = String.isNotBlank(acw.Exp_Del_Date) ? date.valueOf(UtilityController.convertDateFormat(acw.Exp_Del_Date)) :null,
                            Client_Expected_Delivery_Date__c = String.isNotBlank(acw.ClientExpDelDate) ? date.valueOf(UtilityController.convertDateFormat(acw.ClientExpDelDate)) :null,
                            Terminate__c = acw.Terminate != null && acw.Terminate == 'TRUE' ? true : false,
                            FC__c = acw.FC != null && acw.FC == 'TRUE' ? true : false
                        );
                        if(string.isNotBlank(acw.quoteNo) && foundQuotes.contains(acw.quoteNo))
                            assetCont.Quotation__r = new Quotation__c(LeaseSoftQuoteId__c = acw.quoteNo);
                        
                        if(acc != NULL && acc.End_User_Id__c != NULL)
                            assetCont.End_User__r = new Account(End_User_Id__c = acc.End_User_Id__c);
                        contractList.add(assetCont);
                    }
                }
            
            
                if(accMap.values().size()>0)
                    upsert accMap.values() End_User_Id__c;
                if(contractList.size()>0)
                    upsert contractList leasesoft_Id__c; 
                
                isSuccess = true;
            }
            catch(Exception e){
                Database.rollback(sp);
                isSuccess = false;
                errorMsg = e.getMessage()+' '+e.getLineNumber();
            }
            finally{
                SmasConstants.createLog(isSuccess,errorMsg,'CreateAssetAndContractInSFDCController','Leased_Asset__c and Asset_Contract__c',System.now(),res.getBody());
            }
        }else
            SmasConstants.createLog(false,'Blank Body','CreateAssetAndContractInSFDCController','Leased_Asset__c and Asset_Contract__c',System.now(),res.getBody());
    }
    
    /****************  Wrapper class to deserialize the response ****************/
    
    public class AssetContractWrapper{
        public String AssetNo;
        public String UniqueID;
        public String QuoteNo;
        public String price;
        public String QuoteType;
        public String RegistrationNumber;
        public String RegistrationDate;
        public String CarName;
        public String ContractStartDate;
        public String ContractEndDate;
        public String City;
        public String Months;
        public String KMs;
        public String Client;
        public String User;
        public String deldate;
        public String Exp_Del_Date;
        public String ClientExpDelDate;
        public String BranchCity;
        public String Status;
        public String Active;
        public Integer EndUserID;
        public String firstName;
        public String lastName;
        public String Address1;
        public String Address2;
        public String Address3;
        public String Address4;
        public String Phone;
        public String Mobile;
        public String Email1;
        public String Email2;
        public String Cancel;
        public String Terminate;
        public String FC;
        public String ExShowRoom;
    }
    
    
}