global class CreateInsuranceScheduler implements Schedulable{
	global void execute(SchedulableContext ctx) {
        //CreateAccountInSFDC.fetchAccount();
        //CreateDealerAccountInSFDCController.getDealerAccounts();
        //CreateQuoteInSFDC.fetchQuote();
        //CreateDeliveryOrdersInSFDCController.getDeliveryOrders();
        //CreateAssetAndContractInSFDCController.getAssetAndContracts();
        CreateInsuranceInSFDCController.getInsurance();
        System.debug('Calling Schduler');
        start();
    }
    
    public static void start(){
        DateTime currentTime = System.now().addMinutes(15);
        String cronExp = ' '+currentTime.second()+' '+currentTime.minute()+' '+currentTime.hour()+' '+currentTime.day()+' '+currentTime.month()+' ? '+currentTime.year();
        System.schedule('fetch Insurance Record From LeaseSoft '+currentTime, cronExp, new CreateInsuranceScheduler()); 
    }
}