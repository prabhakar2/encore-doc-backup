@istest
public class InsuranceRequestComponentCtrlTest {
    Public static testmethod void mytest(){


        Opportunity opp = new Opportunity();
        opp.Name='testopp';
        opp.CloseDate=system.today();
        opp.StageName='Closed Won';
        insert opp;
        
        Quotation__c qu = new Quotation__c();
        qu.Opportunity_Name__c=opp.id;
        qu.Insurance_Created__c=False;
        qu.PDD_Document_Status__c = 'Submitted by Fleet';
        insert qu;
        
        PDD__c pd = new PDD__c();
        pd.Quotation__c=qu.id;
        pd.Status__c='Submitted By Fleet';
        insert pd;
        
        Insurance__c  ins = new Insurance__c();
        ins.Quotation__c=qu.Id;
        insert ins;
        
        Insurance_Request__c  ir = new Insurance_Request__c();
        ir.Engine_No__c = 'engine';
        ir.Chassis_No__c = 'chassis';
        ir.Description__c = 'description';
        ir.Quotation__c =qu.id;
        insert ir;

        
        Test.startTest();
        InsuranceRequestComponentCtrl.getInsuranceObject(qu.id);
        InsuranceRequestComponentCtrl.saveRecord('engine', 'chassis', 'description', qu.Id);
        Test.stopTest();
        
    }
}