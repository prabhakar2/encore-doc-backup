({
    doInit : function(component, event, helper) {
        helper.initailFetch(component, event, helper);
    },
    
    selectAll : function(component, event, helper) {
        var val = event.getSource().get("v.checked");
        var documentList = component.get("v.documentList");
        for(var i=0;i<documentList.length;i++){
            component.set("v.documentList["+i+"].isSelected",val);
        }
    },
    
    selectItem : function(component, event, helper) {
        var allSelect = true;
        var documentList = component.get("v.documentList");
        for(var i=0;i<documentList.length;i++){
            if(!documentList[i].isSelected){
                allSelect = false;
            }
        }
        
        if(allSelect)
            component.find("selectAll").set("v.checked",true);
        else
            component.find("selectAll").set("v.checked",false);
    },
    
    handleUploadFinished : function(component, event, helper) {
        var docId = event.getParam("files")[0].documentId;
        event.getSource().set("v.label",event.getParam("files")[0].name);
        event.getSource().set("v.title",true);
        var docTypeId = event.getSource().get("v.recordId");
        if(docId && docTypeId){
           
            helper.updateDoc(component, event,docId,docTypeId);
           // helper.initailFetch(component, event, helper);
            //$A.get('e.lightning:openFiles').fire({
                //recordIds: [event.getParam("files")[0].documentId]
            //});
            
        }
    },
    
    submitForApproval : function(component, event, helper) {
        var selectedIdList = [];
        var errorDocsName='';
        var documentList = component.get("v.documentList");
        for(var i=0;i<documentList.length;i++){
            if(documentList[i].isSelected){
                if(documentList[i].pdd.Status__c == 'Pending' && !documentList[i].pdd.Original_Submitted_chk__c 
                   && !documentList[i].docUpload){
                    errorDocsName += documentList[i].pdd.Document_Master__r.Name+',';
                    //helper.fireToast("Error","Error","Please select atleast one document.",1000);
                }
                selectedIdList.push(documentList[i].pdd.Id);
            }
        }
        
        if(errorDocsName.length > 0){
            helper.fireToast("Error","Error","Please Upload Document for "+errorDocsName+".",1000);
            return;
        }
        if(selectedIdList.length > 0){
            var action = component.get("c.approvalSubmit");
            action.setParams({
                "selectedIds" : selectedIdList,
                "wrapList" : JSON.stringify(component.get("v.documentList"))
            });
            action.setCallback(this,function(result){
                var res = result.getReturnValue();
                if(res && res == 'Success'){
                    helper.fireToast("Success","Success","Approval Submitted Successfully.",1000);
                    helper.initailFetch(component, event, helper);
                }else{
                    var errorMsg = 'Any of the selected Document is '+result.getError()[0].pageErrors[0].statusCode + '. ' +result.getError()[0].pageErrors[0].message;
                    helper.fireToast("Error","Error",errorMsg,1000);
                }
                    
            });
            $A.enqueueAction(action);
        }else{
            helper.fireToast("Error","Error","Please select atleast one document.",1000);
        }
        
        
    },
    
     navigateToDocLink : function(component, event, helper) {
        var urlVal = event.getSource().get("v.alternativeText");
        window.open(urlVal,'_blank');
    },
})