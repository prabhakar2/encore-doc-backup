({
    initailFetch : function(component, event,docId,docTypeId){
        var action = component.get("c.getPDD");
        action.setParams({
            "qtId":component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var res = response.getReturnValue();
            
            if (res) {
                component.set("v.documentList", res);
            }
        });
        $A.enqueueAction(action);
    },
	updateDoc : function(component, event,docId,docTypeId){
        var action = component.get("c.updateNewDoc");
        action.setParams({
            "docId" : docId,
            "docTypeId" : docTypeId
        });
        $A.enqueueAction(action);
        //this.initailFetch(component, event, helper);
    },
    
    fireToast : function(type,title,message,duration){
        var toast = $A.get("e.force:showToast").setParams({"type":type,"title":title,"message":message,"duration":duration}).fire();
    }
})