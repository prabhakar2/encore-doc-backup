({
    doInit : function(component, event, helper) {
       // console.log('do init');
        var sobjectId = component.get("v.recordId");
        component.set('v.objid',sobjectId);
        var action = component.get('c.getInsuranceObject');
        action.setParams({
            sobjectId : sobjectId
        });
        action.setCallback(this,function(response){
            if(response.getState() === "SUCCESS") {
                var resp = response.getReturnValue();
                if(resp == ''){
                   // console.log('No error Found');
                    component.set('v.flag',true);
                }else{
                   // console.log(resp);
                    component.set('v.errormsg',resp);
                }
            }
        });
        $A.enqueueAction(action);
    },
    doSave : function(component, event, helper){
        var engine = component.get('v.engine');
        var chassis = component.get('v.chassis');
        var description = component.get('v.description');
        var recordid = component.get("v.objid");
       // console.log('=========='+engine+'==='+chassis+'===='+description);
        var action = component.get('c.saveRecord');
        action.setParams({
            engine : engine,
            chassis : chassis,
            description : description,
            recordid : recordid
        });
        action.setCallback(this,function(response){
            if(response.getState() === "SUCCESS") {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Add Insurance Request",
                    "message": "Sucessfully Created"
                });
                resultsToast.fire();
                
                // Close the action panel
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
            }else{
               // console.log('fail');
            }
        });
        $A.enqueueAction(action);
    }
})