({
	doInit : function(component, event, helper) {
		var dt = new Date();
        var currentMonth = dt.getMonth()+1;
        var FY ;
        if(currentMonth > 3)
            FY = dt.getFullYear();
        else
            FY = dt.getFullYear()-1;
        component.set("v.currentFY",FY);
        
        /*************************************************************************
         * ***********************************************************************/
        
        var action = component.get("c.initialFetch");
        action.setParams({
            "accId" : component.get("v.simpleRecord.Account__c"),
            "camId" : component.get("v.recordId")
        });
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            //alert(JSON.stringify(JSON.parse(res).profitlossList));
            if(res && !res.includes("Error")){
                component.set("v.accObj",JSON.parse(res).accObj);
                component.set("v.cam",JSON.parse(res).camObj);
                component.set("v.keyMangProfile",JSON.parse(res).keyProfileList);
                component.set("v.balanceSheet",JSON.parse(res).balanceSheetList[0]);
                component.set("v.balanceSheetLast",JSON.parse(res).balanceSheetList[1]);
                component.set("v.profitLoss",JSON.parse(res).profitlossList[0]);
                component.set("v.profitLossLast",JSON.parse(res).profitlossList[1]);
                component.set("v.camDocList",JSON.parse(res).camDocList);
            }else{
                
            }
            component.set("v.toggleSpinner",false);
        });
        $A.enqueueAction(action);
        
	},
    
    navigateToDocLink : function(component, event, helper) {
        var urlVal = event.getSource().get("v.alternativeText");
        window.open(urlVal,'_blank');
    },
})