({
	doInit : function(component, event, helper) {
       $A.get("e.force:closeQuickAction").fire();
        window.setTimeout(function(){
            
            var action = component.get("c.populateRating");
            action.setParams({
                "camRatingId":component.get("v.recordId")
            });
            action.setCallback(this, function(response){
                var res = response.getReturnValue();
                if (res === 'Success') {
                    $A.get("e.force:refreshView").fire();
                    $A.get("e.force:closeQuickAction").fire();
                    helper.fireToast("Success","Success","Rating Updated Successfully.",1);
                }
                component.set("v.toggleSpinner",false);
            });
            
            $A.enqueueAction(action);},1000);
        
	}
})