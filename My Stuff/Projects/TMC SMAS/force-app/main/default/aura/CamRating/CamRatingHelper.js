({
	fireToast : function(type,title,message,duration) {
        $A.get("e.force:showToast").setParams({"type":type,"title":title,"message":message,"duration":duration}).fire();
	}
})