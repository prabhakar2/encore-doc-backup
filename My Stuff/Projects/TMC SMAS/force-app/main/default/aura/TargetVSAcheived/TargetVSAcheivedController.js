({
    doInit : function(component, event, helper) {
        var dt = new Date();
        var day = '0';
        var currentMonth = dt.getMonth()+1;
        var FY ;
        if(currentMonth > 3)
            FY = dt.getFullYear();
        else
            FY = dt.getFullYear()-1;
        if(dt.getDate() < 10){
            day+=dt.getDate();
        }else
            day = dt.getDate();
        
        var dtStr = dt.getFullYear()+'-'+currentMonth+'-'+day;
        
        component.set("v.currentFY",FY);
        
        component.set("v.currentUserId",$A.get("$SObjectType.CurrentUser.Id"));
        //alert(component.get("v.currentUserId"));
        helper.fetchData(component, event, helper);
    },
    
    openPopup : function(component, event, helper) {
        //component.set("v.toggleSpinner",true);
        var index = event.currentTarget.getAttribute('id');
        component.set("v.selectTargetIndex",index);
        var targetObj = component.get("v.targetList["+index+"]");
        component.set("v.selectedTarg",targetObj);
        
        
        component.set("v.openModal",true);
        helper.fetchMonthlyTarget(component, event, helper,null);
        
    },
    
    closeModal : function(component, event, helper) {
        component.set("v.openModal",false);
    },
    addFilter : function(component, event, helper) {
        component.set("v.toggleSpinner",true);
        helper.fetchData(component, event, helper);
        
        
        
    },
    
    saveMonthlyTarget : function(component, event, helper) {
        var selectedIndex = component.get("v.selectTargetIndex");
        var action = component.get("c.UpdateMonthlyTarget");
        var selectedYear = component.get("v.selectedYear");
        action.setParams({
            "selectedYear" : selectedYear,
            "targStr" : JSON.stringify(component.get("v.selectedTarg")),
            "targList":JSON.stringify(component.get("v.monthlyTargetList"))
        });
        
        action.setCallback(this, function(res){
            var result = res.getReturnValue();
            if(result  && !result.includes('Error')){
                
                //component.get("v.promise").then(function(modal){
                // modal.close();
                //});
                component.set("v.openModal",false);
                helper.fireToast("Success","Success","Target Updated Successfully for "+component.get("v.targetList["+selectedIndex+"].User__r.Name")+".",1000);
                helper.fetchData(component, event, helper);
            }else {
                component.set("v.errorMsg",'Amount is required with number');
            }
        });
        $A.enqueueAction(action);
    },
    
    yearSelection : function(component,event,helper){
        component.set("v.toggleSpinner",true);
        var selectedYear = event.getSource().get("v.value");
        helper.fetchData(component, event, helper);
    },
    
    calculateTotalAmt : function(component,event,helper){
        var monthTarList = component.get("v.monthlyTargetList");
        helper.calculatedValue(component, event, helper); 
    },
    
    handleRender : function(component,event,helper){
        if(component.get("v.errorMsg")){
            window.setTimeout(function(){
                component.set("v.errorMsg","");
            },3000);
        }
    },
    
    
    handleEvent: function(component,event,helper){
        var tar = event.getParams("targ");
        //('calling handle event');
        component.set("v.selectedTarg",tar);
        var index = event.getParam("operationIndex");
        component.set("v.selectTargetIndex",index);
        var monthTargetList = event.getParam("monthlyTarList");
        //alert(JSON.stringify(monthTargetList));
        component.set("v.monthlyTargetList",monthTargetList);  
    }
})