({
    fetchData : function(component, event, helper) {
        var action = component.get("c.fetchTargets");
        var selectedYear = component.get("v.selectedYear");
        //alert(component.get("v.selectedUser"));
        action.setParams({
            "Year" : selectedYear,
            "userId" : component.get("v.selectedUser")
        });
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            if(res && result.getState()==='SUCCESS'){
                component.set("v.targetList",JSON.parse(res).targList);
                if(component.get('v.targetList')){
                    var userIdStr='';
                    component.get('v.targetList').forEach(function(obj){
                        userIdStr+='\''+obj['User__c']+'\',';
                    });
                    
                    if(userIdStr.length>0){
                        userIdStr = userIdStr.substring(0,userIdStr.length-1);
                        component.set('v.usrFilterStr',userIdStr);
                    }
                }
                
                
                component.set("v.totalAmount",JSON.parse(res).totalAmount);
                component.set("v.totalNumber",JSON.parse(res).totalNumber);
            }else if(res.includes('Error')){
                component.set("v.errorMsg",res);
            }
            
            component.set("v.toggleSpinner",false);
        });
        $A.enqueueAction(action);
    },
    
    
    
    fetchMonthlyTarget : function(component, event, helper,selectedYear) {
        var action = component.get("c.getMonthlyTargets");
        var selectedYear = component.get("v.selectedYear");
        console.log('=======selectedYear===='+selectedYear);
        //alert(JSON.stringify(component.get("v.selectedTarg")));
        action.setParams({
            "targetStr" : JSON.stringify(component.get("v.selectedTarg")),
            "year" : selectedYear
        });
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            if(res){
                component.set("v.monthlyTargetList",res);
                helper.calculatedValue(component, event, helper);
                
            }
            component.set("v.toggleSpinner",false);
        });
        $A.enqueueAction(action);
    },
    
    calculatedValue : function(component, event, helper){
        var monthTarList = component.get("v.monthlyTargetList");
        
        var totalLRVAmt = 0;
        var totalHRVAmt = 0;
        var totalFLAmt = 0;
        var totalBusAmt = 0;
        var totalVal =  0;
        if(monthTarList.length >0){
            for(var i=0;i<monthTarList.length;i++){
                for(var j=0 ; j<monthTarList[i].monthlyTargetList.length ; j++){
                    console.log(monthTarList[i].monthlyTargetList[j].Type__c);
                        console.log(monthTarList[i].monthlyTargetList[j].Target_Amount__c);
                    if(monthTarList[i].monthlyTargetList[j].Type__c =='HRV Existing' || 
                       monthTarList[i].monthlyTargetList[j].Type__c =='HRV New'){
                        totalHRVAmt += parseInt(monthTarList[i].monthlyTargetList[j].Target_Amount__c);
                        console.log(monthTarList[i].monthlyTargetList[j].Type__c);
                        console.log(monthTarList[i].monthlyTargetList[j].Target_Amount__c);
                    }
                    if(monthTarList[i].monthlyTargetList[j].Type__c =='LRV Existing' || 
                       monthTarList[i].monthlyTargetList[j].Type__c =='LRV New'){
                        totalLRVAmt +=parseInt(monthTarList[i].monthlyTargetList[j].Target_Amount__c);
                    }
                    if(monthTarList[i].monthlyTargetList[j].Type__c =='FL Existing' || 
                       monthTarList[i].monthlyTargetList[j].Type__c =='FL New'){
                        totalFLAmt +=parseInt(monthTarList[i].monthlyTargetList[j].Target_Amount__c);
                    }
                    if(monthTarList[i].monthlyTargetList[j].Type__c =='FL Bus Existing' || 
                       monthTarList[i].monthlyTargetList[j].Type__c =='FL Bus New'){
                        totalBusAmt +=parseInt(monthTarList[i].monthlyTargetList[j].Target_Amount__c);
                    }
                }
            }
        }
        totalVal = totalLRVAmt + totalHRVAmt + totalFLAmt +totalBusAmt;
        component.set("v.totalLRVAmt",totalLRVAmt);
        component.set("v.totalHRVAmt",totalHRVAmt);
        component.set("v.totalFLAmt",totalFLAmt);
        component.set("v.totalBusAmt",totalBusAmt);
        component.set("v.selectedTarg.Total_Amount__c",totalVal);   
    },
    
    fireToast : function(type,title,message,duration) {
        var showToast = $A.get("e.force:showToast").setParams({"type":type,"title":title,"message":message,"duration":1000}).fire();
    }
})