({
    doInit : function(component, event, helper) {
        var action = component.get("c.checkUserProfile");
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            component.set("v.currentUserProfile",res);
        });
        $A.enqueueAction(action);
        
        helper.initiateData(component, event);
    },
    
    navigateToDocLink : function(component, event, helper) {
        var urlVal = event.getSource().get("v.alternativeText");
        window.open(urlVal,'_blank');
    },
    
    // For select all Checkboxes 
    selectAll: function(component, event, helper) {
        //get the header checkbox value  
        var selectedHeaderCheck = event.getSource().get("v.value");
        // get all checkbox on table with "boxPack" aura id (all iterate value have same Id)
        // return the List of all checkboxs element 
        var getAllId = component.find("boxPack");
        // If the local ID is unique[in single record case], find() returns the component. not array   
        if(! Array.isArray(getAllId)){
            if(selectedHeaderCheck == true){ 
                component.find("boxPack").set("v.value", true);
            }else{
                component.find("boxPack").set("v.value", false);
            }
        }else{
            // check if select all (header checkbox) is true then true all checkboxes on table in a for loop  
            // and set the all selected checkbox length in selectedCount attribute.
            // if value is false then make all checkboxes false in else part with play for loop 
            // and select count as 0 
            if (selectedHeaderCheck == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    if((component.get("v.documentList["+i+"].doc.Document__r.Name") != "Performa Invoice" 
                       && component.get("v.currentUserProfile") == "Sales") || (component.get("v.currentUserProfile") == "System Administrator"))
                        component.find("boxPack")[i].set("v.value", true);
                }
            } else {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("boxPack")[i].set("v.value", false);
                }
            } 
        }  
        
    },
    
    submitForApproval: function(component, event, helper){        
        // create var for store record id's for selected checkboxes  
        var approveIds = [];
        // get all checkboxes 
        var getAllId = component.find("boxPack");
        // If the local ID is unique[in single record case], find() returns the component. not array
        if(! Array.isArray(getAllId)){
            if (getAllId.get("v.value") == true) {
                approveIds.push(getAllId.get("v.text"));
            }
        }else{
            // play a for loop and check every checkbox values 
            // if value is checked(true) then add those Id (store in Text attribute on checkbox) in approveIds var.
            for (var i = 0; i < getAllId.length; i++){
                if (getAllId[i].get("v.value") == true){
                    approveIds.push(getAllId[i].get("v.text"));
                }
            }
        }
        if($A.util.isEmpty(approveIds)){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Warning",
                "message": "Please select at least one record for approval.",
                "type":"warning"
            });
            toastEvent.fire();           
        }else{
            
            //  Logic to handle physical document and scan copy document validation.
            var errorDocsName='';
            var documentList = component.get('v.documentList');
            for(var i=0;i<documentList.length;i++){
                if(approveIds.includes(documentList[i].doc.Id)){
                    if(!documentList[i].doc.Original_Submitted__c && (!documentList[i].isDocUpload) && (!documentList[i].doc.VRL_Document_URL__c )){
                        errorDocsName += documentList[i].doc.Document__r.Name+',';
                    }
                }
            }
            errorDocsName = errorDocsName.substr(0, errorDocsName.length-1);
            if(errorDocsName.length > 0){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Warning",
                    "message": "Please Upload Document for "+errorDocsName+".",
                    "type":"warning"
                });
                toastEvent.fire();
                return;
            }else{
            	helper.submitRecordsForApproval(component, event, approveIds, documentList);
            }
        }
    },
    
    redirectToDetail: function(component, event, helper){
        var docId = event.currentTarget.getAttribute("data-docid");
        var redirectToDetail = $A.get("e.force:navigateToSObject");
        redirectToDetail.setParams({
            "recordId" : docId
        });
        redirectToDetail.fire();
    },
    
    handleUploadFinished :  function(component, event, helper) {
        var docId = event.getParam("files")[0].documentId;
        event.getSource().set("v.label",event.getParam("files")[0].name);
        event.getSource().set("v.title",true);
        var docTypeId = event.getSource().get("v.recordId");
        if(docId && docTypeId){
            helper.updateDoc(component, event,docId,docTypeId);
            helper.initiateData(component, event);
            /*$A.get('e.lightning:openFiles').fire({
                recordIds: [event.getParam("files")[0].documentId]
            });*/
        }
    }
})