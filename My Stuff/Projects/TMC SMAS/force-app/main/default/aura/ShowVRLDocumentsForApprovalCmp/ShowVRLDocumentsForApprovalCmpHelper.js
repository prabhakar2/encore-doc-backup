({
    initiateData : function(component, event){
        var recordId = component.get("v.recordId");
        var action = component.get("c.getVRLDocumnets");
        action.setParams({
            "quoteId" : recordId 
        });
        action.setCallback(this, function(response){
        	var state = response.getState();
            //alert(response.getReturnValue());
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                
                component.set("v.documentList", result);
            }else{
                var errorMsg = '';
                var errorResult = response.getError();
                if(errorResult != undefined && errorResult.length>0){
                    for(var i=0;i<errorResult.length;i++){
                        errorMsg+= errorResult[i].message+"\n";
                    }
                }
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": errorMsg,
                    "type":"error"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    submitRecordsForApproval : function(component, event, selectedRecords, documentList) {
	    var action = component.get("c.initApprovalProcessForRecords");
        action.setParams({
            "recordsIds": selectedRecords,
            "wrapperList": JSON.stringify(documentList)
        });
        action.setCallback(this, function(response){
        	var state = response.getState();
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                //alert(result);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success",
                    "message": result,
                    "type":"success"
                });
                toastEvent.fire();
                this.initiateData(component, event);
            }else{
                var errorMsg = '';
                var errorResult = response.getError();
                if(!$A.util.isEmpty(errorResult)){
                    for(var i=0;i<errorResult.length;i++){
                        if(!$A.util.isEmpty(errorResult[i].pageErrors)){
                            errorMsg+= errorResult[i].pageErrors[i].message+"\n";
                        }
                    }
                }                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Warning!",
                    "message": errorMsg,
                    "type":"error"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    
    updateDoc : function(component, event,docId,docTypeId){
        var action = component.get("c.updateNewDoc");
        
        action.setParams({
            "docId" : docId,
            "docTypeId" : docTypeId
        });
        $A.enqueueAction(action);
    }
})