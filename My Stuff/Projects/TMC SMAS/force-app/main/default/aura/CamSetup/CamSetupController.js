({
    doInit : function(component, event, helper) {
        /******************for financial year population******************************
         * *************************************************************************/
        
        var dt = new Date();
        var day = '0';
        var currentMonth = dt.getMonth()+1;
        var FY ;
        if(currentMonth > 3)
            FY = dt.getFullYear();
        else
            FY = dt.getFullYear()-1;
        if(dt.getDate() < 10){
            day+=dt.getDate();
        }else
            day = dt.getDate();
        
         var dtStr = dt.getFullYear()+'-'+currentMonth+'-'+day;
        
        component.set("v.currentDate",dtStr);
        component.set("v.currentFY",FY);
        
        /**************************************************************************/
        
        /*var obj = window.location.href.split('lightning/')[1].split('/')[1];
        if(obj == 'CAM__c'){
            component.set("v.currentRecord","CAM");
            component.set("v.recordId",component.get("v.simpleRecord.Account__c"));
            if(component.get("v.simpleRecord.Id")){
                helper.fetchCurrentCAM(component, event,helper);
            }
                
        }else if(obj == 'Account'){
            component.set("v.currentRecord","Account");
            helper.fetchDraftCam(component, event,helper);
        }*/
        
        helper.fetchPicklistValues(component, event, helper);
        
    },
    
    handleRender : function(component,event,helper){
        if(component.get("v.errorMsg")){
            window.setTimeout(function(){
                component.set("v.errorMsg","");
            },3000);
        }
    },
    
    doSave : function(component, event, helper) {
        component.set("v.toggleSpinner",true);
        
        var action = component.get("c.commitToDB");
        if(parseInt(component.get("v.showScreen")) == 1){
            action.setParams({
                "accStr" : JSON.stringify(component.get("v.accObj")),
                "camStr" : JSON.stringify(component.get("v.cam")),
                "blanceSheetStr" : null,
                "profitLossSheetStr" : null,
                "keyManageStr" : null
            });
        } /*if(parseInt(component.get("v.showScreen")) == 2){
            
            action.setParams({
                "accStr" : JSON.stringify(component.get("v.accObj")),
                "camStr" : JSON.stringify(component.get("v.cam")),
                "blanceSheetStr" : null,
                "profitLossSheetStr" : null,
                "keyManageStr" : null
            });
        }*/else if(parseInt(component.get("v.showScreen")) == 2){
            
            action.setParams({
                "accStr" : JSON.stringify(component.get("v.accObj")),
                "camStr" : JSON.stringify(component.get("v.cam")),
                "blanceSheetStr" : null,
                "profitLossSheetStr" : null,
                "keyManageStr" : null
            });
        }
        else if(parseInt(component.get("v.showScreen")) == 3){
            //alert(component.get("v.totalAsset")+'@@'+component.get("v.totalAssetLast"));
            action.setParams({
                "accStr" : JSON.stringify(component.get("v.accObj")),
                "camStr" : JSON.stringify(component.get("v.cam")),
                "blanceSheetStr" : JSON.stringify(component.get("v.balanceSheet"))+'@@'+JSON.stringify(component.get("v.balanceSheetLast")),
                "totalAssetStr" : component.get("v.totalAsset")+'@@'+component.get("v.totalAssetLast"),
                "totalLiabilityStr" : component.get("v.totalEqulityLiabilities")+'@@'+component.get("v.totalEqulityLiabilitiesLast"),
                "profitLossSheetStr" : null,
                "keyManageStr" : null
            });
        }else if(parseInt(component.get("v.showScreen")) == 4){
            action.setParams({
                "accStr" : JSON.stringify(component.get("v.accObj")),
                "camStr" : JSON.stringify(component.get("v.cam")),
                "blanceSheetStr" : null,
                "profitLossSheetStr" : JSON.stringify(component.get("v.profitLoss"))+'@@'+JSON.stringify(component.get("v.profitLossLast")),
                "keyManageStr" : JSON.stringify(component.get("v.keyMangProfile"))
            });
        }else if(parseInt(component.get("v.showScreen")) == 5){
            action.setParams({
                "accStr" : JSON.stringify(component.get("v.accObj")),
                "camStr" : JSON.stringify(component.get("v.cam")),
                "blanceSheetStr" : null,
                "profitLossSheetStr" : null,
                "keyManageStr" : JSON.stringify(component.get("v.keyMangProfile"))
            });
        }
        
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            if(res && res.includes("Success")){
                var currentScreen = parseInt(component.get("v.showScreen"));
                component.set("v.showScreen",++currentScreen);
            }else{
                
                component.set("v.errorMsg",res);
            }
            component.set("v.toggleSpinner",false);
        });
        $A.enqueueAction(action);
    },
    
    goBack : function(component, event, helper) {
        var screenNo = parseInt(component.get("v.showScreen"));
        component.set("v.showScreen",--screenNo);
        component.set("v.toggleSpinner",false);
    },
    
    addRow : function(component, event, helper) {
        helper.addRowItem(component, event);
    },
    
    removeRow : function(component, event, helper) {
        var indx = event.getSource().get("v.alternativeText");
        var itemList = component.get("v.keyMangProfile");
        var arr = []; 
        Object.keys(itemList).forEach(function(i){
            if(indx != i)
                arr.push(itemList[i]);
        });
        component.set("v.keyMangProfile",arr);
    },
    
    doFinalSave : function(component, event, helper) {
        component.set("v.toggleSpinner",true);
        var screen = '';
        
        if(parseInt(component.get("v.showScreen")) == 6)
            screen = 'Not Final';
        else if(parseInt(component.get("v.showScreen")) == 7)
            screen = 'Final';
        
        var action = component.get("c.finalCommitToDB");
        if(parseInt(component.get("v.showScreen")) == 6){
            action.setParams({
                "camStr" : JSON.stringify(component.get("v.cam")),
                "docStr" : null,
                "screen" : screen
            });
        }else if(parseInt(component.get("v.showScreen")) == 7){
           action.setParams({
                "camStr" : JSON.stringify(component.get("v.cam")),
                "docStr" : JSON.stringify(component.get("v.CAMDocList")),
                "screen" : screen
            });
        }
        
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            if(res == 'Success'){
                if(parseInt(component.get("v.showScreen")) == 7){
                    helper.fireToast("Success","Success","CAM Updated Successfully.",1000);
                    $A.get("e.force:closeQuickAction").fire();
                }else if(parseInt(component.get("v.showScreen")) == 6){
                    var currentScreen = parseInt(component.get("v.showScreen"));
                    component.set("v.showScreen",++currentScreen);
                }
            }else
                component.set("v.errorMsg",res);
            
            component.set("v.toggleSpinner",false);
        });
        $A.enqueueAction(action);
    }, 
    
    handleUploadFinished :  function(component, event, helper) {
        var docId = event.getParam("files")[0].documentId;
        event.getSource().set("v.label",event.getParam("files")[0].name);
        var docTypeId = event.getSource().get("v.recordId");
        if(docId && docTypeId){
            helper.updateDoc(component, event,docId,docTypeId);
            /*$A.get('e.lightning:openFiles').fire({
                recordIds: [event.getParam("files")[0].documentId]
            });*/
        }
    },
    
    setMultiSelect: function (component, event, helper) {
        var selectedValues = event.getParam("value");
        var multiConditionStr= '' ;
        component.set("v.selectedConditions", selectedValues);
        component.get("v.selectedConditions").forEach(function(i){
            multiConditionStr += i+';';
        });
        
        component.set("v.cam.Choose_Condition__c",multiConditionStr);
    },
    
    calculateTotalNonCurrentAsset : function(component, event, helper) {
        helper.calculateTotalNonCurrentAsset(component, event, helper);
       
    },
    
    calculateTotalNonCurrentAssetLast : function(component, event, helper) {
        helper.calculateTotalNonCurrentAssetLast(component, event, helper);
    },
    
    calculateTotalCurrentAsset : function(component, event, helper) {
        helper.calculateTotalCurrentAsset(component, event, helper);
    },
    
    calculateTotalCurrentAssetLast : function(component, event, helper) {
        helper.calculateTotalCurrentAssetLast(component, event, helper);
    },
    
    calculateTotalEquity : function(component, event, helper) {
        helper.calculateTotalEquity(component, event, helper);
    },
    
    calculateTotalEquityLast : function(component, event, helper) {
        helper.calculateTotalEquityLast(component, event, helper);
    },
    
    calculateTotalNonCurrentLiability : function(component, event, helper) {
        helper.calculateTotalNonCurrentLiability(component, event, helper);
    },
    
    calculateTotalNonCurrentLiabilityLast : function(component, event, helper) {
        helper.calculateTotalNonCurrentLiabilityLast(component, event, helper);
    },
    
    calculateTotalCurrentLiability : function(component, event, helper) {
        helper.calculateTotalCurrentLiability(component, event, helper);
    },
    
    calculateTotalCurrentLiabilityLast : function(component, event, helper) {
        helper.calculateTotalCurrentLiabilityLast(component, event, helper);
    },
    
    calculateTotalIncome : function(component, event, helper) {
        helper.calculateTotalIncome(component, event, helper);
    },
    
    calculateTotalIncomeLast : function(component, event, helper) {
        helper.calculateTotalIncomeLast(component, event, helper);
    },
    
    calculateTotalExpenses : function(component, event, helper) {
        helper.calculateTotalExpenses(component, event, helper);
    },
    
    calculateTotalExpensesLast : function(component, event, helper) {
        helper.calculateTotalExpensesLast(component, event, helper);
    },
    
    calculateProfitAfterExcepItem : function(component, event, helper) {
        helper.calculateProfitAfterExcepItem(component, event, helper);
    },
    calculateProfitAfterExcepItemLast : function(component, event, helper) {
        helper.calculateProfitAfterExcepItemLast(component, event, helper);
    },
    calculateProfitBeforeTax : function(component, event, helper) {
        helper.calculateProfitBeforeTax(component, event, helper);
    },
    
    calculateProfitBeforeTaxLast : function(component, event, helper) {
        helper.calculateProfitBeforeTaxLast(component, event, helper);
    },
    
    calculateTotalTaxExpense : function(component, event, helper) {
        helper.calculateTotalTaxExpense(component, event, helper);
    },
    calculateTotalTaxExpenseLast : function(component, event, helper) {
        helper.calculateTotalTaxExpenseLast(component, event, helper);
    },
    changeScreen : function(component, event, helper) {
        var val = event.getSource().get("v.label").split(" ")[1];
        component.set("v.showScreen",parseInt(val));
        
    },
})