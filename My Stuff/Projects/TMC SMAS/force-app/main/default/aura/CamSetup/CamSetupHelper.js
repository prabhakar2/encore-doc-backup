({
	fireToast : function(type,title,message,duration) {
        var toast = $A.get("e.force:showToast").setParams({"type" : type,"title" : title,"message":message,"duration":duration}).fire();
	},
    
    fetchPicklistValues : function(component, event, helper) {
        var action = component.get("c.getPicklistValues");
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            if(res){
                component.set("v.orgType",res.orgTypeList);
                component.set("v.industry",res.accIndustryList);
                component.set("v.carEntitlement",res.carEntitlementList);
                component.set("v.reputationValues",res.reputationValues);
                component.set("v.prodOfferedValues",res.prodOffered);
                
                component.set("v.FY",res.FY);
                var plValues = [];
                for (var i = 0; i < res.multipleConditions.length; i++) {
                    plValues.push({
                        label: res.multipleConditions[i],
                        value: res.multipleConditions[i]
                    });
                }
                //alert("calling picklist");
                component.set("v.multipleConditionValues",plValues);
                this.getInitialData(component,event,helper);
            }
        });
        $A.enqueueAction(action);
    },
    
    getInitialData : function(component,event,helper){
        var obj = window.location.href.split('lightning/')[1].split('/')[1];
        if(obj == 'CAM__c'){
            component.set("v.currentRecord","CAM");
            component.set("v.recordId",component.get("v.simpleRecord.Account__c"));
            if(component.get("v.simpleRecord.Id")){
                //alert("calling");
                helper.fetchCurrentCAM(component, event,helper);
            }
        }else if(obj == 'Account'){
            component.set("v.currentRecord","Account");
            helper.fetchDraftCam(component, event,helper);
        } 
    },
    
    fetchCurrentCAM : function(component, event,helper){
        this.fetchAccDetails(component, event);
        var action = component.get("c.getCurrentCam");
        action.setParams({
            "camId" : component.get("v.simpleRecord.Id")
        });
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            if(res && !res.includes('Error')){
                component.set("v.cam",JSON.parse(res.split('@@')[0]));
                
                component.set("v.balanceSheet",JSON.parse(res.split('@@')[2]));
                component.set("v.balanceSheetLast",JSON.parse(res.split('@@')[3]));
                component.set("v.profitLoss",JSON.parse(res.split('@@')[4]));
                component.set("v.profitLossLast",JSON.parse(res.split('@@')[5]));
                component.set("v.CAMDocList",JSON.parse(res.split('@@')[6]));
                if(JSON.parse(res.split('@@')[1]).length > 0)
                    component.set("v.keyMangProfile",JSON.parse(res.split('@@')[1]));
                else
                    this.addRowItem(component, event);
                if(!component.get("v.cam.Sales_person_name__c"))
                    component.set("v.cam.Sales_person_name__c",$A.get("$SObjectType.CurrentUser.Id"));
                if(!component.get("v.cam.BDM__c"))
                    component.set("v.cam.BDM__c",$A.get("$SObjectType.CurrentUser.Id"));
                if(!component.get("v.balanceSheet.Id"))
                	component.set("v.balanceSheet.Financial_Year__c",component.get("v.currentFY"));
                if(!component.get("v.balanceSheetLast.Id"))
                	component.set("v.balanceSheetLast.Financial_Year__c",component.get("v.currentFY")-1);
                if(!component.get("v.profitLoss.Id"))
                	component.set("v.profitLoss.Year_ended__c",component.get("v.currentFY"));
                if(!component.get("v.profitLossLast.Id"))
                	component.set("v.profitLossLast.Year_ended__c",component.get("v.currentFY")-1);
                
                helper.calculateTotalNonCurrentAsset(component, event, helper);
                helper.calculateTotalNonCurrentAssetLast(component, event, helper);
                helper.calculateTotalCurrentAsset(component, event, helper);
                helper.calculateTotalCurrentAssetLast(component, event, helper);
                helper.calculateTotalEquity(component, event, helper);
                helper.calculateTotalEquityLast(component, event, helper);
                helper.calculateTotalNonCurrentLiability(component, event, helper);
                helper.calculateTotalNonCurrentLiabilityLast(component, event, helper);
                helper.calculateTotalCurrentLiability(component, event, helper);
                helper.calculateTotalCurrentLiabilityLast(component, event, helper);
                helper.calculateTotalIncome(component, event, helper);
                helper.calculateTotalIncomeLast(component, event, helper);
                helper.calculateTotalExpenses(component, event, helper);
                helper.calculateTotalExpensesLast(component, event, helper);
                helper.calculateTotalTaxExpense(component, event, helper);
                helper.calculateTotalTaxExpenseLast(component, event, helper);
            }else{
                component.set("v.errorMsg",res);
                component.set("v.toggleSpinner",false);
            }
        });
        $A.enqueueAction(action);
    },
    fetchDraftCam : function(component, event,helper){
        this.fetchAccDetails(component, event);
        var action = component.get("c.getDraftCam");
        action.setParams({
            "accId" : component.get("v.recordId")
        });
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            
            if(res && !res.includes('Error')){
                component.set("v.cam",JSON.parse(res.split('@@')[0]));
                component.set("v.balanceSheet",JSON.parse(res.split('@@')[2]));
                component.set("v.balanceSheetLast",JSON.parse(res.split('@@')[3]));
                component.set("v.profitLoss",JSON.parse(res.split('@@')[4]));
                component.set("v.profitLossLast",JSON.parse(res.split('@@')[5]));
                component.set("v.CAMDocList",JSON.parse(res.split('@@')[6]));
                if(JSON.parse(res.split('@@')[1]).length > 0){
                    component.set("v.keyMangProfile",JSON.parse(res.split('@@')[1]));
                }else
                    this.addRowItem(component, event);
                if(!component.get("v.cam.Sales_person_name__c"))
                    component.set("v.cam.Sales_person_name__c",$A.get("$SObjectType.CurrentUser.Id"));
                if(!component.get("v.cam.BDM__c"))
                    component.set("v.cam.BDM__c",$A.get("$SObjectType.CurrentUser.Id"));
                 if(!component.get("v.balanceSheet.Id"))
                	component.set("v.balanceSheet.Financial_Year__c",component.get("v.currentFY"));
                if(!component.get("v.balanceSheetLast.Id"))
                	component.set("v.balanceSheetLast.Financial_Year__c",component.get("v.currentFY")-1);
                if(!component.get("v.profitLoss.Id"))
                	component.set("v.profitLoss.Year_ended__c",component.get("v.currentFY"));
                if(!component.get("v.profitLossLast.Id"))
                	component.set("v.profitLossLast.Year_ended__c",component.get("v.currentFY")-1);
                
                helper.calculateTotalNonCurrentAsset(component, event, helper);
                helper.calculateTotalNonCurrentAssetLast(component, event, helper);
                helper.calculateTotalCurrentAsset(component, event, helper);
                helper.calculateTotalCurrentAssetLast(component, event, helper);
                helper.calculateTotalEquity(component, event, helper);
                helper.calculateTotalEquityLast(component, event, helper);
                helper.calculateTotalNonCurrentLiability(component, event, helper);
                helper.calculateTotalNonCurrentLiabilityLast(component, event, helper);
                helper.calculateTotalCurrentLiability(component, event, helper);
                helper.calculateTotalCurrentLiabilityLast(component, event, helper);
                helper.calculateTotalIncome(component, event, helper);
                helper.calculateTotalIncomeLast(component, event, helper);
                helper.calculateTotalExpenses(component, event, helper);
                helper.calculateTotalExpensesLast(component, event, helper);
                helper.calculateTotalTaxExpense(component, event, helper);
                helper.calculateTotalTaxExpenseLast(component, event, helper);
            }else{
                component.set("v.errorMsg",res);
                component.set("v.toggleSpinner",false);
            }
                
        });
        $A.enqueueAction(action);
       
    },
    
    
    
    fetchAccDetails : function(component, event){
        var action = component.get("c.initialFetch");
        //alert(component.get("v.recordId"));
        action.setParams({
            "accId" : component.get("v.recordId")
        });
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            if(res && !res.includes("Error")){
                component.set("v.accObj",JSON.parse(res));
                component.set("v.toggleSpinner",false);
            }else{
                this.fireToast("Error","Error",res,1000);
                $A.get("e.force:closeQuickAction").fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    addRowItem : function(component,event){
        var arr = component.get("v.keyMangProfile");
        var action = component.get("c.addNewRow");
        action.setParams({
            "camId" : component.get("v.cam.Id")
        });
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            if(res){
                arr.push(res);
            }
            component.set("v.keyMangProfile",arr);
        });
        $A.enqueueAction(action);
    },
    
    updateDoc : function(component, event,docId,docTypeId){
        var action = component.get("c.updateNewDoc");
        
        action.setParams({
            "docId" : docId,
            "docTypeId" : docTypeId
        });
        $A.enqueueAction(action);
    },
    
    calculateTotalNonCurrentAsset : function(component, event, helper) {
        var totalValue = (component.get("v.balanceSheet.Property_Plant_and_Equipment_PPE__c") ? parseFloat(component.get("v.balanceSheet.Property_Plant_and_Equipment_PPE__c")) : 0) +
            (component.get("v.balanceSheet.Intangible_assets__c") ? parseFloat(component.get("v.balanceSheet.Intangible_assets__c")):0 )+
            (component.get("v.balanceSheet.Investments_in_subsidiaries__c") ? parseFloat(component.get("v.balanceSheet.Investments_in_subsidiaries__c")):0)+
            (component.get("v.balanceSheet.Financial_Assets_Investments_NCA__c") ? parseFloat(component.get("v.balanceSheet.Financial_Assets_Investments_NCA__c")) : 0) +
            (component.get("v.balanceSheet.Financial_Assets_Loans__c")? parseFloat(component.get("v.balanceSheet.Financial_Assets_Loans__c")) : 0) +
            (component.get("v.balanceSheet.Other_Financial_Assets__c") ? parseFloat(component.get("v.balanceSheet.Other_Financial_Assets__c")) : 0)+
            (component.get("v.balanceSheet.Non_current_tax_assets__c") ? parseFloat(component.get("v.balanceSheet.Non_current_tax_assets__c")) : 0)+
            (component.get("v.balanceSheet.Other_non_current_assets__c") ? parseFloat(component.get("v.balanceSheet.Other_non_current_assets__c")) : 0);
        component.set("v.totalNonCurrentAsset",totalValue.toFixed(2));
        helper.calculateTotalAsset(component,event,helper);
       
    },
    
    calculateTotalNonCurrentAssetLast : function(component, event, helper) {
        var totalValue = (component.get("v.balanceSheetLast.Property_Plant_and_Equipment_PPE__c") ? parseFloat(component.get("v.balanceSheetLast.Property_Plant_and_Equipment_PPE__c")) : 0) +
            (component.get("v.balanceSheetLast.Intangible_assets__c") ? parseFloat(component.get("v.balanceSheetLast.Intangible_assets__c")):0 )+
            (component.get("v.balanceSheetLast.Investments_in_subsidiaries__c") ? parseFloat(component.get("v.balanceSheetLast.Investments_in_subsidiaries__c")):0)+
            (component.get("v.balanceSheetLast.Financial_Assets_Investments_NCA__c") ? parseFloat(component.get("v.balanceSheetLast.Financial_Assets_Investments_NCA__c")) : 0) +
            (component.get("v.balanceSheetLast.Financial_Assets_Loans__c")? parseFloat(component.get("v.balanceSheetLast.Financial_Assets_Loans__c")) : 0) +
            (component.get("v.balanceSheetLast.Other_Financial_Assets__c") ? parseFloat(component.get("v.balanceSheetLast.Other_Financial_Assets__c")) : 0)+
            (component.get("v.balanceSheetLast.Non_current_tax_assets__c") ? parseFloat(component.get("v.balanceSheetLast.Non_current_tax_assets__c")) : 0)+
            (component.get("v.balanceSheetLast.Other_non_current_assets__c") ? parseFloat(component.get("v.balanceSheetLast.Other_non_current_assets__c")) : 0);
        component.set("v.totalNonCurrentAssetLast",totalValue.toFixed(2));
        helper.calculateTotalAssetLast(component,event,helper);
    },
    
    calculateTotalCurrentAsset : function(component, event, helper) {
        var totalValue = (component.get("v.balanceSheet.Inventories__c") ? parseFloat(component.get("v.balanceSheet.Inventories__c")) : 0) +
            (component.get("v.balanceSheet.Financial_Assets_Investments_CA__c") ? parseFloat(component.get("v.balanceSheet.Financial_Assets_Investments_CA__c")):0 )+
            (component.get("v.balanceSheet.Financial_Assets_Trade_Receivables__c") ? parseFloat(component.get("v.balanceSheet.Financial_Assets_Trade_Receivables__c")):0)+
            (component.get("v.balanceSheet.Cash_and_cash_equivalents__c") ? parseFloat(component.get("v.balanceSheet.Cash_and_cash_equivalents__c")) : 0) +
            (component.get("v.balanceSheet.Bank_balances_other_than_above__c")? parseFloat(component.get("v.balanceSheet.Bank_balances_other_than_above__c")) : 0) +
            (component.get("v.balanceSheet.Other_financial_assets_CA__c") ? parseFloat(component.get("v.balanceSheet.Other_financial_assets_CA__c")) : 0) + 
            (component.get("v.balanceSheet.Other_current_assets__c") ? parseFloat(component.get("v.balanceSheet.Other_current_assets__c")) : 0 );
        component.set("v.totalCurrentAsset",totalValue.toFixed(2));
        helper.calculateTotalAsset(component,event,helper);
    },
    calculateTotalCurrentAssetLast : function(component, event, helper) {
        var totalValue = (component.get("v.balanceSheetLast.Inventories__c") ? parseFloat(component.get("v.balanceSheetLast.Inventories__c")) : 0) +
            (component.get("v.balanceSheetLast.Financial_Assets_Investments_CA__c") ? parseFloat(component.get("v.balanceSheetLast.Financial_Assets_Investments_CA__c")):0 )+
            (component.get("v.balanceSheetLast.Financial_Assets_Trade_Receivables__c") ? parseFloat(component.get("v.balanceSheetLast.Financial_Assets_Trade_Receivables__c")):0)+
            (component.get("v.balanceSheetLast.Cash_and_cash_equivalents__c") ? parseFloat(component.get("v.balanceSheetLast.Cash_and_cash_equivalents__c")) : 0) +
            (component.get("v.balanceSheetLast.Bank_balances_other_than_above__c")? parseFloat(component.get("v.balanceSheetLast.Bank_balances_other_than_above__c")) : 0) +
            (component.get("v.balanceSheetLast.Other_financial_assets_CA__c") ? parseFloat(component.get("v.balanceSheetLast.Other_financial_assets_CA__c")) : 0)+ 
            (component.get("v.balanceSheetLast.Other_current_assets__c") ? parseFloat(component.get("v.balanceSheetLast.Other_current_assets__c")) : 0 );
        component.set("v.totalCurrentAssetLast",totalValue.toFixed(2));
        helper.calculateTotalAssetLast(component,event,helper);
    },
    calculateTotalEquity : function(component, event, helper) {
        var totalValue = (component.get("v.balanceSheet.Equity_Share_Capital__c") ? parseFloat(component.get("v.balanceSheet.Equity_Share_Capital__c")) : 0) +
            (component.get("v.balanceSheet.Other_Equity__c") ? parseFloat(component.get("v.balanceSheet.Other_Equity__c")):0 );
        component.set("v.totalEquity",totalValue.toFixed(2));
        helper.calculateTotalEquityLiabilities(component, event,helper);
    },
    
    calculateTotalEquityLast : function(component, event, helper) {
        var totalValue = (component.get("v.balanceSheetLast.Equity_Share_Capital__c") ? parseFloat(component.get("v.balanceSheetLast.Equity_Share_Capital__c")) : 0) +
            (component.get("v.balanceSheetLast.Other_Equity__c") ? parseFloat(component.get("v.balanceSheetLast.Other_Equity__c")):0 );
        component.set("v.totalEquityLast",totalValue.toFixed(2));
        helper.calculateTotalEquityLiabilitiesLast(component, event,helper);
    },
    
    calculateTotalNonCurrentLiability : function(component, event, helper) {
        var totalValue = (component.get("v.balanceSheet.Financial_Liabilities_Borrowings_NCL__c") ? parseFloat(component.get("v.balanceSheet.Financial_Liabilities_Borrowings_NCL__c")) : 0) +
            (component.get("v.balanceSheet.Financial_Liabilities_Trade_Payables_NCL__c") ? parseFloat(component.get("v.balanceSheet.Financial_Liabilities_Trade_Payables_NCL__c")):0 )  +
            (component.get("v.balanceSheet.Long_term_Provision__c") ? parseFloat(component.get("v.balanceSheet.Long_term_Provision__c")):0 )  +
            (component.get("v.balanceSheet.Other_non_current_liabilities__c") ? parseFloat(component.get("v.balanceSheet.Other_non_current_liabilities__c")):0 ) ;
        component.set("v.totalNonCurrentLiabilities",totalValue.toFixed(2));
        helper.calculateTotalLiabilities(component, event,helper);
    },
    calculateTotalNonCurrentLiabilityLast : function(component, event, helper) {
        var totalValue = (component.get("v.balanceSheetLast.Financial_Liabilities_Borrowings_NCL__c") ? parseFloat(component.get("v.balanceSheetLast.Financial_Liabilities_Borrowings_NCL__c")) : 0) +
            (component.get("v.balanceSheetLast.Financial_Liabilities_Trade_Payables_NCL__c") ? parseFloat(component.get("v.balanceSheetLast.Financial_Liabilities_Trade_Payables_NCL__c")):0 )  +
            (component.get("v.balanceSheetLast.Long_term_Provision__c") ? parseFloat(component.get("v.balanceSheetLast.Long_term_Provision__c")):0 )  +
            (component.get("v.balanceSheetLast.Other_non_current_liabilities__c") ? parseFloat(component.get("v.balanceSheetLast.Other_non_current_liabilities__c")):0 ) ;
        component.set("v.totalNonCurrentLiabilitiesLast",totalValue.toFixed(2));
        helper.calculateTotalLiabilitiesLast(component, event,helper);
    },
    calculateTotalCurrentLiability : function(component, event, helper) {
        var totalValue = (component.get("v.balanceSheet.Financial_Liabilities_Borrowings_CL__c") ? parseFloat(component.get("v.balanceSheet.Financial_Liabilities_Borrowings_CL__c")) : 0) +
            (component.get("v.balanceSheet.Financial_Liabilities_Trade_Payables_CL__c") ? parseFloat(component.get("v.balanceSheet.Financial_Liabilities_Trade_Payables_CL__c")):0 )  +
            (component.get("v.balanceSheet.Other_current_financial_liabilities__c") ? parseFloat(component.get("v.balanceSheet.Other_current_financial_liabilities__c")):0 )  +
            (component.get("v.balanceSheet.Short_term_Provisions__c") ? parseFloat(component.get("v.balanceSheet.Short_term_Provisions__c")):0 ) +
            (component.get("v.balanceSheet.Other_Current_Liabilities__c") ? parseFloat(component.get("v.balanceSheet.Other_Current_Liabilities__c")):0 ) ;
        component.set("v.totalCurrentLiabilities",totalValue.toFixed(2));
        helper.calculateTotalLiabilities(component, event,helper);
    },
    calculateTotalCurrentLiabilityLast : function(component, event, helper) {
        var totalValue = (component.get("v.balanceSheetLast.Financial_Liabilities_Borrowings_CL__c") ? parseFloat(component.get("v.balanceSheetLast.Financial_Liabilities_Borrowings_CL__c")) : 0) +
            (component.get("v.balanceSheetLast.Financial_Liabilities_Trade_Payables_CL__c") ? parseFloat(component.get("v.balanceSheetLast.Financial_Liabilities_Trade_Payables_CL__c")):0 )  +
            (component.get("v.balanceSheetLast.Other_current_financial_liabilities__c") ? parseFloat(component.get("v.balanceSheetLast.Other_current_financial_liabilities__c")):0 )  +
            (component.get("v.balanceSheetLast.Short_term_Provisions__c") ? parseFloat(component.get("v.balanceSheetLast.Short_term_Provisions__c")):0 ) +
            (component.get("v.balanceSheetLast.Other_Current_Liabilities__c") ? parseFloat(component.get("v.balanceSheetLast.Other_Current_Liabilities__c")):0 ) ;
        component.set("v.totalCurrentLiabilitiesLast",totalValue.toFixed(2));
        helper.calculateTotalLiabilitiesLast(component, event,helper);
    },
    
    calculateTotalAsset : function(component, event){
        var totalValue = (component.get("v.totalNonCurrentAsset") != null ? parseFloat(component.get("v.totalNonCurrentAsset")) : 0) + 
            (component.get("v.totalCurrentAsset") != null ? parseFloat(component.get("v.totalCurrentAsset")) : 0);
        component.set("v.totalAsset",totalValue.toFixed(2));
    },
    
    calculateTotalAssetLast : function(component, event){
        var totalValue = (component.get("v.totalNonCurrentAssetLast") != null ? parseFloat(component.get("v.totalNonCurrentAssetLast")) : 0) + 
            (component.get("v.totalCurrentAssetLast") != null ? parseFloat(component.get("v.totalCurrentAssetLast")) : 0);
        component.set("v.totalAssetLast",totalValue.toFixed(2));
    },
    
    calculateTotalLiabilities : function(component, event,helper){
        var totalValue = (component.get("v.totalNonCurrentLiabilities") != null ? parseFloat(component.get("v.totalNonCurrentLiabilities")) : 0) + 
            (component.get("v.totalCurrentLiabilities") != null ? parseFloat(component.get("v.totalCurrentLiabilities")) : 0);
        component.set("v.totalLiabilities",totalValue.toFixed(2));
        this.calculateTotalEquityLiabilities(component, event,helper);
    },
    calculateTotalLiabilitiesLast : function(component, event,helper){
        var totalValue = (component.get("v.totalNonCurrentLiabilitiesLast") != null ? parseFloat(component.get("v.totalNonCurrentLiabilitiesLast")) : 0) + 
            (component.get("v.totalCurrentLiabilitiesLast") != null ? parseFloat(component.get("v.totalCurrentLiabilitiesLast")) : 0);
        component.set("v.totalLiabilitiesLast",totalValue.toFixed(2));
        this.calculateTotalEquityLiabilitiesLast(component, event,helper);
    },
    
    calculateTotalEquityLiabilities : function(component, event){
        var totalValue = (component.get("v.totalLiabilities") != null ? parseFloat(component.get("v.totalLiabilities")) : 0) + 
            (component.get("v.totalEquity") != null ? parseFloat(component.get("v.totalEquity")) : 0);
        component.set("v.totalEqulityLiabilities",totalValue.toFixed(2));
    },
    
    calculateTotalEquityLiabilitiesLast : function(component, event){
        var totalValue = (component.get("v.totalLiabilitiesLast") != null ? parseFloat(component.get("v.totalLiabilitiesLast")) : 0) +
            (component.get("v.totalEquityLast") != null ? parseFloat(component.get("v.totalEquityLast")) : 0);
        component.set("v.totalEqulityLiabilitiesLast",totalValue.toFixed(2));
    },
    
    calculateTotalIncome : function(component, event, helper) {
        var totalValue = (component.get("v.profitLoss.Revenue_from_operations__c") ? parseFloat(component.get("v.profitLoss.Revenue_from_operations__c")) : 0) +
            (component.get("v.profitLoss.Other_income__c") ? parseFloat(component.get("v.profitLoss.Other_income__c")):0 );
        component.set("v.totalIncome",totalValue.toFixed(2));
        helper.calculateProfitBeforeExcepItem(component, event, helper);
    },
    calculateTotalIncomeLast : function(component, event, helper) {
        var totalValue = (component.get("v.profitLossLast.Revenue_from_operations__c") ? parseFloat(component.get("v.profitLossLast.Revenue_from_operations__c")) : 0) +
            (component.get("v.profitLossLast.Other_income__c") ? parseFloat(component.get("v.profitLossLast.Other_income__c")):0 );
        component.set("v.totalIncomeLast",totalValue.toFixed(2));
        helper.calculateProfitBeforeExcepItemLast(component, event, helper);
    },
    
    calculateTotalExpenses : function(component, event, helper) {
        var totalValue = (component.get("v.profitLoss.Cost_of_materials_consumed__c") ? parseFloat(component.get("v.profitLoss.Cost_of_materials_consumed__c")) : 0) +
            (component.get("v.profitLoss.Purchases_of_stock_in_trade__c") ? parseFloat(component.get("v.profitLoss.Purchases_of_stock_in_trade__c")):0 )+
            (component.get("v.profitLoss.Changes_in_inventories_of_finished_goods__c") ? parseFloat(component.get("v.profitLoss.Changes_in_inventories_of_finished_goods__c")):0 )+
            (component.get("v.profitLoss.Excise_duty_on_sale_of_goods__c") ? parseFloat(component.get("v.profitLoss.Excise_duty_on_sale_of_goods__c")):0 )+
            (component.get("v.profitLoss.Employee_benefits_expense__c") ? parseFloat(component.get("v.profitLoss.Employee_benefits_expense__c")):0 )+
            (component.get("v.profitLoss.Finance_costs__c") ? parseFloat(component.get("v.profitLoss.Finance_costs__c")):0 )+
            (component.get("v.profitLoss.Depreciation_and_amortisation_expense__c") ? parseFloat(component.get("v.profitLoss.Depreciation_and_amortisation_expense__c")):0 )+
            (component.get("v.profitLoss.Other_expenses__c") ? parseFloat(component.get("v.profitLoss.Other_expenses__c")):0 );
        component.set("v.totalExpense",totalValue.toFixed(2));
        helper.calculateProfitBeforeExcepItem(component, event, helper);
        //helper.calculateProfitAfterExcepItem(component, event, helper);
    },
    
    calculateTotalExpensesLast : function(component, event, helper) {
        var totalValue = (component.get("v.profitLossLast.Cost_of_materials_consumed__c") ? parseFloat(component.get("v.profitLossLast.Cost_of_materials_consumed__c")) : 0) +
            (component.get("v.profitLossLast.Purchases_of_stock_in_trade__c") ? parseFloat(component.get("v.profitLossLast.Purchases_of_stock_in_trade__c")):0 )+
            (component.get("v.profitLossLast.Changes_in_inventories_of_finished_goods__c") ? parseFloat(component.get("v.profitLossLast.Changes_in_inventories_of_finished_goods__c")):0 )+
            (component.get("v.profitLossLast.Excise_duty_on_sale_of_goods__c") ? parseFloat(component.get("v.profitLossLast.Excise_duty_on_sale_of_goods__c")):0 )+
            (component.get("v.profitLossLast.Employee_benefits_expense__c") ? parseFloat(component.get("v.profitLossLast.Employee_benefits_expense__c")):0 )+
            (component.get("v.profitLossLast.Finance_costs__c") ? parseFloat(component.get("v.profitLossLast.Finance_costs__c")):0 )+
            (component.get("v.profitLossLast.Depreciation_and_amortisation_expense__c") ? parseFloat(component.get("v.profitLossLast.Depreciation_and_amortisation_expense__c")):0 )+
            (component.get("v.profitLossLast.Other_expenses__c") ? parseFloat(component.get("v.profitLossLast.Other_expenses__c")):0 );
        component.set("v.totalExpenseLast",totalValue.toFixed(2));
        helper.calculateProfitBeforeExcepItemLast(component, event, helper);
    },
    
    calculateProfitBeforeExcepItem : function(component, event, helper) {
        var totalValue = (component.get("v.totalIncome") ? parseFloat(component.get("v.totalIncome")) : 0) -
            (component.get("v.totalExpense") ? parseFloat(component.get("v.totalExpense")):0 );
        component.set("v.profitBeforeException",totalValue.toFixed(2));
        helper.calculateProfitAfterExcepItem(component, event, helper);
    },
    
    calculateProfitBeforeExcepItemLast : function(component, event, helper) {
        var totalValue = (component.get("v.totalIncomeLast") ? parseFloat(component.get("v.totalIncomeLast")) : 0) -
            (component.get("v.totalExpenseLast") ? parseFloat(component.get("v.totalExpenseLast")):0 );
        component.set("v.profitBeforeExceptionLast",totalValue.toFixed(2));
        helper.calculateProfitAfterExcepItemLast(component, event, helper);
    },
    
    calculateProfitAfterExcepItem : function(component, event, helper) {
        
        var totalValue = (component.get("v.profitBeforeException") ? parseFloat(component.get("v.profitBeforeException")) : 0) +
            (component.get("v.profitLoss.Exceptional_gain_net__c") ? parseFloat(component.get("v.profitLoss.Exceptional_gain_net__c")) : 0);
        component.set("v.profitAfterException",totalValue.toFixed(2));
        helper.calculateProfitBeforeTax(component, event, helper);
         
    },
    
    calculateProfitAfterExcepItemLast : function(component, event, helper) {
        
        var totalValue = (component.get("v.profitBeforeExceptionLast") ? parseFloat(component.get("v.profitBeforeExceptionLast")) : 0) +
            (component.get("v.profitLossLast.Exceptional_gain_net__c") ? parseFloat(component.get("v.profitLossLast.Exceptional_gain_net__c")) : 0);
        component.set("v.profitAfterExceptionLast",totalValue.toFixed(2));
        helper.calculateProfitBeforeTaxLast(component, event, helper);
         
    },
    
    calculateProfitBeforeTax : function(component, event, helper) {
        var totalValue = (component.get("v.profitAfterException") ? parseFloat(component.get("v.profitAfterException")) : 0) +
            (component.get("v.profitLoss.Share_of_profit_of_joint_ventures__c") ? parseFloat(component.get("v.profitLoss.Share_of_profit_of_joint_ventures__c")) : 0);
        component.set("v.profitBeforeTax",totalValue.toFixed(2));
        helper.calculateProfitForYear(component, event, helper);
    },
    
    calculateProfitBeforeTaxLast : function(component, event, helper) {
        var totalValue = (component.get("v.profitAfterExceptionLast") ? parseFloat(component.get("v.profitAfterExceptionLast")) : 0) +
            (component.get("v.profitLossLast.Share_of_profit_of_joint_ventures__c") ? parseFloat(component.get("v.profitLossLast.Share_of_profit_of_joint_ventures__c")) : 0);
        component.set("v.profitBeforeTaxLast",totalValue.toFixed(2));
        helper.calculateProfitForYearLast(component, event, helper);
    },
    
    calculateTotalTaxExpense : function(component, event, helper) {
        var totalValue = (component.get("v.profitLoss.Current_tax__c") ? parseFloat(component.get("v.profitLoss.Current_tax__c")) : 0) +
            (component.get("v.profitLoss.Deferred_tax__c") ? parseFloat(component.get("v.profitLoss.Deferred_tax__c")) : 0);
        component.set("v.totalTaxExpense",totalValue.toFixed(2));
        helper.calculateProfitForYear(component, event, helper);
    },
    
    calculateTotalTaxExpenseLast : function(component, event, helper) {
        var totalValue = (component.get("v.profitLossLast.Current_tax__c") ? parseFloat(component.get("v.profitLossLast.Current_tax__c")) : 0) +
            (component.get("v.profitLossLast.Deferred_tax__c") ? parseFloat(component.get("v.profitLossLast.Deferred_tax__c")) : 0);
        component.set("v.totalTaxExpenseLast",totalValue.toFixed(2));
        helper.calculateProfitForYearLast(component, event, helper);
    },
    
    calculateProfitForYear : function(component, event, helper) {
        var totalValue = (component.get("v.profitBeforeTax") ? parseFloat(component.get("v.profitBeforeTax")) : 0) +
            (component.get("v.totalTaxExpense") ? parseFloat(component.get("v.totalTaxExpense")) : 0);
        component.set("v.profitForYearContOper",totalValue.toFixed(2));
    },
    calculateProfitForYearLast : function(component, event, helper) {
        var totalValue = (component.get("v.profitBeforeTaxLast") ? parseFloat(component.get("v.profitBeforeTaxLast")) : 0) +
            (component.get("v.totalTaxExpenseast") ? parseFloat(component.get("v.totalTaxExpenseLast")) : 0);
        component.set("v.profitForYearContOperLast",totalValue.toFixed(2));
    }
 })