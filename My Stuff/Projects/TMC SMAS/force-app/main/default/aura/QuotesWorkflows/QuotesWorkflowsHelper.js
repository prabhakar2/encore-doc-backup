({
    refreshList : function(component, event) {
        var List = component.get("v.TaskList");       
        var TaskList=[];
        //alert("calling from helper");
        if(List){
            for(var i=0 ; i<List.length; i++){ 
                if(List[i].Istime){
                    if(!List[i].IsComplete){
                        var sDate = new Date() ;
                        var Duedate = new Date(List[i].Ts.Custom_Due_Date_Time__c);                       
                        var days = parseInt((Duedate - sDate) / (1000 * 60 * 60 * 24));
                        var hours = parseInt(Math.abs(Duedate - sDate) / (1000 * 60 * 60) % 24);
                        var minutes = parseInt(Math.abs(Duedate.getTime() - sDate.getTime()) / (1000 * 60) % 60);
                        var seconds = parseInt(Math.abs(Duedate.getTime() - sDate.getTime()) / (1000) % 60);
                        List[i].Timer=days + " days, " + hours + " hours, " + minutes+ " mins, " + seconds + " seconds";
                    }
                }
                TaskList.push(List[i]);
                component.set("v.TaskList", TaskList); 
            }   
        }
    }
})