({
	doInit : function(component, event, helper) {
    	 var recordId = component.get("v.recordId");       
        var action = component.get("c.getworkflowsTasks");
        //alert(recordId);
        action.setParams({
            "quoteId" : recordId 
        });
        action.setCallback(this, function(response){
        	var state = response.getState();
            var result = response.getReturnValue();
            if(state === "SUCCESS" && result && result.length > 0){
                component.set("v.TaskList", result);
                window.setInterval(function(){ 
                    helper.refreshList(component, event)
                }, 1000); 
            }
        });
        $A.enqueueAction(action);
    },
    
    redirectToDetail: function(component, event, helper){
        var docId = event.currentTarget.getAttribute("data-docid");
        var redirectToDetail = $A.get("e.force:navigateToSObject");
        redirectToDetail.setParams({
            "recordId" : docId
        });
        redirectToDetail.fire();
    },
})