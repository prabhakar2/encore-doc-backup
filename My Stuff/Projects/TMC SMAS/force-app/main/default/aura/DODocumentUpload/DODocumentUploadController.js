({
	doInit : function(component, event, helper) {
        var action = component.get("c.getDoDocument");
        action.setParams({
            "DOId":component.get("v.recordId")
        });
         action.setCallback(this, function(response){
            var res = response.getReturnValue();
             if (res) {
                component.set("v.doDocList", res);
            }
        });
     $A.enqueueAction(action);
    },
    
    handleUploadFinished : function(component, event, helper) {
        var docId = event.getParam("files")[0].documentId;
        event.getSource().set("v.label",event.getParam("files")[0].name);
        var docTypeId = event.getSource().get("v.recordId");
        if(docId && docTypeId){
            helper.updateDoc(component, event,docId,docTypeId);
            $A.get('e.lightning:openFiles').fire({
                recordIds: [event.getParam("files")[0].documentId]
            });
        }
    }
})