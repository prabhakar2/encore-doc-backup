({
	updateDoc : function(component, event,docId,docTypeId){
        var action = component.get("c.updateNewDoc");
        
        action.setParams({
            "docId" : docId,
            "docTypeId" : docTypeId
        });
        $A.enqueueAction(action);
    }
})