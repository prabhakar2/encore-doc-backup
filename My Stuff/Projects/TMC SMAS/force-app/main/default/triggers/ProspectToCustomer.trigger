trigger ProspectToCustomer on Opportunity (After insert , After Update ) {
    
    list<Id> accIds = new list<Id>();
    list<Account> accounts = new list<account>();
    for(opportunity o:trigger.new){
        accIds.add(o.accountId);
    }
    for(account a:[select Id, Type from account where Id IN :accIds]){
        for(opportunity opp:trigger.new){
            if(opp.StageName=='closed won'){
                a.Type='Customer';
                accounts.add(a);
            }
        }
    }
    update accounts;
}