trigger OpportunityAccountTrigger on Opportunity (before insert) {
    Map<Id,Account> accIdToAccountMap = new Map<Id,Account>();
    for(Opportunity opp:trigger.new){
        if(opp.AccountId!=NULL){
            accIdToAccountMap.put(opp.AccountId,null);
        }
    }
    if(accIdToAccountMap.keySet().size() > 0){
        for(Account accdata:[SELECT id,name,Is_Dormant__c 
                             FROM Account 
                             WHERE id in: accIdToAccountMap.keySet() 
                             AND Is_Dormant__c != NULL]){
                                 
                                 if(accIdToAccountMap.containsKey(accdata.Id)){
                                     accIdToAccountMap.put(accdata.Id,accdata);
                                 }
                             }
    }
    
    
    for(Opportunity opp : trigger.new){
        if(opp.AccountId != null && accIdToAccountMap.containsKey(opp.AccountId)){
            if(accIdToAccountMap.get(opp.AccountId) != null 
               && accIdToAccountMap.get(opp.AccountId).Is_Dormant__c != null 
               && (accIdToAccountMap.get(opp.AccountId).Is_Dormant__c == 'Yes' 
                   || accIdToAccountMap.get(opp.AccountId).Is_Dormant__c == 'Dormant')){
                    opp.addError('Opportunity Can\'t be created, Associated Account is Dormant.');
                }
        }
    }
}