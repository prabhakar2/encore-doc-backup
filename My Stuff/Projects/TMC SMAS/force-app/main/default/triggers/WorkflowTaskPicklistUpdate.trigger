trigger WorkflowTaskPicklistUpdate on VRL_Documents__c (after update) {
    Map<Id,List<VRL_Documents__c>> QtToProcessDOVRLMap = new Map<Id,List<VRL_Documents__c>>();
    Map<Id,VRL_Documents__c> rejectedDocToQuoteMap = new Map<Id,VRL_Documents__c>();
    List<Quotation__c> qtListToUpdate = new List<Quotation__c>();
    
    for(VRL_Documents__c vrl : trigger.new){
        if(vrl.Quotation__c != null && vrl.Status__c != null && vrl.Required_Stagee__c == 'To Process DO'){  //&& trigger.oldMap.get(vrl.Id).Status__c != vrl.Status__c
             QtToProcessDOVRLMap.put(vrl.Quotation__c,null);
        }
        
    }
   
    
    if(QtToProcessDOVRLMap.keySet().size() > 0){
        Map<Id,Quotation__c> quoteMap = new Map<Id,Quotation__c>();
        for(VRL_Documents__c vrl : [SELECT Id,Status__c,Quotation__c,Required_Stagee__c,Quotation__r.Car_DO__c 
                                    FROM VRL_Documents__c 
                                    WHERE Quotation__c IN :QtToProcessDOVRLMap.keySet()
                                    AND Required_Stagee__c = 'To Process DO'
                                    ORDER BY Quotation__c,Status__c]){
                                        
                                        List<VRL_Documents__c> tempList = new List<VRL_Documents__c>();
                                        if(QtToProcessDOVRLMap.get(vrl.Quotation__c) != null)
                                            tempList=QtToProcessDOVRLMap.get(vrl.Quotation__c);
                                            tempList.add(vrl);
                                        QtToProcessDOVRLMap.put(vrl.Quotation__c, tempList);
                                    }
             System.debug('QtToProcessDOVRLMap--->'+QtToProcessDOVRLMap);
             System.debug('QtToProcessDOVRLMap--->'+QtToProcessDOVRLMap.size());
        
        for(Id qtId : QtToProcessDOVRLMap.keySet()){
            if(QtToProcessDOVRLMap.get(qtId) != null && QtToProcessDOVRLMap.get(qtId).size() > 0){
                Quotation__c quote = new Quotation__c();
                quote.Id = qtId;
                Integer allDocCount = QtToProcessDOVRLMap.get(qtId).size();
                Integer pendingCount= 0;
                Integer fleetOpenCount = 0;
                Integer fleetCloseCount = 0;
                Integer doOpenCount = 0;
                Integer CMTOpenCount = 0;
                Integer CMTCloseCount = 0;
                Integer rejectedByCmt=0;
                Integer rejectedByFleet=0;
                for(VRL_Documents__c vrl : QtToProcessDOVRLMap.get(qtId)){
                    
                    if(vrl.Status__c == 'Pending from Sales'){
                        if(pendingCount != allDocCount)
                       pendingCount++;
                    }
                    if(vrl.Status__c == 'Submitted by Sales'){
                        if(fleetOpenCount != allDocCount)
                            fleetOpenCount++;
                    }
                    if(vrl.Status__c == 'Received by Fleet'){
                        if(fleetCloseCount != allDocCount)
                            fleetCloseCount++;
                        if(doOpenCount != allDocCount)
                            doOpenCount++;
                    }
                    if(vrl.Status__c == 'Rejected by Fleet'){
                        if(rejectedByFleet != allDocCount)
                        rejectedByFleet++;
                    }
                    
                    if(vrl.Status__c == 'Submitted by Fleet'){
                        if(CMTOpenCount != allDocCount)
                        CMTOpenCount++;
                    }
                    if(vrl.Status__c == 'Received by CMT'){
                        if(CMTCloseCount != allDocCount)
                            CMTCloseCount++;
                     
                    }
                    if(vrl.Status__c == 'Rejected by CMT'){
                        if(rejectedByCmt != allDocCount)
                       rejectedByCmt++;
                        
                    }
                        
                }
                System.debug('pendingCount-->'+pendingCount);
                System.debug('fleetOpenCount-->'+fleetOpenCount);
                System.debug('fleetCloseCount-->'+fleetCloseCount);
                if(pendingCount>0){
                    System.debug('Hello');
                    quote.Fleet_VRL_Workflow_task__c='';
                    quote.DO_Workflow_Task__c='';
                    quote.CMT_VRL_Workflow_Task__c='';
                }else{
                    if(fleetOpenCount>0 && rejectedByCmt>0){
                
                        quote.Fleet_VRL_Workflow_task__c='Open';
                        quote.CMT_VRL_Workflow_Task__c='';
                        System.debug(' quote.Fleet_VRL_Workflow_task__c-->'+ quote.Fleet_VRL_Workflow_task__c);
                        System.debug(' quote.CMT_VRL_Workflow_Task__c-->'+ quote.CMT_VRL_Workflow_Task__c);
                        
                    }
                 
                    if(fleetOpenCount>0 && fleetCloseCount>0 && CMTCloseCount>0){
                           
                        quote.Fleet_VRL_Workflow_task__c='Open';
                        quote.DO_Workflow_Task__c='';
                        quote.CMT_VRL_Workflow_Task__c = '';
                        System.debug('quote.Fleet_VRL_Workflow_task__c-->'+quote.Fleet_VRL_Workflow_task__c);
                    }
                     if(fleetOpenCount>0 && fleetCloseCount>0 && rejectedByCmt==0 && CMTCloseCount==0){
                       
                        quote.Fleet_VRL_Workflow_task__c='Closed';
                        quote.DO_Workflow_Task__c='';
                        quote.CMT_VRL_Workflow_Task__c = '';
                    }
                    if(rejectedByCmt>0 && fleetOpenCount==0){
                        quote.CMT_VRL_Workflow_Task__c = 'Closed';
                        quote.Fleet_VRL_Workflow_task__c='Open';
                        
                    }
                    if(rejectedByFleet>0 ){
                        System.debug('fleet close');
                        System.debug('quote.CMT_VRL_Workflow_Task__c-->'+ quote.CMT_VRL_Workflow_Task__c);
                          quote.Fleet_VRL_Workflow_task__c = 'closed';
                          quote.DO_Workflow_Task__c='';
                          quote.CMT_VRL_Workflow_Task__c = '';
                    }
                    if(fleetOpenCount == allDocCount){
                        System.debug('fleet open');
                        quote.Fleet_VRL_Workflow_task__c = 'Open';
                        quote.DO_Workflow_Task__c='';
                        quote.CMT_VRL_Workflow_Task__c = '';
                    }
                    if(fleetCloseCount == allDocCount){
                        
                        quote.Fleet_VRL_Workflow_task__c = 'Closed';
                        quote.DO_Workflow_Task__c='Open';
                        quote.CMT_VRL_Workflow_Task__c = '';
                    }
                    if(CMTOpenCount == allDocCount){
                        quote.CMT_VRL_Workflow_Task__c = 'Open';
                        quote.Fleet_VRL_Workflow_task__c='Closed';
                           System.debug('cmt open');
                    }
                    if(CMTCloseCount == allDocCount){
                        System.debug('cmt close');
                        quote.CMT_VRL_Workflow_Task__c = 'Closed';
                        quote.Fleet_VRL_Workflow_task__c='Closed';
                        quote.DO_Workflow_Task__c='Closed';
                    }
                    if(fleetOpenCount>0){
                         quote.CMT_VRL_Workflow_Task__c = ''; 
                    }
                  
                    
                    if(fleetOpenCount>0 && CMTCloseCount>0 ){
                         quote.Fleet_VRL_Workflow_task__c = 'Open';
                         quote.CMT_VRL_Workflow_Task__c = '';
                    }
                    if(fleetOpenCount>0 && rejectedByFleet>0){
                           quote.Fleet_VRL_Workflow_task__c = 'Closed';
                         quote.CMT_VRL_Workflow_Task__c = '';
                    }
                    if(fleetOpenCount>0 && CMTOpenCount>0 ){
                           quote.Fleet_VRL_Workflow_task__c = 'Open';
                         quote.CMT_VRL_Workflow_Task__c = '';
                    }
                if(doOpenCount>0 && CMTOpenCount>0 && CMTOpenCount!=allDocCount){
                         quote.Fleet_VRL_Workflow_task__c = 'Closed';
                         quote.DO_Workflow_Task__c = 'Open';
                }
                    
                    if(fleetCloseCount>0 && CMTCloseCount>0){
                          quote.Fleet_VRL_Workflow_task__c = 'Closed';
                         quote.CMT_VRL_Workflow_Task__c = ''; 
                    }
                    if(fleetCloseCount>0 && CMTCloseCount>0 && CMTOpenCount>0){
                        quote.Fleet_VRL_Workflow_task__c = 'Closed';
                         quote.CMT_VRL_Workflow_Task__c = ''; 
                    }
                if(CMTCloseCount > 0 && CMTOpenCount > 0 && CMTCloseCount != allDocCount && CMTOpenCount != allDocCount && rejectedByCmt==0 && fleetOpenCount==0 && fleetCloseCount==0 ){
                       quote.Fleet_VRL_Workflow_task__c = 'Closed';
                       quote.CMT_VRL_Workflow_Task__c = 'Open';
                    
                }
                    if(CMTCloseCount > 0 && CMTOpenCount > 0 && rejectedByCmt>0 &&  CMTCloseCount != allDocCount && CMTOpenCount != allDocCount && rejectedByCmt != allDocCount && fleetOpenCount==0 && fleetCloseCount==0){
                          quote.Fleet_VRL_Workflow_task__c = 'Open';
                          quote.CMT_VRL_Workflow_Task__c = 'Closed';

                    }
                    if(rejectedByFleet>0 && CMTCloseCount>0 && rejectedByCmt>0 && CMTOpenCount>0){
                         quote.Fleet_VRL_Workflow_task__c = 'Closed';
                          quote.CMT_VRL_Workflow_Task__c = '';

                    }
                      if(rejectedByFleet>0 && CMTCloseCount>0 && rejectedByCmt>0 && CMTOpenCount>0 && fleetOpenCount>0){
                         quote.Fleet_VRL_Workflow_task__c = 'Closed';
                          quote.CMT_VRL_Workflow_Task__c = '';

                    }
                    if(fleetOpenCount>0 && fleetOpenCount!=allDocCount){
                         quote.Fleet_VRL_Workflow_task__c = 'Open';
                          quote.CMT_VRL_Workflow_Task__c = '';
                    }
                }
                
               
            /*    if(fleetOpenCount > 0 && doOpenCount > 0 && doOpenCount != allDocCount && fleetOpenCount != allDocCount )
                    quote.Fleet_VRL_Workflow_task__c = 'Open';
                else if(fleetCloseCount > 0 && fleetOpenCount > 0 && fleetCloseCount != allDocCount && fleetOpenCount != allDocCount)
                    quote.Fleet_VRL_Workflow_task__c = 'Closed';
               */ 

                	
                qtListToUpdate.add(quote);
            }
        }
        System.debug('qtListToUpdate--->'+qtListToUpdate);
         if(qtListToUpdate.size() > 0)
            update qtListToUpdate;
    }
  string codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
        codecover='str';
}
        
       /* Map<Id,Map<String,Integer>> dataMap = new Map<Id,Map<String,Integer>>();
        for(AggregateResult ar : [SELECT Status__c,Quotation__c,count(Id) cnt 
                                  FROM  VRL_Documents__c  
                                  WHERE Required_Stage__c = 'To Process DO'
                                  AND Quotation__c IN :QtToProcessDOVRLMap.keySet()
                                  GROUP BY Quotation__c,Status__c 
                                  ORDER BY Quotation__c]){
                                      Id quoteId = (Id)ar.get('Quotation__c');
                                      if(dataMap.containsKey(quoteId)){
                                          Map<string,Integer> temp = dataMap.get(quoteId);
                                          temp.put((string)ar.get('Status__c'),(Integer)ar.get('cnt'));
                                          dataMap.put(quoteId,temp);
                                      }else{
                                          dataMap.put(quoteId,new Map<string,Integer>{(string)ar.get('Status__c')=>(Integer)ar.get('cnt')});
                                      }
                                  }
        
        for(Id quoteId : dataMap.keySet()){
            Map<string,Integer> statusMap  = dataMap.get(quoteId);
            if(statusMap.keySet().size()==1){
                for(string runStatus : statusMap.keySet())
                    quoteMap = setStatusOnQuotation(quoteId,runStatus,statusMap.get(runStatus),quoteMap);
            }else{
                
            }
        }
    }
    
    
    private Map<Id,Quotation__c> setStatusOnQuotation(string quoteId,string status,Integer count,Map<Id,Quotation__c> quoteMap){
        if(quoteMap.containsKey(quoteId)){
            Quotation__c temp = quoteMap.get(quoteId);
            quoteMap.put(quoteId,temp);
        }else{
            Quotation__c temp = new Quotation__c(Id = quoteId);
            quoteMap.put(quoteId,temp);
        }
        return quoteMap;
    }
    
    */