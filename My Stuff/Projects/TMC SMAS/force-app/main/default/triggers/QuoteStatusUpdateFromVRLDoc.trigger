trigger QuoteStatusUpdateFromVRLDoc on VRL_Documents__c (after insert, after update) {
    
    
    Set<Id> QuoteIds = new Set<Id>();
    Map<Id,List<VRL_Documents__c>> MapQuoteIdToDocs = new Map<Id,List<VRL_Documents__c>>();
    List<Quotation__c> LstQuoteToUpdate = new List<Quotation__c>();
    
    for(VRL_Documents__c vr : trigger.new){        
        if(vr.Quotation__c!=null){
            QuoteIds.add(vr.Quotation__c);          
        }
    }    
    System.debug('QuoteIds>>>>'+QuoteIds);
    if(QuoteIds.size() > 0){
         Map<Id,string> statusMap = new Map<Id,string>();
        for(AggregateResult ar : [SELECT Quotation__c, Status__c 
                                   FROM VRL_Documents__c 
                                   WHERE Quotation__c IN : QuoteIds 
                                   AND Required_Stagee__c='To Process DO'
                                   GROUP BY Quotation__C,STATUS__C 
                                   ORDER BY QUOTATION__C,STATUS__C]){
                                       
                                       Id quoteId = (Id)ar.get('Quotation__c');
                                       if(statusMap.containsKey(quoteId)){
                                           statusMap.put(quoteId,'Pending from Sales');
                                       }else{
                                           statusMap.put(quoteId,(string)ar.get('status__C'));
                                       }
                                   }
        
        System.debug('statusMap>>>>'+statusMap);
        
        for(Id qId : statusMap.keySet()){
            Quotation__c temp = new Quotation__c();
            temp.Id = qId;
            temp.VRL_Document_Status__c = statusMap.get(qId);
            LstQuoteToUpdate.add(temp);
        }
        if(LstQuoteToUpdate.size() > 0)
            update LstQuoteToUpdate;
    }
    
        
}