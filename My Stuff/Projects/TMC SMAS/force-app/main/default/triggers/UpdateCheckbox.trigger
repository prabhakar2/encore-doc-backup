trigger UpdateCheckbox on DO_Line_Item__c (after insert,after update,after delete,after undelete) {
    Map<id,String> QuoteLineItemMap=new map<id,String>();
    List<Quotation__c> QuoteList = new List<Quotation__c>();
    List<Quotation__c> quoteCheckBoxList=new List<Quotation__c>();
    List<Quotation__c> quoteInfoList = new List<Quotation__c>();
    Map<id,Boolean> quoteIdToDLIChkBoxMap=new Map<Id,Boolean>();
    Set<Id> quoteToUncheckCarDo = new Set<Id>(); // set of record they will uncheck when do line item docancel field will be checked
    Set<Id> QuoteIdSet = new Set<Id>();
    for(DO_Line_Item__c lineItem : (trigger.IsInsert || trigger.IsUpdate ||trigger.isUndelete)?trigger.new:trigger.old){
        if(lineItem.Quotation__c!=NULL && 
           (trigger.isInsert || (trigger.isUpdate && trigger.oldMap.get(lineItem.Id).Subcode__c != lineItem.Subcode__c)))
            QuoteIdSet.add(lineItem.Quotation__c);
           
         if(lineItem.Quotation__c != null && lineItem.DoCancel__c){
         quoteToUncheckCarDo.add(lineItem.Quotation__c); // collect all the quotation to uncheck the record
        }    
    }
    for(DO_Line_Item__c dlItemObj:trigger.new){
        if(dlItemObj.Quotation__c!=NULL && (trigger.isInsert || trigger.isUpdate))
            quoteIdToDLIChkBoxMap.put(dlItemObj.Quotation__c,dlItemObj.DoCancel__c);
        
    }
    String str='';
    List<DO_Line_Item__c> lineItemList = [SELECT id,Quotation__c,Subcode__c FROM DO_Line_Item__c WHERE Quotation__c IN:QuoteIdSet];
    if(lineItemList.size() > 0){
        for(DO_Line_Item__c lineItem:lineItemList){
            if(QuoteLineItemMap.containsKey(lineItem.Quotation__c)){ 
                String tre = QuoteLineItemMap.get(lineItem.Quotation__c);
                if(lineItem.Subcode__c=='CAR' || lineItem.Subcode__c == 'Model' ){
                    Integer buy = Integer.valueOf(tre.split('&')[0])+1;
                    str = String.valueOf(buy)+'&'+tre.split('&')[1];
                    QuoteLineItemMap.put(lineItem.Quotation__c,str);
                }
                if(lineItem.Subcode__c=='YR1'){
                    Integer lease = Integer.valueOf(tre.split('&')[1])+1;
                    str = tre.split('&')[0]+'&'+String.valueOf(lease);
                    QuoteLineItemMap.put(lineItem.Quotation__c,str);
                }
            }
            else{
                if(lineItem.Subcode__c=='CAR' || lineItem.Subcode__c == 'Model'){
                    str = '1'+'&'+'0';
                    QuoteLineItemMap.put(lineItem.Quotation__c,str);
                }else{
                    str = '0'+'&'+'1';
                    QuoteLineItemMap.put(lineItem.Quotation__c,str);
                }  
            }
        }
        for(Id Quoteid : QuoteLineItemMap.keySet()){
            Quotation__c quot = new Quotation__c();
            if(QuoteLineItemMap.containsKey(Quoteid)){
                List<String> strList =  QuoteLineItemMap.get(Quoteid).split('&');
                if(Integer.valueOf(strList[0]) > 0)
                    quot.Car_DO__c = true;
                else
                    quot.Car_DO__c = false;
                if(Integer.valueOf(strList[1])>0)
                    quot.Insurance_DO__c = true;
                else
                    quot.Insurance_DO__c = false;
                quot.Id = Quoteid;
                QuoteList.add(quot);
            }
        }
    }else{
        for(Quotation__c quot : [SELECT Id,Name,Car_DO__c,Insurance_DO__c from Quotation__c where Id In:QuoteIdSet]){
            Quotation__c quot2 = new Quotation__c();
            quot2.Id = quot.Id;
            quot2.Car_DO__c = false;
            quot2.Insurance_DO__c = false;
            QuoteList.add(quot2);
        }
    }
    
    for(DO_Line_Item__c dlObj:trigger.new){
        if(dlObj.Quotation__c!=NULL){
            if(dlObj.Subcode__c=='CAR' || dlObj.Subcode__c == 'Model' ){
            if(quoteIdToDLIChkBoxMap.containsKey(dlObj.Quotation__c)&& quoteIdToDLIChkBoxMap.get(dlObj.Quotation__c)!=NULL){
                Quotation__c quot = new Quotation__c();  
                quot.id=dlObj.Quotation__c;
                quot.DO_Cancel__c=quoteIdToDLIChkBoxMap.get(dlObj.Quotation__c);
                quoteCheckBoxList.add(quot);
            }
            }
        }
    }
    
    System.debug('QuoteList-->'+QuoteList);
    if(QuoteList.size() > 0)        
        update QuoteList;
    
    if(quoteCheckBoxList.size()>0)
        update quoteCheckBoxList;
    
    if(quoteToUncheckCarDo.size() >0){
      for(Id quoteId : quoteToUncheckCarDo){
       quoteInfoList.add(new Quotation__c(id=quoteId ,Car_DO__c=false)); // update the quotation with false value here 
      }
    } 
    
    if(quoteInfoList.size() >0)
        update quoteInfoList;
        
}