trigger populateDueDateOnTask on Task (before insert, before update){

    Set<String> assignedUser = new Set<String>();
    Map<String, String> branchUserMap = new Map<String, String>();
    Map<String, Branch__c> userToBranchMap = new Map<String, Branch__c>();
    
    if(trigger.new != null && trigger.new.size()>0){
        for(Task tsk : trigger.new){
            if(tsk.Start_Date__c != null && tsk.Step__c != null && tsk.TAT__c != null){
                assignedUser.add(tsk.OwnerId);  
            }
        }
        System.debug('-----------1>');
        //  Querying user to get the branch.
        if(assignedUser != null && assignedUser.size()>0){
            for(User u : [SELECT Id, Branch__c FROM User WHERE Id IN : assignedUser]){
                branchUserMap.put(u.Branch__c,u.Id);            }
        }
        System.debug('----====---2>');
        //  Querying the branch and holidays.
        if(branchUserMap != null && branchUserMap.size()>0){
            for(Branch__c branch : [SELECT Working_Days__c, Start_Time__c, End_Time__c, Branch__c, (SELECT Start_Date__c, End_Date__c FROM Holidays__r) FROM Branch__c WHERE Branch__c IN : branchUserMap.keySet()]){
                if(String.isNotBlank(branch.Branch__c) && branchUserMap.containsKey(branch.Branch__c)){
                    userToBranchMap.put(branchUserMap.get(branch.Branch__c), branch);
                }
            }
        }
         System.debug('---3>');
        for(Task tsk : trigger.new){
            Boolean isWorkingDay = false;
            Boolean isWorkingTime = false;
            Boolean isHoliday = false;
            Boolean loopCondition = true;
            DateTime taskDueDate;
            
            if(tsk.Start_Date__c != null && tsk.Step__c != null && tsk.TAT__c != null){
                Branch__c branch = new Branch__c();
                Integer minutes = 0;
                Datetime taskStartDate = tsk.Start_Date__c;               
                
                if(userToBranchMap != null && userToBranchMap.containsKey(tsk.OwnerId) && userToBranchMap.get(tsk.OwnerId) != null){
                    branch = userToBranchMap.get(tsk.OwnerId);
                    System.debug('>>>>>>'+tsk.Start_Date__c.time().addMinutes(Integer.valueOf(tsk.TAT__c)));
                    System.debug('>>>'+branch.End_Time__c);
                    if(tsk.Start_Date__c.time() < branch.Start_Time__c){
                        taskStartDate = Datetime.newInstance(taskStartDate.date(), branch.Start_Time__c);
                    }else if(tsk.Start_Date__c.time() >= branch.End_Time__c){
                        taskStartDate = Datetime.newInstance(taskStartDate.date(), branch.Start_Time__c).addDays(1);
                    }else{
                        
                        if(tsk.Start_Date__c.time().addMinutes(Integer.valueOf(tsk.TAT__c)) > branch.End_Time__c){
                            taskStartDate = Datetime.newInstance(taskStartDate.date(), branch.Start_Time__c).addDays(1);
                            DateTime diffDate = Datetime.newInstance(tsk.Start_Date__c.date(), branch.End_Time__c);
                            Long dt2Long = diffDate.getTime();
                            Long dt1Long = tsk.Start_Date__c.getTime();
                            Long milliseconds = dt2Long - dt1Long;
                            Long seconds = milliseconds / 1000;
                            minutes = Integer.valueOf(seconds / 60);
                        }
                        //taskStartDate = taskStartDate.addMinutes(minutes);
                    }
                    System.debug('---4>');
                    while(loopCondition){                        
                        String dayOfWeek=taskStartDate.format('EEEE');
                        
                        if(String.isNotBlank(branch.Working_Days__c) && branch.Working_Days__c.contains(dayOfWeek)){
                            isWorkingDay = true;
                        }
                        
                        if(taskStartDate.time()>=branch.Start_Time__c && taskStartDate.time()<=branch.End_Time__c){
                            isWorkingTime = true;
                        }
                        
                        if(branch.Holidays__r != null && branch.Holidays__r.size()>0){
                            for(Holiday__c hDay : branch.Holidays__r){
                                if(hDay.Start_Date__c != null && hDay.End_Date__c != null){
                                    if(taskStartDate.date() != hDay.Start_Date__c && taskStartDate.date() != hDay.End_Date__c){
                                        isHoliday = true;
                                    }else{
                                        isHoliday = false;
                                        break;
                                    }
                                }
                            }
                        }else{
                            isHoliday = true;
                        }
                        
                        if(isWorkingDay && isWorkingTime && isHoliday){
                            taskDueDate = taskStartDate;
                            loopCondition = false;
                            break;
                        }else{
                            if(isWorkingDay == false || isHoliday == false){
                                taskStartDate = taskStartDate.addDays(1);
                                isWorkingTime = false;
                            }else if(isWorkingTime == false){
                                taskStartDate = taskStartDate.addHours(1);
                                isWorkingDay = false;
                                isHoliday = false;
                            }
                        }
                    }
                }else{
                    taskDueDate = taskStartDate;
                }
                if(taskDueDate != null){
                    if(tsk.Start_Date__c.time()>=branch.End_Time__c){
                        taskDueDate = taskDueDate.addMinutes(Integer.valueOf(tsk.TAT__c));
                        tsk.Custom_Due_Date_Time__c = taskDueDate;
                        tsk.ActivityDate = Date.valueOf(taskDueDate);
                    }else{
                        //tsk.Custom_Due_Date_Time__c = taskDueDate.addMinutes(finalMinutes);
                        tsk.Custom_Due_Date_Time__c = taskDueDate.addMinutes(Integer.valueOf(tsk.TAT__c) - minutes);
                        tsk.ActivityDate = Date.valueOf(taskDueDate);
                    }
                }
            }
        }
    }

}