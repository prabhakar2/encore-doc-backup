trigger UpdateAmount on Insurance__c (After insert , After Update , After Delete) {
    
    Set<Id> accidset = new Set<id>();
    
    for(Insurance__c insu : !trigger.Isdelete?trigger.new:trigger.old){
        if(insu.Insurance_Company_Account__c!=NULL)
            accidset.add(insu.Insurance_Company_Account__c);
        if(trigger.IsUpdate && (insu.Insurance_Company_Account__c!=trigger.oldMap.get(insu.Id).Insurance_Company_Account__c)){
            accidset.add(trigger.oldMap.get(insu.Id).Insurance_Company_Account__c);
        }
        
    }
    
    List<Account> accupdateList = new List<Account>();
    Boolean isupdate = false;
    for(AggregateResult agg : [SELECT Insurance_Company_Account__c accid,SUM(Amount__c) amt from Insurance__c where 
                               Insurance_Company_Account__c!=NULL AND Insurance_Company_Account__c IN:accidset group by Insurance_Company_Account__c]){
                                   Account acc = new Account();
                                   acc.Id = (Id)agg.get('accid');
                                   acc.Insurance_Amount_Consumed__c = (Decimal)agg.get('amt');
                                   accupdateList.add(acc);
                                   isupdate = true;
                               }
    if(!isupdate && accidset.size() > 0 && trigger.isUpdate){
        for(Id accid : accidset){
            Account acc = new Account();
            acc.Id = accid;
            acc.Insurance_Amount_Consumed__c = 0;
            accupdateList.add(acc);
        }
    }
    system.debug('--->>>>'+accidset);
    if(trigger.isDelete && !isupdate && accidset.size() > 0){
        for(Id transid:trigger.oldMap.keyset()){
            if(trigger.oldMap.get(transid).Insurance_Company_Account__c!=NULL){
                Account acc = new Account();
                acc.Id = trigger.oldMap.get(transid).Insurance_Company_Account__c;
                acc.Insurance_Amount_Consumed__c = 0;
                accupdateList.add(acc);
            }
        }
    }
    system.debug('--->>>>'+accupdateList);
    if(accupdateList.size() > 0)
        update accupdateList;
}