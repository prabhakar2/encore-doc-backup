trigger QuotationPopulateProfitability on Quotation__c (after insert,after update) {
Map<id,Decimal> mapQuotationByTargetId=new map<id,Decimal>();
    Map<id,Decimal> mapQuotationByTargetId2=new map<id,Decimal>();
    Map<id,Integer> mapQuotationByTargetIdcount=new map<id,Integer>(); 
    Map<id,Integer> mapQuotationByTargetIdcountnew=new map<id,Integer>(); 
    List<Monthly_Target__c> mnthlyTrgtList=new List<Monthly_Target__c>();
    List<Monthly_Target__c> mnthlyTrgtList2=new List<Monthly_Target__c>();
    List<Monthly_Target__c> mnthlyTrgtCountList1=new List<Monthly_Target__c>();
    List<Monthly_Target__c> mnthlyTrgtCountList2=new List<Monthly_Target__c>();
    for(Quotation__c quote:trigger.new){
        if(quote.Monthly_Target__c != null){
            mapQuotationByTargetId2.put(quote.Monthly_Target__c,0.0);
            mapQuotationByTargetId.put(quote.Monthly_Target__c,0.0);
            mapQuotationByTargetIdcount.put(quote.Monthly_Target__c,0);
             mapQuotationByTargetIdcountnew.put(quote.Monthly_Target__c,0);
        }
        
    }
    if(mapQuotationByTargetId.keySet().size() > 0){
        for(AggregateResult agg : [SELECT count(id) Ids,SUM(Profitability__c) profVal,Monthly_Target__c mth
                                   FROM Quotation__c 
                                   WHERE Monthly_Target__c IN :mapQuotationByTargetId.keySet()
                                   AND Client_Type__c='New' 
                                   AND 	RV_Type__c='HRV'
                                   GROUP BY Monthly_Target__c ]){
                                       mapQuotationByTargetId.put((Id)agg.get('mth'), (Decimal)agg.get('profVal'));
                                       mapQuotationByTargetIdcountnew.put((Id)agg.get('mth'), (Integer)agg.get('Ids'));
                                       System.debug(mapQuotationByTargetIdcountnew);
                                   }
        
        
        
        for(Id mtId : mapQuotationByTargetId.keySet()){
            Monthly_Target__c mt = new Monthly_Target__c();
            mt.Id = mtId;
            mt.HRV_Amount_Acheived__c = mapQuotationByTargetId.get(mtId);
            
            mnthlyTrgtList.add(mt);
            //System.debug('@@'+mnthlyTrgtList);
        }
          for(Id mtIds:mapQuotationByTargetIdcountnew.keySet()){
            Monthly_Target__c mtObject = new Monthly_Target__c();
            mtObject.Id = mtIds;
            mtObject.HRV_New_No_Achieved__c=mapQuotationByTargetIdcountnew.get(mtIds);
            mnthlyTrgtCountList1.add(mtObject);
               System.debug('@@'+mnthlyTrgtCountList1);
        }
        
        if(mnthlyTrgtList.size() > 0)
            update mnthlyTrgtList;
        
         if(mnthlyTrgtCountList1.size() > 0 )
            update mnthlyTrgtCountList1;
    }
    
    if(mapQuotationByTargetId2.keySet().size() > 0){
        for(AggregateResult agg : [SELECT count(id) ids,SUM(Profitability__c) profVal,Monthly_Target__c mth
                                   FROM Quotation__c 
                                   WHERE Monthly_Target__c IN :mapQuotationByTargetId2.keySet()
                                   AND Client_Type__c='Existing' 
                                   AND 	RV_Type__c='HRV'
                                   GROUP BY Monthly_Target__c ]){
                                       mapQuotationByTargetId2.put((Id)agg.get('mth'), (Decimal)agg.get('profVal')); 
                                       mapQuotationByTargetIdcount.put((Id)agg.get('mth'), (Integer)agg.get('ids'));
                                   }
        
        
        
        for(Id mtId : mapQuotationByTargetId2.keySet()){
            Monthly_Target__c mt = new Monthly_Target__c();
            mt.Id = mtId;
            
            mt.HRV_Existing_Amount_Acheived__c = mapQuotationByTargetId2.get(mtId);
            mnthlyTrgtList2.add(mt);
           // System.debug('@@'+mnthlyTrgtList);
        }
        
        for(Id mtIds:mapQuotationByTargetIdcount.keySet()){
            Monthly_Target__c mtObj = new Monthly_Target__c();
            mtObj.Id = mtIds;
            mtObj.HRV_Existing_No_Achieved__c=mapQuotationByTargetIdcount.get(mtIds);
            mnthlyTrgtCountList2.add(mtObj);
        }
        if(mnthlyTrgtList2.size() > 0)
            update mnthlyTrgtList2;
        
         if(mnthlyTrgtCountList2.size() > 0)
            update mnthlyTrgtCountList2;
    }
    
    
}