trigger Insurance_Created on Insurance__c (after insert,after update,after delete ,after undelete ) {
    
    Set<id> quoteid =new Set<id>();
    List<Quotation__c> qlist = new list<Quotation__c>();
    for(Insurance__c inc :(trigger.isdelete || trigger.isupdate)?trigger.old:trigger.new){
        if(inc.Quotation__c!=NULL)
            quoteid.add(inc.Quotation__c);
    }
    system.debug('quoteid--->>>'+quoteid);
    List<Quotation__c> quoteList  = new List<Quotation__c>();
    List <Quotation__c> quolist = [Select Id ,(Select id from  Insurances__r) from Quotation__c where id in :quoteid];
    
    if (quolist.size()>0){
        for (Quotation__c quo: quolist ){
            if(quo.Insurances__r.size()>0){
                quo.Insurance_Created__c=True;
            }
            else{
                quo.Insurance_Created__c=False;
            }
            qlist.add(quo);
        }
        update qlist;
    } 
    
}