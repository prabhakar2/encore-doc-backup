trigger CreateReceiptTrigger on Receipt_Register__c (after insert) {
	Map<String,Id> customerNoToAccMap = new Map<String,Id>();
    Map<String,Id> transNumberMap = new Map<String,Id>();
    List<Receipt__c> recListToInsert = new List<Receipt__c>();
    for(Receipt_Register__c rr : trigger.new){
        if(rr.Customer_Number__c != null)
            customerNoToAccMap.put(rr.Customer_Number__c , null);
        
        if(rr.Receipt_Number__c != null)
            transNumberMap.put(rr.Transaction_Number__c, null);
    }
    
    if(customerNoToAccMap.keySet().size() > 0){
        for(Account acc : [SELECT Id,Oracle_Id__c FROM Account WHERE Oracle_Id__c IN :customerNoToAccMap.keySet()]){
            if(customerNoToAccMap.containsKey(acc.Oracle_ID__c)){
                customerNoToAccMap.put(acc.Oracle_ID__c, acc.Id);
            }
        }
    }
    
    if(transNumberMap.keySet().size() > 0){
        for(Receipt__c rec: [SELECT Id,Receipt_Number__c,Transaction_No__c FROM Receipt__c WHERE Transaction_No__c IN :transNumberMap.keySet()]){
            if(transNumberMap.containsKey(rec.Transaction_No__c)){
                transNumberMap.put(rec.Transaction_No__c, rec.Id);
            }
        }
    }
    
    for(Receipt_Register__c rr : trigger.new){
        if(rr.Receipt_Status_Dsp__c != 'Reversal-User Error'){
            Receipt__c rec = new Receipt__c();
            if(transNumberMap.containsKey(rr.Transaction_Number__c) && transNumberMap.get(rr.Transaction_Number__c) != null)
                rec.Id = transNumberMap.get(rr.Transaction_Number__c);
            rec.Applied_Invoice_Amt__c = rr.Receipt_Amount__c != null ? rr.Receipt_Amount__c : 0.0;
            rec.Transaction_No__c = rr.Transaction_Number__c != null ? rr.Transaction_Number__c : '';
            rec.Account__c = customerNoToAccMap.get(rr.Customer_Number__c) != null ? customerNoToAccMap.get(rr.Customer_Number__c) : null;
            rec.Receipt_Number__c = rr.Receipt_Number__c != null ? rr.Receipt_Number__c : '';
            rec.Receipt_Date__c = rr.Receipt_Date__c != null ? Date.valueOf(rr.Receipt_Date__c) : null;
            rec.Receipt_Status_Dsp__c = rr.Receipt_Status_Dsp__c != null ? rr.Receipt_Status_Dsp__c : '';
            recListToInsert.add(rec);
        }
    }
    
    if(recListToInsert.size() > 0)
        upsert recListToInsert;
}