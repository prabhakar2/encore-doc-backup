trigger PrimaryContactPicklistTrg  on Contact (before insert, before Update) {
    
    List<String> Picklistvalue = new List<String>();
    map<id,set<string>> AccountContactmap1=new map<id,set<string>>();
    List<id> accId = new List<id>();
    for(contact cc : trigger.new){
        if(cc.AccountId != null && cc.primary_Contact__c != null 
           && (trigger.isInsert || (trigger.isUpdate && trigger.oldMap.get(cc.Id).Primary_Contact__c != cc.Primary_Contact__c))){
            accId.add(cc.AccountId);
        }
    }
    
    if(accId.size() > 0 ){
        for(contact con: [select accountId,Primary_Contact__c from contact where accountId IN :accId AND Id NOT IN :trigger.new]){
            if(con.Primary_Contact__c!=null){
                set<string> conset= new set<string>();
                if(AccountContactmap1.containsKey(con.accountId))
                    conset= AccountContactmap1.get(con.accountId);
                conset.add(con.Primary_Contact__c);
                AccountContactmap1.put(con.accountId,conset);
            }
        }
        
        if(AccountContactmap1.keySet().size() > 0){
            for(contact c : trigger.New){     
                if(c.Primary_Contact__c!=null && c.AccountId!=null){
                    if(AccountContactmap1.containsKey(c.accountId) && AccountContactmap1.get(c.accountId).contains(c.Primary_Contact__c))
                        c.addError('You can not add same type of contact for this account');
                }
            }
        }
    }
}