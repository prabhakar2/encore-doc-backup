/*	Description : If monthly target user is Owner of account of Quotation and month is Do Line Item Created date month and its subcode is either car or model , Type is  RV type  +	 Client Type
 *                then monthly target id is populated on that Quotation
 *                If Car Do is unchecked then Monthly Target field is to be blank on Quotation.   
 *  Created By : Shalini Singh
 */
trigger QuotationMonthlyTargetPopulate on Quotation__c (before Update,after insert) {

    set<id> qtOwnerId=new Set<id>();
    map<string,string> quotDLIMap= new map<string,string>(); 
    set<String> quoteId=new Set<String>();
    Map<id,id> maptargetidbyQuotation=new Map<id,id>();
    Map<String,id> mapMonthlyTargetByDateId=new Map<String,id>();
    Map<String,Decimal> mapQutationTypeByProfitability =new Map<String,Decimal>();
    Map<Id,Id> accIdToAccMap = new Map<Id,Id>();
    Map<Id,String> RVClientTypeMergeKeyMap = new Map<Id,String>();
    private final  Map<Integer,String> monthMap = new Map<Integer,String>{1=>'Jan',2=>'Feb',3=>'Mar',4=>'Apr',5=>'May',6=>'Jun',7=>'Jul',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec'};
    for(Quotation__c QID : trigger.new){
        if(QID.Account__c != null){
            accIdToAccMap.put(QID.Account__c, null);
            
            if(QID.RV_Type__c != null && QID.Client_Type__c!= null){
             RVClientTypeMergeKeyMap.put(QID.Account__c,QID.RV_Type__c.trim()+' '+QID.Client_Type__c.trim());
            }
        }
        
        qtOwnerId.add(QID.OwnerId);
        quoteId.add(QID.id);
    }
    
    if(accIdToAccMap.keySet().size() > 0){
        for(Account acc : [SELECT Id,OwnerId FROM Account WHERE Id IN:accIdToAccMap.keySet()]){
            accIdToAccMap.put(acc.Id,acc.OwnerId);
        }
    }
    
    for(DO_Line_Item__c dodate:[select Created_On__c ,Quotation__c,SubCode__c 
                                from DO_Line_Item__c 
                                where SubCode__c IN ('CAR','Model')
                                AND Created_On__c!=NULL AND Quotation__c IN: quoteId]){
                                
                                 if(monthMap.containsKey(dodate.Created_On__c.month())){
                                    quotDLIMap.put(dodate.Quotation__c,monthMap.get(dodate.Created_On__c.month()));
                                 }   
                                      
    }
    System.debug('mapMonthlyTargetByDateId111--->'+[select id,User__c,Month__c ,Type__c from Monthly_Target__c ]);
    system.debug('accIdToAccMap.values()'+accIdToAccMap.values());
    system.debug('quotDLIMap.values()'+quotDLIMap.values());
    system.debug('RVClientTypeMergeKeyMap.values()'+RVClientTypeMergeKeyMap.values());
    for(Monthly_Target__c monthlydata:[select id,User__c,Month__c ,Type__c
                                       from Monthly_Target__c 
                                       where User__c IN : accIdToAccMap.values() 
                                       AND Month__c IN : quotDLIMap.values() AND Type__c In :RVClientTypeMergeKeyMap.values()]){
                                         
                                           mapMonthlyTargetByDateId.put(monthlydata.User__c + monthlydata.Month__c + monthlydata.Type__c, monthlydata.id);
     }
    
    
    
    if(mapMonthlyTargetByDateId.keySet().size() > 0){
        for(Quotation__c qt : trigger.new){
            if(quotDLIMap.containsKey(qt.Id) && quotDLIMap.get(qt.Id)!=null && qt.RV_Type__c!=NULL && qt.Client_Type__c!=NULL){
                String month = quotDLIMap.get(qt.Id);
                Id ownerId = accIdToAccMap.get(qt.Account__c);
                String RvClietStr = qt.RV_Type__c.trim()+' '+qt.Client_Type__c.trim();
                if(mapMonthlyTargetByDateId.containsKey(ownerId + month + RvClietStr )){
                    qt.Monthly_Target__c=mapMonthlyTargetByDateId.get(ownerId + month +RvClietStr);
                    
                }
            }
            
            if(trigger.isUpdate && qt.Car_DO__c==false){
                qt.Monthly_Target__c=null;
            }
        }
    }
   
    
}