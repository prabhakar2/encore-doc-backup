trigger PopulateCamOnAccountIfApproved on CAM__c (after insert,after update) {
    List<Account> accountlist=new List<Account>();
    Map<id,CAM__c> acccammap=new Map<id,CAM__c>();
    for(CAM__c cam:trigger.new){
        if(cam.Account__c != NULL && cam.Stage__c != null && cam.Stage__c == 'CAM Approved' 
          && (trigger.isInsert  || (trigger.isUpdate && cam.Stage__c != trigger.oldMap.get(cam.id).Stage__c))){ 
               acccammap.put(cam.Account__c,cam);
           }
    }
    
    if(acccammap.keySet().size() > 0){
        for(CAM__C camdata:[SELECT Next_Review_Date__c,Account__c, Limit_Applied_For__c, Approval_Status__c,LastModifiedDate,
                            Id, Name FROM CAM__c 
                            where Account__c IN :acccammap.keySet()
                            AND Stage__c = 'CAM Approved'
                            ORDER BY CreatedDate DESC Limit 1  ]){
                                acccammap.put(camdata.Account__c,camdata);         
                            }
        
        for(id accid : acccammap.keySet()){
            if(acccammap.containsKey(accId)){
                Account acc = new Account();
                acc.Id = accId;
                acc.Limit_Approved_Amount__c = acccammap.get(acc.Id) != null && acccammap.get(acc.Id).Limit_Applied_For__c != null ? acccammap.get(acc.Id).Limit_Applied_For__c : 0.0;
                acc.Limit_Valid_End_Date__c= acccammap.get(acc.Id) != null && acccammap.get(acc.Id).Next_Review_Date__c != null ?  String.valueOf(acccammap.get(acc.Id).Next_Review_Date__c) : '';
                accountlist.add(acc);
            }
        }
        if(accountlist.size()>0)
            update accountlist;
    }
    
}