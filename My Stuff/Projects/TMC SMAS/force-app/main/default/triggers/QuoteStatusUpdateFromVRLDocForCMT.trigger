trigger QuoteStatusUpdateFromVRLDocForCMT on VRL_Documents__c (after insert, after update) {
 Set<Id> QuoteIds = new Set<Id>();
    Map<Id,List<VRL_Documents__c>> MapQuoteIdToDocs = new Map<Id,List<VRL_Documents__c>>();
    List<Quotation__c> LstQuoteToUpdate = new List<Quotation__c>();
    for(VRL_Documents__c vr : trigger.new){        
        if(vr.Quotation__c!=null){
            QuoteIds.add(vr.Quotation__c);          
        }
    }    
    if(QuoteIds.size()>0){
        List<VRL_Documents__c> vDocsList = [SELECT Id, Name, Document__c, Quotation__c, Status__c, Required_Stage__c FROM VRL_Documents__c WHERE Quotation__c IN : QuoteIds AND (Required_Stage__c='To Process DO' OR Required_Stage__c='Fleet to Submit')];
        if(vDocsList.size()>0){
            for(VRL_Documents__c vDoc : vDocsList){
                if(MapQuoteIdToDocs.containsKey(vDoc.Quotation__c)){
                    MapQuoteIdToDocs.get(vDoc.Quotation__c).add(vDoc);
                }
                else{
                    MapQuoteIdToDocs.put(vDoc.Quotation__c,new List<VRL_Documents__c>{vDoc});
                }
            } 
            for(Id DocId : MapQuoteIdToDocs.keySet()){               
                Set<String> StatusList = new Set<String>();
                for(VRL_Documents__c vDoc : MapQuoteIdToDocs.get(DocId)){
                    StatusList.add(vDoc.Status__c);
                }            
                if(StatusList.size()>1){                
                    LstQuoteToUpdate.add(new Quotation__c(Id=DocId, VRL_Document_Status_CMT__c='Pending from Sales'));               
                }
                else{
                    string firstelement =(new list<string>(StatusList) )[0];
                    LstQuoteToUpdate.add(new Quotation__c(Id=DocId, VRL_Document_Status_CMT__c=firstelement));                
                }
            }
            if(LstQuoteToUpdate.size()>0){
                Update LstQuoteToUpdate;
            }
        }
    }
}