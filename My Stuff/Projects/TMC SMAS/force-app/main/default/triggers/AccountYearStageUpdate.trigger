trigger AccountYearStageUpdate on CAM__c (after insert,after update) {
    
    Set<String> accountIdSet= new Set<string>();
    for(CAM__C cam : trigger.isDelete?trigger.old:trigger.New){
        if(cam.Account__c != NULL )
            accountIdSet.add(cam.Account__c);
    }
    if(accountIdSet.size()>0){
        string currentFY = CAMSetupController.getFY_inTrigger();
        
        Map<Id,CAM__C> accIdToCAMMap = new Map<Id,CAM__c>();
        Map<Id,String> camAppMap = new Map<Id,String>();
        for(CAM__C cam:[SELECT Account__c,year__c,Stage__c 
                        FROM CAM__C 
                        WHERE Account__c IN :accountIdSet
                        AND Account__r.RecordType.Name='Prospect'
                        ORDER BY Account__c,Stage__c,CreatedDate DESC]){
                            
                            System.debug('>>>>>cam'+cam);
                            if(!accIdToCAMMap.containsKey(cam.Account__c))
                                accIdToCAMMap.put(cam.Account__c,cam);
                            
                           camAppMap.put(cam.Account__c,cam.Stage__c);
                           System.debug('>>>>>camAppMap'+camAppMap);     
                        }
        
        
        List<Account> accList = new List<Account>();
        if(accIdToCAMMap.keySet().size()>0){           
            
            for(Id accId : accIdToCAMMap.keySet()){
                CAM__c cam = accIdToCAMMap.get(accId);
                Account temp = new Account(Id = accId,
                                           Year__c = cam.year__c,
                                           CAM_Stage__c = cam.stage__c);
                
                temp.Cam_Approved__c = camAppMap.get(accId) != null && camAppMap.get(accId) == 'CAM Approved' ? true : false;
                if(temp.Cam_Approved__c)
                    temp.Stage__c = 'S2';
                else
                    temp.Stage__c = 'S4';
                
                accList.add(temp);
                System.debug('accList>>>>>>'+accList);
            }
        }/*else if(accIdToCAMMap.keySet().size()==0){
            for(Id accId : accountIdSet){
                Account temp = new Account();
                temp.Id = accId;
                temp.Stage__c = 'S4';
                temp.Cam_Approved__c = false;
                temp.Year__c = '';
                temp.CAM_Stage__c = '';
                accList.add(temp);
            }
        }*/
        
        if(accList.size()>0)
            update accList;
    }    
}