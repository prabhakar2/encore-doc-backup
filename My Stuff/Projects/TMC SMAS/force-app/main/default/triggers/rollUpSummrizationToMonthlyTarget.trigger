/*
  Description : Roll up the record for the quotation object to Monthly Target object here 
  @uthor : Avaneesh Singh Salesforce Developer TechMatrix consulting PVT LTD
  Created date : 24/05/2019
*/
trigger rollUpSummrizationToMonthlyTarget on Quotation__c (after insert,after update) {
    List<Quotation__c> QuotationInfosList = new List<Quotation__c>();
    QuotationInfosList = Trigger.Isinsert || Trigger.isUndelete ? Trigger.New : Trigger.old;
   rollUpSummrizationToMonthlyTargetHelper.runTrigger(QuotationInfosList);
}