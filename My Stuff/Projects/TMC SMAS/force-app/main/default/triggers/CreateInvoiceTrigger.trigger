trigger CreateInvoiceTrigger on Sales_Register__c (after insert) {
    Map<String,Id> customerNoToAccMap = new Map<String,Id>();
    Map<String,Id> invoiceNumberMap = new Map<String,Id>();
    List<Invoice__c> invListToInsert = new List<Invoice__c>();
    for(Sales_Register__c sr : trigger.new){
        if(sr.Customer_Number__c != null)
            customerNoToAccMap.put(sr.Customer_Number__c , null);
        if(sr.Invoice_Number__c != null)
            invoiceNumberMap.put(sr.Invoice_Number__c, null);
    }
    
    if(customerNoToAccMap.keySet().size() > 0){
        for(Account acc : [SELECT Id,Oracle_Id__c FROM Account WHERE Oracle_Id__c IN :customerNoToAccMap.keySet()]){
            if(customerNoToAccMap.containsKey(acc.Oracle_ID__c)){
                customerNoToAccMap.put(acc.Oracle_ID__c, acc.Id);
            }
        }
    }
    
    if(invoiceNumberMap.keySet().size() > 0){
        for(Invoice__c inv : [SELECT Id,Invoice_Number__c FROM Invoice__c WHERE Invoice_Number__c IN :invoiceNumberMap.keySet()]){
            if(invoiceNumberMap.containsKey(inv.Invoice_Number__c)){
                invoiceNumberMap.put(inv.Invoice_Number__c, inv.Id);
            }
        }
    }
    
    for(Sales_Register__c sr : trigger.new){
        Invoice__c inv = new Invoice__c();
        if(invoiceNumberMap.containsKey(sr.Invoice_Number__c) && invoiceNumberMap.get(sr.Invoice_Number__c) != null)
            inv.Id = invoiceNumberMap.get(sr.Invoice_Number__c);
        inv.Invoice_Date__c = sr.Invoice_Date__c != null ? Date.valueOf(sr.Invoice_Date__c) : null;
        inv.Invoice_Number__c= sr.Invoice_Number__c != null ? sr.Invoice_Number__c : '';
        if(sr.Customer_Number__c != null){
            inv.Customer_Number__c = sr.Customer_Number__c != null ? sr.Customer_Number__c : '';
            inv.Client_Name__c = customerNoToAccMap.get(sr.Customer_Number__c) != null ? customerNoToAccMap.get(sr.Customer_Number__c) : null;
        }
        inv.Invoice_Type__c = sr.Invoice_Type__c != null ? sr.Invoice_Type__c : '';
        inv.Payment_Days__c = sr.Payment_Days__c != null ? sr.Payment_Days__c : 0;
        inv.Paid_Amount__c = sr.Invoice_Amount__c != null ? sr.Invoice_Amount__c : 0.0;
        invListToInsert.add(inv);
        
    }
    
    if(invListToInsert.size() > 0)
        upsert invListToInsert;
}