trigger QuoteStatusUpdateFromPDDdoc on PDD__c (after update) {
    Set<Id> quotationSet = new Set<Id>();
    Map<id,quotation__c> quoteMap = new Map<Id,Quotation__c>();
    List<String> docNameList = label.Not_Mandatory_Document.split(',');
    Map<id,boolean> originalRCMap = new Map<Id,boolean>();
    List<Document_Master__c> originalRCDoc = [SELECT Id,Name FROM Document_Master__c WHERE Name = 'Original RC' LIMIT 1];
    
    for(PDD__c pd : trigger.new){
        if(pd.Quotation__c != null){
            if(pd.Status__c != null && pd.Status__c != trigger.oldMap.get(pd.Id).Status__c && pd.Document_Master__c != null){
                quotationSet.add(pd.Quotation__c);
                if(originalRCDoc.size() > 0){
                    if(pd.Document_Master__c == originalRCDoc[0].Id && pd.Status__c != 'Pending' ){
                        originalRCMap.put(pd.Quotation__c, true);
                    }
                }
                
            }
            
        }
        
        
    }
    
    if(quotationSet.size() > 0){
        Map<Id,string> statusMap = new Map<Id,string>();
        for(AggregateResult ar : [SELECT quotation__c,status__c 
                                  FROM PDD__c  
                                  WHERE Quotation__c IN :quotationSet
                                  AND Document_Master__c != null AND
                                  Document_Master__r.Name NOT IN :docNameList
                                  GROUP BY Quotation__C,STATUS__c 
                                  ORDER BY Quotation__C,STATUS__c]){
                                      
                                      Id quoteId = (Id)ar.get('quotation__c');
                                      if(statusMap.containsKey(quoteId)){
                                          statusMap.put(quoteId,'Pending');
                                      }else{
                                          statusMap.put(quoteId,(string)ar.get('status__C'));
                                      }
                                      
                                      if(!quoteMap.containsKey(quoteId)){
                                          quoteMap.put(quoteId,new Quotation__c(Id = quoteId));
                                      }
                                  }
        
        for(string key : quoteMap.keySet()){
            if(statusMap.containsKey(key)){
                Quotation__C temp = quoteMap.get(key);
                temp.PDD_Document_Status__c = statusMap.get(key);
                temp.Original_RC__c = originalRCMap.get(key) != null ? originalRCMap.get(key) : false;
                quoteMap.put(key,temp);
            }
        }
        if(quoteMap.keySet().size()>0)
            update quoteMap.values();
    }
    /*Set<Id> quotationSet = new Set<Id>();
    Map<id,quotation__c> quoteMap = new Map<Id,Quotation__c>();
    List<String> notMandateDocLst = label.Not_Mandatory_Document.split(',');
    List<Document_Master__c> docMasterList = [SELECT id 
                                              FROM Document_Master__c 
                                              WHERE Name IN :notMandateDocLst];
    
    for(PDD__c pd : trigger.new){
        if(pd.quotation__c != NULL 
           && pd.status__c != NULL 
           && pd.status__c != trigger.oldMap.get(pd.id).status__c){
               quotationSet.add(pd.quotation__c);
               if(docMasterList.size() > 0){
                   for(Document_Master__c dm : docMasterList){
                       if(pd.Document_Master__c == dm.Id){
                           
                       }
                   }
               }
               
               
               if(docMasterList.size()>0 && pd.document_Master__c == docMasterList[0].Id){
                   if(!quoteMap.containsKey(pd.Quotation__c)){
                       quoteMap.put(pd.Quotation__c,new Quotation__c(Id = pd.Quotation__c,
                                                                     Original_Rc__c = TRUE));
                   }
               }
           }
    }
    if(quotationSet.size()>0){
        Map<Id,string> statusMap = new Map<Id,string>();
        
        string query = 'SELECT quotation__C,status__C ';
        query += 'FROM PDD__C  WHERE Quotation__C IN :quotationSet ';
        if(docMasterList.size()>0)
            query += ' AND document_Master__c != \''+docMasterList[0].Id +'\'';
        query += ' GROUP BY Quotation__C,STATUS__C ORDER BY QUOTATION__C,STATUS__C';
        
        for(AggregateResult ar: Database.query(query)){
                                     Id quoteId = (Id)ar.get('quotation__c');
                                     if(statusMap.containsKey(quoteId)){
                                         statusMap.put(quoteId,'Pending');
                                     }else{
                                         statusMap.put(quoteId,(string)ar.get('status__C'));
                                     }
                                     
                                     if(!quoteMap.containsKey(quoteId)){
                                         quoteMap.put(quoteId,new Quotation__c(Id = quoteId));
                                     }
                                     
                                 }
        
        
        for(string key : quoteMap.keySet()){
            if(statusMap.containsKey(key)){
                Quotation__C temp = quoteMap.get(key);
                temp.PDD_Document_Status__c = statusMap.get(key);
                quoteMap.put(key,temp);
            }
        }
    }
    
    
    if(quoteMap.keySet().size()>0)
        update quoteMap.values();*/
    
}