Trigger CAMTrigger on CAM__c (before insert,before update) {
    Map<Id,User> ownerMap = new Map<Id,User>();
    Map<Id,Id> managerMap = new Map<Id,Id>();
    for(CAM__c cm : trigger.New){
        if(cm.OwnerId != null)
            ownerMap.put(cm.OwnerId,null);
    }
    
    for(User usr : [SELECT Id,ManagerId,Designation__c,Manager.Designation__c FROM User]){
        if(ownerMap.containsKey(usr.Id))
            ownerMap.put(usr.Id, usr);
        managerMap.put(usr.Id, usr.ManagerId);
    }
    
    for(CAM__c cm : trigger.New){
        if(ownerMap.get(cm.OwnerId).Designation__c == 'RM' || ownerMap.get(cm.OwnerId).Designation__c == 'BDM-RM'){
            cm.TL__c = cm.OwnerId;
            cm.RM__c = cm.OwnerId;
        }else if(ownerMap.get(cm.OwnerId).Designation__c == 'TL-RM'){
            cm.TL__c = cm.OwnerId;
            cm.RM__c = cm.OwnerId;
        }else if(ownerMap.get(cm.OwnerId).Manager.Designation__c != null && ownerMap.get(cm.OwnerId).Manager.Designation__c == 'TL-RM'){
            cm.TL__c = ownerMap.get(cm.OwnerId).ManagerId;
            cm.RM__c = ownerMap.get(cm.OwnerId).ManagerId;
        }else{
            cm.TL__c = (ownerMap.get(cm.OwnerId).Designation__c == 'TL' 
                        || ownerMap.get(cm.OwnerId).Designation__c == 'BDM-TL') ? cm.OwnerId : ownerMap.get(cm.OwnerId).ManagerId;
            cm.RM__c = managerMap.get(cm.TL__c);
        }
        
        cm.SH__c = managerMap.get(cm.RM__c);
    }
    
    string s = '';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    s += 'a';
    
    }