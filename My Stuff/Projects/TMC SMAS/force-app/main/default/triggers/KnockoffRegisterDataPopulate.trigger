/**
*@File Name : KnockoffRegisterDataPopulate
* @Trigger Name : KnockoffRegisterDataPopulate
* @Description : To populate  Knock Off Register fields into Knock Off if oracle id ,transaction no,invoice present in any account, recepiet, invoice.
* @Author : Shalini Singh  
* @Last Modified By : Shalini Singh
**/
trigger KnockoffRegisterDataPopulate on Knof_Off_Register__c (before insert) {
    Map<String,String> knkAccountMap=new map<String,String>();
    Map<String,String> knkInvoiceMap=new map<String,String>();
    Map<String,String> knkRecepietMap=new map<String,String>();
 
    
    Map<Knof_Off_Register__c,Boolean> checkCriteriaMap=new Map<Knof_Off_Register__c,Boolean>();
    
    List<Knock_Off__c> knkoffList=new List<Knock_Off__c>();
    //Database.SaveResult[] srList;
    
    system.debug('trigger.new'+trigger.new);
    for(Knof_Off_Register__c knkoffObj:trigger.new){
        
        if(knkoffObj.Oracle_Id__c!=NULl && knkoffObj.Invoice_Number__c!=NULL && knkoffObj.Transaction_Number__c!=NULL && knkoffObj.Unique_Id__c!=NULL){
            knkAccountMap.put(knkoffObj.Oracle_Id__c,null);
            knkInvoiceMap.put(knkoffObj.Invoice_Number__c,null);
            knkRecepietMap.put(knkoffObj.Transaction_Number__c,null);
            
        }
    }
    
    if(knkAccountMap.keySet().size()>0){
        for(Account acc :[select  Id,Oracle_ID__c from account where Oracle_ID__c IN:knkAccountMap.keySet()]){
            knkAccountMap.put(acc.Oracle_ID__c,acc.Id);
        }
    }
    
    
    if(knkInvoiceMap.keySet().size()>0){
        for(Invoice__c inv : [select Id,Invoice_Number__c from Invoice__c where Invoice_Number__c IN : knkInvoiceMap.keySet()]){
            knkInvoiceMap.put(inv.Invoice_Number__c,inv.Id);
        }
    }
    
    if(knkRecepietMap.keySet().size()>0){
        for(Receipt__c recpt : [select Id,Transaction_No__c from Receipt__c where Transaction_No__c IN : knkRecepietMap.keySet()]){
            knkRecepietMap.put(recpt.Transaction_No__c,recpt.Id);
        }
    }
    System.debug('knkRecepietMap-->'+knkRecepietMap);
    for(Knof_Off_Register__c knkoffReg:trigger.new){
        if(knkAccountMap.get(knkoffReg.Oracle_ID__c)!=NULL  && knkInvoiceMap.get(knkoffReg.Invoice_Number__c)!=NULL && knkRecepietMap.get(knkoffReg.Transaction_Number__c)!=NULL)
            checkCriteriaMap.put(knkoffReg,true);
        else
            checkCriteriaMap.put(knkoffReg,false);
    }
    System.debug('checkCriteriaMap-->'+checkCriteriaMap);
    
   for(Knof_Off_Register__c kckOfRegistrObj :trigger.new){
        if(checkCriteriaMap.containsKey(kckOfRegistrObj) && checkCriteriaMap.get(kckOfRegistrObj)){
            Knock_Off__c  knkofObj=new Knock_Off__c();
            if(knkAccountMap.containsKey(kckOfRegistrObj.Oracle_ID__c) && knkAccountMap.get(kckOfRegistrObj.Oracle_ID__c)!=NULL ){
                knkofObj.Account__c=knkAccountMap.get(kckOfRegistrObj.Oracle_ID__c);
            }
            if(knkInvoiceMap.containsKey(kckOfRegistrObj.Invoice_Number__c) && knkInvoiceMap.get(kckOfRegistrObj.Invoice_Number__c)!=NULL){
                knkofObj.Invoice__c=knkInvoiceMap.get(kckOfRegistrObj.Invoice_Number__c);
            }
            if(knkRecepietMap.containsKey(kckOfRegistrObj.Transaction_Number__c) && knkRecepietMap.get(kckOfRegistrObj.Transaction_Number__c)!=NULL){
                knkofObj.Receipt__c=knkRecepietMap.get(kckOfRegistrObj.Transaction_Number__c);
            }
            knkofObj.Line_Item_Description__c=kckOfRegistrObj.Line_Item_Description__c;
            knkofObj.Amount__c= (kckOfRegistrObj.Amount__c==0) || (kckOfRegistrObj.Amount__c==Null)?0.0:kckOfRegistrObj.Amount__c;
            knkofObj.knock_Off_Unique_ID__c=kckOfRegistrObj.Unique_Id__c;
            knkoffList.add(knkofObj);
        }
    }
    if(knkoffList.size()>0){
   String jsonString = json.serialize(knkoffList);
    knockOffInsertFutureCall.knockOffInsert(jsonString);
    }
    

    
    for(Knof_Off_Register__c knkoffReg:trigger.new){
        if(checkCriteriaMap.containsKey(knkoffReg) && !checkCriteriaMap.get(knkoffReg)){
            system.debug('error at  '+ knkoffReg);
            
            knkoffReg.addError('Required field is missing!');
        }
    }
    
    
}