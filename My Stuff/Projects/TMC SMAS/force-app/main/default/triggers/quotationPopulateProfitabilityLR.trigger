trigger quotationPopulateProfitabilityLR on Quotation__c (after insert,after update) {
    
    Map<String,Monthly_Target__c> mapIdToMontlyTarget = new Map<String,Monthly_Target__c>();
    
    for(Quotation__c quot : trigger.new)
        if(quot.Monthly_Target__c != NULL) 
            mapIdToMontlyTarget.put(quot.Monthly_Target__c,new Monthly_Target__c());
       
    
    
    if(mapIdToMontlyTarget != NULL && mapIdToMontlyTarget.keySet().size() > 0)
        for(AggregateResult ar : [SELECT Monthly_Target__c mt,Client_Type__c ct,RV_Type__c rt,Count(Id) cou,SUM(Profitability__c) pro 
                                  FROM Quotation__c 
                                  WHERE Monthly_Target__c IN :mapIdToMontlyTarget.keySet()
                                  AND RV_Type__c != NULL
                                  AND Client_Type__c !=NULL
                                  GROUP BY Monthly_Target__c,Client_Type__c,RV_Type__c]) {
                                      
                                      if(mapIdToMontlyTarget.containsKey((String)ar.get('mt'))) {
                                          Monthly_Target__c monthlyTarget = mapIdToMontlyTarget.get((String)ar.get('mt')); 
                                          String clientType = (String)ar.get('ct');
                                          if(clientType == 'New') {
                                              String rvType = (String)ar.get('rt');
                                              if(rvType == 'HRV') {
                                                  monthlyTarget.HRV_New_No_Achieved__c = (Integer)ar.get('cou');
                                                  monthlyTarget.HRV_Amount_Acheived__c = (Decimal)ar.get('pro');  
                                              }
                                              else if(rvType == 'LRV') {
                                                  monthlyTarget.LRV_New_No_Achieved__c = (Integer)ar.get('cou');
                                                  monthlyTarget.LRV_New_Amount_Acheived__c = (Decimal)ar.get('pro'); 		    
                                              }
                                              else if(rvType == 'FL Bus'){
                                                  monthlyTarget.FL_Bus_New_No_Achieved__c = (Integer)ar.get('cou');
                                                  monthlyTarget.FL_Bus_New_Amount_Achieved__c = (Decimal)ar.get('pro');
                                              }
                                              else {
                                                  monthlyTarget.FL_Passenger_New_No_Achieved__c = (Integer)ar.get('cou');
                                                  monthlyTarget.FL_Passenger_New_Amount_Achieved__c = (Decimal)ar.get('pro');     
                                              }
                                          }
                                          else {
                                              String rvType = (String)ar.get('rt');
                                              if(rvType == 'HRV') {
                                                  monthlyTarget.HRV_Existing_No_Achieved__c = (Integer)ar.get('cou');
                                                  monthlyTarget.HRV_Existing_Amount_Acheived__c = (Decimal)ar.get('pro'); 	    
                                              }
                                              else if(rvType == 'LRV') {
                                                  monthlyTarget.LRV_Existing_No_Achieved__c = (Integer)ar.get('cou');
                                                  monthlyTarget.LRV_Existing_Amount_Acheived__c = (Decimal)ar.get('pro');     
                                              }
                                              else if(rvType == 'FL Bus') {
                                                  monthlyTarget.FL_Bus_Existing_No_Achieved__c = (Integer)ar.get('cou');
                                                  monthlyTarget.FL_Bus_Existing_Amount_Achieved__c = (Decimal)ar.get('pro');     
                                              }
                                              else {
                                                  monthlyTarget.FL_Passenger_Existing_No_Achieved__c = (Integer)ar.get('cou');
                                                  monthlyTarget.FL_Passenger_Existing_Amount_Achieved__c = (Decimal)ar.get('pro'); 
                                              }    
                                          }
                                          mapIdToMontlyTarget.put((String)ar.get('mt'),monthlyTarget);                                          
                                      }
                                  }
    
    
    if(mapIdToMontlyTarget != NULL && mapIdToMontlyTarget.keySet().size() > 0)
        for(String mtId : mapIdToMontlyTarget.keySet()) {
            Monthly_Target__c mt = mapIdToMontlyTarget.get(mtId);
            mt.Id = mtId;
            mapIdToMontlyTarget.put(mtId,mt);
        }
    
    if(mapIdToMontlyTarget != NULL && mapIdToMontlyTarget.keySet().size() > 0)
        update mapIdToMontlyTarget.values();
    
}