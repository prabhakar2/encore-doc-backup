/*
 * trigger name:paymentrequestpopulateassetcontract
 * date:12 march 2019
 * Developed By:Shalini Singh (with Prabhakar)
 * Test Class:
 * 
 * Description : this trigger is used to populate asset in Payment request using related quotation 
*/
trigger PaymentRequestPopulateAssetContract on Payment_Request__c (before insert,before update) {
   Map<Id,Id> Qtassetmap=new Map<id,Id>();
    
    for(Payment_Request__c pr:Trigger.new){
        if(pr.Quotation__c!=NULL)
            Qtassetmap.put(pr.Quotation__c,null);
    } 
    for(Asset_Contract__c  Asst:[select Id,Quotation__c 
                                 from Asset_Contract__c 
                                 where  Quotation__c IN :Qtassetmap.keySet()]){
        Qtassetmap.put(Asst.Quotation__c, Asst.Id);  
    }
    for(Payment_Request__c prs:Trigger.new){
      if(Qtassetmap.containsKey(prs.Quotation__c))
          prs.Asset_Contract__c=Qtassetmap.get(prs.Quotation__c);
    }
    
}