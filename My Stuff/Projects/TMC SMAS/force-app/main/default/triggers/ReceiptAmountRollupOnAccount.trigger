trigger ReceiptAmountRollupOnAccount on Receipt__c (after insert,after update,after delete) {
    
    Map<Id,Decimal> recToAmtMap = new Map<Id,Decimal>();
    List<Account> accListToUpdate = new List<Account>();
    for(Receipt__c rec: trigger.isDelete ? trigger.old : trigger.new){
        if(rec.Account__c != null){
            recToAmtMap.put(rec.Account__c,0.0);
        }
    }
    if(recToAmtMap.keySet().size() > 0){
        for(AggregateResult agg : [SELECT SUM(Applied_Invoice_Amt__c) totalAmt,Account__c  accId
                                   FROM Receipt__c 
                                   WHERE Account__c IN :recToAmtMap.keySet() 
                                   AND Applied_Invoice_Amt__c != null
                                   GROUP BY Account__c]){
                                       
                                       recToAmtMap.put((Id)agg.get('accId'), (Decimal)agg.get('totalAmt'));
                                   }
        for(Id accId : recToAmtMap.keySet()){
            Account acc = new Account();
            acc.Id = accId;
            acc.Payment_Received__c = recToAmtMap.get(accId);
            accListToUpdate.add(acc);
        }
        
        if(accListToUpdate.size() > 0)
            update accListToUpdate; 
    }
}