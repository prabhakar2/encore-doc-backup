trigger AccountStageValidationTrg on Account (before insert,before update) {
    for(Account acc : trigger.new){
        if(trigger.isInsert && acc.Stage__c != null
           || (trigger.isUpdate && acc.Stage__c != trigger.oldMap.get(acc.Id).Stage__c)){
               if((acc.Stage__c == 'S2' || acc.Stage__c == 'S1' || acc.Stage__c == 'Client') &&
                  (acc.Year__c == null || acc.Year__c != getFY() || acc.CAM_Stage__c != 'CAM Approved')){
                      acc.addError('Stage can not be changed without having current year CAM.');
                      
                  }
           }
    }
    
    private static String getFY(){
        Date currentDate = System.today();
        Integer FY ;
        if(currentDate.month() > 3)
            FY = currentDate.year()+1;
        else
            FY = currentDate.year();
        return FY+'';
    }
}