trigger UpdateMLASignedOnAccount on MLA__c (after insert,after update,after delete) {
    Set<Id> accIdSet = new Set<Id>();
    List<Account> accListToUpdate = new List<Account>();
    for(MLA__c mla : trigger.isDelete ? trigger.old :  trigger.new){
        if(mla.Account__c != null && mla.Vatted_by__c != null && mla.Client_Signing_Authority__c != null && mla.SMAS_Signing_Authority__c != null){
            accIdSet.add(mla.Account__c);
        }
    }
    
    if((trigger.isInsert || trigger.isUpdate) && accIdSet.size() > 0){
        for(Account acc : [SELECT Id,MLA_Signed__c FROM Account WHERE Id IN :accIdSet]){
            if(!acc.MLA_Signed__c){
                acc.MLA_Signed__c = true;
                accListToUpdate.add(acc);
            }
        }
    }
    
    if(trigger.isDelete  && accIdSet.size() > 0){
        List<MLA__c> mlaList = [SELECT Id,Account__c FROM MLA__c WHERE Account__c IN :accIdSet];
        if(mlaList.size() <= 0){
            for(Id accId : accIdSet){
                Account acc = new Account();
                acc.Id = accId;
                acc.MLA_Signed__c = false;
                acc.Stage__c = 'S2';
                accListToUpdate.add(acc);
            }
        }
    }
    
    
    if(accListToUpdate.size() > 0)
        update accListToUpdate;
}