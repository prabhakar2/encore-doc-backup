trigger InvoiceValueRollupInAccount on Invoice__c (after insert,after update,after delete) {
    Map<Id,Decimal> invToAmtMap = new Map<Id,Decimal>();
    List<Account> accListToUpdate = new List<Account>();
    
    for(Invoice__c inv: trigger.isDelete ? trigger.old : trigger.new){
        if(inv.Client_Name__c != null){
            invToAmtMap.put(inv.Client_Name__c,0.0);
        }
    }
    
    if(invToAmtMap.keySet().size() > 0){
        for(AggregateResult agg : [SELECT SUM(Paid_Amount__c) totalAmt,Client_Name__c  accId
                                   FROM Invoice__c 
                                   WHERE Client_Name__c IN :invToAmtMap.keySet()
                                   AND Paid_Amount__c != null
                                   GROUP BY Client_Name__c]){
                                       
                                       invToAmtMap.put((Id)agg.get('accId'),(Decimal)agg.get('totalAmt'));
                                   }
        for(Id accId : invToAmtMap.keySet()){
            Account acc = new Account();
            acc.Id = accId;
            acc.Total_Invoiced_value__c = invToAmtMap.get(accId);
            accListToUpdate.add(acc);
        }
        
        if(accListToUpdate.size() > 0)
            update accListToUpdate;
    }
}