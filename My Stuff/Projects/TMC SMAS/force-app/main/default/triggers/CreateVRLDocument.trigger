trigger CreateVRLDocument on Quotation__c (after insert, after update) {
    List<VRL_Documents__c> listVRL=new List<VRL_Documents__c>();
    List<PDD__c> pddList = new List<PDD__c>();
    Set<Id> qtIdSet = new Set<Id>(); 
    List<Quotation__c> quoteListToUpdate = new List<Quotation__c>();
    List<Document_Master__c> domList=[SELECT id,Name,RecordType.Name, Required_Stage__c 
                                      FROM Document_Master__c 
                                      WHERE (RecordType.Name='VRL' OR RecordType.Name='PDD') 
                                      AND Active__c=true];
    for(Quotation__c qt:trigger.New){
        if((qt.Status__c == 'Approved' || 
            (label.Status_values_for_no_update_in_status.contains(qt.Status__c) 
             && qt.Status__c != 'Rejected')) 
           && qt.VRLDocumentsCreated__c == false){
               for(Document_Master__c docMas : domList){
                   
                   if(docMas.RecordType.Name == 'VRL'){
                       VRL_Documents__c vrlDoc=new VRL_Documents__c();
                       vrlDoc.Quotation__c=qt.Id;
                       vrlDoc.Document__c= docMas.id;
                       vrlDoc.Required_Stage__c= docMas.Required_Stage__c;
                       listVRL.add(vrlDoc);
                   }else if(docMas.RecordType.Name == 'PDD' ){
                       PDD__c pdd = new PDD__c();
                       pdd.Document_Master__c = docMas.id;
                       pdd.Quotation__c = qt.Id;
                       pddList.add(pdd);
                   }
               }
               qtIdSet.add(qt.Id);
           }
    }
    
    for(Quotation__c qt : [SELECT id,VRLDocumentsCreated__c 
                           FROM Quotation__c 
                           WHERE id IN :qtIdSet]){
                               qt.VRLDocumentsCreated__c = true;
                               quoteListToUpdate.add(qt);
                           }
    
    if(listVRL.size() > 0 && quoteListToUpdate.size() > 0){
        update quoteListToUpdate;
        insert listVRL;
    }
    if(pddList.size() > 0)
        insert pddList;
    
}