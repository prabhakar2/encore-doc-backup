/*
##################################################################################################################################
# Project Name..........: CSS McmAccounts Visibility Issue 
# File..................: Trigger : "Oppurtunity_Field_Update"
# Version...............: 1.0
# Created by............: Sunny Kumar
# Created Date..........: 17-Sep-2013
# Last Modified by......: Sunny Kumar
# Last Modified Date....: 01-Jan-2014
# Description...........: It will update Is_Domestic Flag true based on matched portfolio in Domestic portfolio object and
#                         Update all related/associated Child Cases.
# History...............: 25-Jun-2019 : Added logic to exclude DM Accounts (because the Domestic box is checked by the DM 
#                         Account Search instead)
#####################################################################################################################################
*/

trigger Oppurtunity_Field_Update on Opportunity (before insert,before update,after update)
{
  if(!FollowUpTaskHelper.hasAlreadyCreatedFollowUpTasks()){
   
    List<Opportunity> Opp_toUpdate = new List<Opportunity>();
    Set<String> oppName = new Set<String>();
    List<Domestic_Only_Portfolio__c> portfolios =[select Name from Domestic_Only_Portfolio__c];
    
    integer iCount=0;
    if(Trigger.isUpdate){
        for(Opportunity objOpp : trigger.new){
            if(Trigger.oldmap.get(objOpp.id).msport__c!=objOpp.msport__c){
                iCount=iCount+1;
            }
        }
    }
    
    
    for(Domestic_Only_Portfolio__c opps:portfolios)
            {
                oppName.add(opps.Name);
            }
    if(Trigger.isInsert)
    {    
        try
        { 
            for(Opportunity c : trigger.new)
            {
                if(c.DM_Account_External_ID__c==null){
                    if(oppName.contains(c.msport__c))
                    {
                        c.IS_DOMESTIC__c = true;
                    }
                    else
                    {
                        c.IS_DOMESTIC__c = false;
                    }
                }
            }
        }
        catch(exception e)
        {
           throw e;
        }     
    }
    else if(Trigger.isUpdate && Trigger.isBefore)
    {   
        
      
        for(Opportunity objOpp : trigger.new)
        {
            if(objOpp.DM_Account_External_ID__c==null){
                if(Trigger.oldmap.get(objOpp.id).msport__c==objOpp.msport__c && Trigger.oldmap.get(objOpp.id).Is_Domestic__c==true 
                        && objOpp.Is_Domestic__c==true && !oppName.contains(objOpp.msport__c))
                {
                         objOpp.IS_DOMESTIC__c = false;   
                }
                else if(Trigger.oldmap.get(objOpp.id).msport__c==objOpp.msport__c && Trigger.oldmap.get(objOpp.id).Is_Domestic__c==false 
                        && objOpp.Is_Domestic__c==true && !oppName.contains(objOpp.msport__c))
                {
                         objOpp.IS_DOMESTIC__c = false;   
                }
                else if(Trigger.oldmap.get(objOpp.id).msport__c==objOpp.msport__c && Trigger.oldmap.get(objOpp.id).Is_Domestic__c==true 
                            &&objOpp.Is_Domestic__c==false && oppName.contains(objOpp.msport__c))
                {
                        objOpp.IS_DOMESTIC__c = true;
                }
                else if((Trigger.oldmap.get(objOpp.id).msport__c!=objOpp.msport__c && iCount<=50))//This code will not wok for more than 50 msport changed value becuase of salesforce governor limit, For bulk data we are using the batch
                {
                    if(oppName.contains(objOpp.msport__c))
                    {
                       objOpp.IS_DOMESTIC__c = true;
                    }
                    else
                    {
                       objOpp.IS_DOMESTIC__c = false;
                    }
                }
            }
        } 
    }else if(Trigger.isUpdate && Trigger.isAfter && iCount<=50){  
        
          Set<String> setDomesticOppId=new Set<String>();
          Set<String> setNonDomesticOppId=new Set<String>();
          List<Case> lstUpdateCase=new List<Case>();
        
        for(Opportunity objOpp : trigger.new)
        { 
          
          if(objOpp.DM_Account_External_ID__c==null){
              if((Trigger.oldmap.get(objOpp.id).msport__c!=objOpp.msport__c))
                {
                    if(oppName.contains(objOpp.msport__c))
                    {
                        if(Trigger.oldMap.get(objOpp.id).Is_Domestic__c==false)
                        {
                            setDomesticOppId.add(objOpp.id);
                        }                   
                    }
                    else
                    {
                        if(Trigger.oldMap.get(objOpp.id).Is_Domestic__c==true)
                        {
                            setNonDomesticOppId.add(objOpp.id);
                        }
                    }
                }
           }
           
            if(setDomesticOppId.size()>0)
            {
                for(Case objCase:[Select id,Is_Domestic__c from Case where Opportunity__c!=null and Opportunity__c IN:setDomesticOppId and Is_Domestic__c=false])
                {
                    objCase.Is_Domestic__c=true;
                    lstUpdateCase.add(objCase);
                }
            } 
                  
            if(setNonDomesticOppId.size()>0)
            {
                for(Case objCase:[Select id,Is_Domestic__c from Case where Opportunity__c!=null and Opportunity__c IN:setNonDomesticOppId and Is_Domestic__c=true])
                {
                    objCase.Is_Domestic__c=false;
                    lstUpdateCase.add(objCase);
                }
            }
          
            if(lstUpdateCase.size()>0 && lstUpdateCase.size()<=7000) /*6000 limit to avoid DML opertaion limit which is 10000*/
            {
                 //trigger.new[0].addError('=========='+lstUpdateCase.size()); 
              // update lstUpdateCase;
                Database.update(lstUpdateCase,false);
            }          
         }
     }
   } 
}