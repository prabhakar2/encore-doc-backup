/*
#######################################################################################################
# History: 2019-06-25 - Consolidated this code with other case triggers and deactivated this code.
#                       You can find this code in the caseTrigger and caseTriggerHandler now.
#######################################################################################################
*/

trigger FieldUpdate on Case (before insert){

    Map<String, String> fieldListMap = new Map<String, String>{
'Employee Site'                         => 'Employee_Site__c',
'Complainant\'s State'                   => 'State_of_AEM_Complainant__c',
'Quality Assurance Violation 1'         => 'Quality_Assurance_Violation_1__c',
'Quality Assurance Violation 2'         => 'Quality_Assurance_Violation_2__c',
'Quality Assurance Violation 3'         => 'Quality_Assurance_Violation_3__c',
'Quality Assurance Violation 4'         => 'Quality_Assurance_Violation_4__c',
'Quality Assurance Violation 5'         => 'Quality_Assurance_Violation_5__c',
'Collector Code of Employee'            => 'Collector_Code__c',
'Collector Name'                        => 'Collector_Name__c',
'GM Collector Code'                     => 'GM_Collector_Code__c',
'AM Collector Code'                     => 'AM_Collector_Code__c',
'Employee Alias'                        => 'Alias_for_AEM__c',
'Employee Team'                      => 'Team_ID_for_AEM__c',
'Employee User ID'                      => 'User_ID_for_AEM__c',
'CC of Employee Submitting ACD'         => 'CC_of_Employee_Submitting_CDI__c',
'Source'                                => 'Source__c',
'CC of GM Submitting ACD'               => 'GM_Collector_Code__c',
'AEM Notes'                             => 'AEM_Notes__c',
'Email Address of Employee'             => 'Email_Address_of_Employee__c',
'Email Address of Group Manager of Employee' => 'Email_of_GM_of_Employee__c',
'Email Address of Division Manager of Employee' => 'Email_of_DM_of_Employee__c',
'Email Address of Director of Employee' => 'Email_of_Director_of_Employee__c',
'Does it Require CSS Investigation'     => 'Requires_CSS_Investigation__c',
'Check Date'                            => 'Posting_Check_Date_BG__c',
'Check Number'                          => 'Check_Number__c',
'SOL Date'                              => 'Posting_SOL_Date_BG__c',
'POC Date'                              => 'Posting_POC_Date_BG__c',
'Check Amount'                          => 'Posting_Check_Amount_BG__c',
'Is there a Judgment (009)'             => 'Judgment_Available__c',
'Misc'                                  => 'Misc__c',
'Removal Request'                       => 'Removal_Request__c',
'BK Case #'                             => 'BK_Case_Number__c',
'Has consumer made a payment to the issuer' => 'Consumer_Payment_to_Issuer__c',
'If yes, date of payment made to issuer' => 'CRP_Issuer_Payment_Date_Background__c',
'Is investigation regarding Credit Reporting' => 'Credit_Report_Investigation__c',
'If yes, which Credit Bureau'           => 'Credit_Bureau__c',
'Is investigation regarding payments'   => 'Payment_Investigation__c',
'If yes, has Posting investigated'      => 'Posting_Investigation__c',
'Describe results of Posting investigation' => 'Posting_Investigation_Details__c',
'Additional Information'                => 'Additional_Information',
'MCM Account'                           => 'MCM_Account_Number__c',
'Alleged Employee Misconduct 1'         => 'Alleged_Employee_Misconduct_1__c',
'Alleged Employee Misconduct 2'         => 'Alleged_Employee_Misconduct_2__c',
'Alleged Employee Misconduct 3'         => 'Alleged_Employee_Misconduct_3__c',
'Alleged Employee Misconduct 4'         => 'Alleged_Employee_Misconduct_4__c',
'Alleged Employee Misconduct 5'         => 'Alleged_Employee_Misconduct_5__c',
'Alleged Consumer Dissatisfaction Source'         => 'Consumer_Dissatisfaction_From__c',
'GM Submitting ACD'                     => 'GM_Submitting_ACD__c',
'Specific Alleged Employee Misconduct'  => 'Quality_Assurance_Violation_1__c',
'GUI Notes'                             => 'GUI_Notes__c',
'Alleged Employee Misconduct'           => 'Alleged_Employee_Misconduct_1__c',
'Allegation from Source'                => 'AEM_Notes__c' ,
'Consumer Name'                         => 'SuppliedName' ,
'Account'                               => 'Provided_Account_Number__c' ,
'Last 4 of SSN'                         => 'Last_4_of_SSN__c',
'Address'                                => 'Current_Address__c',
'City'                                   => 'Current_City__c',
'State'                                  => 'CCO_State__c',
'Zip Code'                               => 'Zip_Code__c',
'Email Address'                          => 'SuppliedEmail',
'Phone Number'                           => 'Phone_Number__c',
'CCO Category'                           => 'CCO_Category__c',
'CCO Sub Category'                       => 'CCO_Sub_Category__c',
'Supporting Documentation'               => 'Supporting_Documentation__c',
'Preferred Method of Contact'            => 'Preferred_Method_of_Contact__c',
'Additional Information'                 => 'Additional_Information__c',
'Desired Resolution'                     => 'Desired_Resolution__c',
'Type'                                   => 'Type',
'Case Type'                              => 'Case_Type__c'   




    };

    List<String> descpSplitList;
    List<String> labelValuePairSplitList;
    for(Case cas : trigger.new){
        if(!String.isBlank(cas.Description)){
            descpSplitList = cas.Description.split('\n');
       
            for(String labelValuePair : descpSplitList){
                labelValuePairSplitList = labelValuePair.split(':');
                if (
                    labelValuePairSplitList.size() > 1 &&
                    fieldListMap.containsKey(labelValuePairSplitList[0])
                ) {
                    cas.put(fieldListMap.get(labelValuePairSplitList[0]), labelValuePairSplitList[1]);
                }
            }
        }
    }
}