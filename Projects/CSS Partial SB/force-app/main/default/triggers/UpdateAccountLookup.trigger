trigger UpdateAccountLookup on Case (before insert, before update) {
if (trigger.isBefore && (trigger.isInsert||trigger.isUpdate))
{   


Map<String, String> myPickListMap = new Map<String, String> {'Called restricted number(s) that had been marked as "Do Not Call"'=>'HAR1', 
    'Created or processed payment(s) without the consumer\'s/payee\'s consent' => 'UAP1', 
    'Threatened legal action on an account(s) not in the pre-legal process'=>'LEG1', 
    'Did not avoid discussing the account with a 3rd party'=>'TPD1', 
    'Suggested/inferred that the call was being made from a credit reporting agency, government office, law firm or legal office'=>'FMR1', 
    'Did not provide accurate information pertaining to credit reporting of an account(s)'=>'FMR2', 
    'Did not inform the consumer the correct reporting status of the account once paid'=>'FMR3', 
    'Threatened to communicate false credit information to a credit reporting agency'=>'FMR4', 
    'Did not provide accurate information to a consumer which was supported by the account information in MCM\'s technology systems and/or available media on the account'=>'FMR5', 
    'Did not provide the correct balance or state the account balance to the consumer'=>'FMR6', 
    'Used profane and/or abusive language on a call'=>'FDCPA1', 
    'Called a phone number outside of the designated time zone without authorization to do so'=>'FDCPA2', 
    'Threatened violence on a phone call'=>'FDCPA3', 
    'Threatened to ruin the caller\'s reputation'=>'FDCPA4', 
    'Contacted the consumer immediately after the consumer terminated the call'=>'FDCPA5', 
    'Called a number multiple times in a given day without the consumer\'s/3rd parties\' consent'=>'FDCPA6', 
    'Contacted a consumer after a Cease And Desist had been documented on the account'=>'FDCPA7', 
    'Contacted a consumer at a place of employment when the consumer had informed MCM that they could not receive calls at work or if state-specific requirements prohibit calls to places of employment'=>'FDCPA8', 
    'Asked a third party for an alternate contact phone number for the consumer when a home phone number for the consumer was already available'=>'FDCPA9', 
    'Inappropriately and deliberately hung up on a call without advising that the call would be terminated'=>'FAIL1', 
    'Did not transfer a call per caller\'s request to a U.S. based AM'=>'FAIL2', 
    'Intentionally violating the Account Manager Manual Call Policy'=>'FAIL3', 
    'Was unprofessional or rude'=>'FAIL4', 
    'Did not follow MCM\'s answering machine/voicemail policies and related FDCPA policy, including creating a false sense of urgency*'=>'FAIL5', 
    'Purposely misrepresented what occurred on a call'=>'FAIL6', 
    'Did not protect the privacy of consumer\'s data by saving bank account and/or credit card information in the notes or on reminder screens'=>'FAIL7', 
    'Did not disclose their full and correct identity (i.e. approved alias) and misrepresented their identity (title, position or alias)'=>'FAIL8', 
    'Offered a settlement that was not in line with the Call Center Account Settlement Policy and/or did not honor a valid marketing letter offer on an account'=>'FAIL9', 
    'Did not establish Right Party contact in a manner compliant with FDCPA policy'=>'FAIL10', 
    'Did not state the mini-Miranda before providing confidential account information'=>'FAIL11', 
    'Did not state the Debt Collection Company disclosure before discussing confidential account information'=>'FAIL12', 
    'Did not transfer a call to a supervisor when asked to do so by a caller'=>'FAIL13', 
    'Did not disclose their physical location when specifically asked by a consumer'=>'FAIL14', 
    'Did not properly document the account'=>'FAIL15', 
    'Did not add the appropriate warning codes to an account'=>'FAIL16', 
    'Did not disable wrong party numbers'=>'FAIL17', 
    'Did not enter the correct dialer disposition code'=>'FAIL18', 
    'Did not speak to the consumer prior to taking ownership of an account'=>'FAIL19', 
    'Did not follow all other applicable MCM policies'=>'FAIL20', 
    'Did not follow the approved Dispute procedure including appropriate warning code, explanation of the process to the consumer and notating account regarding dispute'=>'FAIL21',
    'Set up a payment plan in which the Method, Amount, Date did not match the consumer\'s intent and understanding'=>'UAP2',
    'Did not provide the Quality Assurance statement during the call/first available opportunity'=>'FDCPA10',
    'Did not follow the correct procedures when notified by the consumer that the consumer is represented by an attorney'=>'FDCPA11',
    'Did not state proper disclosures to the consumer. (Disclosures include reason for the communication, name of the creditor, MCM\'s company name, employee\'s name/alias)'=>'FDCPA12',
    'Intentionally contacted a consumer with attorney POA on file'=>'FDCPA13',
    'Did not restate the proper disclosures with each person that joined the call'=>'FDCPA14',
    'Disclosed account information to an unauthorized 3rd party'=>'FDCPA15',
    'Handled an ineligible account(s) (e.g. Bankruptcy, Deceased, and Compliance)'=>'FAIL22',
    'Did not use the approved verbiage when discussing an account in the pre-legal queues'=>'FAIL23',
    'Did not follow all applicable state law exceptions as noted in the State Alert Information box in GUI'=>'FAIL24',
    'Did not follow the correct procedures on handling an account with a RT1 warning code or an account that is Inside the Validation period'=>'FAIL25',
    'Failed to properly review notes on account, resulting in a mishandling of an account'=>'FAIL26',
    'Knowingly called consumer\'s place of employment within the 30 days of attempting to contact the consumer at the consumer\'s residence. (This attempt includes senidng a letter and allowing enough time for the letter to return)'=>'FAIL27',
    'Took over call, did not properly document the account'=>'FAIL28',
    'Did not service the account in accordance with the Consumer Bill of Rights' => 'FAIL29',
    'Threatened arrest/seizure of property' => 'FDCPA16',
    '--None--'=>'--None--'};

// Map to account ID
set<Id> OpportunityIds = new set<ID>();
  for(Case c: trigger.new)
  { 
    OpportunityIds.add(c.Opportunity__c);
  }
  Map<Id, Id> OpportunityToAccountMap = new Map<Id, Id>();
  for(Opportunity o :[select Id, AccountId from Opportunity where id in: OpportunityIds])
  {
       OpportunityToAccountMap.put(o.Id,o.AccountId);
  }
  for(Case c: trigger.new)
  {
       c.Accountid = OpportunityToAccountMap.get(c.Opportunity__c);
       }
       
       
       
       if (trigger.isBefore && (trigger.isInsert||trigger.isUpdate))
{   
    
  for(Case c: trigger.new) {
  
    c.Compliance_Violation_Code_1__c = myPickListMap.get(c.Quality_Assurance_Violation_1__c);
c.Compliance_Violation_Code_2__c = myPickListMap.get(c.Quality_Assurance_Violation_2__c);
c.Compliance_Violation_Code_3__c = myPickListMap.get(c.Quality_Assurance_Violation_3__c);
c.Compliance_Violation_Code_4__c = myPickListMap.get(c.Quality_Assurance_Violation_4__c);
c.Compliance_Violation_Code_5__c = myPickListMap.get(c.Quality_Assurance_Violation_5__c);

  }
  }
}
}