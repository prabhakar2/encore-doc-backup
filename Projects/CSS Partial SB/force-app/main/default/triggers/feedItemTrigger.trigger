trigger feedItemTrigger on FeedItem (before update, before delete) {

    if(Trigger.isBefore && Trigger.isUpdate){
        /*
        =================================================================
        This is not needed now because the option to edit chatter posts
        is not enabled under chatter settings. If it is ever enabled,
        then you will need to uncomment this section of the code.
        =================================================================
        feedItemTriggerHandler.restrictEdit(Trigger.OldMap, Trigger.NewMap);
        =================================================================
        */
    }
    if(Trigger.isBefore && Trigger.isDelete){
        feedItemTriggerHandler.restrictDelete(Trigger.New, Trigger.Old);
    }
}