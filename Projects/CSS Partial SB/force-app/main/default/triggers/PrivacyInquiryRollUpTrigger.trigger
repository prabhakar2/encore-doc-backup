trigger PrivacyInquiryRollUpTrigger on Privacy_Inquiry__c (before insert , before update,after insert,after Update) {
    if(trigger.isBefore){
     // record date to update the reset clock in 366 days
        PrivacyInquiryRollUpTriggerHelper.getInstance().dataValidation(trigger.new);
    }
    
    if(trigger.isAfter){
     // roll up the field values counting 
        PrivacyInquiryRollUpTriggerHelper.getInstance().dataRollUp(trigger.new);
    }
}