trigger ComplianceAccountUpdate on Compliance__c (before insert, before update) {
if (trigger.isBefore && (trigger.isInsert||trigger.isUpdate))
{   
// Map to account ID
set<Id> OpportunityIds = new set<ID>();
  for(Compliance__c c: trigger.new)
  { 
    OpportunityIds.add(c.MCM_Account__c);
  }
  Map<Id, Id> OpportunityToAccountMap = new Map<Id, Id>();
  for(Opportunity o :[select Id, AccountId from Opportunity where id in: OpportunityIds])
  {
       OpportunityToAccountMap.put(o.Id,o.AccountId);
  }
  for(Compliance__c c: trigger.new)
  {
       c.Account__c = OpportunityToAccountMap.get(c.MCM_Account__c);
  }
  
}
}