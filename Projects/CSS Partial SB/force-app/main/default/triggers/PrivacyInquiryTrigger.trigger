/***********************************************************************************
 * Name : PrivacyInquiryTrigger
 * Description: Trigger for mapping Individual with Privacy Inquiry.
 * Created By : Prabhakar Joshi
 * Created Date:26-11-2019
 * *******************************************************************************/
trigger PrivacyInquiryTrigger on Privacy_Inquiry__c (before insert) {
    if(trigger.isBefore){
        if(trigger.isInsert){
            PrivacyInquiryTrigger_Helper.createInstance(trigger.new);
        }
    }
     
    if(trigger.isAfter){
        
    }
}