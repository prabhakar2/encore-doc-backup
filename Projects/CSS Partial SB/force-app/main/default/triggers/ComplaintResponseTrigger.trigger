trigger ComplaintResponseTrigger on Complaint_Response__c (before update) {

    //before update handling
    if(Trigger.isBefore && Trigger.IsUpdate){
       ComplaintResponseHandler.updateRollUpFields(Trigger.New);        
    }

}