/*
##################################################################################################################################
# Project Name..........: Case Trigger - 1 Trigger Per Object
# File..................: caseTrigger
# Version...............: 1.0
# Created by............: Katerina Ames
# Created Date..........: 05-Apr-2019
# Last Modified by......: Katerina Ames
# Last Modified Date....: 05-Apr-2019
# Description...........: Primary trigger on the Case object.
#####################################################################################################################################
*/

trigger caseTrigger on Case (before insert, before update, after insert, after update) {
    System.debug('*************** Testing By Dev*****************');
    if(Trigger.isBefore && Trigger.isInsert){
       caseTriggerHandler.checkCRAMInsert(Trigger.new);
       caseTriggerHandler.fieldUpdate(Trigger.new);
    }
    if(Trigger.isBefore && Trigger.isUpdate){
       caseTriggerHandler.checkCRAMUpdate(Trigger.oldMap, Trigger.new);
       caseTriggerHandler.regexDMAccountCommentary(Trigger.oldMap, Trigger.newMap);
    }
   
}