trigger Opportunity_UpdateCase on Opportunity (after update) {
        List<Case> casesToBeUpdated = new List<Case>();
        
        
        Map<Id,Id> oppIdAccIdMap = new Map<Id,Id>();
        Map<Id, Opportunity> oldOpportunityMap = new Map<Id, Opportunity>(Trigger.old);
        Set<Id> accIdSet = new Set<Id>();
        //Check records where Account is changed
        for(Opportunity opp:Trigger.new)
        {
            if(opp.AccountId!=null && opp.AccountId!=oldOpportunityMap.get(opp.Id).AccountId)
            {
               oppIdAccIdMap.put(opp.Id,opp.AccountId); 
               accIdSet.add(opp.AccountId);
            }
        }
        
        Map<Id, Id> accIdContactIdMap = new Map<Id,Id>();
        List<Contact> contactList = [select Id, AccountId from Contact where AccountId In :accIdSet];
        for(Contact c:contactList)
        {
          accIdContactIdMap.put(c.AccountId, c.Id);
        }
        
        if(oppIdAccIdMap.size()>0)
        {
            List<Case> caseList = [select Id,AccountId,Opportunity__c, ContactId from Case where Opportunity__c in :oppIdAccIdMap.keySet()];
            
            if(caseList!=null && caseList.size()>0)
            {
                for(Case mcase:caseList)
                {
                    if(oppIdAccIdMap.get(mcase.Opportunity__c)!=null){
                        mcase.AccountId = oppIdAccIdMap.get(mcase.Opportunity__c);
                        Id cid = accIdContactIdMap.get(oppIdAccIdMap.get(mcase.Opportunity__c));
                        System.debug('\n\n ### CID = '+ cid +','+oppIdAccIdMap.get(mcase.Opportunity__c));
                        if(cid!=null)
                            mcase.ContactId = cid;
                            
                        casesToBeUpdated.add(mcase);
                    }
                }
            }
            System.debug('### casesToBeUpdated' +casesToBeUpdated);
            Database.SaveResult[] srlist = Database.update(casesToBeUpdated,false);
            String errorMsg='';
            for (Database.SaveResult sr : srList)
            {   
            
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Case created/updated :'+(sr.getId()));
                    
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        errorMsg= '<br/>\nThe following error has occurred.'+                 
                                   err.getStatusCode() + ': ' + err.getMessage()+
                                '<br/><br/>\n\nCase fields that affected this error: ' + err.getFields();
                        System.debug(errorMsg);
                                
                    }
                }
             }
        }
}