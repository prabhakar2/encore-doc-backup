trigger RollUpTriggerPrivacyInquiry on Privacy_Inquiry__c (After insert,After update, After delete, After undelete) {

    Map<Id, Individual> individualMaptoUpdate = new Map<Id, Individual> ();
    List<Privacy_Inquiry__c> PrivacyInquiryList = new List<Privacy_Inquiry__c>();
    PrivacyInquiryList = Trigger.Isinsert || Trigger.isUndelete ? Trigger.New : Trigger.old;
    
    for(Privacy_Inquiry__c pi: PrivacyInquiryList ){
        if(pi.Individual__c !=null){
            if(!individualMaptoUpdate.containsKey(pi.Individual__c))
                individualMaptoUpdate.put(pi.Individual__c, new individual(Id = pi.Individual__c, Total_Privacy_Inquiry__c=0));
        }
    }
    
    
    List<AggregateResult> aggResult = new List<AggregateResult>();
    aggResult = [Select count(id) , Individual__c From Privacy_Inquiry__c Where Individual__c IN : individualMaptoUpdate.keySet() Group By Individual__c];
    
    for(AggregateResult ar : aggResult){
        Id individualObjId = (ID)ar.get('Individual__c');
        if(individualMaptoUpdate.containsKey(individualObjId)){
            Individual ind = individualMaptoUpdate.get(individualObjId);
            ind.Total_Privacy_Inquiry__c= (Integer)ar.get('expr0');
            individualMaptoUpdate.put(individualObjId , ind);
        }
    }
    
    if(individualMaptoUpdate.size() > 0)
    update individualMaptoUpdate.values();
    
}