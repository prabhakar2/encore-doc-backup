trigger Opp_Field_Update on Domestic_Only_Portfolio__c  (before insert,before update) {
List<Opportunity> Opp_toUpdate = new List<Opportunity>();
     Set<String> oppName = new Set<String>();
     List<Case> lstCases = new List<Case>();
     Set<String> accntID =  new Set<String>();
    // List<Domestic_Only_Portfolio__c> portfolios =[select Name from Domestic_Only_Portfolio__c];
     //system.debug('+++++'+portfolios.size());
        for(Domestic_Only_Portfolio__c opps:Trigger.new)
            {
                oppName.add(opps.Name);
            }
            system.debug('+++++'+oppName.size());
           try
              { 
                  for(Opportunity c : [SELECT IS_DOMESTIC__c, Name FROM Opportunity WHERE id!=null and msport__c !=NULL and msport__c in :oppName])
                  {
                     c.IS_DOMESTIC__c = true;
                     Opp_toUpdate.add(c);
                     accntID.add(c.Name);
                  }
                   system.debug('+++++'+accntID.size());
                 for(Case cs :[select Is_Domestic__c FROM Case WHERE Opportunity__r.Name in : accntID]){
                  cs.IS_DOMESTIC__c = true;
                  lstCases.add(cs);
                 }
                  system.debug('+++++'+lstCases.size());
              }
        catch(DmlException e){
          system.debug('+++++'+e.getMessage());
          }
    
      if(Opp_toUpdate.size()>0)
          update Opp_toUpdate;
      if(lstCases.size()>0){
          //update lstCases;
          Database.SaveResult[] srlist = Database.update(lstCases,false);
            String errorMsg='';
            for (Database.SaveResult sr : srList)
            {   
            
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Case created/updated :'+(sr.getId()));
                    
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        errorMsg= '<br/>\nThe following error has occurred.'+                 
                                   err.getStatusCode() + ': ' + err.getMessage()+
                                '<br/><br/>\n\nCase fields that affected this error: ' + err.getFields();
                        System.debug(errorMsg);
                                
                    }
                }
             }
          }
          
}