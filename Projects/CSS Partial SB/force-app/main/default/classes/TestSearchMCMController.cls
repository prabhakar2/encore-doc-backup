/**
AUTHOR:    LENNARD SANTOS
COMPANY:    CLOUDSHERPAS
DATE CREATED:  FEB 21 2013
DESCRIPTION: Test coverage for the Custom Search controller(SearchMCMController) 
HISTORY: 2-21-2013  Created.
         4-24-2013  Modified.
**/
@isTest(seeAllData=True)
private class TestSearchMCMController{
    public String SFDCMCMAccountId;
    public Boolean outpStatus;
    public String outpErrorMsg;
    //Test if value is null when called by getValidateInput method
    static testMethod void nullTestValidate(){
        SearchMCMController SearchValid = new SearchMCMController();
        SearchValid.SearchNumber = '';
        SearchValid.getValidateInput();
        System.AssertEquals(SearchValid.SearchNumber,'');
    } 
    //Test if value is not numeric
    static testMethod void invalidSearch() {
        SearchMCMController SearchValid = new SearchMCMController();
        SearchValid.SearchNumber = 'abc';
        SearchValid.getValidateInput();
        System.AssertEquals(SearchValid.SearchNumber,'abc');
    }
    //test for valid search value returns true status response
    static testMethod void validSearch() {
        Test.StartTest();
        Test.setMock(WebServiceMock.class, new TestCallWebSvcTrue());
        SearchMCMController SearchValid = new SearchMCMController();
        SearchValid.SearchNumber = '1234';
        SearchValid.getValidateInput();
        Test.StopTest();
        System.AssertEquals(SearchValid.OpportunityID,'/test123');
        System.Assert(SearchValid.successStr=='Success');       
        //verify that search number and message are cleared up on success
        System.AssertEquals(SearchValid.SearchNumber,'');
        System.Assert(SearchValid.Message=='');

    }
    static testMethod void validSearchNotFound() {
        Test.StartTest();
        Test.setMock(WebServiceMock.class, new TestCallWebSvcFalse());
        SearchMCMController SearchValid = new SearchMCMController();
        SearchValid.SearchNumber = '1234';
        SearchValid.getValidateInput();
        Test.StopTest();
        System.AssertEquals(SearchValid.OpportunityID,'');
        System.Assert(SearchValid.Message!='');   
        System.debug('** Not Found **' + SearchValid);
        System.AssertEquals(SearchValid.SearchNumber,'1234');

    }
    //Mock Response for web service call upon success
    public class TestCallWebSvcTrue implements WebServiceMock{
        public void doInvoke(
             Object stub,
             Object request,
             Map<String,Object> response,
             String endpoint,
             String soapAction,
             String requestName,
             String responseNS,
             String responseName,
             String responseType
         ){
             wwwApprouterComConnectorsResponse57_V3.Output_element outp = new wwwApprouterComConnectorsResponse57_V3.Output_element();
             outp.SFDCMCMAccountId = 'test123';
             outp.Status = true;            
             outp.ErrorMsg = 'Success';
             wwwApprouterComConnectorsResponse57_V3.OutputParams o = new wwwApprouterComConnectorsResponse57_V3.OutputParams();
             o.Output = outp;
             response.put('response_x', o );
             system.debug('****' + response);
         }

    }

    
    //Mock Response for web service upon failure
    public class TestCallWebSvcFalse implements WebServiceMock{
        public void doInvoke(
             Object stub,
             Object request,
             Map<String,Object> response,
             String endpoint,
             String soapAction,
             String requestName,
             String responseNS,
             String responseName,
             String responseType
         ){
             wwwApprouterComConnectorsResponse57.Output_element outp = new wwwApprouterComConnectorsResponse57.Output_element();            
             outp.Status = false;
             outp.ErrorMsg = 'MCM Account not found';
             wwwApprouterComConnectorsResponse57.OutputParams o = new wwwApprouterComConnectorsResponse57.OutputParams();
             o.Output = outp;
             response.put('response_x', o );
             system.debug('****' + response);
         }

    }   
}