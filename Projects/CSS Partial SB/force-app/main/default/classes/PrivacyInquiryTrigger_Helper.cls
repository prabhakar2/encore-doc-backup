/***********************************************************************************
 * Class Name : PrivacyInquiryTrigger_Helper
 * Description: Helper class for PrivacyInquiryTrigger
 * Created By : Prabhakar Joshi
 * Created Date:26-11-2019
 * *******************************************************************************/
public class PrivacyInquiryTrigger_Helper {
    public static PrivacyInquiryTrigger_Helper instance = NULL;
    public static List<Privacy_Inquiry__c> recordList;
    
    private PrivacyInquiryTrigger_Helper(){
        processData();
    }
    
    public static PrivacyInquiryTrigger_Helper createInstance(List<Privacy_Inquiry__c> triggerNew){
        recordList = triggerNew;
        if(instance == NULL){
            instance = new PrivacyInquiryTrigger_Helper();
        }
        return instance;
    }
    
    private static void processData(){
        Map<String,Id> uniqueKeyToIndMap = new Map<String,Id>();
        Map<String,Privacy_Inquiry__c> keyToInqMap = new Map<String,Privacy_Inquiry__c>();
        Id dummyRecId;
        
        /*****To create and map the dummy Individual with PI record in case of where the unique Id not created *****
		************************************************************************************************************/
        List<Individual> dummyIndRecord = [SELECT Id,FirstName,LastName,Last_4_SSN__c,Unique_Identifier__c 
                                           FROM Individual
                                           WHERE Name = 'Can Not Find Individual' LIMIT 1];
        
        if(dummyIndRecord.size() > 0){
            dummyRecId = dummyIndRecord[0].Id;
        }else{
            Individual ind = new Individual();
            ind.LastName = 'Can Not Find Individual';
            insert ind;
            dummyRecId = ind.Id;
        }
        /**********************************************************************************************************/
        
        
        for(Privacy_Inquiry__c pi : recordList){
            if(pi.First_Name__c != NULL && pi.Last_Name__c != NULL && pi.Last_4_SSN__c != NULL){
                String key = pi.First_Name__c.subString(0,1).toUpperCase()+pi.Last_Name__c.subString(0,1).toUpperCase()+pi.Last_4_SSN__c+pi.First_Name__c.length()+pi.Last_Name__c.length();
                uniqueKeyToIndMap.put(key,NULL);
                keyToInqMap.put(key,pi);
            }else{
                pi.Individual__c = dummyRecId;
            }
        }
        
        if(uniqueKeyToIndMap.keySet().size() > 0){
            for(Individual ind : [SELECT Id,FirstName,LastName,Last_4_SSN__c,Unique_Identifier__c 
                                  FROM Individual 
                                  WHERE Unique_Identifier__c IN :uniqueKeyToIndMap.keySet()]){
                                      uniqueKeyToIndMap.put(ind.Unique_Identifier__c,ind.Id);
                                  }
            
            List<Individual> indListToInsert = new List<Individual>();
            for(String key : keyToInqMap.keySet()){
                if(uniqueKeyToIndMap.containsKey(key) && uniqueKeyToIndMap.get(key) == NULL){
                    Individual ind = new Individual();
                    ind.FirstName = keyToInqMap.get(key).First_Name__c != NULL ? keyToInqMap.get(key).First_Name__c : '';
                    ind.LastName = keyToInqMap.get(key).Last_Name__c != NULL ? keyToInqMap.get(key).Last_Name__c : '';
                    ind.Last_4_SSN__c = keyToInqMap.get(key).Last_4_SSN__c != NULL ? keyToInqMap.get(key).Last_4_SSN__c : '';
                    ind.Unique_Identifier__c = key;
                    ind.Alternative_Name__c = keyToInqMap.get(key).Alternative_Name__c != NULL ? keyToInqMap.get(key).Alternative_Name__c : '';
                    ind.Street_Address__c = keyToInqMap.get(key).Street_Address__c != NULL ? keyToInqMap.get(key).Street_Address__c : '';
                    ind.City__c = keyToInqMap.get(key).City__c != NULL ? keyToInqMap.get(key).City__c : '';
                    ind.State__c = keyToInqMap.get(key).State__c != NULL ? keyToInqMap.get(key).State__c : '';
                    ind.Zip_Code__c = keyToInqMap.get(key).Zip_Code__c != NULL ? keyToInqMap.get(key).Zip_Code__c : '';
                    ind.Phone_Number__c = keyToInqMap.get(key).Phone_Number__c != NULL ? keyToInqMap.get(key).Phone_Number__c : '';
                    ind.Certify_Regulation_Eligibility__c = keyToInqMap.get(key).Certify_Regulation_Eligibility__c != NULL ? keyToInqMap.get(key).Certify_Regulation_Eligibility__c : false;
                    ind.Address_Line_2__c = keyToInqMap.get(key).Address_Line_2__c != NULL ? keyToInqMap.get(key).Address_Line_2__c : '';
                    ind.Email__c = keyToInqMap.get(key).Relationship_to_Company__c == 'Consumer' ? keyToInqMap.get(key).Email__c != NULL ? keyToInqMap.get(key).Email__c : '' : keyToInqMap.get(key).Authorized_Agent_Email__c != NULL ? keyToInqMap.get(key).Authorized_Agent_Email__c : '';
                    indListToInsert.add(ind);
                }
            }
            if(indListToInsert.size() > 0){
                insert indListToInsert;
                
                for(Individual ind : indListToInsert){
                    if(uniqueKeyToIndMap.containsKey(ind.Unique_Identifier__c) && uniqueKeyToIndMap.get(ind.Unique_Identifier__c) == NULL){
                        uniqueKeyToIndMap.put(ind.Unique_Identifier__c,ind.Id);
                    }
                }
            }
            if(uniqueKeyToIndMap.keySet().size() > 0){
                for(Privacy_Inquiry__c pi : recordList){
                    String key = pi.First_Name__c.subString(0,1).toUpperCase()+pi.Last_Name__c.subString(0,1).toUpperCase()+pi.Last_4_SSN__c+pi.First_Name__c.length()+pi.Last_Name__c.length();
                    pi.Individual__c = uniqueKeyToIndMap.get(key);
                    pi.Unique_Identifier__c = key;
                }
            }
        }
    }
}