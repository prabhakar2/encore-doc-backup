/*******************************************************************************************
* Class Name  : CCPA_pdfPageController
* Description : Controller to generate the PDF for Privacy Inquiry Record details
* Created By  : Shivangi Srivastava 
* Created Date: 17-Nov-2019
* Modified By : Prabhakar Joshi
* *****************************************************************************************/

public class CCPA_pdfPageController {
    
    // Map for 'Doesn\'t Meet Requirements' Letter type because it exceeds the character limit in custom setting.
    public static Final Map<String,String> ExceptionalValueMap = new Map<String,String>{'Doesn\'t Meet Requirements'=>'No Meet Requirements'};
    
    public string ccpa_language {get; private set;} // To set the language for PDF.
    public string LetterCode 	{get; private set;} // To set the letter Code according to selected letter type.
    public string BottomLine 	{get; private set;} // To set the bottom line of the PDF.
    
    
    /************************************* Constructor **************************************/
    public CCPA_pdfPageController(ApexPages.StandardController stdController){
        Id privacyId =  ApexPages.currentPage().getparameters().get('Id'); //To set the record Id.
        List<Privacy_Inquiry__c> privacyInqList = [SELECT Id,Name,First_Name__c,Last_Name__c,Letter__c,
                                                   Template_Language__c,Company_Selection__c,Relationship_to_Company__c,
                                                   Authorized_Agent_First_Name__c,Authorized_Agent_Last_Name__c
                                                   FROM Privacy_Inquiry__c WHERE Id =: privacyId LIMIT 1];
        if(privacyInqList.size() > 0){
            setPDF(privacyInqList[0]);
        }
    } 
    
    /***************** Method to set the PDF Content from the Custom Metadata and Custom Setting *****************
	**************************************************************************************************************/
    private void setPDF(Privacy_Inquiry__c pi){
        Map<String,CCPA_Language__mdt> metaDataMap = new Map<String,CCPA_Language__mdt>();
        for(CCPA_Language__mdt langManager : [SELECT Id, language__c, Label, DeveloperName, MasterLabel, QualifiedApiName, 
                                              NamespacePrefix 
                                              FROM CCPA_Language__mdt]){
                                                  metaDataMap.put(langManager.Label,langManager);
                                              }
        if(metaDataMap.keySet().size() > 0){
            String key = '';
            if(ExceptionalValueMap.containsKey(pi.Letter__c)){
                key = ExceptionalValueMap.get(pi.Letter__c)+'@@'+pi.Template_Language__c;
            }else{
                key = pi.Letter__c+'@@'+pi.Template_Language__c;
            }
            
            if(metaDataMap.containsKey(key)){
                if(metaDataMap.get(key) != NULL && metaDataMap.get(key).language__c != NULL){
                    ccpa_language = metaDataMap.get(key).language__c;
                    if(ccpa_language.containsIgnoreCase('@FirstName@')){
                        ccpa_language = ccpa_language.replace('@FirstName@', pi.Relationship_to_Company__c =='Authorized Agent' && pi.Letter__c == 'Need Authorization' ? pi.Authorized_Agent_First_Name__c != NULL ? pi.Authorized_Agent_First_Name__c : '' : pi.First_Name__c != NULL ? pi.First_Name__c : '');
                    }
                    if(ccpa_language.containsIgnoreCase('@C_FirstName@')){
                        ccpa_language = ccpa_language.replace('@C_FirstName@', pi.Relationship_to_Company__c =='Authorized Agent' && pi.Letter__c == 'Need Authorization' ? pi.First_Name__c  != NULL ? pi.First_Name__c : '': pi.First_Name__c != NULL ? pi.First_Name__c : '');
                    }
                    if(ccpa_language.containsIgnoreCase('@LastName@')){
                        ccpa_language = ccpa_language.replace('@LastName@', pi.Relationship_to_Company__c =='Authorized Agent' && pi.Letter__c == 'Need Authorization' ? pi.Authorized_Agent_Last_Name__c != NULL ? pi.Authorized_Agent_Last_Name__c : '' : pi.Last_Name__c != NULL ? pi.Last_Name__c : '');
                    }
                    if(ccpa_language.containsIgnoreCase('@C_LastName@')){
                        ccpa_language = ccpa_language.replace('@C_LastName@', pi.Relationship_to_Company__c =='Authorized Agent' && pi.Letter__c == 'Need Authorization' ? pi.Last_Name__c  != NULL ? pi.Last_Name__c : '': pi.Last_Name__c != NULL ? pi.Last_Name__c : '');
                    }
                }
                
                if(Privacy_Letter_Code_Manager__c.getValues(key) != NULL && Privacy_Letter_Code_Manager__c.getValues(key).Letter_Code__c != NULL)
                    LetterCode = Privacy_Letter_Code_Manager__c.getValues(key).Letter_Code__c;
            }
        }
        Map<String,String> bottomLineMap = new Map<String,String>();
        for(Bottom_Line_Manager__mdt bottomLineMgr : [SELECT Id, Bottom_Line__c, Label, DeveloperName, MasterLabel, QualifiedApiName
                                                      FROM Bottom_Line_Manager__mdt]){
                                                          bottomLineMap.put(bottomLineMgr.label,bottomLineMgr.Bottom_Line__c);
                                                      }
        
        if(bottomLineMap.keySet().size() > 0){
            if(bottomLineMap.containsKey(pi.Template_Language__c) && bottomLineMap.get(pi.Template_Language__c) != NULL){
                BottomLine = bottomLineMap.get(pi.Template_Language__c);
                if(BottomLine.containsIgnoreCase('@RecordName@')){
                    BottomLine = BottomLine.replace('@RecordName@', pi.Name);
                }
            } 
        }
    }
}