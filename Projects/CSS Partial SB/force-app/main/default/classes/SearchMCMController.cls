/**
AUTHOR:    LENNARD SANTOS
COMPANY:    CLOUDSHERPAS
DATE CREATED:  JANUARY 16 2013
DESCRIPTION: Controller class for Custom Search Page
HISTORY: 1-16-2013  Created.
         2-13-2013 Updated based on sample WSDL file.
         5-09-2013 Added reference to mcm account search custom setting for service endpoint
         
**/

public class SearchMCMController{
    public String SearchNumber{get;set;}
    public String Message{get;set;}
    Pattern searchPattern = Pattern.compile('[0-9]*');
    public String OpportunityID{get;set;}
    public String ConsName{get;set;}
    public String successStr {get;set;}

    public void getValidateInput(){

        Matcher searchMatcher = searchPattern.Matcher(SearchNumber);
        if(SearchNumber==''){
            OpportunityID='';
            ConsName='';
            Message=Label.Custom_Search_Missing_Value;
        } else if(!searchMatcher.Matches()){ 
            OpportunityID='';
            ConsName='';
            Message=Label.Custom_Search_Invalid_Value;
        } else {
            searchCI();
        }
    }
    
    public void searchCI(){ 
        
        Message = '';
        successStr = '';
        
        wwwApprouterComConnectorsRequest579_V3.Input_element inputElem = new wwwApprouterComConnectorsRequest579_V3.Input_element();
                
        //inputElem.MCMAccount = Integer.valueOf(Long.valueOf(SearchNumber)); 
        inputElem.MCMAccount = Decimal.valueOf(SearchNumber);         
        wwwApprouterComConnectorsWsdl_V3.TestUpdSFCDPort  updCall = new wwwApprouterComConnectorsWsdl_V3.TestUpdSFCDPort();
        //retrieve endpoint from custom setting
        MCM_Account_Search_Settings__c MCMSettings =  MCM_Account_Search_Settings__c.getOrgDefaults();
        updCall.endpoint_x = MCMSettings.Endpoint__c;
        
        updCall.clientCertName_x = 'MCMAccountSearch';
        updCall.timeout_x = 120000; 
        wwwApprouterComConnectorsResponse57_V3.Output_element outp = new wwwApprouterComConnectorsResponse57_V3.Output_element();
        // added try catch, instead of displaying TIMED OUT error on the VF page, it will now prompt as error message so as for the search field and search
        // button to remain accessible. 
        
        try {
            outp = updCall.TestUpdSFCD(inputElem);           
        } catch (exception e){
            System.debug('ERROR:' + e);
            Message = '[' + e.getMessage() + ']. Please contact your system administrator.';
        }
        
        if(outp.Status==true){
            OpportunityID='/'+outp.SFDCMCMAccountId;
            SearchNumber = '';
            successStr = outp.ErrorMsg;
           
        }else {
            OpportunityID='';
            Message = outp.ErrorMsg;
        }        
        System.debug('@@' + outp);
        system.debug('@@ ' + outp.SFDCMCMAccountId +' @@Status '+outp.Status +'@@ErrorMsg '+outp.ErrorMsg);      
    }
}