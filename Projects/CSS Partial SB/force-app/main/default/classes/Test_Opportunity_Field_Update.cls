/*
##################################################################################################################################
# Project Name..........: CSS McmAccounts Visibility Issue 
# File..................: It is a Common Test Class for "Oppurtunity_Field_Update" Trigger and "FollowUpTaskHelper" Class
# Version...............: 1.0
# Created by............: Sunny Kumar
# Created Date..........: 10-Dec-2013
# Last Modified by......: Sunny Kumar
# Last Modified Date....: 12-Dec-2013
# Description...........: It will create a Domestic,Opp,case record for unit testing of code. 
#####################################################################################################################################
*/

/**  @isTest
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 */
 
@isTest(SeeAllData= true)
public class Test_Opportunity_Field_Update
{
     /* Test method to cover the trigger : 'Oppurtunity_Field_Update' */
     static testMethod void TestOpportunity()
     {
        Test.startTest();
        List<Domestic_Only_Portfolio__c> portfolioNames = new List<Domestic_Only_Portfolio__c>();    
        
        portfolioNames = [select Name from Domestic_Only_Portfolio__c]; 
 
        
        //List<Domestic_Only_Portfolio__c> a_01 = new List<Domestic_Only_Portfolio__c>([Select Name from Domestic_Only_Portfolio__c limit 1]);
        if(!portfolioNames.isEmpty()){
        system.debug('Testing Domestic Record' + portfolioNames.size()+'========'+portfolioNames );
                   
        /* Inserting Opportunity record */
        Opportunity objOpp_01 =  new Opportunity();
        objOpp_01.Name = 'Opps_01';
        objOpp_01.StageName = 'None';
        objOpp_01.CloseDate = Date.Today();
        objOpp_01.msport__c = portfolioNames[0].Name;
        objOpp_01.IS_DOMESTIC__c = true;
        insert objOpp_01;  
        
        
        /* Fethching Case RecordType From Desxcribe Call */
        Schema.DescribeSObjectResult caseDescribe = Schema.SObjectType.Case;  
        Map<String,Schema.RecordTypeInfo> rtMapByName = caseDescribe.getRecordTypeInfosByName();  
        Schema.RecordTypeInfo rtName = rtMapByName.get('Correspondence');
       
        /* Creating a Case Record */
        case c=new case();
        c.origin='Email';  
        c.type='Bankruptcy';  
        c.RecordtypeId= rtName.getRecordTypeId();
        c.IS_DOMESTIC__c = false;
        c.Opportunity__c=objOpp_01.Id;
        c.status='Open';
        c.Received_Date__c=System.Today();
        c.Type='Dispute';       
        insert c;
        
        objOpp_01.msport__c = portfolioNames[2].Name;
        update objOpp_01;
        
        objOpp_01.msport__c = '';
        update objOpp_01;
        
        system.debug('Inserting objOpp1 -->' + objOpp_01.msport__c);    
        }
        Test.stopTest(); 
          
     }
      /* Test method to cover the Class : 'FollowUpTaskHelper' */
     static testMethod void TestFollowUpTaskHelper()
     {
        FollowUpTaskHelper futh = new FollowUpTaskHelper();
        FollowUpTaskHelper.getFollowUpSubject('Hello');
        FollowUpTaskHelper.hasAlreadyCreatedFollowUpTasks();
        FollowUpTaskHelper.setAlreadyCreatedFollowUpTasks(); 
     }
}