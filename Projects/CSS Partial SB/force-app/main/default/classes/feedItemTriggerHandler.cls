public class feedItemTriggerHandler {

      /*
    =================================================================
    This is not needed now because the option to edit chatter posts
    is not enabled under chatter settings. If it is ever enabled,
    then you will need to uncomment this section of the code.
    =================================================================
    static public void restrictEdit(Map<Id, feedItem> oldItem, Map<Id, feedItem> newItem){
        
        for(feedItem newPost : newItem.values()){
            feedItem oldPost = oldItem.get(newPost.id);
            if(oldPost.Body!=newPost.Body){
               newPost.adderror('FeedItem edit unauthorized.');
            }
        }
    }
    =================================================================
    */
    
    static public void restrictDelete(List<feedItem> oldItem, List<feedItem> newItem){
        for(feedItem newPost : newItem){
            newPost.adderror('FeedItem delete unauthorized.');
        }
    }
}