@isTest(seeAllData=True)
private class SendToDMLetterExt_Test{

    
    static testMethod void testSendToDM(){
        
        Account a = new Account();
        a.FirstName = 'First Name';
        a.LastName  = 'LastName';
        insert a;
        
        Opportunity opp = new Opportunity();       
        opp.AccountId   = a.Id;
        opp.Name      = 'Test Opp';
        opp.StageName = 'Application Started';
        opp.CloseDate = System.today().addDays(15);
        opp.DM_Account_External_ID__c = '1023325'; 
        insert opp;
        
        Case c = new Case();
        c.Subject = 'Test Case';
        c.Opportunity__c = opp.Id;
        c.AccountId = a.Id;
        c.status    = 'New';
        insert c;
        
        Letter__c ltr = new Letter__c();
        ltr.ParentCase__c = c.Id;
        ltr.ParentOpportunity__c = opp.Id;
        ltr.ParentAccount__c  = a.Id;
        ltr.Recipient_Type__c = 'Consumer';
        ltr.Attach_Media__c   = 'Yes';
        ltr.Account_update__c = 'Yes';
        ltr.Atty_info_not_provided__c  = 'Yes';
        ltr.Credit_reporting_accuracy__c  = 'Yes';
        ltr.Credit_reporting_frp__c  = 'Yes';
        ltr.Credit_reporting_reage__c = 'Yes';
        ltr.Credit_reporting_sol__c  = 'Yes';
        ltr.Dispute_removal__c  = 'Yes';
        ltr.Unclear_dispute__c  = 'Yes';
        ltr.RecordTypeId = Schema.SObjectType.Letter__c.getRecordTypeInfosByName().get('DRL1').getRecordTypeId();
        Insert ltr;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ltr);
        SendToDMLetterExt Ctrl = new SendToDMLetterExt(sc);
        
        Test.StartTest();
            Test.setMock(HttpCalloutMock.class, new ExampleCalloutMock());
            Ctrl.sendToDMLetter();
        Test.stopTest();
        
    }
    
    //Mock Response for web service call upon success
    public class ExampleCalloutMock implements HttpCalloutMock{
        public HttpResponse respond(HTTPRequest req){
            HttpResponse res = new HttpResponse();
            res.setStatus('OK');
            res.setStatusCode(200);
            String jsonStr = '{ "status" : { "code" : 200, "message" : "Request Success ! Account search performed.", "uniqueErrorId" : "", "messageCode" : "" }} ';
            res.setBody(jsonStr);
            return res;
        }
    }
    
    

}