public class PrivacyInquiryRollUpTriggerHelper {
    private static PrivacyInquiryRollUpTriggerHelper instance = null;
    private PrivacyInquiryRollUpTriggerHelper(){
        
    }
    
    public void dataValidation(List<Privacy_Inquiry__c> privacyList){
        if(privacyList != null && privacyList.size() > 0){
          Set<Id> individualRecordInfoSet = new Set<Id>();
          
           for(Privacy_Inquiry__c pi :  privacyList){
                if(pi.Date_Final_Response_Sent__c != null && pi.Individual__c != null && (pi.Request_Personal_Information__c || pi.Request_to_Delete_Data__c)){
                  individualRecordInfoSet.add(pi.Individual__c);  
                }
           }
           
           if(individualRecordInfoSet.size() > 0){
           
              Map<Id,Individual> individualRecordMap = new Map<Id,Individual>([select id , name,Privacy_Inquiry_Roll_Up_Date__c from individual where id 
                                                                                  In:individualRecordInfoSet]);
                  
                  List<Individual> individualRecordToUpdate = new List<Individual>();
                  
                  for(Privacy_Inquiry__c pi :  privacyList){
                        if(pi.Date_Final_Response_Sent__c != null && pi.Individual__c != null && individualRecordMap.containsKey(pi.Individual__c) && (pi.Request_Personal_Information__c || pi.Request_to_Delete_Data__c)){
                           if(individualRecordMap.get(pi.Individual__c).Privacy_Inquiry_Roll_Up_Date__c != null ){
                             if(individualRecordMap.get(pi.Individual__c).Privacy_Inquiry_Roll_Up_Date__c.daysBetween(pi.Date_Final_Response_Sent__c) < 366 ){
                              pi.Privacy_Inquiry_Roll_Up_Date__c  = individualRecordMap.get(pi.Individual__c).Privacy_Inquiry_Roll_Up_Date__c;
                             }else{
                               /*Individual ind = individualRecordMap.get(pi.Individual__c);
                               ind.Privacy_Inquiry_Roll_Up_Date__c = pi.Date_Final_Response_Sent__c ;
                               individualRecordToUpdate.add(ind);*/
                               pi.Privacy_Inquiry_Roll_Up_Date__c  = NULL;//pi.Date_Final_Response_Sent__c ;
                             }
                           }else{
                               Individual ind = individualRecordMap.get(pi.Individual__c);
                               ind.Privacy_Inquiry_Roll_Up_Date__c = pi.Date_Final_Response_Sent__c ;
                               individualRecordToUpdate.add(ind);
                               pi.Privacy_Inquiry_Roll_Up_Date__c  = pi.Date_Final_Response_Sent__c ;
                               
                           }
                        }
                   }
                   
                  if(individualRecordToUpdate.size() > 0){
                   update individualRecordToUpdate;
                  } 
                                                                             
                                                                                  
           }
           
        }
    }
    
    public void dataRollUp(List<Privacy_Inquiry__c> privacyList){
        if(privacyList != null && privacyList.size() > 0){
            Map<Id,Individual> rollUpIndividualMap = new Map<Id,Individual>();
            
            for(Privacy_Inquiry__c pi :  privacyList){
                if(pi.Date_Final_Response_Sent__c != null && pi.Individual__c != null && (pi.Request_Personal_Information__c || pi.Request_to_Delete_Data__c) && pi.Privacy_Inquiry_Roll_Up_Date__c != NULL){
                    if(!rollUpIndividualMap.containsKey(pi.Individual__c)){
                        rollUpIndividualMap.put(pi.Individual__c ,  new Individual(id=pi.Individual__c,Asset_Acceptance_Delete_Count__c=0
                                                                                   ,Asset_Acceptance_Disclose_Count__c=0 , Midland_Credit_Management_Disclose_Count__c=0
                                                                                   ,Midland_Credit_Management_Delete_Count__c=0,Midland_Funding_Delete_Count__c=0
                                                                                   ,Midland_Funding_Disclose_Count__c=0,Atlantic_Credit_Finance_Delete_Count__c=0
                                                                                   ,Atlantic_Credit_Finance_Disclose_Count__c=0));
                    }
                }
            }
            
            List<Date> dateInfoList = new List<Date>();
            
                for(individual  ind: [select id , name,Privacy_Inquiry_Roll_Up_Date__c from individual 
                                      where id IN : rollUpIndividualMap.keyset()]){
                        if(ind.Privacy_Inquiry_Roll_Up_Date__c != null){
                            dateInfoList.add(ind.Privacy_Inquiry_Roll_Up_Date__c);
                        }
                }   
     
            
            
            List<AggregateResult> rollUpResultAggList = [select count(id) ,Max(Date_Final_Response_Sent__c ),Individual__c ,Company_Selection__c,Request_Personal_Information__c,
                                                         Request_to_Delete_Data__c from Privacy_Inquiry__c where Individual__c =:rollUpIndividualMap.keyset() 
                                                         and (Request_to_Delete_Data__c=true or Request_Personal_Information__c=true) and Date_Final_Response_Sent__c !=null
                                                         and Privacy_Inquiry_Roll_Up_Date__c =: dateInfoList group by Individual__c,Company_Selection__c,Request_Personal_Information__c,
                                                         Request_to_Delete_Data__c];
     
            
            if(rollUpResultAggList != null && rollUpResultAggList.size() > 0){
                for(AggregateResult ar : rollUpResultAggList){
                    if(ar.get('Company_Selection__c') != null){
                        Integer counter =  (Integer)ar.get('expr0');
                        Id individualId = (Id)ar.get('Individual__c');
                        String companySelection = (String)ar.get('Company_Selection__c');
                        switch on companySelection{
                            when 'Midland Credit Management'{
                                if(ar.get('Request_Personal_Information__c') == true){
                                    individual ind = rollUpIndividualMap.get(individualId);
                                    ind.Midland_Credit_Management_Disclose_Count__c += counter;
                                    rollUpIndividualMap.put(individualId,ind);
                                }
                                if(ar.get('Request_to_Delete_Data__c') == true){
                                    individual ind = rollUpIndividualMap.get(individualId);
                                    ind.Midland_Credit_Management_Delete_Count__c += counter; 
                                    rollUpIndividualMap.put(individualId,ind);
                                }
                            }
                            when 'Midland Funding'{
                                if(ar.get('Request_Personal_Information__c') == true){
                                    individual ind = rollUpIndividualMap.get(individualId);
                                    ind.Midland_Funding_Disclose_Count__c += counter;
                                    rollUpIndividualMap.put(individualId,ind);
                                }
                                if(ar.get('Request_to_Delete_Data__c') == true){
                                    individual ind = rollUpIndividualMap.get(individualId);
                                    ind.Midland_Funding_Delete_Count__c += counter;
                                    rollUpIndividualMap.put(individualId,ind);
                                }
                            }
                            when 'Asset Acceptance'{
                                if(ar.get('Request_Personal_Information__c') == true){
                                    individual ind = rollUpIndividualMap.get(individualId);
                                    ind.Asset_Acceptance_Disclose_Count__c += counter;
                                    rollUpIndividualMap.put(individualId,ind);
                                }
                                if(ar.get('Request_to_Delete_Data__c') == true){
                                    individual ind = rollUpIndividualMap.get(individualId);
                                    ind.Asset_Acceptance_Delete_Count__c += counter;
                                    rollUpIndividualMap.put(individualId,ind);
                                }
                            }
                            when 'Atlantic Credit & Finance'{
                                if(ar.get('Request_Personal_Information__c') == true){
                                    individual ind = rollUpIndividualMap.get(individualId);
                                    ind.Atlantic_Credit_Finance_Disclose_Count__c += counter;
                                    rollUpIndividualMap.put(individualId,ind);
                                }
                                if(ar.get('Request_to_Delete_Data__c') == true){
                                    individual ind = rollUpIndividualMap.get(individualId);
                                    ind.Atlantic_Credit_Finance_Delete_Count__c += counter;
                                    rollUpIndividualMap.put(individualId,ind);
                                }
                            }
                        }
                    }
                }
            }
            
            if(rollUpIndividualMap.size() > 0)
            update rollUpIndividualMap.values();
        }
    }
    
    public static PrivacyInquiryRollUpTriggerHelper getInstance(){
        if(instance== null) instance = new PrivacyInquiryRollUpTriggerHelper();
        return instance;
    }
    
}