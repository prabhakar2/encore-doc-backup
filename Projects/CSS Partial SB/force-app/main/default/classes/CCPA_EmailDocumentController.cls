/*******************************************************************************************
 * Class Name  : CCPA_EmailDocumentController
 * Description : Controller to view the PDF for Privacy Inquiry Record details and send the email.
 * Created By  : Shivangi Srivastava 
 * Created Date: 12-Dec-2019
 * Modified By : Prabhakar Joshi
 * *****************************************************************************************/
public class CCPA_EmailDocumentController { 
    public Id privacyId 			{get; private set;} // To set the current record Id
    public Privacy_Inquiry__c pi	{get; private set;} // To set the current record
    
    /**************************************** Constructor ********************************************/
    public CCPA_EmailDocumentController(ApexPages.StandardController std){
        privacyId = ApexPages.currentPage().getparameters().get('Id');
        List<Privacy_Inquiry__c> privacyInqList = [SELECT Id,Name,First_Name__c,Last_Name__c,Letter__c,Template_Language__c,
                                                   Company_Selection__c,Salutation__c,Authorized_Agent_Email__c, Email__c,Relationship_to_Company__c
                                                   FROM Privacy_Inquiry__c WHERE Id =: privacyId LIMIT 1];
        if(!privacyInqList.isEmpty()){
            pi = privacyInqList[0];
        }
    }
    
    /************************************* Method to save the PDF in Documents ***********************************/
    public PageReference SavePDF(){
        Document doc = createDoc();
        if(doc.Id != NULL)
            return new PageReference('/'+privacyId);
        else
            return NULL;
    }
    
    /******************************************* Method to Save & Email the PDF *********************************/
    public PageReference SaveAndEmailPDF(){
        Document doc = createDoc();
        sendEmail(doc, pi);
        return new PageReference('/'+privacyId);
    }
    
    /****************************************** Method to create the document ***********************************/
    private Document createDoc(){
        PageReference pdf  = page.CCPA_pdfPage;
        pdf.getParameters().put('id', privacyId);
        Blob b;
        
        if(Test.isRunningTest())
            b = Blob.valueOf('test');
        else
            b = pdf.getContentAsPdf();
        
        Date dt = System.today();
        
        Map<String,String> ExceptionalValueMap = CCPA_pdfPageController.ExceptionalValueMap;
        String letterName = '';    
        if(ExceptionalValueMap.containsKey(pi.Letter__c)){
            letterName = ExceptionalValueMap.get(pi.Letter__c);
        }else{
            letterName = pi.Letter__c;
        }
        String LetterCode = (Privacy_Letter_Code_Manager__c.getValues(letterName+'@@'+pi.Template_Language__c) != NULL && Privacy_Letter_Code_Manager__c.getValues(letterName+'@@'+pi.Template_Language__c).Letter_Code__c != NULL) ? Privacy_Letter_Code_Manager__c.getValues(letterName+'@@'+pi.Template_Language__c).Letter_Code__c : '';
        
        Document doc = new Document();
        doc.Name = pi.First_Name__c.subString(0,1).toUpperCase()+pi.Last_Name__c.toUpperCase()+'_'+pi.Company_Selection__c+'_'+pi.Name+'_'+LetterCode+'_'+dt.Month()+dt.day()+dt.year()+'.pdf';
        doc.Body = b ;       
        doc.FolderId = [SELECT id,Name FROM Folder WHERE Name = 'CCPA Shared Folder' LIMIT 1].Id;
        doc.ContentType = 'application/pdf';
        insert doc;
        return doc;
    }
    
    /************************************ Method to send the PDF Document in email *********************************/
    private static void sendEmail(Document doc, Privacy_Inquiry__c pi){
        OrgWideEmailAddress[] owea = [SELECT Id, address FROM OrgWideEmailAddress WHERE Address = 'noreply@mcmcg.com' LIMIT 1];
        Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
        if ( owea.size() > 0 ) {
            msg.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        
        if(pi.Relationship_to_Company__c == 'Consumer')
            msg.toAddresses = new String[] { pi.Email__c };
                
        else if(pi.Relationship_to_Company__c == 'Authorized Agent')
            msg.toAddresses = new String[] { pi.Authorized_Agent_Email__c };
                
        
        msg.subject = '[Encrypt] Privacy Request';// setSubject
        string htmlBody;
        htmlBody = 'Hello '+pi.Salutation__c;  //+pi.First_Name__c+' '+pi.Last_Name__c;
        htmlBody += '<br/></br/>Attached is a letter with more information regarding your recent privacy request.<br/><br/>';
		htmlBody += 'Sincerely, <br/>Consumer Privacy Team ';
		msg.htmlbody = htmlBody; 
        Messaging.EmailFileAttachment pdfDoc = new Messaging.EmailFileAttachment();
        pdfDoc.setFileName(doc.Name);
        pdfDoc.setBody(doc.Body);
        msg.setFileAttachments(new Messaging.EmailFileAttachment[] {pdfDoc}); 
        
        if(!Test.isRunningTest())
            messaging.sendEmail(new List<Messaging.SingleEmailMessage>{msg});
    }
}