/**
 * A utility class to process Complaint Response records for the Complaint Response Trigger. This handler 
 * counts the number of child cases on a complaint response record in which the original lender on the case
 * is Synchrony but the case is missing the Synchrony Id, and updates the Cases_Missing_Synchrony_Id__c 
 * field on the Complaint Response record with this count. This field serves as a pseudo-rollup field for
 * children from a lookup relationship.
 * @Created 2019-02-18
 */
public class ComplaintResponseHandler {
    
    //create set of all Synchrony accounts for use in soql query
    @TestVisible
    private static Set<String> originalLenders = new Set<String>{
            'GE ELECTRIC CAPITAL CORP', 'GE MONEY BANK', 
            'GENERAL ELECTRIC CAPITAL CORP', 'SYNCHRONY BANK', 
            'GE MONEY', 'GE CAPITAL- SHERMAN', 'FINANCIAL', 
            'GEMB', 'GE MONEY-SQUARE TWO', 'GE CAPITAL RETAIL BANK'
    };
            
    //before update
    public static void updateRollUpFields(List<Complaint_Response__c> newCR){
    	
        //pull in all childCases that are Synchrony accounts but missing the SynchronyId
        List<Case> childCases = [SELECT Internal_Complaint_Response__c
                                 FROM Case
                                 WHERE Internal_Complaint_Response__c IN :newCR AND GE_ID__c=null
                                		AND Original_Lender_1__c IN :originalLenders];
        //iterate through each CR record; update Cases Missing Synchrony Id field on CR record
        for(Complaint_Response__c cr: newCR){
            List<String> missingSynchronyId = new List<String>();
            for(Case c : childCases){
                if(c.Internal_Complaint_Response__c==cr.Id){
                	missingSynchronyId.add(cr.Id);
                }
            }
            Integer countMissingSynchronyId = missingSynchronyId.size();
            cr.Cases_Missing_Synchrony_Id__c = countMissingSynchronyId;
            system.debug('**# child cases missing synchrony ID: ' + countMissingSynchronyId);
        }
    }
    
}