public class SendToDMCaseExt {
    
    public String CaseId                      {get; set;}
    public String ConxExternalId              {get; set;}
    public String AcctExternalId              {get; set;}
    public List<String> ConxTagAdd            {get; set;}
    public List<String> ConxTagRemove         {get; set;}
    public List<String> AccountTagAdd         {get; set;}
    public List<String> AccountTagRemove      {get; set;}
    
    public String accountAgencyId             {get; set;}
    public string consumerAgencyId            {get; set;}
    public String jsonStringAcc               {get; set;}
    public String jsonStringConx              {get; set;}
    public String DMAccountCommentary         {get; set;}
    
    Public DMIntegrationSettings__c DMSettings =  DMIntegrationSettings__c.getOrgDefaults();
    Public String HeaderAPIKey         = DMSettings.HeaderAPIKey__c;
    Public String HeaderAppId          = DMSettings.HeaderAppId__c;
    Public String HeaderKeySecret      = DMSettings.HeaderKeySecret__c;
    Public String HeaderUserId         = DMSettings.HeaderUserId__c;
    Public String HeaderCorrelationId  = DMSettings.HeaderCorrelationId__c;
    Public String HeaderContentType    = DMSettings.HeaderContentType__c;
    
    public Case c  {get; set;}
    
    
    //============ Constructor ================
    public SendToDMCaseExt(ApexPages.StandardController stdController){
        this.c = (Case)stdController.getRecord();
        CaseId = c.Id;
    }
        
    
    public pageReference sendToDM(){
        ConxExternalId = string.valueof([SELECT Consumer_External_ID__c from Case WHERE Id = :CaseId limit 1].Consumer_External_ID__c);
        AcctExternalId = string.valueof([SELECT Account_External_ID__c from Case WHERE Id = :CaseId limit 1].Account_External_ID__c);
        
        system.debug('ConxExternalId>>>'+ConxExternalId);    
        system.debug('AcctExternalId>>>'+AcctExternalId);
        
        accountAgencyId     = c.Account_External_ID__c;
        consumerAgencyId     = c.Consumer_External_ID__c;
        
        // ============ Section for Remove Consumer Tags =======================//
        
        //generate list of tags to be removed on consumer
        ConxTagRemove = new List<String>();
        if(c.Consumer_Status_to_Remove__c !=null && c.Consumer_Status_to_Remove__c !=''){
            String[] conxStTagsRmv = c.Consumer_Status_to_Remove__c.split(';');
            for(String conxSTr :conxStTagsRmv){
                ConxTagRemove.add(conxSTr);
            }
        }
        if(c.Language_Tag_to_Remove__c !=null && c.Language_Tag_to_Remove__c !=''){
            String[] LangTagsRmv = c.Language_Tag_to_Remove__c.split(';');
            for(String langTr :LangTagsRmv){
                ConxTagRemove.add(langTr);
            }
        }
        
        if(ConxTagRemove != null && ConxTagRemove.size()>0){
            
            //set request audit fields on case
            c.Consumer_Tags_Remove_SentUser__c = userInfo.getFirstName()+' '+userInfo.getLastName();
            c.Consumer_Tags_Remove_SentDateTime__c = system.Now();
            
            //generate endpoint URL
            String ConxUnassignTagsURL =  DMSettings.DMConsumerRESTAPI__c+ConxExternalId+'/arevents';
            
            Map<String, Object> ConxTagRmvBody = new Map<String, Object>();
            String ConxTagRmvResponseStatus = '';
            
            for(String tag : ConxTagRemove){
                
                //generate message
                String ConxRemoveTagsMessage = 'REMOVE ' + tag  + ' arevent triggered by CSS Salesforce user ' + userInfo.getName() + '.';
                String jsonId = consumerAgencyId;
                //generate json
                String JsonStrForConxTagRmv = generateJSONContent('REMOVE',tag,jsonId,ConxRemoveTagsMessage,'Consumer');
                
                //make callout
                HttpResponse responseConxTagRmv = makePostCallout(JsonStrForConxTagRmv,ConxUnassignTagsURL);
                
                //process response
                ConxTagRmvBody = (Map<String, Object>) JSON.deserializeUntyped(responseConxTagRmv.getBody());
                if(responseConxTagRmv.getStatusCode()==200){
                    ConxTagRmvResponseStatus = ConxTagRmvResponseStatus + tag + '>> Success!; ';
                }else if(ConxTagRmvBody.containsKey('message')){
                    ConxTagRmvResponseStatus = ConxTagRmvResponseStatus + tag + '>> Error ' + responseConxTagRmv.getStatusCode() + ': ' + String.ValueOf(ConxTagRmvBody.get('message')) + '; ';
                }else{
                    ConxTagRmvResponseStatus = ConxTagRmvResponseStatus + tag + '>> Unknown Error.' + responseConxTagRmv.getStatusCode() + '; ';
                }
            }

            //set response audit fields on case
            c.Consumer_Tags_Remove_Status_Long__c = ConxTagRmvResponseStatus.trim();
            c.Consumer_Tags_Remove_StatusDateTime__c = system.Now();
            
        }else{
            c.Consumer_Tags_Remove_SentUser__c       = null;
            c.Consumer_Tags_Remove_SentDateTime__c   = null;
            c.Consumer_Tags_Remove_Status__c         = null;
            c.Consumer_Tags_Remove_StatusDateTime__c = null;
        }
        
        // ============ Section for Add Consumer Tags =======================//        
        
        //generate list of tags to be added on consumer
        ConxTagAdd = new List<String>();
        if(c.Consumer_Status_to_Add__c !=null && c.Consumer_Status_to_Add__c !=''){
            String[] conxStatusTags = c.Consumer_Status_to_Add__c.split(';');
            for(String conxST :conxStatusTags){
                ConxTagAdd.add(conxST);
            }
        }
        if(c.Language_Tag_to_Add__c !=null && c.Language_Tag_to_Add__c !=''){
            String[] LangTags = c.Language_Tag_to_Add__c.split(';');
            for(String langT :LangTags){
                ConxTagAdd.add(langT);
            }
        } 
        
        if(ConxTagAdd != null && ConxTagAdd.size()>0){
            
            //set request audit fields on case
            c.Consumer_Tags_Add_SentUser__c = userInfo.getFirstName()+' '+userInfo.getLastName();
            c.Consumer_Tags_Add_SentDateTime__c = system.Now();
            
            //generate endpoint URL
            String ConxAssignTagsURL = DMSettings.DMConsumerRESTAPI__c+ConxExternalId+'/arevents';
            
            Map<String, Object> ConxTagAddBody = new Map<String, Object>();
            String ConxTagAddResponseStatus = '';
            
            for(String tag : ConxTagAdd){
                
                //generate message
                String ConxAssignTagsMessage = 'CSSREP ' + tag  + ' arevent triggered by CSS Salesforce user ' + userInfo.getName() + '.';
                
                //generate json
                String JsonStrForConxTagAdd = generateJSONContent('CSSREP',tag,consumerAgencyId,ConxAssignTagsMessage,'Consumer');
                
                //make callout
                HttpResponse responseConxTagAdd = makePostCallout(JsonStrForConxTagAdd,ConxAssignTagsURL);
                
                //process response
                ConxTagAddBody = (Map<String, Object>) JSON.deserializeUntyped(responseConxTagAdd.getBody());
                if(responseConxTagAdd.getStatusCode()==200){
                    ConxTagAddResponseStatus = ConxTagAddResponseStatus + tag + '>> Success!; ';
                }else if(ConxTagAddBody.containsKey('message')){
                    ConxTagAddResponseStatus = ConxTagAddResponseStatus + tag + '>> Error ' + responseConxTagAdd.getStatusCode() + ': ' + String.ValueOf(ConxTagAddBody.get('message')) + '; ';
                }else{
                    ConxTagAddResponseStatus = ConxTagAddResponseStatus + tag + '>> Unknown Error.' + responseConxTagAdd.getStatusCode() + '; ';
                }
            }
            
            //set response audit fields on case
            c.Consumer_Tags_Add_Status_Long__c = ConxTagAddResponseStatus.trim();
            c.consumer_Tags_Add_StatusDateTime__c = system.Now();
            
        }else{
            c.Consumer_Tags_Add_SentUser__c       = null;
            c.Consumer_Tags_Add_SentDateTime__c   = null;
            c.Consumer_Tags_Add_Status_Long__c    = null;
            c.Consumer_Tags_Add_StatusDateTime__c = null;
        }
        
        // --------------- Section for Remove Account Tags ---------------- //
         
        //generate list of tags to be removed on account
        AccountTagRemove = new List<String>();
        if(c.Account_Status_to_Remove__c != null && c.Account_Status_to_Remove__c != ''){
            String[] accStTagsRmv = c.Account_Status_to_Remove__c.split(';');
            for(String accStTr :accStTagsRmv){
                AccountTagRemove.add(accStTr);
            }
        }
        if(c.Communication_Tag_to_Remove__c != null && c.Communication_Tag_to_Remove__c != ''){
            String[] commTagsRmv = c.Communication_Tag_to_Remove__c.split(';');
            for(String comTr :commTagsRmv){
                AccountTagRemove.add(comTr);
            }
        }
        if(c.Dispute_Tag_to_Remove__c != null && c.Dispute_Tag_to_Remove__c != ''){
            String[] disTagsRmv = c.Dispute_Tag_to_Remove__c.split(';');
            for(String disTr :disTagsRmv){
                AccountTagRemove.add(disTr);
            }
        }
        if(c.Complaint_Tag_to_Remove__c != null && c.Complaint_Tag_to_Remove__c != ''){
            String[] compTagsRmv = c.Complaint_Tag_to_Remove__c.split(';');
            for(String compTr :compTagsRmv){
                AccountTagRemove.add(compTr);
            }
        }
        
        if(AccountTagRemove != null && AccountTagRemove.size()>0){
            
            //set request audit fields on case
            c.Account_Tags_Remove_SentUser__c = userInfo.getFirstName()+' '+userInfo.getLastName();
            c.Account_Tags_Remove_SentDateTime__c = system.Now();             
            
            //generate endpoint URL
            String acctUnassignTagsURL = DMSettings.DMAccountRESTAPI__c+AcctExternalId+'/arevents';
            
            Map<String, Object> AcctTagRmvBody = new Map<String, Object>();
            String AcctTagRmvResponseStatus = '';
            
            for(String tag : AccountTagRemove){
            
                //generate message
                String AcctRemoveTagsMessage = 'REMOVE ' + tag  + ' arevent triggered by CSS Salesforce user ' + userInfo.getName() + '.';
                
                //generate json
                String JsonStrForAcctTagRem = generateJSONContent('REMOVE',tag,accountAgencyId,AcctRemoveTagsMessage,'Account');
                
                //make callout
                HttpResponse responseAcctTagRmv = makePostCallout(JsonStrForAcctTagRem,acctUnassignTagsURL);
                
                //process response
                AcctTagRmvBody = (Map<String, Object>) JSON.deserializeUntyped(responseAcctTagRmv.getBody());
                if(responseAcctTagRmv.getStatusCode()==200){
                    AcctTagRmvResponseStatus = AcctTagRmvResponseStatus + tag + '>> Success!; ';
                }else if(AcctTagRmvBody.containsKey('message')){
                    AcctTagRmvResponseStatus = AcctTagRmvResponseStatus + tag + '>> Error ' + responseAcctTagRmv.getStatusCode() + ': ' + String.ValueOf(AcctTagRmvBody.get('message')) + '; ';
                }else{
                    AcctTagRmvResponseStatus = AcctTagRmvResponseStatus + tag + '>> Unknown Error.' + responseAcctTagRmv.getStatusCode() + '; ';
                }
            }
            
            //set response audit fields on case
            c.Account_Tags_Remove_Status_Long__c = AcctTagRmvResponseStatus.trim();
            c.Account_Tags_Remove_StatusDateTime__c = system.Now();
            
        }else{
            c.Account_Tags_Remove_SentUser__c = null;
            c.Account_Tags_Remove_SentDateTime__c = null;
            c.Account_Tags_Remove_Status__c = null;
            c.Account_Tags_Remove_StatusDateTime__c = null;
        }
           
        // ============ Section For Add Account Tags ===========================//
         
        //generate list of tags to be added on account
        AccountTagAdd = new List<String>();
        if(c.Account_Status_to_Add__c !=null && c.Account_Status_to_Add__c !=''){
            String[] accStatusTags = c.Account_Status_to_Add__c.split(';');
            for(String accS : accStatusTags){
                AccountTagAdd.add(accS);
            }
        }
        if(c.Communication_Tag_to_Add__c !=null && c.Communication_Tag_to_Add__c !=''){
            String[] commTags = c.Communication_Tag_to_Add__c.split(';');
            for(String comT : commTags){
                AccountTagAdd.add(comT);
            }
        }
        if(c.Dispute_Tag_to_Add__c !=null && c.Dispute_Tag_to_Add__c !=''){
            String[] dispTags = c.Dispute_Tag_to_Add__c.split(';');
            for(String dispT :dispTags){
                AccountTagAdd.add(dispT);
            }
        }
        if(c.Complaint_Tag_to_Add__c !=null && c.Complaint_Tag_to_Add__c !=''){
            String[] compTags = c.Complaint_Tag_to_Add__c.split(';');
            for(String compT :compTags){
                AccountTagAdd.add(compT);
            }
        }    
        
        if(AccountTagAdd != null && AccountTagAdd.size()>0){

            //set request audit fields on case
            c.Account_Tags_Add_SentUser__c = userInfo.getFirstName()+' '+userInfo.getLastName();
            c.Account_Tags_Add_SentDateTime__c = system.Now();    
            
            //generate endpoint URL
            String AcctAssignTagsURL = DMSettings.DMAccountRESTAPI__c+AcctExternalId+'/arevents';
            
            Map<String, Object> AcctTagAddBody = new Map<String, Object>();
            String AcctTagAddResponseStatus = '';
            
            for(String tag : AccountTagAdd){
                
               //generate message
                String AcctAssignTagsMessage = 'CSSREP ' + tag  + ' arevent triggered by CSS Salesforce user ' + userInfo.getName() + '.';
                
                //generate json
                String JsonStrForAcctTagAdd = generateJSONContent('CSSREP',tag,accountAgencyId,AcctAssignTagsMessage,'Account');

                //make callout
                HttpResponse responseAcctTagAdd = makePostCallout(JsonStrForAcctTagAdd,AcctAssignTagsURL);
                
                //process response
                AcctTagAddBody = (Map<String, Object>) JSON.deserializeUntyped(responseAcctTagAdd.getBody());
                if(responseAcctTagAdd.getStatusCode()==200){
                    AcctTagAddResponseStatus = AcctTagAddResponseStatus + tag + '>> Success!; ';
                }else if(AcctTagAddBody.containsKey('message')){
                    AcctTagAddResponseStatus = AcctTagAddResponseStatus + tag + '>> Error ' + responseAcctTagAdd.getStatusCode() + ': ' + String.ValueOf(AcctTagAddBody.get('message')) + '; ';
                }else{
                    AcctTagAddResponseStatus = AcctTagAddResponseStatus + tag + '>> Unknown Error.' + responseAcctTagAdd.getStatusCode() + '; ';
                }
            }
            
            //set response audit fields on case
            c.Account_Tags_Add_Status_Long__c = AcctTagAddResponseStatus.trim();
            c.Account_Tags_Add_StatusDateTime__c = system.Now();
            
        }else{
            c.Account_Tags_Add_SentUser__c = null;
            c.Account_Tags_Add_SentDateTime__c = null;
            c.Account_Tags_Add_Status_Long__c = null;
            c.Account_Tags_Add_StatusDateTime__c = null;
        }
        
        // --------------- Section for Add Account Commentary ---------------- //
        
        if(c.DM_Account_Commentary__c != null && c.DM_Account_Commentary__c != ''){
            
            //set request audit fields on case
            c.DM_Account_Commentary_Add_SentUser__c     = userInfo.getFirstName()+' '+userInfo.getLastName();
            c.DM_Account_Commentary_Add_SentDateTime__c = system.Now();
            
            //generate endpoint URL
            String AcctCommentaryURL = DMSettings.DMAccountRESTAPI__c+AcctExternalId+'/arevents';
            
            //generate full commentary message to send to DM
            DMAccountCommentary = c.DM_Account_Commentary__c + ' (' + userInfo.getName() + ' ' + system.Now() + ')';
            
            //generate json
            String JsonStrForCommentary = generateJSONContent('COMMENT','COMMENT',accountAgencyId,DMAccountCommentary,'Account');

            //make callout
            HttpResponse responseAcctComAdd = makePostCallout(JsonStrForCommentary,AcctCommentaryURL);
            
            //process response
            String AcctCommResponseStatus = '';
            Map<String, Object> ComAddbody = (Map<String, Object>) JSON.deserializeUntyped(responseAcctComAdd.getBody());
            if(responseAcctComAdd.getStatusCode()==200){
                AcctCommResponseStatus = 'COMMENT>>' + 'Success!';
            }else if(ComAddbody.containsKey('message')){
                AcctCommResponseStatus = 'COMMENT>>' + 'Error '+responseAcctComAdd.getStatusCode()+': '+String.ValueOf(ComAddbody.get('message'));
            }else{
                AcctCommResponseStatus = 'COMMENT>>' + 'Unknown Error. '+responseAcctComAdd.getStatusCode();
            }
            
            //set response audit fields on case
            c.DM_Account_Commentary_Add_Status_Long__c = AcctCommResponseStatus.trim();
            c.DM_Account_Commentary_Add_StatusDateTime__c = system.Now();
            
        }else{
            c.DM_Account_Commentary_Add_SentUser__c = null;
            c.DM_Account_Commentary_Add_SentDateTime__c = null;
            c.DM_Account_Commentary_Add_Status__c = null;
            c.DM_Account_Commentary_Add_StatusDateTime__c = null;
        }
        
        // --------------- Hide the Send to DM button --------------- // 
        c.Send_updates_to_DM__c = false;
        
        // --------------- Makes Chatter Post ---------------- // 
        makeChatterPost();
        
        // --------------- Makes Post Comments --------------- // 
        postCaseComments();
        
        try{
            update c;
            String Message = Label.Sent_To_DM_Success_Message;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, Message));
            
            pageReference resultsPage = new pageReference('/apex/SendToDMResults_Case?id='+c.Id);
            resultsPage.setRedirect(true);
            return resultsPage;
        
        }catch(DmlException e){ 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage())); 
            return null; 
        }
        
    }
    
    
    @TestVisible
    private String generateJSONContent(String action, String result, String id, String message, String strType){
        String jsonStr = '';
        JSONGenerator gen = JSON.createGenerator(true);  
        gen.writeStartObject();
            if(strType == 'Account'){
                gen.writeStringField('actionCodeShortName', action); 
                gen.writeStringField('resultCodeShortName', result);
                gen.writeStringField('accountAgencyIdentifier', id); //DM Account Id
                gen.writeStringField('message', message); 
            }else if(strType == 'Consumer'){
                gen.writeStringField('actionCodeShortName', action);
                gen.writeStringField('resultCodeShortName', result);
                gen.writeStringField('consumerAgencyIdentifier', id); //DM Consumer Id
                gen.writeStringField('message', message);
            }
        gen.writeEndObject();
        jsonStr = gen.getAsString();
        system.debug('jsonStr>>>'+jsonStr);
        
        return jsonStr;
    }
    
    @TestVisible 
    private HttpResponse makePostCallout(String JsonString, String EndURL){
        system.debug('JsonString>>>'+JsonString);
        system.debug('EndURL>>>'+EndURL);
    
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(EndURL);
        request.setMethod('POST');
        request.setHeader('Content-Type', HeaderContentType);
        request.setHeader('mcm-appid', HeaderAppId);
        request.setHeader('correlation-id', HeaderCorrelationId);
        request.setHeader('apikey', HeaderAPIKey);
        request.setHeader('mcm-userid', HeaderUserId);
        request.setHeader('keySecret', HeaderKeySecret);
        
        // Set the body as a JSON object
        request.setBody(JsonString);
        request.setTimeOut(20000);
        
        HttpResponse response = http.send(request);
        
        // Parse the JSON response
        system.debug('response>>>'+response);
        
        return response;
    }
       
    @TestVisible
    private void postCaseComments(){
        if(DMAccountCommentary!=null && DMAccountCommentary!=''){
            CaseComment CaCom = new CaseComment();
            CaCom.CommentBody = DMAccountCommentary;
            CaCom.ParentId    = c.Id;
            Insert CaCom;
        }
    }
    
    @TestVisible
    private void makeChatterPost(){
        string body = 'Sent the following updates to DM:\n';
        if(ConxTagAdd != null && ConxTagAdd.size()>0){
            body += 'Add Consumer Tags: ' + c.Consumer_Tags_Add_Status_Long__c + '\n';
        }
        if(ConxTagRemove != null && ConxTagRemove.size()>0){
            body += 'Remove Consumer Tags: ' + c.Consumer_Tags_Remove_Status_Long__c + '\n';
        }
        if(AccountTagAdd != null && AccountTagAdd.size()>0){
            body += 'Add Account Tags: ' + c.Account_Tags_Add_Status_Long__c + '\n';
        }
        if(AccountTagRemove != null && AccountTagRemove.size()>0){
            body += 'Remove Account Tags: ' + c.Account_Tags_Remove_Status_Long__c + '\n';
        }
        if(DMAccountCommentary != null && DMAccountCommentary != ''){  
            body += 'Add Account Commentary: ' + DMAccountCommentary + '\n' + c.DM_Account_Commentary_Add_Status_Long__c + '\n';
        }
        FeedItem post = new FeedItem();
        post.ParentId = c.Id;
        post.Body     = body;
        Insert post;
    }
}