public class SendToDMLetterExt{

    Public DMIntegrationSettings__c DMSettings =  DMIntegrationSettings__c.getOrgDefaults();
    Public String EndpointURL          = DMSettings.DMAccountRESTAPI__c;
    Public String HeaderContentType    = DMSettings.HeaderContentType__c;
    Public String HeaderAPIKey         = DMSettings.HeaderAPIKey__c;
    Public String HeaderAppId          = DMSettings.HeaderAppId__c;
    Public String HeaderKeySecret      = DMSettings.HeaderKeySecret__c;
    Public String HeaderUserId         = DMSettings.HeaderUserId__c;
    Public String HeaderCorrelationId  = DMSettings.HeaderCorrelationId__c;
    
    public String AcctExternalId   {get; set;}
    public String ConxExternalId   {get; set;}
    public String ParentAccountId   {get; set;}
    public String ParentOpportunityId {get; set;}
    public String RelatedPersonType   {get; set;}
    
    public Letter__c Letter  {get; set;}
    
    
    // Controller constructor start here
    public SendToDMLetterExt(ApexPages.StandardController stdController){
        this.Letter = (Letter__c)stdController.getRecord();
        Letter__c ltr = [SELECT Id,Account_Number__c,Consumer_External_ID__c,Related_Person_Type__c,ParentAccount__c,ParentOpportunity__c FROM Letter__c WHERE Id =:Letter.Id];
        AcctExternalId = ltr.Account_Number__c;
        ConxExternalId = ltr.Consumer_External_ID__c;
        ParentAccountId = ltr.ParentAccount__c;
        ParentOpportunityId = ltr.ParentOpportunity__c;
        RelatedPersonType = ltr.Related_Person_Type__c;
    }
    
    public pageReference sendToDMLetter(){
        //update audit fields for when user clicks Send to DM button
        Letter.DM_Sent_User__c     = userInfo.getFirstName()+' '+userInfo.getLastName();
        Letter.DM_Sent_DateTime__c = system.Now();
        
        String jsonStr = '';
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('tableName', 'CSS_DATA');
        gen.writeFieldName('userDefinedData');
        gen.writeStartArray();
            gen.writeStartObject();
                gen.writeStringField('fieldName', 'UDEFACCOUNTING_REQUEST');
                gen.writeStringField('fieldValue',(Letter.Accounting_request__c == 'Yes') ? 'true' : 'false');
            gen.writeEndObject();
            gen.writeStartObject();
                gen.writeStringField('fieldName','UDEFACCOUNT_UPDATE');
                gen.writeStringField('fieldValue',(Letter.Account_update__c == 'Yes') ? 'true' : 'false');
            gen.writeEndObject();
            gen.writeStartObject();
                gen.writeStringField('fieldName','UDEFATTY_INFO_NOT_PROVIDED');
                gen.writeStringField('fieldValue',(Letter.Atty_info_not_provided__c == 'Yes') ? 'true' : 'false');
            gen.writeEndObject();
            gen.writeStartObject();   
                gen.writeStringField('fieldName','UDEFCONSUMER_ID');
                gen.writeStringField('fieldValue',(Letter.Consumer_External_Id__c != null) ? String.ValueOf(Letter.Consumer_External_Id__c) : '');
            gen.writeEndObject();
            gen.writeStartObject();   
                gen.writeStringField('fieldName','UDEFCREDIT_REPORTING_ACCURACY');
                gen.writeStringField('fieldValue',(Letter.Credit_reporting_accuracy__c == 'Yes') ? 'true' : 'false');
            gen.writeEndObject();
            gen.writeStartObject(); 
                gen.writeStringField('fieldName','UDEFCREDIT_REPORTING_FRP');
                gen.writeStringField('fieldValue',(Letter.Credit_reporting_frp__c == 'Yes') ? 'true' : 'false');
            gen.writeEndObject();
            gen.writeStartObject();  
                gen.writeStringField('fieldName','UDEFCREDIT_REPORTING_REAGE');
                gen.writeStringField('fieldValue',(Letter.Credit_reporting_reage__c == 'Yes') ? 'true' : 'false');
            gen.writeEndObject();
            gen.writeStartObject();  
                gen.writeStringField('fieldName','UDEFCREDIT_REPORTING_SOL');
                gen.writeStringField('fieldValue',(Letter.Credit_reporting_sol__c == 'Yes') ? 'true' : 'false');
            gen.writeEndObject();
            gen.writeStartObject();  
                gen.writeStringField('fieldName','UDEFDATE_TIME_SENT_TO_DM');
                gen.writeNumberField('fieldValue',Letter.DM_Sent_DateTime__c.getTime()); 
            gen.writeEndObject();
            gen.writeStartObject();  
                gen.writeStringField('fieldName','UDEFDISPUTE_REMOVAL');
                gen.writeStringField('fieldValue',(Letter.Dispute_removal__c == 'Yes') ? 'true' : 'false');
            gen.writeEndObject();
            gen.writeStartObject(); 
                gen.writeStringField('fieldName','UDEFLETTER_NAME');
                gen.writeStringField('fieldValue',(Letter.Letter_Code__c != null) ? String.ValueOf(Letter.Letter_Code__c) : '');
            gen.writeEndObject();
            gen.writeStartObject();  
                gen.writeStringField('fieldName','UDEFLETTER_RECORD_ID');
                gen.writeStringField('fieldValue',Letter.Id);
            gen.writeEndObject();
            gen.writeStartObject(); 
                gen.writeStringField('fieldName','UDEFMEDIA_BUNDLE_ID');
                gen.writeStringField('fieldValue',(Letter.Media_Bundle_Id__c != null) ? Letter.Media_Bundle_Id__c : '');
            gen.writeEndObject();
            gen.writeStartObject(); 
                gen.writeStringField('fieldName','UDEFRELATED_PERSON_ID');
                gen.writeStringField('fieldValue',(Letter.Related_Person_External_Id__c!=null) ? String.ValueOf(Letter.Related_Person_External_Id__c) : ''); 
            gen.writeEndObject();
            gen.writeStartObject();   
                gen.writeStringField('fieldName','UDEFSF_CASE_ID');
                gen.writeStringField('fieldValue',Letter.ParentCase__c);
            gen.writeEndObject();
            gen.writeStartObject(); 
                gen.writeStringField('fieldName','UDEFUNCLEAR_DISPUTE');
                gen.writeStringField('fieldValue',(Letter.Unclear_dispute__c == 'Yes') ? 'true' : 'false');
            gen.writeEndObject();
            gen.writeStartObject();  
                gen.writeStringField('fieldName','UDEFRELATED_PERSON_TYPE');
                gen.writeStringField('fieldValue',(RelatedPersonType !=null) ? RelatedPersonType : '');
            gen.writeEndObject();
            gen.writeStartObject(); 
                gen.writeStringField('fieldName','UDEFTX_FIN_CODE');
                gen.writeStringField('fieldValue',(Letter.TX_Fin_Code__c == 'Yes') ? 'Yes' : 'No');   
            gen.writeEndObject();
        gen.writeEndArray();            
        gen.writeEndObject();
        
        jsonStr = gen.getAsString();
        system.debug('jsonStr>>>'+jsonStr);
        
        // --------------- SENDS TO DM Post all Information --------------- //
        HttpResponse responseOfDM = makePostCallout(jsonStr);
        
        // ---- Update audit fields for the response from DM ---- //
        if(responseOfDM.getStatus() == 'OK'){
            Letter.DM_Sent_Status__c = 'Success!'; 
        }else{
            Letter.DM_Sent_Status__c = 'Error!: ' + responseOfDM.getStatus();
        }
        Letter.DM_Sent_Status_DateTime__c = system.Now();
        
        // ---- Update Letter Status and hide the Send to DM button ---- //
        if(responseOfDM.getStatus() == 'OK'){
            Letter.Letter_Status__c  = 'Sent'; //if success then Sent, else 'Error' 
        }else{
            Letter.Letter_Status__c  = 'Error';
        }
        
        // --------------- Make Chatter Posts on Letter, Case, Account ---------------- // 
        makeChatterPost(responseOfDM);
        
        try{
            update Letter;
            String Message = Label.Sent_To_DM_Success_Message;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, Message));
            
            pageReference resultsPage = new pageReference('/apex/SendToDMResults_Letter?id='+Letter.Id);
            resultsPage.setRedirect(true);
            return resultsPage;
        
        }catch(DmlException e){ 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage())); 
            return null; 
        }
    }
    

    @TestVisible
    private void makeChatterPost(HttpResponse res){
        //Map<String, Object> jsonBody = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        string body = 'Letter requested to DM on '+Letter.DM_Sent_DateTime__c+' Letter Code: '+Letter.Letter_Code__c+'\n';
        body += 'Response: '+Letter.DM_Sent_Status__c+'\n';
        
        List<FeedItem> chatterPosts = new List<FeedItem>();
        
        // ----- Letter object chatter post ----- //
        FeedItem postL = new FeedItem();
        postL.ParentId = Letter.Id;
        postL.Body     = body;
        chatterPosts.add(postL);
        
        // ----- Case object chatter post ----- //
        FeedItem postC = new FeedItem();
        postC.ParentId = Letter.ParentCase__c;
        postC.Body     = body;
        chatterPosts.add(postC);
        
        Insert chatterPosts;
    }
    

    @TestVisible 
    private HttpResponse makePostCallout(String JsonString){
        system.debug('JsonString>>>'+JsonString);
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(EndpointURL+AcctExternalId+'/userdefinedpages/CSS_DATA/data');
        request.setMethod('POST');
        request.setHeader('Content-Type', HeaderContentType);
        request.setHeader('mcm-appid', HeaderAppId);
        request.setHeader('correlation-id', HeaderCorrelationId);
        request.setHeader('apikey', HeaderAPIKey);
        request.setHeader('mcm-userid', HeaderUserId);
        request.setHeader('keySecret', HeaderKeySecret);
        
        // Set the body as a JSON object
        request.setBody(JsonString);
        request.setTimeOut(40000);
        system.debug('request>>>'+request);
        
        HttpResponse response = http.send(request);
        
        // Parse the JSON response
        system.debug('response>>>'+response);
        
        return response;
    }
}