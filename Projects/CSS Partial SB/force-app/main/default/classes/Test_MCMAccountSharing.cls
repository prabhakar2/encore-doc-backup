/*
##################################################################################################################################
# Project Name..........: CSS McmAccounts Visibility Issue 
# File..................: It is a Test Class for "MCMAccountSharing" Class
# Version...............: 1.0
# Created by............: Sunny Kumar
# Created Date..........: 30-Dec-2013
# Last Modified by......: Sunny Kumar
# Last Modified Date....: 30-Dec-2013
# Description...........: It will cover the code for class  MCMAccountSharingBatch and syncMCMAccountSharing . 
#####################################################################################################################################
*/

/**  @isTest
 * This class contains unit tests for validating the behaviour of Apex classes
 * and triggers.
 */
 
@isTest(SeeAllData= false)
public class Test_MCMAccountSharing
{
    static testMethod void TestOpp()
    {

        /* Inserting Opportunity record */
        Opportunity objOpp_01 =  new Opportunity();
        objOpp_01.Name = 'Opps_01';
        objOpp_01.StageName = 'None';
        objOpp_01.CloseDate = Date.Today();
        objOpp_01.msport__c = '444testdata';
        insert objOpp_01; 
        
        EmailNotification__c objEmail = new EmailNotification__c();
        objEmail.Email__c = 'test@mcmcg.com';
        objEmail.Name = 'TestUser';    
        insert objEmail ;
        
        List<case> lstCase=new List<case>{new Case(Opportunity__c=objOpp_01.Id)};
        insert lstCase;
        
        List<Domestic_Only_Portfolio__c> portfolioNames = new List<Domestic_Only_Portfolio__c>{
                                                            new Domestic_Only_Portfolio__c (name='444testdata'),
                                                            new Domestic_Only_Portfolio__c (name='555')};            
        insert portfolioNames;
        Set<String> setPortfolioNumber=new set<String>{'444testdata','555'};        
        System.Assert(database.countquery('SELECT COUNT() FROM Opportunity WHERE ID=\'' + objOpp_01.Id + '\' AND IS_Domestic__c=false')>0);             
        
        Test.StartTest();
        ID batchprocessid = Database.executeBatch(new MCMAccountSharingBatch('Email'),200);
        Test.StopTest();
        
        
        syncMCMAccountSharing MCMAccount = new syncMCMAccountSharing();
        MCMAccount.startMCMAccountSharing();
        MCMAccount.startMCMAccountSharing();
        MCMAccount.isRequestSubmit=true;
        MCMAccount.startMCMAccountSharing();
        
        ApexPages.StandardSetController StandCtrl = new ApexPages.StandardSetController(new List<Domestic_Only_Portfolio__c>());
        syncMCMAccountSharing MCMAcc = new syncMCMAccountSharing(StandCtrl);
        
        ApexClass objApexClass=new ApexClass();
        objApexClass =[Select a.Id From ApexClass a where name='MCMAccountSharingBatch' limit 1];
        List<AsyncApexJob> lstAsyncApexJob = ([SELECT Status, ApexClassId,CompletedDate,CreatedDate,CreatedBy.Name,CreatedBy.Email,JobItemsProcessed,TotalJobItems,NumberOfErrors  
                                                FROM AsyncApexJob 
                                                WHERE ApexClassId=:objApexClass.id 
                                                AND CreatedDate=TODAY 
                                                AND ( Status='Queued' OR Status='Processing' OR Status='Preparing') limit 1]);
                                         
        If(lstAsyncApexJob!=null && lstAsyncApexJob.size()>0)
        {
            system.abortJob(lstAsyncApexJob.get(0).Id);
        }
        MCMAccount.startMCMAccountSharing();        
    }
}