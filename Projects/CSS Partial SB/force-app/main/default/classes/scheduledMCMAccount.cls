/*
##################################################################################################################################
# Project Name..........: CSS McmAccounts Visibility Issue 
# File..................: Class : scheduledMCMAccount
# Version...............: 1.0
# Created by............: Sunny Kumar
# Created Date..........: 24-Dec-2013
# Last Modified by......: Sunny Kumar
# Last Modified Date....: 01-Jan-2014
# Description...........: This is scheduler class to start batch processing. 
#####################################################################################################################################
*/

global class scheduledMCMAccount implements Schedulable 
{
    public Boolean isTestCall=false;      
    public scheduledMCMAccount(){}
    
    global void execute(SchedulableContext sc)
    {
          ApexClass objApexClass=new ApexClass();
          objApexClass =[Select a.Id From ApexClass a where name='MCMAccountSharingBatch' limit 1];
          List<AsyncApexJob> lstAsyncApexJob = ([SELECT Status, ApexClassId,CompletedDate,CreatedDate,CreatedBy.Name,CreatedBy.Email,JobItemsProcessed,TotalJobItems,NumberOfErrors  
                                                FROM AsyncApexJob 
                                                WHERE ApexClassId=:objApexClass.id 
                                                AND CreatedDate=TODAY 
                                                AND ( Status='Queued' OR Status='Processing' OR Status='Preparing') limit 1]);
        
       
        if(lstAsyncApexJob.size()==0 && !isTestCall)
        {
            Integer iBatchSize=syncMCMAccountSharing.getBatchSize();
            String strEmailSubject='Nightly job ';
            MCMAccountSharingBatch objMCMAccount = new MCMAccountSharingBatch(strEmailSubject); 
            if(!Test.isRunningTest()){
              database.executebatch(objMCMAccount,iBatchSize);
            }
            try{
                CronTrigger objCron = [SELECT id,TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :sc.getTriggerId() and CronJobDetail.Name Like 'nextScheduledMCMAccountHourlyActivity_%'];
                system.abortJob(objCron.id);
            }catch(Exception ex){}
                  
        }else{
                  scheduledMCMAccount scheduler = new scheduledMCMAccount();
                  System.schedule('nextScheduledMCMAccountHourlyActivity_'+System.Now(), '0 '   + String.valueOf(59) + ' * * * ?', scheduler);
              //system.abortJob(sc.getTriggerId()); 
            
        }   
    }
}