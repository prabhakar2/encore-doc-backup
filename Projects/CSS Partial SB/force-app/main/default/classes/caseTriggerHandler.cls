/*
##################################################################################################################################
# Project Name..........: Case Trigger - 1 Trigger Per Object
# File..................: caseTriggerHandler
# Version...............: 1.0
# Created by............: Katerina Ames
# Created Date..........: 08-Apr-2019
# Last Modified by......: Katerina Ames
# Last Modified Date....: 08-Apr-2019
# Description...........: Handler class containing all the code referenced in the caseTrigger. As of 08-Apr-2019 this trigger
# ....................... round-robin assigns the CRAM RoundRobin Number for all new CRAM records. This will fire if a new case
# ....................... with CRAM record type is inserted OR if the record type on an existing Case is changed to CRAM.
#####################################################################################################################################
*/

public class caseTriggerHandler {

    @testVisible
    public static string CRAMRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CRAM').getRecordTypeId();
    
    @testVisible
    public static void checkCRAMUpdate(Map<Id,Case> oldCases, List<Case> newCases){
        //create new list containing all existing records that were changed to CRAM record type 
        List<Case> newCRAMCasesList = new List<Case>();
        for(Case newCase : newCases){
            Case oldCase = oldCases.get(newCase.Id);
            if(oldCase.RecordTypeId!=CRAMRecordTypeId && newCase.RecordTypeId==CRAMRecordTypeId){
                newCRAMCasesList.add(newCase);
            }
        }
        system.debug('>>>newCRAMCasesList size: '+newCRAMCasesList.size());
        //run the CRAM round-robin assignment just on the new CRAM Cases list
        if(newCRAMCasesList.size()>0){
            //assignCRAMRoundRobin(newCRAMCasesList);
            addRoundRobinNumber(newCRAMCasesList);
        }
    }
    
    @testVisible
    public static void checkCRAMInsert(List<Case> newCases){
        //create list containing all new cases with CRAM record type
        List<Case> newCRAMCasesList = new List<Case>();
        for(Case newCase : newCases){
            if(newCase.RecordTypeId==CRAMRecordTypeId){
                newCRAMCasesList.add(newCase);
            }
        }
        system.debug('>>>newCRAMCasesList size: '+newCRAMCasesList.size());
        //run the CRAM round-robin assignment just on the new CRAM Cases list
        if(newCRAMCasesList.size()>0){
            //assignCRAMRoundRobin(newCRAMCasesList);
            addRoundRobinNumber(newCRAMCasesList);
        }
    }
    
    @testVisible
    public static void addRoundRobinNumber(List<Case> newCases){
        //determine the most recent CRAM case that was assigned
        List<Case> lastCRAMCase = [SELECT Id, CRAM_RoundRobin_Number__c 
                                   FROM Case 
                                   WHERE RecordTypeId=:CRAMRecordTypeId AND CRAM_RoundRobin_Number__c!=null 
                                   ORDER BY CRAM_RoundRobin_Number__c DESC LIMIT 1];
        Integer lastNumber = 1; //will start the list at the beginning (2) if no CRAM cases already exist
        if(lastCRAMCase.size()>0){
            lastNumber = Integer.valueOf(lastCRAMCase[0].CRAM_RoundRobin_Number__c);
        }
        system.debug('>>>caseTriggerHandler.addRoundRobinNumber -- lastNumber = '+lastNumber);
        //assign the CRAM RoundRobin Number and new owner
        for(Case c : newCases){
            if(c.RecordTypeId==CRAMRecordTypeId){
                c.CRAM_RoundRobin_Number__c = lastNumber+1;
                lastNumber++;
            }
        }
        
    }
    
    @testVisible
    public static void regexDMAccountCommentary(Map<Id,Case> oldCaseMap, Map<Id,Case> newCaseMap){
        String message;
        //This list will apply to all cases
        List<String> rxListA = new List<String>{
            '[0-9]{3}-[0-9]{2}-[0-9]{4}',               //ssn with dashes
            '4[0-9]{12}(?:[0-9]{3})?',                  //visa
            '(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}', //mastercard
            '3[47][0-9]{13}',                           //amex
            '[0-9]{16}'                                 //generic 16digit cc#
        };
        //This list will only apply to cases which do not have the Account Number in Commentary box checked
        //this is necessary because SSNs and Account #s are both 9 digit numbers
        List<String> rxListB = new List<String>{
            '[^0-9][0-9]{9}[^0-9]',
            '^[0-9]{9}[^0-9]',
            '[^0-9][0-9]{9}$',
            '^[0-9]{9}$'                                 //9 digit ssn no dashes
        }; 
        for(Case newCase : newCaseMap.values()){
            Case oldCase = oldCaseMap.get(newCase.id);
            if(newCase.DM_Account_Commentary__c!=null &&
               newCase.DM_Account_Commentary__c!=oldCase.DM_Account_Commentary__c){
                for(string s : rxListA){
                    Pattern p = Pattern.compile(s);
                    Matcher pm = p.matcher(newCase.DM_Account_Commentary__c);
                    if(pm.find()){
                        message = 'DM Account Commentary cannot contain SSNs or CC#s. [Apex: CaseTrigger]';
                        newCase.DM_Account_Commentary__c.adderror(message);
                    }
                    system.debug('**** ' + message);
                }
                for(string s : rxListB){
                    Pattern p = Pattern.compile(s);
                    Matcher pm = p.matcher(newCase.DM_Account_Commentary__c);
                    if(pm.find() && newCase.Account_Number_in_Commentary__c==false){
                        message = 'DM Account Commentary cannot contain SSNs or CC#s. [Apex: CaseTrigger]';
                        newCase.DM_Account_Commentary__c.adderror(message);
                    }
                    system.debug('**** ' + message);
                }
            }
        }
    }
    
    @testVisible
    static public void fieldUpdate(List<Case> newCase){
        Map<String, String> fieldListMap = new Map<String, String>{
'Employee Site'                         => 'Employee_Site__c',
'Complainant\'s State'                   => 'State_of_AEM_Complainant__c',
'Quality Assurance Violation 1'         => 'Quality_Assurance_Violation_1__c',
'Quality Assurance Violation 2'         => 'Quality_Assurance_Violation_2__c',
'Quality Assurance Violation 3'         => 'Quality_Assurance_Violation_3__c',
'Quality Assurance Violation 4'         => 'Quality_Assurance_Violation_4__c',
'Quality Assurance Violation 5'         => 'Quality_Assurance_Violation_5__c',
'Collector Code of Employee'            => 'Collector_Code__c',
'Collector Name'                        => 'Collector_Name__c',
'GM Collector Code'                     => 'GM_Collector_Code__c',
'AM Collector Code'                     => 'AM_Collector_Code__c',
'Employee Alias'                        => 'Alias_for_AEM__c',
'Employee Team'                      => 'Team_ID_for_AEM__c',
'Employee User ID'                      => 'User_ID_for_AEM__c',
'CC of Employee Submitting ACD'         => 'CC_of_Employee_Submitting_CDI__c',
'Source'                                => 'Source__c',
'CC of GM Submitting ACD'               => 'GM_Collector_Code__c',
'AEM Notes'                             => 'AEM_Notes__c',
'Email Address of Employee'             => 'Email_Address_of_Employee__c',
'Email Address of Group Manager of Employee' => 'Email_of_GM_of_Employee__c',
'Email Address of Division Manager of Employee' => 'Email_of_DM_of_Employee__c',
'Email Address of Director of Employee' => 'Email_of_Director_of_Employee__c',
'Does it Require CSS Investigation'     => 'Requires_CSS_Investigation__c',
'Check Date'                            => 'Posting_Check_Date_BG__c',
'Check Number'                          => 'Check_Number__c',
'SOL Date'                              => 'Posting_SOL_Date_BG__c',
'POC Date'                              => 'Posting_POC_Date_BG__c',
'Check Amount'                          => 'Posting_Check_Amount_BG__c',
'Is there a Judgment (009)'             => 'Judgment_Available__c',
'Misc'                                  => 'Misc__c',
'Removal Request'                       => 'Removal_Request__c',
'BK Case #'                             => 'BK_Case_Number__c',
'Has consumer made a payment to the issuer' => 'Consumer_Payment_to_Issuer__c',
'If yes, date of payment made to issuer' => 'CRP_Issuer_Payment_Date_Background__c',
'Is investigation regarding Credit Reporting' => 'Credit_Report_Investigation__c',
'If yes, which Credit Bureau'           => 'Credit_Bureau__c',
'Is investigation regarding payments'   => 'Payment_Investigation__c',
'If yes, has Posting investigated'      => 'Posting_Investigation__c',
'Describe results of Posting investigation' => 'Posting_Investigation_Details__c',
'Additional Information'                => 'Additional_Information',
'MCM Account'                           => 'MCM_Account_Number__c',
'Alleged Employee Misconduct 1'         => 'Alleged_Employee_Misconduct_1__c',
'Alleged Employee Misconduct 2'         => 'Alleged_Employee_Misconduct_2__c',
'Alleged Employee Misconduct 3'         => 'Alleged_Employee_Misconduct_3__c',
'Alleged Employee Misconduct 4'         => 'Alleged_Employee_Misconduct_4__c',
'Alleged Employee Misconduct 5'         => 'Alleged_Employee_Misconduct_5__c',
'Alleged Consumer Dissatisfaction Source'         => 'Consumer_Dissatisfaction_From__c',
'GM Submitting ACD'                     => 'GM_Submitting_ACD__c',
'Specific Alleged Employee Misconduct'  => 'Quality_Assurance_Violation_1__c',
'GUI Notes'                             => 'GUI_Notes__c',
'Alleged Employee Misconduct'           => 'Alleged_Employee_Misconduct_1__c',
'Allegation from Source'                => 'AEM_Notes__c' ,
'Consumer Name'                         => 'SuppliedName' ,
'Account'                               => 'Provided_Account_Number__c' ,
'Last 4 of SSN'                         => 'Last_4_of_SSN__c',
'Address'                                => 'Current_Address__c',
'City'                                   => 'Current_City__c',
'State'                                  => 'CCO_State__c',
'Zip Code'                               => 'Zip_Code__c',
'Email Address'                          => 'SuppliedEmail',
'Phone Number'                           => 'Phone_Number__c',
'CCO Category'                           => 'CCO_Category__c',
'CCO Sub Category'                       => 'CCO_Sub_Category__c',
'Supporting Documentation'               => 'Supporting_Documentation__c',
'Preferred Method of Contact'            => 'Preferred_Method_of_Contact__c',
'Additional Information'                 => 'Additional_Information__c',
'Desired Resolution'                     => 'Desired_Resolution__c',
'Type'                                   => 'Type',
'Case Type'                              => 'Case_Type__c'   




        };
        List<String> descpSplitList;
        List<String> labelValuePairSplitList;
        for(Case cas : newCase){
            if(!String.isBlank(cas.Description)){
                descpSplitList = cas.Description.split('\n');
           
                for(String labelValuePair : descpSplitList){
                    labelValuePairSplitList = labelValuePair.split(':');
                    if (
                        labelValuePairSplitList.size() > 1 &&
                        fieldListMap.containsKey(labelValuePairSplitList[0])
                    ) {
                        cas.put(fieldListMap.get(labelValuePairSplitList[0]), labelValuePairSplitList[1]);
                    }
                }
            }
        }
    } 
}