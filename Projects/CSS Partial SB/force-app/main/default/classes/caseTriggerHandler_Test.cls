/*
##################################################################################################################################
# Project Name..........: Case Trigger - 1 Trigger Per Object
# File..................: caseTriggerHandler_Test
# Version...............: 1.0
# Created by............: Katerina Ames
# Created Date..........: 08-Apr-2019
# Last Modified by......: Katerina Ames
# Last Modified Date....: 08-Apr-2019
# Description...........: Test class for the caseTriggerHandler. 
#####################################################################################################################################
*/

@isTest
public class caseTriggerHandler_Test {

    public static Opportunity myOpp = new Opportunity();
    public static string CRAMRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CRAM').getRecordTypeId();
    public static string CorrRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Correspondence').getRecordTypeId();
   
    @testSetup
    public static void testSetup(){
        //create parent account
        Account acct = new Account(
            FirstName='Test',
            LastName='Consumer'
        );
        insert acct;
        //create parent opportunity
        Opportunity opp = new Opportunity(
            AccountId = acct.id,
            CloseDate = date.TODAY(),
            Name = '1234567890',
            StageName = 'Not Applicable'
        );
        insert opp;
        myOpp = opp;
        system.debug('>>>myOpp: '+myOpp);
    }

    @isTest
    public static void testPOS(){
        Case c = new Case(
            Origin = 'Mail',
            Status = 'Open',
            Opportunity__c = myOpp.id,
            RecordTypeId = CRAMRecordTypeId
        );
        insert c;
        List<Case> newCase = [SELECT Id, CRAM_RoundRobin_Number__c FROM Case LIMIT 1];
        Decimal newRoundRobinNumber = newCase[0].CRAM_RoundRobin_Number__c;
        system.debug('>>>caseTriggerHandler_Test.testPOS(): newRoundRobinNumber = '+newRoundRobinNumber);
        system.assert(newRoundRobinNumber>1);
    }
    
    @isTest
    public static void testNEG(){
        Case c = new Case(
            Origin = 'Mail',
            Status = 'Open',
            Opportunity__c = myOpp.id,
            RecordTypeId = CorrRecordTypeId
        );
        insert c;
        List<Case> newCase = [SELECT Id, CRAM_RoundRobin_Number__c FROM Case LIMIT 1];
        Decimal newRoundRobinNumber = newCase[0].CRAM_RoundRobin_Number__c;
        system.assert(newRoundRobinNumber==null||newRoundRobinNumber==0);
    }   
    
    @isTest
    public static void testREGEX(){
        Case c = new Case(
            Origin = 'Mail',
            Status = 'Open',
            Opportunity__c = myOpp.id,
            RecordTypeId = CorrRecordTypeId
        );
        insert c;
        c.DM_Account_Commentary__c = 'Test with an SSN 123-45-6789 SSN Test,';
        try{
            update c;
        } catch (DMLException e){
            system.assert(e.getMessage().contains('DM Account Commentary cannot contain SSNs or CC#s. [Apex: CaseTrigger]'));
        }
    }
    
    @isTest
    public static void testFieldUpdate(){
        Case c = new Case(
            Origin = 'Mail',
            Status = 'Open',
            Opportunity__c = myOpp.id,
            RecordTypeId = CorrRecordTypeId,
            Description = 'Consumer Name: TestPerson\nLast four digits of SSN: 9876\nAccount: 55555555\nAddress: ,\nCity:\nState:\nZip Code:\nEmail Address: \nPhone Number: (123) 456-7890\nCCO Category: Took or threatened to take negative or legal action\nCCO Sub Category: Your company threatened to take negative action against me'
        );
        insert c;
        
        Case cc = [SELECT SuppliedName, Last_4_of_SSN__c, Provided_Account_Number__c, Current_Address__c, Current_City__c,
                  CCO_State__c, Zip_Code__c, SuppliedEmail, Phone_Number__c, CCO_Category__c, CCO_Sub_Category__c
                  FROM Case
                  WHERE Id=:c.Id LIMIT 1];
        system.assertEquals('TestPerson', cc.SuppliedName, 'SuppliedName');
        //system.assertEquals('9876', cc.Last_4_of_SSN__c, 'Last4ofSSN');
        system.assertEquals('55555555', cc.Provided_Account_Number__c, 'Provided Account Number');
        system.assertEquals('(123) 456-7890', cc.Phone_Number__c, 'Phone number');
        system.assertEquals('Took or threatened to take negative or legal action', cc.CCO_Category__c, 'CCO Category');
        system.assertEquals('Your company threatened to take negative action against me', cc.CCO_Sub_Category__c, 'CCO Sub Category');
    }
}