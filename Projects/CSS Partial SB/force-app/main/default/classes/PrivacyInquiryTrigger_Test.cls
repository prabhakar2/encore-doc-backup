@isTest
public class PrivacyInquiryTrigger_Test {
	@isTest
    static void test1(){
        Individual ind = new Individual();
        ind.FirstName = 'test';
        ind.LastName = 'test';
        insert ind;
        
        Privacy_Inquiry__c inq = new Privacy_Inquiry__c();
        inq.Individual__c = ind.Id;
        inq.First_Name__c = 'test';
        inq.Last_Name__c = 'test';
        inq.Last_4_SSN__c = '1234';
        inq.Confirm_Information_is_Correct__c  = true;
        inq.Request_to_Delete_Data__c  = true;
        inq.Company_Selection__c = 'Midland Credit Management';
        inq.Relationship_to_Company__c = 'Consumer';
        inq.Response_Preference__c = 'Mail';
        insert inq;
    }
}