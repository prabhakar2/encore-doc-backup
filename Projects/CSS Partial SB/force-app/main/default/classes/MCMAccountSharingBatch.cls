/*
##################################################################################################################################
# Project Name..........: CSS McmAccounts Visibility Issue 
# File..................: Class : "MCMAccountSharingBatch"
# Version...............: 1.0
# Created by............: Sunny Kumar
# Created Date..........: 24-Dec-2013
# Last Modified by......: Sunny Kumar
# Last Modified Date....: 01-Jan-2014
# Description...........: It will update Is_Domestic Flag true based on matched portfolio in Domestic portfolio object and
#                         Update all related/associated Child Cases. 
#####################################################################################################################################
*/

global class MCMAccountSharingBatch implements Database.Batchable<sObject>{

public Map<String,String> mapPortfolioNumber=new Map<String,String>();
public String strSubject{get;set;}


public MCMAccountSharingBatch(String strSubject){
   for(Domestic_Only_Portfolio__c obj:[Select id,Name from Domestic_Only_Portfolio__c]){
      mapPortfolioNumber.put(obj.Name,obj.Name);
   }
   this.strSubject=strSubject;
}


global Database.QueryLocator start(Database.BatchableContext BC){
     
     Set<String> setPortfolioNumber=new Set<String>();
     /*for(Domestic_Only_Portfolio__c obj:[Select id,Name from Domestic_Only_Portfolio__c]){
      mapPortfolioNumber.put(obj.Name);
      setPortfolioNumber.add(obj.Name);
     }*/
     setPortfolioNumber=mapPortfolioNumber.keySet();
     if(setPortfolioNumber.size()==0){
        setPortfolioNumber.add('Not Found');
     }
  
 
     return Database.getQueryLocator([Select id,IS_DOMESTIC__c,msport__c from Opportunity where
                                      (Owner.IsActive=true and ((msport__c IN:setPortfolioNumber and Is_Domestic__c=false) OR 
                                      (msport__c NOT IN :setPortfolioNumber and Is_Domestic__c=true)))]);
   }
 global void execute(Database.BatchableContext BC, List<sObject> scope){
     List<Opportunity> lstOpp=new List<Opportunity>();
     List<Case> lstCase=new List<Case>();
     Set<Id> setOppId=new Set<Id>();
     Map<String,List<Case>> mapOppCase=new  Map<String,List<Case>>();
     
     for(sobject objSobject : scope)
      {
        Opportunity objOpp=(Opportunity) objSobject;
        setOppId.add(objOpp.id);
     }
      
      for(Case objCase:[Select id,Is_Domestic__c,Opportunity__c from Case where Owner.IsActive=true and Opportunity__c IN:setOppId order by lastmodifiedDate desc limit 9000]){
        if(mapOppCase.get(objCase.Opportunity__c)==null){
            mapOppCase.put(objCase.Opportunity__c,new List<Case>{objCase});
        }else{
            mapOppCase.get(objCase.Opportunity__c).add(objCase);
        }
      }
      
        
    
     for(sobject objSobject : scope)
      {
        Opportunity objOpp=(Opportunity) objSobject;
        Boolean isDomestic=true;
        if(mapPortfolioNumber.get(objOpp.msport__c)==null){
              isDomestic=false;
        }
        objOpp.Is_Domestic__c=isDomestic;
        lstOpp.add(objOpp);
        
        if(mapOppCase.get(objOpp.id)!=null){
            for(Case objCase:mapOppCase.get(objOpp.id)){
              if(objCase.Is_Domestic__c!=isDomestic){
                   objCase.Is_Domestic__c=isDomestic;
                   lstCase.add(objCase);
               }
            }
        }
      }
      FollowUpTaskHelper.setAlreadyCreatedFollowUpTasks();
      update lstOpp;
      if(lstCase.size()>0){
        update lstCase;
      }
    }

   global void finish(Database.BatchableContext BC){
                    
    AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email,ExtendedStatus FROM AsyncApexJob WHERE Id =:BC.getJobId()];
   // Send an email to the Apex job's submitter notifying of job completion.
   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
   String[] toAddresses = new String[] {a.CreatedBy.Email};
   mail.setToAddresses(toAddresses);
   
   List<String> lstEmailId=new List<String>();
   for (EmailNotification__c objEmail : EmailNotification__c.getAll().values()){
     lstEmailId.add(objEmail.Email__c);
   }
   if(lstEmailId.size()>0){
     mail.setCcAddresses(lstEmailId);
   }
   mail.setSubject(''+strSubject+':' + a.Status);
  
   String strEmailContent=' The batch Apex job processed <b>' + a.TotalJobItems +
                          '</b> batches with <b>'+ a.NumberOfErrors + '</b> failures. <br> <br>';
      if(a.NumberOfErrors>0){
        strEmailContent=strEmailContent+' Please find the error detail in below: <br> ';
        strEmailContent=strEmailContent+' <b> <font color=\'red\'>'+a.ExtendedStatus+'</font></b>';
      }
                 
   mail.setHtmlBody(strEmailContent);
   Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
  }

}