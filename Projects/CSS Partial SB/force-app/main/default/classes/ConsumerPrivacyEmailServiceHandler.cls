/*

    Description : When an inquiry would be come from email this email service handler would create 
                  a record and this inquiry a dummy individual(standard salesforce object) record would be 
                  created or available named as can not find individual and this service will create a new record    
                  in other hand a trigger is available in inquiry object which will manage the record in salesforce
                  

    @uthor      : Avaneesh Singh, Developer, India 
    Created Date: 3/12/2019
    HandlerClass:ConsumerPrivacyEmailServiceHandlerHelper
*/


global class ConsumerPrivacyEmailServiceHandler implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope){
       
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        // Creating a dummy indivdual record here 
        ConsumerPrivacyEmailServiceHandlerHelper helperClassInstance = ConsumerPrivacyEmailServiceHandlerHelper.createInstance();
        Individual recordIndividual = helperClassInstance.getDummyIndividual();
        
        Privacy_Inquiry__c pi = new Privacy_Inquiry__c(Individual__c=recordIndividual.id); /* instance of privacy child record here */ 
        
        if(email.htmlBody!= null && email.htmlBody!= ''){
            pi.EmailFormText__c = email.htmlBody;
            Map<String,String> fieldMappingMap = helperClassInstance.getMapField(); // fetch the field mapping here 
            List<String> allSplitList = new List<String>();
            allSplitList = email.htmlBody.split('\n'); // split the email text body here 
            Map<String,Schema.DisplayType> fieldWithReferenceMap = helperClassInstance.getFieldWithReference('Privacy_Inquiry__c');
            boolean flag = false;
             if(allSplitList.size() > 0){
                    
                    for(String str : allSplitList ){
                       
                        List<String> fieldDataList = str.trim().split(':');
                        // first index contains key and second index contains values
                        
                        if(fieldMappingMap.containsKey(fieldDataList[0].trim().toLowerCase()) && fieldDataList.size() > 0 && fieldDataList.size() <=2){
                         flag=true;
                         String fieldApiName = fieldMappingMap.get(fieldDataList[0].trim().toLowerCase());    
                           if(fieldDataList[1] != null && fieldDataList[1] != ''){
                               String fieldDataValue =  fieldDataList[1].trim().removeEndIgnoreCase('<br/>');
                               
                               if(fieldWithReferenceMap.containsKey(fieldApiName.toLowerCase()) && fieldWithReferenceMap.get(fieldApiName.toLowerCase()) == Schema.DisplayType.BOOLEAN){
                                if(fieldDataValue.trim().equalsIgnoreCase('true') || fieldDataValue.trim().equalsIgnoreCase('yes')){
                                    pi.put(fieldApiName ,True);
                                }else if(fieldDataValue.trim().equalsIgnoreCase('false') || fieldDataValue.trim().equalsIgnoreCase('no')){
                                    pi.put(fieldApiName ,False);
                                }
                            }else{
                                pi.put(fieldApiName ,fieldDataValue);
                            }
                           }
                        }
                    }
                
              }  
            if(flag){  
                Id recordTypeId= Schema.SObjectType.Privacy_Inquiry__c.getRecordTypeInfosByName().get('CCPA').getRecordTypeId();
                
                // field should be default in the record are here as per requirement 
                
                pi.put('source__c','web');
                pi.put('Template_Language__c','English');
                pi.put('Received_Date__c',date.today());
                pi.put('recordtypeid',recordtypeid);
                try{
                 insert pi;
                }catch(Exception e){
                }
            }
        }
        
        List<Attachment> attList = new List<Attachment>();
            
        /* if attachment is exists then we need to insert */
        if(email.binaryAttachments != null && email.binaryAttachments.size() > 0 ){
            
           if(pi.id != null){ 
            for(Messaging.InboundEmail.BinaryAttachment ba : email.binaryAttachments){
              
              Attachment att = new Attachment();
              att.body = ba.body;
              att.name = ba.filename;
              att.parentid = pi.id;
              
              attList.add(att);
            
            }
            }
          
        }
        
        if (email.textAttachments != null && email.textAttachments.size() > 0) {
            if(pi.id != null){
            for (integer i = 0 ; i < email.textAttachments.size() ; i++) {
                Attachment attachment = new Attachment();
                attachment.ParentId = pi.Id;
                attachment.Name = email.textAttachments[i].filename;
                attachment.Body = blob.valueOf(email.textAttachments[i].body);
                attList.add(attachment);
             }
             }
        }
        
          
         if(attList.size() > 0){
          try{
          insert attList;
          }catch(Exception e){
          }
         }
        
        
        return result;
    }
    
}