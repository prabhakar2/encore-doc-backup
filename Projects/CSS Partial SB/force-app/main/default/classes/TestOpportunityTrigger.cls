@isTest
private class TestOpportunityTrigger {

    @isTest
    private static void testTrigger(){
        //create parent account
        Account acct = new Account(
            FirstName='Test',
            LastName='Consumer'
        );
        insert acct;
        //create parent opportunity
        Opportunity opp = new Opportunity(
            AccountId = acct.id,
            CloseDate = date.TODAY(),
            Name = '1234567890',
            StageName = 'Not Applicable'
        );
        insert opp;
        //create case
        Case c = new Case(
            Origin = 'Mail',
            Status = 'Open',
            Opportunity__c = opp.id,
            Quality_Assurance_Violation_1__c='Did not avoid discussing the account with a 3rd party',
            Quality_Assurance_Violation_2__c='Did not provide accurate information pertaining to credit reporting of an account(s)',
            Quality_Assurance_Violation_3__c='Was unprofessional or rude',
            Quality_Assurance_Violation_4__c='Did not disable wrong party numbers',
            Quality_Assurance_Violation_5__c='Took over call, did not properly document the account'
        );
        
        insert c;

        /*system.assertEquals(c.AccountId==acct.Id);
        system.assert('TPD1',c.Compliance_Violation_Code_1__c,'Compliance Violation Code 1 not correct');
        system.assert('FMR2',c.Compliance_Violation_Code_2__c,'Compliance Violation Code 2 not correct');
        system.assert('FAIL4',c.Compliance_Violation_Code_3__c,'Compliance Violation Code 3 not correct');
        system.assert('FAIL17',c.Compliance_Violation_Code_4__c,'Compliance Violation Code 4 not correct');
        system.assert('FAIL28',c.Compliance_Violation_Code_5__c,'Compliance Violation Code 5 not correct');*/
    
    }





/*
   @isTest(SeeAllData=true)
    static void HandleOpportunityTest() {

for (Account a : [SELECT ID, Name FROM Account Where Name Like 'Test%'])
    {
        List<Account> accList = createPersonAccounts(2);
        Account acc1 = accList[0];
        Account acc2 = accList[1];
        
        Opportunity opp =   createOpportunity(acc1);

        Case mcase = createCase(opp,acc1);
        
        opp.AccountId = acc2.Id;
        update opp;     
    } 
    }
    static List<Account> createPersonAccounts(Integer numberOfAccounts) {
    
        List<Account> accts = new List<Account>();
        Id persRecTypeId = [select Id from RecordType where SObjectType = 'Account' and Name = 'Person Account'].Id;
          for (Integer idx = 0; idx < numberOfAccounts; idx++) {
            System.debug('idx='+idx);
            Account acct = new Account();       
            acct.RecordTypeId = persRecTypeId;
            acct.FirstName = 'T01FN-'+idx;
            acct.LastName = 'T01LN-'+idx;
           
            accts.add(acct);
        }   
        insert accts;
        return accts;    
    }
    
    
    static Opportunity createOpportunity(Account acct) {
       
        Opportunity opp = new Opportunity();       
        opp.AccountId = acct.Id;
        opp.Name = 'Test Opp';
        opp.StageName = 'Application Started';
        opp.CloseDate = System.today(); 
        
        insert opp ;
        return opp ;  
    } 
    
    static Case createCase(Opportunity opp, Account acc)
    {
     Case mcase = new Case();
     mcase.Subject= 'Test Case';
     mcase.Opportunity__c = opp.Id;
     mcase.AccountId = acc.Id;
     mcase.status = 'New';
     insert mcase;
     return mcase;
    }
    
    */
    
    
 }