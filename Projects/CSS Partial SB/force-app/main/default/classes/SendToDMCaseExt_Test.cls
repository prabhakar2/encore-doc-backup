@isTest(seeAllData=True)
global class SendToDMCaseExt_Test {
    
    @isTest static void testElseSendToDM(){
    
        Account a = new Account();
        a.FirstName = 'First Name';
        a.LastName  = 'LastName';
        insert a;
        
        Opportunity opp = new Opportunity();       
        opp.AccountId   = a.Id;
        opp.Name      = 'Test Opp';
        opp.StageName = 'Application Started';
        opp.CloseDate = System.today().addDays(15);
        opp.DM_Account_External_ID__c = '0001234'; 
        insert opp ;
        
        Case c = new Case();
        c.Subject = 'Test Case';
        c.Opportunity__c = opp.Id;
        c.AccountId = a.Id;
        c.status    = 'New';
        insert c;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        SendToDMCaseExt Ctrl = new SendToDMCaseExt(sc);
        Test.StartTest();
            Test.setMock(HttpCalloutMock.class, new ExampleCalloutMockSuccess());
            Ctrl.sendToDM();
        Test.stopTest();    
   
    }
    
    
    @isTest static void testSendToDMSuccess() {
        
        Account a = new Account();
        a.FirstName = 'First Name';
        a.LastName  = 'LastName';
        a.DM_Consumer_External_ID__c = '000000';
        insert a;
        
        Opportunity opp = new Opportunity();       
        opp.AccountId   = a.Id;
        opp.Name      = 'Test Opp';
        opp.StageName = 'Not Applicable';
        opp.CloseDate = System.today().addDays(15);
        opp.DM_Account_External_ID__c = '11111'; 
        insert opp ;
        
        Case c = new Case();
        c.Subject = 'Test Case';
        c.Opportunity__c = opp.Id;
        c.AccountId = a.Id;
        c.Status = 'New';
        c.Consumer_Status_to_Remove__c = 'DMONLY';
        c.Language_Tag_to_Remove__c = 'SPNUNCNF';
        c.Account_Status_to_Remove__c = 'CND';
        c.Communication_Tag_to_Remove__c = 'ATTORNEY';
        c.Dispute_Tag_to_Remove__c = 'BALRVW';
        c.Complaint_Tag_to_Remove__c = 'CMPLNT';
        c.Consumer_Status_to_Add__c  = 'TEMPHRS';
        c.Language_Tag_to_Add__c = 'NESCNF';
        c.Account_Status_to_Add__c = 'DCA';
        c.Communication_Tag_to_Add__c = 'ATTORNEY';
        c.Dispute_Tag_to_Add__c = 'BALCNF';
        c.Complaint_Tag_to_Add__c = 'CMPLNT';
        c.DM_Account_Commentary__c = 'test';
        insert c;
        String accountAgencyId = c.Account_External_ID__c;
        String consumerAgencyId = c.Consumer_External_ID__c;
        Case testCase = [SELECT Id, Subject, Opportunity__c, AccountId, Status, Consumer_Status_to_Remove__c, Language_Tag_to_Remove__c,
                        Account_Status_to_Remove__c, Communication_Tag_to_Remove__c, Dispute_Tag_to_Remove__c, Complaint_Tag_to_Remove__c,
                        Consumer_Status_to_Add__c, Language_Tag_to_Add__c, Account_Status_to_Add__c, Communication_Tag_to_Add__c,
                        Dispute_Tag_to_Add__c, Complaint_Tag_to_Add__c, DM_Account_Commentary__c, Account_External_ID__c, Consumer_External_ID__c
                        FROM Case
                        WHERE Id=:c.Id LIMIT 1];
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
        SendToDMCaseExt Ctrl = new SendToDMCaseExt(sc);
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new ExampleCalloutMockSuccess());
            Ctrl.sendToDM();
        Test.stopTest();
     
    }
    
    @isTest static void testSendToDMError() {
        
        Account a = new Account();
        a.FirstName = 'First Name';
        a.LastName  = 'LastName';
        a.DM_Consumer_External_ID__c = '000000';
        insert a;
        
        Opportunity opp = new Opportunity();       
        opp.AccountId   = a.Id;
        opp.Name      = 'Test Opp';
        opp.StageName = 'Not Applicable';
        opp.CloseDate = System.today().addDays(15);
        opp.DM_Account_External_ID__c = '11111'; 
        insert opp ;
        
        Case c = new Case();
        c.Subject = 'Test Case';
        c.Opportunity__c = opp.Id;
        c.AccountId = a.Id;
        c.Status = 'New';
        c.Consumer_Status_to_Remove__c = 'DMONLY';
        c.Language_Tag_to_Remove__c = 'SPNUNCNF';
        c.Account_Status_to_Remove__c = 'CND';
        c.Communication_Tag_to_Remove__c = 'ATTORNEY';
        c.Dispute_Tag_to_Remove__c = 'BALRVW';
        c.Complaint_Tag_to_Remove__c = 'CMPLNT';
        c.Consumer_Status_to_Add__c  = 'TEMPHRS';
        c.Language_Tag_to_Add__c = 'NESCNF';
        c.Account_Status_to_Add__c = 'DCA';
        c.Communication_Tag_to_Add__c = 'ATTORNEY';
        c.Dispute_Tag_to_Add__c = 'BALCNF';
        c.Complaint_Tag_to_Add__c = 'CMPLNT';
        c.DM_Account_Commentary__c = 'test';
        insert c;
        String accountAgencyId = c.Account_External_ID__c;
        String consumerAgencyId = c.Consumer_External_ID__c;
        Case testCase = [SELECT Id, Subject, Opportunity__c, AccountId, Status, Consumer_Status_to_Remove__c, Language_Tag_to_Remove__c,
                        Account_Status_to_Remove__c, Communication_Tag_to_Remove__c, Dispute_Tag_to_Remove__c, Complaint_Tag_to_Remove__c,
                        Consumer_Status_to_Add__c, Language_Tag_to_Add__c, Account_Status_to_Add__c, Communication_Tag_to_Add__c,
                        Dispute_Tag_to_Add__c, Complaint_Tag_to_Add__c, DM_Account_Commentary__c, Account_External_ID__c, Consumer_External_ID__c
                        FROM Case
                        WHERE Id=:c.Id LIMIT 1];
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
        SendToDMCaseExt Ctrl = new SendToDMCaseExt(sc);
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new ExampleCalloutMockError());
            Ctrl.sendToDM();
        Test.stopTest();
     
    }
    
    @isTest static void testSendToDMUnknownError() {
        
        Account a = new Account();
        a.FirstName = 'First Name';
        a.LastName  = 'LastName';
        a.DM_Consumer_External_ID__c = '000000';
        insert a;
        
        Opportunity opp = new Opportunity();       
        opp.AccountId   = a.Id;
        opp.Name      = 'Test Opp';
        opp.StageName = 'Not Applicable';
        opp.CloseDate = System.today().addDays(15);
        opp.DM_Account_External_ID__c = '11111'; 
        insert opp ;
        
        Case c = new Case();
        c.Subject = 'Test Case';
        c.Opportunity__c = opp.Id;
        c.AccountId = a.Id;
        c.Status = 'New';
        c.Consumer_Status_to_Remove__c = 'DMONLY';
        c.Language_Tag_to_Remove__c = 'SPNUNCNF';
        c.Account_Status_to_Remove__c = 'CND';
        c.Communication_Tag_to_Remove__c = 'ATTORNEY';
        c.Dispute_Tag_to_Remove__c = 'BALRVW';
        c.Complaint_Tag_to_Remove__c = 'CMPLNT';
        c.Consumer_Status_to_Add__c  = 'TEMPHRS';
        c.Language_Tag_to_Add__c = 'NESCNF';
        c.Account_Status_to_Add__c = 'DCA';
        c.Communication_Tag_to_Add__c = 'ATTORNEY';
        c.Dispute_Tag_to_Add__c = 'BALCNF';
        c.Complaint_Tag_to_Add__c = 'CMPLNT';
        c.DM_Account_Commentary__c = 'test';
        insert c;
        String accountAgencyId = c.Account_External_ID__c;
        String consumerAgencyId = c.Consumer_External_ID__c;
        Case testCase = [SELECT Id, Subject, Opportunity__c, AccountId, Status, Consumer_Status_to_Remove__c, Language_Tag_to_Remove__c,
                        Account_Status_to_Remove__c, Communication_Tag_to_Remove__c, Dispute_Tag_to_Remove__c, Complaint_Tag_to_Remove__c,
                        Consumer_Status_to_Add__c, Language_Tag_to_Add__c, Account_Status_to_Add__c, Communication_Tag_to_Add__c,
                        Dispute_Tag_to_Add__c, Complaint_Tag_to_Add__c, DM_Account_Commentary__c, Account_External_ID__c, Consumer_External_ID__c
                        FROM Case
                        WHERE Id=:c.Id LIMIT 1];
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
        SendToDMCaseExt Ctrl = new SendToDMCaseExt(sc);
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new ExampleCalloutMockUnknownError());
            Ctrl.sendToDM();
        Test.stopTest();
     
    }
    
    //Mock Response for web service call upon success
    global class ExampleCalloutMockSuccess implements HttpCalloutMock{
        global HttpResponse respond(HTTPRequest req){
            HttpResponse res = new HttpResponse();
            res.setStatus('OK');
            res.setStatusCode(200);
            res.setHeader('Content-Type', 'application/json');
            String jsonStr = '{ "status": "success" }';
            res.setBody(jsonStr);
            return res;
        }
    }
    
    //Mock Response for web service call upon error
    global class ExampleCalloutMockError implements HttpCalloutMock{
        global HttpResponse respond(HTTPRequest req){
            HttpResponse res = new HttpResponse();
            res.setStatus('Unsupported Media Type');
            res.setStatusCode(415);
            res.setHeader('Content-Type', 'application/json');
            String jsonStr = '{ "codeKey": "web.application.exception.errorcode","message": "HTTP 415 Unsupported Media Type", "status": "fail" }';
            res.setBody(jsonStr);
            return res;
        }
    }
    
    //Mock Response for web service call upon unknown error
    global class ExampleCalloutMockUnknownError implements HttpCalloutMock{
        global HttpResponse respond(HTTPRequest req){
            HttpResponse res = new HttpResponse();
            res.setStatus('Unknown Error');
            res.setStatusCode(500);
            res.setHeader('Content-Type', 'application/json');
            String jsonStr = '{ "status": "fail" }';
            res.setBody(jsonStr);
            return res;
        }
    }



}