@isTest
public class feedItemTriggerHandler_Test{

    /*
    =================================================================
    This is not needed now because the option to edit chatter posts
    is not enabled under chatter settings. If it is ever enabled,
    then you will need to uncomment this section of the code.
    =================================================================
    @isTest
    public static void testEdit(){
        Account acct = new Account(
            FirstName='Test',
            LastName='Consumer'
        );
        insert acct;
        FeedItem post = new FeedItem();
            post.ParentId = acct.Id;
            post.Body = 'Test';
        insert post;
            post.Body = 'Edited';
        try{
            update post;
        } catch(DMLException e){
            system.assert(e.getMessage().contains('FeedItem edit unauthorized.'));
        }
    }
    ======================================================
    */
    @isTest
    public static void testDelete(){
        Account acct = new Account(
            FirstName='Test',
            LastName='Consumer'
        );
        insert acct;
        FeedItem post = new FeedItem();
            post.ParentId = acct.Id;
            post.Body = 'Test';
        insert post;
        try{
            delete post;
        } catch(DMLException e){
            system.assert(e.getMessage().contains('FeedItem delete unauthorized.'));
        }
    }
    
}