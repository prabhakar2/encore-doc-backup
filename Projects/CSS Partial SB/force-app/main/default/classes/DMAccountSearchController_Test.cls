@isTest(seeAllData=True)
private class DMAccountSearchController_Test {
    
    static testMethod void nullTestValidate(){
        
        DMAccountSearchController SearchValid = new DMAccountSearchController();
        SearchValid.SearchAcctNumber = '';
        SearchValid.ValidateAccNo();
        System.AssertEquals(SearchValid.SearchAcctNumber,'');
     
    } 
    static testMethod void invalidSearch() {
        DMAccountSearchController SearchValid = new DMAccountSearchController();
        SearchValid.SearchAcctNumber = 'abc';
        SearchValid.ValidateAccNo();
        System.AssertEquals(SearchValid.SearchAcctNumber,'abc');
    }
    
    static testMethod void validSearch() {
        DMAccountSearchController SearchValid = new DMAccountSearchController();
        SearchValid.SearchAcctNumber = '1234';
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new ExampleCalloutMock());
        SearchValid.ValidateAccNo();
        Test.stopTest();
        
    }
    
    
    //Mock Response for web service call upon success
    public class ExampleCalloutMock implements HttpCalloutMock{
        public HttpResponse respond(HTTPRequest req){
            HttpResponse res = new HttpResponse();
            res.setStatus('OK');
            res.setStatusCode(200);
            String jsonStr = '{ "status" : { "code" : 200, "message" : "Request Success ! Account search performed.", "uniqueErrorId" : "", "messageCode" : "" }, "custom" : { }, "data" : [ { "consumerId" : 304, "consumerAccountId" : "104", "consumerLegacyIdentifier" : "21649955", "prefix" : "", "suffix" : "", "city" : "WOODLAND", "postalCode" : "95776", "countryCode" : null, "county" : null, "chargeOffDate" : "2016-09-06", "chargeOffAmount" : null, "principalBalanceChargeOffAmount" : null, "sellerName" : null, "sellerId" : null, "lastPurchaseDate" : "2017-12-08", "portfolioNumber" : 14, "originalAccountNumber" : "2447962240240766", "originalLender" : "Credit One Bank, N.A.", "lastPurchaseAmount" : 29.53, "lastBalanceTransferAmount" : null, "lastCashAdvanceAmount" : null, "affinity" : null, "collectionBalance" : 0, "paidBalance" : 672, "lastSuccessfulPaymentAmount" : 37, "lastSuccessfulPaymentDate" : "2017-11-29", "accountOpenDate" : "2013-10-31", "lastBalanceTransferDate" : null, "lastCashAdvanceDate" : null, "paperType" : "CC01", "placementDate" : "2018-02-22", "sor" : "Apollo", "originalAccountTrimmed" : "2447962240240766", "consumerAccountAgencyId" : "1026423", "consumerAgencyId" : "1025343", "consumerFirstName" : "SALVADOR", "consumerMiddleName" : null, "consumerLastName" : "STANTON", "consumerLast4SSN" : "8677", "consumerSSN" : "106858677", "consumerAddress" : "3252 W 157TH ST, 1235 SDCA ST", "consumerState" : "CA", "purchasedFrom" : null, "oaldStatus" : null, "assignedFirm" : null, "dob" : "1950-04-10", "poolNumber" : "14", "productDescription" : "CC01", "productType" : null, "deliquencyDate" : "2015-11-05", "issuerLastPaymentDate" : "2017-11-29", "issuerLastPaymentAmount" : "37", "solDate" : null, "consumerAddress1" : "3252 W 157TH ST", "consumerAddress2" : "1235 SDCA ST", "consumerAddress3" : null, "workGroup" : "New Business", "currentBalance" : "219.820000", "originalBalance" : "0.000000", "accountTags" : [ { "tagName" : "SOLRECLC", "tagType" : "Internal Operations" }, { "tagName" : "CTR", "tagType" : "Useful Account Information" }, { "tagName" : "CHRGOFF", "tagType" : "Useful Account Information" }, { "tagName" : "ACHNOFND", "tagType" : "Payment Returned, reuse payment" }, { "tagName" : "ATTORNEY", "tagType" : "Special Handling Codes - Account Level " }, { "tagName" : "DSPWIP", "tagType" : "Account Compliance" } ], "consumerTags" : [ { "tagName" : "DCA_C", "tagType" : "Consumer with DCA" }, { "tagName" : "PHNENRCH", "tagType" : "Useful Consumer Information" } ], "relatedPerson" : [ { "relatedPersonId" : "43992", "relationshipType" : "DCA", "firstName" : "Freedom Debt Relief", "middleName" : "1025343", "lastName" : null, "addressLine1" : "PO BOX 2330", "addressLine2" : "", "addressLine3" : "", "city" : "PHOENIX", "state" : "AZ", "postalCode" : "850022330" } ], "coborrower" : [ { "consumerAgencyId" : "1025925", "firstName" : "GARRETT", "middleName" : null, "lastName" : "MACIAS", "ssn" : "109534574", "addressLine1" : "509 E 238TH PL", "addressLine2" : "APT 9", "addressLine3" : null, "city" : "CARSON", "state" : "CA", "postalCode" : "94509" }, { "consumerAgencyId" : "1028471", "firstName" : "JENETTE", "middleName" : null, "lastName" : "HILL", "ssn" : "176616356", "addressLine1" : "2835 VON BRAUN DR", "addressLine2" : "1235 SDCA ST", "addressLine3" : null, "city" : "WOODLAND", "state" : "CA", "postalCode" : "95776" } ] } ], "timestamp" : "2019-03-13T19:06:01.925Z" } ';
            res.setBody(jsonStr);
            return res;
        }
    }
    
    

}