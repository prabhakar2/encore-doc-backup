@isTest(SeeAllData=true)
public class ConsumerPrivacyEmailServiceHandler_test{
    public static testMethod void EmailAttachmentTester(){
        
        ConsumerPrivacyEmailServiceHandler objconfirm = new ConsumerPrivacyEmailServiceHandler();
        
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        objconfirm.handleInboundEmail(email, envelope);
        
        email.subject = 'Test Subject';
        email.htmlBody = responseGenrator();
        envelope.fromAddress = 'avaneesh.singh@mcmcg.com';
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        binaryAttachment.Filename = 'test.pdf';
        binaryAttachment.body = blob.valueOf('my attachment text');
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { binaryattachment };
        
        objconfirm.handleInboundEmail(email, envelope);
        
        Messaging.InboundEmailResult result = objconfirm.handleInboundEmail(email, envelope);
        System.assertEquals( result.success  ,true);
    }
    
    
    public static String responseGenrator(){
        String str ='Consumer\'s Relationship to Company : Consumer\nCompany Selection : Midland Credit Management\nFirst Name : Thag\nLast Name: xyz';
        str+='\nResponse Preferred: Email\nAlternative Name : Papu\nAddress : Delhi\nAddress Line 2 : Test here\nCity : Gurgaon\nState : Haryana';
        str+='\nZip Code : 20130\nLast 4 SSN : 3243\nPhone Number: 0987654325\nemail address : sfdc@gmail.com';
        str+='\nCertify Regulation Eligibility : True\nAuthorized Agent First Name : Prabhakar\nAuthorized Agent Last Name : Joshi\nAuthorized Agent Address : Noida';
        str+='\nAuthorized Agent Phone Number : 1234567890\nAuthorized Agent Email : avaneeshsfdc@gmail.com \nI want to exercise my right to know my personal information : true';
        str+='\nI want to know specific pieces of information collected : False\nI want to exercise my right to request deletion of my personal information : True';
        str+='\nI confirm that I want my  data to be deleted : False\nOn behalf of the consumer, I want to exercise the right to know personal information : True';
        str+='\nOn behalf of the consumer, I want to know specific pieces of information collected : False\nOn behalf of the consumer, I want to exercise the right to request deletion of personal information : True';
        str+='\nOn behalf of the consumer, I confirm that I want the consumer’s data to be deleted : False\n';
        str+='\nPlease confirm the information provided is correct : True';
      return str;
    }
    
    
}