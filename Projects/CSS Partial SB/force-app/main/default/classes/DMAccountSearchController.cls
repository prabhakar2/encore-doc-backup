public class DMAccountSearchController{

    Public String SearchAcctNumber     {get; set;}
    Public String Message              {get; set;}
    Pattern searchPattern = Pattern.compile('[0-9]*');
    
    public String OpportunityId    {get; set;}
    public String AccountId        {get; set;}
    public String AccountName      {get; set;} 
    
    Public List<String> AccountTagsList   = Label.DM_Account_Tags.split(';');
    Public List<String> ConsumerTagsList  = Label.DM_Consumer_Tags.split(';');
    Public List<String> DisputeTagsList   = Label.DM_Dispute_Tags.split(';');
    Public List<String> ComplaintTagsList = Label.DM_Complaint_Tags.split(';');
    Public List<String> DomesticTagsList = Label.DM_Domestic_Tags.split(';');
    
    public String successStr {get; set;}
    public Boolean isError   {get; set;}
   
    Public DMIntegrationSettings__c DMSettings          =  DMIntegrationSettings__c.getOrgDefaults();
    Public String HeaderAPIKey        = DMSettings.HeaderAPIKey__c;
    Public String HeaderAppId         = DMSettings.HeaderAppId__c;
    Public String HeaderCorrelationId = DMSettings.HeaderCorrelationId__c;
    Public String HeaderKeySecret     = DMSettings.HeaderKeySecret__c;
    Public String HeaderUserId        = DMSettings.HeaderUserId__c;
    
    Public String SearchDateTime {get; set;}
    
    public DMAccountSearchController(){
        ApexPages.getMessages().clear();
        SearchAcctNumber = '';
        successStr = '';
        isError = false;
    }
    
    public PageReference ValidateAccNo(){
        SearchDatetime = string.valueOf(system.now());
        Matcher searchMatcher = searchPattern.Matcher(SearchAcctNumber);
        if( SearchAcctNumber == ''){
            Message = Label.Custom_Search_Missing_Value;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Message));
        
        }else if(!searchMatcher.Matches() || SearchAcctNumber.startsWith('0')){ 
            Message = Label.Custom_Search_Invalid_Value;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Message));
        }else{
            
            //Search Account from DM Server
            getAccInfoFromDM();
            if(OpportunityId !=null && isError != true){
                successStr = 'Success';
                SearchAcctNumber = '';
            }else{
                successStr = 'error';
            }
        }
        return null;
    }// end of function...
    
    public void getAccInfoFromDM(){
        
        String DMAccountSearchURL =  DMSettings.DMAccountSearch__c;
         
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        request.setEndpoint(DMAccountSearchURL);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('mcm-appid', HeaderAppId);
        request.setHeader('correlation-id', HeaderCorrelationId);
        request.setHeader('apikey', HeaderAPIKey);
        request.setHeader('mcm-userid', HeaderUserId);
        request.setHeader('keySecret', HeaderKeySecret);
        
        // Set the body as a JSON object
        request.setBody('{ "accountNumber": "'+SearchAcctNumber+'" }');
        request.setTimeout(60000); //60 Seconds...
        
        HttpResponse response = http.send(request);
        
        system.debug('response>>>'+response);
        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {
            system.debug('ResponseBody>>>'+response.getBody());
            // Deserialize the JSON string into collections of primitive data types.
            Map<String, Object> body = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            
            if(body.containsKey('status') || body.containsKey('data')){
                Map<String, Object> status = new Map<String, Object>();
                status               = (Map<String, Object>)body.get('status');
                String StatusCode    = String.valueOf(status.get('code'));
                String message       = String.valueOf(status.get('message'));
                
                if(StatusCode == '200'){
                    if(body.containsKey('data')){
                        List<Object> data = (List<Object>)body.get('data');
                        JsonDataHandler(data);
                    }    
                }else{
                    isError = true;
                    Message = 'Response status exception.\n' + message + ' (' + StatusCode + ')';
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Message));
                }
            }
            
        }else{
            isError = true;
            Message = response.getStatus() + ' (' + response.getStatusCode() + ')';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Message));
        }
    }// end of function
    
    @TestVisible
    private void JsonDataHandler(List<Object> dataList){
        //system.debug('dataList>>>'+dataList);
        Opportunity Oppr = new Opportunity();
        Account Acc = new Account();
        
        if(dataList.size() > 0){
            for(Object obj :dataList){
                if(obj != null){
                    
                    Acc  = new Account();
                    Oppr = new Opportunity();
                    Map<String,Object> mapObj = (Map<string,Object>)obj;
                    
                    Oppr.OALD_Status__c  = String.ValueOf(mapObj.get('oaldStatus'));
                    Acc.DM_Consumer_External_ID__c = String.ValueOf(mapObj.get('consumerAgencyId'));
                    
                    Oppr.MSFNM__c      = String.ValueOf(mapObj.get('consumerFirstName'));
                    Acc.FirstName      = String.ValueOf(mapObj.get('consumerFirstName'));
                    
                    Oppr.MSMNM__c      = String.ValueOf(mapObj.get('consumerMiddleName'));
                    Acc.Consumer_Middle_Name__c = String.ValueOf(mapObj.get('consumerMiddleName'));
                    
                    Oppr.MSLNM__c      = String.ValueOf(mapObj.get('consumerLastName'));
                    Acc.LastName       = String.ValueOf(mapObj.get('consumerLastName'));
                    
                    Oppr.MSSSN__c      = String.ValueOf(mapObj.get('consumerSSN'));
                    Acc.MSSSN__c       = String.ValueOf(mapObj.get('consumerSSN'));
                    
                    String DOB  = String.ValueOf(mapObj.get('dob'));
                    if(DOB !=null){
                        Oppr.Consumer_Date_of_Birth__c  = Date.ValueOf(DOB);
                        Acc.PersonBirthdate   = Date.ValueOf(DOB);
                    }
                    
                    Oppr.DM_Account_External_ID__c = String.ValueOf(mapObj.get('consumerAccountAgencyId'));
                    Oppr.Name     = String.ValueOf(mapObj.get('consumerAccountAgencyId')); // this field is not writeable
                    
                    Oppr.Charge_Off_Amount__c =  double.ValueOf(mapObj.get('chargeOffAmount'));      
                    Oppr.MSAPUR__c  =  double.ValueOf(mapObj.get('originalBalance'));
                    
                    string chargeOffDate = string.ValueOf(mapObj.get('chargeOffDate'));
                    if(chargeOffDate !=null){Oppr.MSCHOF__c =  Date.valueOf(chargeOffDate);}
                    String placementDate = string.valueOf(mapObj.get('placementDate'));
                    if(placementDate != null){ Oppr.MSPDTE__c =  date.valueOf(placementDate);}
                    Oppr.MSPOOL__c =  string.ValueOf(mapObj.get('poolNumber'));
                    Oppr.msport__c =  string.ValueOf(mapObj.get('portfolioNumber'));
                    Oppr.ProductDesc__c =  string.ValueOf(mapObj.get('productDescription'));
                    Oppr.ASFIRM__c =  string.ValueOf(mapObj.get('assignedFirm'));
                    Oppr.MSOACT__c =  string.ValueOf(mapObj.get('originalAccountNumber'));
                    Oppr.Original_Lender__c =  string.ValueOf(mapObj.get('originalLender'));
                    Oppr.CXADDR1__c =  string.ValueOf(mapObj.get('consumerAddress1'));
                    Oppr.CXADDR2__c =  string.ValueOf(mapObj.get('consumerAddress2'));
                    Oppr.CXADDR3__c =  string.ValueOf(mapObj.get('consumerAddress3'));
                    Oppr.CXCITY__c  =  string.ValueOf(mapObj.get('city'));
                    Oppr.CXSTATE__c =  string.ValueOf(mapObj.get('consumerState'));
                    Oppr.CXZIP__c   =  string.ValueOf(mapObj.get('postalCode'));
                    Oppr.MSFIRM__c  =  string.ValueOf(mapObj.get('productType'));
                    Oppr.CSAffinity__c =  string.ValueOf(mapObj.get('affinity'));
                    String deliquencyDate = string.ValueOf(mapObj.get('deliquencyDate'));
                    if(deliquencyDate !=null){Oppr.MSDOC__c =  Date.ValueOf(deliquencyDate);}
                    Oppr.DM_Current_Balance__c = double.valueOf(mapObj.get('currentBalance'));
                    Oppr.MSISLPAM__c = double.valueOf(mapObj.get('issuerLastPaymentAmount'));
                    String IssLastPayDate = string.ValueOf(mapObj.get('issuerLastPaymentDate'));
                    if(IssLastPayDate !=null){Oppr.MSISLPDT__c = date.valueOf(IssLastPayDate);}
                    String solDate = string.valueOf(mapObj.get('solDate'));
                    if(solDate !=null){Oppr.MSLMTD__c = date.valueOf(solDate);}
                    Oppr.MSPCHF__c = string.valueOf(mapObj.get('purchasedFrom'));
                    String accountOpenDate = string.valueOf(mapObj.get('accountOpenDate'));
                    if(accountOpenDate !=null){Oppr.MSOPDT__c = Date.ValueOf(accountOpenDate);}
                    Oppr.Workgroup__c = string.valueOf(mapObj.get('workGroup'));  
                    
                    //Account Tags
                    Oppr.Account_Tags__c   = '';
                    Oppr.Dispute_Tags__c   = '';
                    Oppr.Complaint_Tags__c = '';
                    if(mapObj.containsKey('accountTags')){
                        List<Object> acctTagsList = (List<Object>)mapObj.get('accountTags');
                        String acctTags = '';
                        String dispTags = '';
                        String compTags = '';
                        if(acctTagsList != null){
                            for(Object accTag :acctTagsList){
                                if(accTag !=null){
                                    Map<String,Object> mapAccTag = (Map<string,Object>)accTag;
                                    String thisTag = string.valueOf(mapAccTag.get('tagName'));
                                    if(AccountTagsList.contains(thisTag)){
                                        acctTags += thisTag+'; ';    
                                    }else if(DisputeTagsList.contains(thisTag)){
                                        dispTags += thisTag+'; ';
                                    }else if(ComplaintTagsList.contains(thisTag)){
                                        compTags += thisTag+'; ';
                                    }
                                }
                            }// end of for loop
                            system.debug('acctTags>>>'+acctTags);
                            system.debug('dispTags>>>'+dispTags);
                            system.debug('compTags>>>'+compTags);
                            
                            Oppr.Account_Tags__c   = acctTags.removeEnd('; ');
                            Oppr.Dispute_Tags__c   = dispTags.removeEnd('; ');
                            Oppr.Complaint_Tags__c = compTags.removeEnd('; ');
                        }
                    }// end of accountTags
                    
                    //Consumer Tags
                    Acc.Consumer_Tags__c = '';
                    Acc.Domestic__c = false;
                    Oppr.IS_DOMESTIC__c = false;
                    if(mapObj.containsKey('consumerTags')){
                        List<Object> conxTagsList  = (List<Object>)mapObj.get('consumerTags');
                        String conxTags = '';
                        Boolean domTag = false;
                        if(conxTagsList !=null){               
                            for(Object conxTag :conxTagsList){
                                if(conxTag !=null){
                                    Map<String,Object> mapConxTag = (Map<string,Object>)conxTag;
                                    String thisTag = string.valueOf(mapConxTag.get('tagName'));
                                    if(ConsumerTagsList.contains(thisTag)){
                                        conxTags += thisTag+'; '; 
                                    }else if(DomesticTagsList.contains(thisTag)){
                                        domTag = TRUE;
                                    }   
                                }
                            }// end of for loop
                            
                            system.debug('conxTags>>>'+conxTags);
                            system.debug('domTag>>>>'+domTag);
                            
                            Acc.Consumer_Tags__c = conxTags.removeEnd('; ');
                            Acc.Domestic__c = domTag;
                            Oppr.IS_DOMESTIC__c = domTag;
                        }
                    }// end of consumerTags
                    
                    if(DMSettings.DMRecordTypeId__c !=null ){
                        Oppr.recordTypeId  = DMSettings.DMRecordTypeId__c; //DM Account
                    } 
                    Oppr.Probability   = 0;
                    Oppr.StageName     = 'Not Applicable';
                    Oppr.Type          = 'MCM owned';
                    Oppr.CloseDate     = Date.newInstance(2013,1,1); // This field is required
                    
                    //coborrower info
                    Oppr.Co_Borrower_External_ID__c  = '';
                        Oppr.Co_Borrower_First_Name__c   = '';
                        Oppr.Co_Borrower_Middle_Name__c  = '';
                        Oppr.Co_Borrower_Last_Name__c    = '';
                        Oppr.Co_Borrower_SSN__c          = '';
                        Oppr.Co_Borrower_Address_1__c    = '';
                        Oppr.Co_Borrower_Address_2__c    = '';
                        Oppr.Co_Borrower_Address_3__c    = '';
                        Oppr.Co_Borrower_City__c         = '';
                        Oppr.Co_Borrower_State__c        = '';
                        Oppr.Co_Borrower_Zip__c          = '';
                    if(mapObj.containsKey('coborrower')){
                        List<Object> CoBorrowerList  = (List<Object>)mapObj.get('coborrower');
                        
                        if(CoBorrowerList != null){
                            for(Object CoBorTag :CoBorrowerList){
                                if(CoBorTag !=null){
                                    Map<String,Object> CoBorrower = (Map<String,Object>)CoBorTag;
                                    if(CoBorrower !=null){
                                        Oppr.Co_Borrower_External_ID__c  = string.valueOf(CoBorrower.get('consumerAgencyId'));
                                        Oppr.Co_Borrower_First_Name__c   = string.valueOf(CoBorrower.get('firstName'));
                                        Oppr.Co_Borrower_Middle_Name__c  = string.valueOf(CoBorrower.get('middleName'));
                                        Oppr.Co_Borrower_Last_Name__c    = string.valueOf(CoBorrower.get('lastName'));
                                        Oppr.Co_Borrower_SSN__c          = string.valueOf(CoBorrower.get('ssn'));
                                        Oppr.Co_Borrower_Address_1__c    = string.valueOf(CoBorrower.get('addressLine1'));
                                        Oppr.Co_Borrower_Address_2__c    = string.valueOf(CoBorrower.get('addressLine2'));
                                        Oppr.Co_Borrower_Address_3__c    = string.valueOf(CoBorrower.get('addressLine3'));
                                        Oppr.Co_Borrower_City__c         = string.valueOf(CoBorrower.get('city'));
                                        Oppr.Co_Borrower_State__c        = string.valueOf(CoBorrower.get('state'));
                                        Oppr.Co_Borrower_Zip__c          = string.valueOf(CoBorrower.get('postalCode'));
                                        break; // only for first record  
                                    }
                                }
                            }// end of for loop...
                        }
                    }
                    
                    //related persons info
                    Oppr.Attorney_External_ID__c = '';
                    Oppr.Attorney_First_Name__c  = '';
                    Oppr.Attorney_Middle_Name__c = '';
                    Oppr.Attorney_Last_Name__c   = '';
                    Oppr.Attorney_Address_1__c   = '';
                    Oppr.Attorney_Address_2__c   = '';
                    Oppr.Attorney_Address_3__c   = '';
                    Oppr.Attorney_City__c        = '';
                    Oppr.Attorney_State__c       = '';
                    Oppr.Attorney_Zip__c         = '';
                    Oppr.Power_of_Attorney_External_ID__c = '';
                    Oppr.Power_of_Attorney_First_Name__c  = '';
                    Oppr.Power_of_Attorney_Middle_Name__c = '';
                    Oppr.Power_of_Attorney_Last_Name__c   = '';
                    Oppr.Power_of_Attorney_Address_1__c   = '';
                    Oppr.Power_of_Attorney_Address_2__c   = '';
                    Oppr.Power_of_Attorney_Address_3__c   = '';
                    Oppr.Power_of_Attorney_City__c        = '';
                    Oppr.Power_of_Attorney_State__c       = '';
                    Oppr.Power_of_Attorney_Zip__c         = '';
                    if(mapObj.containsKey('relatedPerson')){
                        List<Object> relatedPersonList  = (List<Object>)mapObj.get('relatedPerson');
                        system.debug('relatedPersonList>>>'+relatedPersonList);
                        if(relatedPersonList != null){
                        for(Object rpObj :relatedPersonList){
                            if(obj !=null){
                                Map<String,Object> reltdPerson = (Map<string,Object>)rpObj;
                                String relationshipType = string.valueOf(reltdPerson.get('relationshipType'));
                                
                                if(relationshipType =='Attorney'){
                                    Oppr.Attorney_External_ID__c = string.valueOf(reltdPerson.get('relatedPersonId'));
                                    Oppr.Attorney_First_Name__c  = string.valueOf(reltdPerson.get('firstName'));
                                    Oppr.Attorney_Middle_Name__c = string.valueOf(reltdPerson.get('middleName'));
                                    Oppr.Attorney_Last_Name__c   = string.valueOf(reltdPerson.get('lastName'));
                                    Oppr.Attorney_Address_1__c   = string.valueOf(reltdPerson.get('addressLine1'));
                                    Oppr.Attorney_Address_2__c   = string.valueOf(reltdPerson.get('addressLine2'));
                                    Oppr.Attorney_Address_3__c   = string.valueOf(reltdPerson.get('addressLine3'));
                                    Oppr.Attorney_City__c        = string.valueOf(reltdPerson.get('city'));
                                    Oppr.Attorney_State__c       = string.valueOf(reltdPerson.get('state'));
                                    Oppr.Attorney_Zip__c         = string.valueOf(reltdPerson.get('postalCode'));
                                }else if(relationshipType =='CSS_POA'){
                                    Oppr.Power_of_Attorney_External_ID__c = string.valueOf(reltdPerson.get('relatedPersonId'));
                                    Oppr.Power_of_Attorney_First_Name__c  = string.valueOf(reltdPerson.get('firstName'));
                                    Oppr.Power_of_Attorney_Middle_Name__c = string.valueOf(reltdPerson.get('middleName'));
                                    Oppr.Power_of_Attorney_Last_Name__c   = string.valueOf(reltdPerson.get('lastName'));
                                    Oppr.Power_of_Attorney_Address_1__c   = string.valueOf(reltdPerson.get('addressLine1'));
                                    Oppr.Power_of_Attorney_Address_2__c   = string.valueOf(reltdPerson.get('addressLine2'));
                                    Oppr.Power_of_Attorney_Address_3__c   = string.valueOf(reltdPerson.get('addressLine3'));
                                    Oppr.Power_of_Attorney_City__c        = string.valueOf(reltdPerson.get('city'));
                                    Oppr.Power_of_Attorney_State__c       = string.valueOf(reltdPerson.get('state'));
                                    Oppr.Power_of_Attorney_Zip__c         = string.valueOf(reltdPerson.get('postalCode'));
                                }
                                
                            }// end of null check..
                        }// end of for loop...
                      }  
                    }// end of related person if
                    
                }// end of check null obj...     
            }// end of for loop...
            
            try {
                
                upsert Acc DM_Consumer_External_ID__c;
                AccountId  = Acc.Id;
                AccountName = Acc.Name;
                Oppr.AccountId  = Acc.Id;
                
                // chatter post for consumer(Account)
                makeChatterPost('Consumer');
                
            } catch(DmlException e) {
                isError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An exception has occurred: ' + e.getMessage()));
            }                  
                
            try{    
                upsert Oppr DM_Account_External_ID__c;
                OpportunityId = Oppr.Id;
                // chatter post for account(Opportunity)
                makeChatterPost('Account');
            } catch(DmlException e) {
                isError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An exception has occurred: ' + e.getMessage()));
            }     
                
            //Send DM CSS Mapping callout
            DMAccountSearchController.makePostCallout(Oppr.DM_Account_External_ID__c,Acc.DM_Consumer_External_ID__c,AccountId,OpportunityId);
            
        }else{
            isError = true;
            Message = 'Response was empty, Please contact with system admin.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Message));
        }
    
    }// end of function

    // Here we will send back Ids for cross reference table DMCSSMapping.
    @future(callout = true)
    @TestVisible
    private static void makePostCallout(String AcctExternalId,String ConxExternalId, String AccId, String OpprId){
        system.debug('AccountId>>>'+AccId);
        system.debug('OpportunityId>>>'+OpprId);
        DMIntegrationSettings__c DMSettings =  DMIntegrationSettings__c.getOrgDefaults();
        String HeaderAPIKey = DMSettings.HeaderAPIKey__c;
        String HeaderAppId  = DMSettings.HeaderAppId__c;
        String HeaderCorrelationId = DMSettings.HeaderCorrelationId__c;
        String HeaderKeySecret     = DMSettings.HeaderKeySecret__c;
        String HeaderUserId        = DMSettings.HeaderUserId__c;
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String DMCSSMapping =  DMSettings.DMCSSMapping__c;
    
        request.setEndpoint(DMCSSMapping);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('mcm-appid', HeaderAppId);
        request.setHeader('correlation-id', HeaderCorrelationId);
        request.setHeader('apikey', HeaderAPIKey);
        request.setHeader('mcm-userid', HeaderUserId);
        request.setHeader('keySecret', HeaderKeySecret);
        
        // Set the body as a JSON object
        string requestBody = '{ "consumerAccountAgencyId" : "'+AcctExternalId+'","consumerAgencyId" : "'+ConxExternalId+'", "salesForceAccountId": "'+OpprId+'", "salesForceConsumerId" : "'+AccId+'" }';
        request.setBody(requestBody);
        request.setTimeout(90000);
        system.debug('requestCSSMap>>>'+requestBody);
        
        HttpResponse response = http.send(request);
        system.debug('responseCSSMap>>>'+response);
        readresponse(response);  
    }
     
    @TestVisible
    private void makeChatterPost(String strType){
        string body = '';
        body += 'DM Account Search performed for '+SearchAcctNumber+' By: '+userInfo.getFirstName()+' '+userInfo.getLastName()+ ', Date: '+searchDateTime;
        FeedItem post = new FeedItem();
        if(strType == 'Account'){
            post.ParentId = OpportunityId;
        }else{
            post.ParentId = AccountId;
        }
        post.Body = body;
        
        try{
            insert post;
        } catch(DmlException e) {
            System.debug('An exception has occurred: ' + e.getMessage());
        }    
    }
    
    public static void readresponse(HttpResponse response)
    {
        // Parse the JSON response
        if (response.getStatusCode() > 201 ) {
           string Emailbody = 'Code: '+response.getStatusCode() + ' Response Body: <br/> '+response.getBody();
            List<string> toaddress = Label.CSSMap_Response_Receiver.split(';'); 
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.setSubject('DM CSS Mapping Response');
            message.setPlainTextBody(Emailbody);
            message.setToAddresses(toaddress);
            Messaging.sendEmail( new Messaging.SingleEmailMessage[] { message } );
        }
    }

}