/*

    Description : When an inquiry would be come from email this email service handler would create 
                  a record and this inquiry a dummy individual(standard salesforce object) record would be 
                  created or available named as can not find individual and this service will create a new record    
                  in other hand a trigger is available in inquiry object which will manage the record in salesforce
                  

    @uthor      : Avaneesh Singh, Developer, India 
    Created Date: 3/12/2019
    
*/

global class ConsumerPrivacyEmailServiceHandlerHelper{
    global static ConsumerPrivacyEmailServiceHandlerHelper instance = null;
    private ConsumerPrivacyEmailServiceHandlerHelper(){}
    
      global map<String,Schema.DisplayType> getFieldWithReference(String objectName){
         Map<String,Schema.DisplayType> fieldWithReferenceMap = new Map<String,Schema.DisplayType>();
         
          if(objectName != null && objectName != ''){
             Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectName.trim()).getDescribe().fields.getMap();
             for (String fieldName: fieldMap.keySet()) {
              Schema.DisplayType fielddataType = fieldMap.get(fieldName).getDescribe().getType();
              fieldWithReferenceMap.put(fieldName, fieldDataType);
            }
          }
          return fieldWithReferenceMap ;
      }
      
      global individual getDummyIndividual(){
      
              List<individual> tempIndividualList = [select id , lastName from individual where lastName = 'Can Not Find Individual'];
              
              if(tempIndividualList != null && tempIndividualList.size() > 0){
                 return tempIndividualList[0];
              }else{
                // if record not found here create record
                 individual ind = new individual ();
                 ind.lastName = 'Can Not Find Individual';
                 insert ind;
                return ind;
              }
      }
     //need to make the field name key as lower char 
     global Map<String,String> getMapField(){
        Map<String,String> mapFiledMapping =  new Map<String,String>();
        mapFiledMapping.put('relationship to company','Relationship_to_Company__c');
        mapFiledMapping.put('company selection','Company_Selection__c');
        mapFiledMapping.put('first name','First_Name__c');
        mapFiledMapping.put('last name','Last_Name__c');
        mapFiledMapping.put('alternative name','Alternative_Name__c');
        mapFiledMapping.put('address','Street_Address__c');
        mapFiledMapping.put('address line 2','Address_Line_2__c');
        mapFiledMapping.put('city','City__c');
        mapFiledMapping.put('state','State__c');
        mapFiledMapping.put('zip code','Zip_Code__c');
        mapFiledMapping.put('last 4 ssn','Last_4_SSN__c');
        mapFiledMapping.put('phone number','Phone_Number__c');
        mapFiledMapping.put('email address','Email__c');
        mapFiledMapping.put('certify regulation eligibility','Certify_Regulation_Eligibility__c');
        mapFiledMapping.put('authorized agent first name','Authorized_Agent_First_Name__c');
        mapFiledMapping.put('authorized agent last name','Authorized_Agent_Last_Name__c');
        mapFiledMapping.put('authorized agent street address','Authorized_Agent_Street_Address__c');
        mapFiledMapping.put('authorized agent address line 2','Authorized_Agent_Address_2__c');
        mapFiledMapping.put('authorized agent city','Authorized_Agent_City__c');
        mapFiledMapping.put('authorized agent state','Authorized_Agent_State__c');
        mapFiledMapping.put('authorized agent zip code','Authorized_Agent_Zip__c');
        mapFiledMapping.put('authorized agent phone number','Authorized_Agent_Phone__c');
        mapFiledMapping.put('authorized agent email','Authorized_Agent_Email__c');
        mapFiledMapping.put('i want to exercise my right to know my personal information','Request_Personal_Information__c');
        mapFiledMapping.put('i want to know specific pieces of information collected','Request_Specific_Pieces_of_Information__c');
        mapFiledMapping.put('i want to exercise my right to request deletion of my personal information','Request_to_Delete_Data__c');
        mapFiledMapping.put('i confirm that I want my  data to be deleted','Confirmation_to_Delete_Data__c');
        mapFiledMapping.put('on behalf of the consumer, i want to exercise the right to know personal information','Request_Personal_Information__c');
        mapFiledMapping.put('on behalf of the consumer, i want to know specific pieces of information collected','Request_Specific_Pieces_of_Information__c');
        mapFiledMapping.put('on behalf of the consumer, i want to exercise the right to request deletion of personal information','Request_to_Delete_Data__c');
        mapFiledMapping.put('on behalf of the consumer, i confirm that i want the consumer’s data to be deleted','Confirmation_to_Delete_Data__c');
        mapFiledMapping.put('response preferred','Response_Preference__c');
        mapFiledMapping.put('please confirm the information provided is correct','Confirm_Information_is_Correct__c');
        
        return mapFiledMapping ;
      }
      
     global static ConsumerPrivacyEmailServiceHandlerHelper createinstance(){
        if(instance == null) instance = new ConsumerPrivacyEmailServiceHandlerHelper();
        return instance;
     }
}