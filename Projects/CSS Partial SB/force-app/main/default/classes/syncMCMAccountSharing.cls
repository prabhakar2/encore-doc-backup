/*
##################################################################################################################################
# Project Name..........: CSS McmAccounts Visibility Issue 
# File..................: Class : syncMCMAccountSharing
# Version...............: 1.0
# Created by............: Sunny Kumar
# Created Date..........: 26-Dec-2013
# Last Modified by......: Sunny Kumar
# Last Modified Date....: 01-Jan-2014
# Description...........: It will call the batch class to sync MCM Accounts based on msport number. 
#####################################################################################################################################
*/

public class syncMCMAccountSharing {

    public String strScheduleName{get;set;}
    public Boolean isAlredyProcessed{get;set;}
    public Boolean isCompletedToday{get;set;}
    public Boolean isRequestSubmit{get;set;}
    Public String strProcessingText{get;set;}
    public Boolean isCompleted{get;set;}
    public String strMessage{get;set;}
    public Integer count{get;set;}
    
    public syncMCMAccountSharing() {
             strScheduleName='';
             isAlredyProcessed=false;
             isRequestSubmit=false;
             isCompleted=false;
           strMessage='Please Wait...';   
           count=0; 
    }
    public syncMCMAccountSharing(ApexPages.StandardSetController controller) {
         strScheduleName='';
         isAlredyProcessed=false;
         isRequestSubmit=false;
         isCompleted=false;
        strMessage='Please Wait...';   
     }
   
   public pagereference startMCMAccountSharing(){
       scheduleMCMAccount();
       
       return null;
   }

      public void scheduleMCMAccount(){
         ApexClass objApexClass=new ApexClass();
       
          try{
               objApexClass =[Select a.Id From ApexClass a where name='MCMAccountSharingBatch' limit 1];
             }catch(Exception e){
            }
        
          for(AsyncApexJob objAsyncApexJob :[SELECT Status, ApexClassId,CompletedDate,CreatedDate,CreatedBy.Name,CreatedBy.Email,JobItemsProcessed,TotalJobItems,NumberOfErrors  FROM AsyncApexJob where ApexClassId=:objApexClass.id and
                                              CreatedDate=TODAY and( Status='Queued' OR Status='Processing' OR Status='Preparing') limit 1]){
                  User objUser2=[Select Name from User where Id=:objAsyncApexJob.CreatedById ];
                  isAlredyProcessed=false;
                  if('Queued,Processing,Preparing'.contains(objAsyncApexJob.Status)){
                      // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,
                      strMessage='<br><br>'+
                                  ' This is'+(isRequestSubmit?' ':' already')+ ' scheduled by <b>'+objAsyncApexJob.CreatedBy.Name+
                                  ' </b> in <b>Apex Batch</b> for update on <b>'+objAsyncApexJob.CreatedDate+'</b>. It may take longer depending on '+
                                  'the records and workflows.'+
                                  ' <br><br>You can monitor the status in <b>Apex Jobs</b> under<b>'+
                                  ' <a href=https://'+ApexPages.currentPage().getHeaders().get('Host')+'/apexpages/setup/listAsyncApexJobs.apexp target=_blank>Monitor</a></b>'+
                                  ' <br/><br/><b>Current status of job is '+objAsyncApexJob.Status+'</b><b>'+(objAsyncApexJob.Status=='Processing'?('('+ objAsyncApexJob.JobItemsProcessed+' out of '+objAsyncApexJob.TotalJobItems)+' .Failure '+objAsyncApexJob.NumberOfErrors+')':'')+'</b>'+
                                  ' <br/><br/><b>When completed an email message will be sent to your email id('+objAsyncApexJob.CreatedBy.Email+') automatically.</b>';
                      if(isRequestSubmit){
                          strProcessingText='Please Wait Your Request is Processing...<br><br>';
                       }else{
                          strProcessingText='Please Wait Previous Request is Processing...<br><br>';
                        }  
                        strMessage=strProcessingText+strMessage;
                      return ;    
                  }
                  }
                    if(isRequestSubmit){
                      isCompleted=true;
                      String strPageMessage='';
                      for(AsyncApexJob objAsyncApexJob :[Select Status, ApexClassId,CompletedDate,CreatedDate,CreatedBy.Name,CreatedBy.Email,JobItemsProcessed,TotalJobItems,
                                                         NumberOfErrors,ExtendedStatus FROM AsyncApexJob where ApexClassId=:objApexClass.id and CreatedDate=TODAY and(Status='Completed' OR Status='Aborted') order by CreatedDate desc limit 1 ]){
                       if(objAsyncApexJob.Status=='Completed' || objAsyncApexJob.Status=='Aborted'){
                            strPageMessage='Your request is successfully processed'+
                                  ' <br><br>You can monitor completion process in <b>Apex Jobs</b> under<b>'+
                                  '<a href=https://'+ApexPages.currentPage().getHeaders().get('Host')+'/apexpages/setup/listAsyncApexJobs.apexp target=_blank>Monitor</a></b>'+
                                  ' <br/><br/><b>Current status of job is '+objAsyncApexJob.Status+'</b><b>'+(('('+ objAsyncApexJob.JobItemsProcessed+' out of '+objAsyncApexJob.TotalJobItems)+' .Failure '+objAsyncApexJob.NumberOfErrors+')')+'</b>';
                                  
                                   if(objAsyncApexJob.NumberOfErrors>0){
                                    strPageMessage=strPageMessage+' <br/> <br/> Please find the error detail in below: <br/>';
                                    strPageMessage=strPageMessage+' <b> <font color=red >'+objAsyncApexJob.ExtendedStatus+'</font></b><br/>';
                                 }
                                  
                                 strPageMessage=strPageMessage+' <br/><br/><b>Message will send to your email id('+objAsyncApexJob.CreatedBy.Email+') automatically.</b>';
                         }
                      }
                      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,strPageMessage));
                  }
                               
                if(!isRequestSubmit){
                   isRequestSubmit=true;
                   synOpportunitySharing();
                }
           return;
      
      }
    
      public void synOpportunitySharing(){
          String strEmailSubject='Manual job ';
          MCMAccountSharingBatch obj=new MCMAccountSharingBatch(strEmailSubject);
          Database.executeBatch(obj,getBatchSize());
      }
      
      public Static Integer getBatchSize(){
        List<MCM_Account_Batch_Size__c>lstBatch= MCM_Account_Batch_Size__c.getAll().values();
        Integer iBatchSize=200;
        if(lstBatch.size()>0){
             iBatchSize=Integer.valueOf(lstBatch.get(0).Batch_Size__c);
        }
        return iBatchSize;
      }
      
      
      
      
     }