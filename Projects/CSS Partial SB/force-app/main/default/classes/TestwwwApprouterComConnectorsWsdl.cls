/**
AUTHOR:    LENNARD SANTOS
COMPANY:    CLOUDSHERPAS
DATE CREATED:  FEB 19 2013
DESCRIPTION: Test coverage for the Web Service WSDL 
HISTORY: 2-19-2013  Created.
**/
@isTest
private class TestwwwApprouterComConnectorsWsdl {
    static testMethod void TestWebService(){
        
        Opportunity opp = new Opportunity(
            
            Name = 'TestAccountName',
            MSACT__c = '12345',
            StageName = 'Not Applicable',
            CloseDate = Date.Today()
            
        );
        insert opp;
        test.StartTest();
        //Call Mock Response
        Test.setMock(WebServiceMock.class, new TestCallWebSvc());
        wwwApprouterComConnectorsWsdl WebServ = new wwwApprouterComConnectorsWsdl();
        wwwApprouterComConnectorsWsdl.TestUpdSFCDPort Port = new wwwApprouterComConnectorsWsdl.TestUpdSFCDPort();
        wwwApprouterComConnectorsRequest579.Input_element inputElem = new wwwApprouterComConnectorsRequest579.Input_element();
        wwwApprouterComConnectorsRequest579 req = new wwwApprouterComConnectorsRequest579();
        wwwApprouterComConnectorsRequest579.InputParams inputParam = new wwwApprouterComConnectorsRequest579.InputParams();
        inputParam.Input = inputElem;
        inputElem.MCMAccount = Integer.valueOf(Long.ValueOf(opp.MSACT__c));
        wwwApprouterComConnectorsResponse57.Output_element outp = Port.TestUpdSFCD(inputElem);
        wwwApprouterComConnectorsResponse57 response = new wwwApprouterComConnectorsResponse57(); 
        System.debug('@@ output' + outp);
        System.debug('@@ input' + inputElem);
        //Assert webservice response value
        System.AssertEquals(inputElem.MCMAccount,12345); 
        System.AssertEquals(outp.Status,True);
        System.AssertEquals(outp.SFDCMCMAccountId,'test98');
        System.AssertEquals(outp.ErrorMsg,'');
        test.StopTest();
}



//Web service interface for test class. Used as Mock Response of the Web Service.
public class TestCallWebSvc implements WebServiceMock{
    public void doInvoke(
        Object stub,
        Object request,
        Map<String,Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType
    ){
          wwwApprouterComConnectorsResponse57.Output_element outp = new wwwApprouterComConnectorsResponse57.Output_element();
          outp.SFDCMCMAccountId = 'test98';
          outp.Status = true;
          outp.ErrorMsg = '';
          wwwApprouterComConnectorsResponse57.OutputParams o = new wwwApprouterComConnectorsResponse57.OutputParams();
          o.Output = outp;
          response.put('response_x', o );
     }

  }

}