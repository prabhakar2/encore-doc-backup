/**
 * This class contains unit tests for validating the behavior of the ComplaintResponseHandler.
 * Positive Test - cases should be included in rollup field on complaint response, value should be > 0
 * Negative Test - NO cases should be included in rollup field on complaint response, value should be = 0
 * @Created 2019-02-18
 */
@isTest
private class ComplaintResponseHandler_Test {

    @TestSetup
    private static void testSetup(){
        
        //create parent account
        Account a = new Account(
            FirstName='Test',
            LastName='Consumer'
        );
        insert a;
               
    }
    
    @isTest
    static void testRollUp() {
        
        Account a = [SELECT Id, Name, FirstName FROM Account WHERE FirstName='Test' LIMIT 1];
        
        //create parent opportunities
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity oppSynchrony = new Opportunity(
            AccountId = a.id,
            CloseDate = date.Today(),
            Original_Lender__c = 'Synchrony Bank',
            Name = 'Synchrony Opp',
            StageName = 'Not Applicable'
        );
        Opportunity oppNotSynchrony = new Opportunity(
            AccountId = a.id,
            CloseDate = date.Today(),
            Original_Lender__c = 'Encore Capital',
            Name = 'Not Synchrony Opp',
            StageName = 'Not Applicable'
        );
        oppList.add(oppSynchrony);
        oppList.add(oppNotSynchrony);
        insert oppList;
        
        //create parent complaint responses
        List<Complaint_Response__c> crList = new List<Complaint_Response__c>();
        Complaint_Response__c cr1 = new Complaint_Response__c(
            Regulator_Complaint__c = '190218-000001',
            Regulator_State__c = 'CA - California',
            Regulator__c = 'CFPB',
            Complainant_Name__c = 'Complainant 1',
            Assigned_to_Attorney__c = 'Team Review',
            Delivery_Type__c = 'Internal',
            Date_of_Complaint__c = date.today(),
            Date_Complaint_Received__c = date.today(),
            Approval_Date__c = date.today(),
            Actual_Due_date__c = date.today(),
            Submission_Classification__c = 'Complaint',
            Regulator_SLA__c = 1,
            Complaint_Status__c = 'Open'
        );
        Complaint_Response__c cr2 = new Complaint_Response__c(
            Regulator_Complaint__c = '190218-000001',
            Regulator_State__c = 'CA - California',
            Regulator__c = 'CFPB',
            Complainant_Name__c = 'Complainant 2',
            Assigned_to_Attorney__c = 'Team Review',
            Delivery_Type__c = 'Internal',
            Date_of_Complaint__c = date.today(),
            Date_Complaint_Received__c = date.today(),
            Approval_Date__c = date.today(),
            Actual_Due_date__c = date.today(),
            Submission_Classification__c = 'Complaint',
            Regulator_SLA__c = 1,
            Complaint_Status__c = 'Open'
        );
        crList.add(cr1);
        crList.add(cr2);
        insert crList;
        
        //create cases
        List<Case> c = new List<Case>();
        Case caseSynchrony = new Case(
            Opportunity__c = oppSynchrony.Id,
            Internal_Complaint_Response__c = cr1.Id,
            Case_Type__c = 'Complaint',
            GE_ID__c = ''
        );
        Case caseNotSynchrony = new Case(
        	Opportunity__c = oppNotSynchrony.Id,
            Internal_Complaint_Response__c = cr2.Id,
            Case_Type__c = 'Complaint',
            GE_ID__c = ''
        );
        c.add(caseSynchrony);
        c.add(caseNotSynchrony);
        insert c;
        
        Test.StartTest();
        //POSITIVE TEST #1 -- synchrony account without synchrony id
        cr1.Complainant_Name__c = 'Complainant 1 - Positive Test';
        update cr1;
        Complaint_Response__c newCR1 = [SELECT Cases_Missing_Synchrony_Id__c FROM Complaint_Response__c WHERE Complainant_Name__c = 'Complainant 1 - Positive Test'];
        system.assert(newCR1.Cases_Missing_Synchrony_Id__c>0);
        
        //NEGATIVE TEST #1 -- synchrony account with synchrony id
        caseSynchrony.GE_ID__c = 'ABCD-123';
        update caseSynchrony;
        cr1.Complainant_Name__c = 'Complainant 1 - Negative Test';
        update cr1;
        Complaint_Response__c newCR2 = [SELECT Cases_Missing_Synchrony_Id__c FROM Complaint_Response__c WHERE Complainant_Name__c = 'Complainant 1 - Negative Test'];
        system.assert(newCR2.Cases_Missing_Synchrony_Id__c==0);
        
    	//NEGATIVE TEST #2 -- not a synchrony account without synchrony id
    	cr2.Complainant_Name__c = 'Complainant 2 - Negative Test';
        update cr2;
        Complaint_Response__c newCR3 = [SELECT Cases_Missing_Synchrony_Id__c FROM Complaint_Response__c WHERE Complainant_Name__c = 'Complainant 2 - Negative Test'];
    	system.assert(newCR3.Cases_Missing_Synchrony_Id__c==0);
        Test.stopTest();

    }
}