/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_scheduledMCMAccount {

    static testMethod void myUnitTest() {
        
       Test.startTest();
         
        /*  scheduledMCMAccount scheduler = new scheduledMCMAccount();
          String sch = '0 0 0 * * ?';
          String strJobId1=system.schedule('dailyScheduledMCMAccountActivity_'+System.Now(), sch, scheduler);
          //system.abortJob(strJobId1);
          CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered,NextFireTime  FROM CronTrigger WHERE id = :strJobId1];
          System.assertEquals(sch,ct.CronExpression);
          
           system.abortJob(strJobId1);
           scheduledMCMAccount scheduler12 = new scheduledMCMAccount();
            strJobId1=System.schedule('nextScheduledMCMAccountHourlyActivity_'+System.Now(), '0 '   + String.valueOf(0) + ' * * * ?', scheduler12);
           
           system.abortJob(strJobId1);*/
         
         try{
                scheduledMCMAccount objScheduler = new scheduledMCMAccount();
                SchedulableContext sc=null;
               objScheduler.execute(sc);
               
               objScheduler.isTestCall=true; 
               objScheduler.execute(sc);
            }catch(System.FinalException ex){}
         
         Test.stopTest(); 
          
         

    }
}