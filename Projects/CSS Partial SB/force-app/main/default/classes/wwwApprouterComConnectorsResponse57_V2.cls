//Generated by wsdl2apex

public class wwwApprouterComConnectorsResponse57_V2 {
    public class Output_element {
        public String SFDCMCMAccountId;
        public Boolean Status;
        public String ErrorMsg;
        private String[] SFDCMCMAccountId_type_info = new String[]{'SFDCMCMAccountId','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] Status_type_info = new String[]{'Status','http://www.w3.org/2001/XMLSchema','boolean','1','1','true'};
        private String[] ErrorMsg_type_info = new String[]{'ErrorMsg','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.approuter.com/connectors/response/579/','true','true'};
        private String[] field_order_type_info = new String[]{'SFDCMCMAccountId','Status','ErrorMsg'};
    }
    public class OutputParams {
        public wwwApprouterComConnectorsResponse57_V2.Output_element Output;
        private String[] Output_type_info = new String[]{'Output','http://www.approuter.com/connectors/response/579/','Output_element','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.approuter.com/connectors/response/579/','true','true'};
        private String[] field_order_type_info = new String[]{'Output'};
    }
}