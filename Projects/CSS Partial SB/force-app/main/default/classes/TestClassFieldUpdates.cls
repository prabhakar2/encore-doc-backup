@isTest
public class TestClassFieldUpdates{
  static testMethod void TestUpdates() {

// Create Parent Object   
Account a = new Account(
FirstName = 'Test',
LastName = 'Smith'
);
insert a;

Opportunity o = new Opportunity(
AccountId = a.Id,
Name = '9999999999',
CloseDate = date.newinstance(2028, 1, 1),
StageName = 'Not Applicable'
);
insert o;

// Tell SF starting test
test.startTest();
  
  
// Set up Case for testing
Case c = new Case(
AccountId = a.Id,
Opportunity__c = o.Id,
Description = 'some long description containing CSS, CA, Stephen Price, LOL, Not following any other applicable MCM policies',
Quality_Assurance_Violation_1__c = 'Threatened violence on a phone call',
Compliance_Violation_Code_1__c = 'FDCPA3',
Quality_Assurance_Violation_2__c = 'Used profane and/or abusive language on a call',
Compliance_Violation_Code_2__c = 'FDCPA1',
Quality_Assurance_Violation_3__c = 'Called a phone number outside of the designated time zone without authorization to do so',
Compliance_Violation_Code_3__c = 'FDCPA2',
Quality_Assurance_Violation_4__c = 'Contacted a consumer after a Cease And Desist had been documented on the account',
Compliance_Violation_Code_4__c = 'FDCPA7',
Quality_Assurance_Violation_5__c = 'Did not service the account in accordance with the Consumer Bill of Rights',
Compliance_Violation_Code_5__c = 'FAIL29'
);
insert c;

update c;

// Stop test
test.stopTest();

Case uc = [SELECT Id, Description, Quality_Assurance_Violation_1__c, Compliance_Violation_Code_1__c, Quality_Assurance_Violation_2__c,
Compliance_Violation_Code_2__c, Quality_Assurance_Violation_3__c, Compliance_Violation_Code_3__c, Quality_Assurance_Violation_4__c, 
Compliance_Violation_Code_4__c, Quality_Assurance_Violation_5__c, Compliance_Violation_Code_5__c, Tier_Violation_1__c,
Tier_Violation_2__c, Tier_Violation_3__c, Tier_Violation_4__c, Tier_Violation_5__c FROM Case WHERE Id = :c.Id];
System.assertEquals('some long description containing CSS, CA, Stephen Price, LOL, Not following any other applicable MCM policies', uc.Description);
System.assertEquals('Threatened violence on a phone call', uc.Quality_Assurance_Violation_1__c);
System.assertEquals('FDCPA3', uc.Compliance_Violation_Code_1__c);
System.assertEquals('Used profane and/or abusive language on a call', uc.Quality_Assurance_Violation_2__c);
System.assertEquals('FDCPA1', uc.Compliance_Violation_Code_2__c);
System.assertEquals('Called a phone number outside of the designated time zone without authorization to do so', uc.Quality_Assurance_Violation_3__c);
System.assertEquals('FDCPA2', uc.Compliance_Violation_Code_3__c);
System.assertEquals('Contacted a consumer after a Cease And Desist had been documented on the account', uc.Quality_Assurance_Violation_4__c);
System.assertEquals('FDCPA7', uc.Compliance_Violation_Code_4__c);
System.assertEquals('Did not service the account in accordance with the Consumer Bill of Rights', uc.Quality_Assurance_Violation_5__c);
System.assertEquals('FAIL29', uc.Compliance_Violation_Code_5__c);


}
}