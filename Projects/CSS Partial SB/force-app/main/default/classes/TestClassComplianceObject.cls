@isTest
public class TestClassComplianceObject{
 static testMethod void TestUpdates(){

// Create Parent Object

Account a = new Account();
a.FirstName = 'John';
a.LastName = 'Smith';
insert a;

// Create Opportunity

Opportunity o = new Opportunity(
AccountId = a.Id,
Name = '1234567890',
StageName = 'Prospecting',
CloseDate = date.today()
);
insert o;


// Start Test
test.startTest();

// Create Compliance Object

Compliance__c com = new Compliance__c(
Name = 'Complaint Number',
MCM_Account__c = o.Id,
Account__c = a.Id,
Complainant_Name__c = 'John Addams',
Complainant_State__c = 'CA',
Regulator_State__c = 'CA',
Regulator__c = 'BBB',
Regulator_City_County__c = 'San Diego'
);
insert com;

update com;


// Create Compliance Object

Marketing__c mark = new Marketing__c(
MCM_Account__c = o.Id,
Account_Name__c = a.id
);
insert mark;

update mark;


// Stop test
test.stopTest();

Compliance__c ucom = [SELECT Id, Name, MCM_Account__c, Account__c, Complainant_Name__c, Complainant_State__c, Regulator__c, Regulator_State__c, Regulator_City_County__c FROM Compliance__c WHERE Id = :com.Id];
System.assertEquals('Complaint Number', ucom.Name);
System.assertEquals(o.Id, ucom.MCM_Account__c);
System.assertEquals(a.Id, ucom.Account__c);
System.assertEquals('John Addams', ucom.Complainant_Name__c);
System.assertEquals('CA', ucom.Complainant_State__c);
System.assertEquals('CA', ucom.Regulator_State__c);
System.assertEquals('BBB', ucom.Regulator__c);
System.assertEquals('San Diego', ucom.Regulator_City_County__c);

Marketing__c umark = [SELECT Id, Name, MCM_Account__c, Account_Name__c FROM Marketing__c WHERE Id = :mark.Id];
System.assertEquals(o.id, umark.MCM_Account__c);
System.assertEquals(a.id, umark.Account_Name__c);



}
}