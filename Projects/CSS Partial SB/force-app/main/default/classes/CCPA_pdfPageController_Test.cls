@isTest
public class CCPA_pdfPageController_Test {
    
    @testSetup
    static void setUp(){
        Id ccpaRecType = Schema.SObjectType.Privacy_Inquiry__c.getRecordTypeInfosByName().get('CCPA').getRecordTypeId();
        Individual Individual_1 = new Individual(FirstName = 'test', LastName = 'Data');
        insert Individual_1;
        
        List<Privacy_Inquiry__c> testPrivacy = new List<Privacy_Inquiry__c>();
        
        for(Integer i = 0; i<10; i++){
            testPrivacy.add(new Privacy_Inquiry__c(RecordTypeId = ccpaRecType,
                                                   Individual__c = Individual_1.Id,
                                                   First_Name__c = 'Dev'+i,
                                                   Last_Name__c = 'Dev_Unit',
                                                   Last_4_SSN__c = '1234',
                                                   Phone_Number__c = '1234567890',
                                                   Street_Address__c = '1234 Address Lane',
                                                   City__c = 'City1',
                                                   State__c = 'state1',
                                                   Zip_Code__c = '112211',
                                                   Company_Selection__c = 'Asset Acceptance',
                                                   Response_Preference__c = 'Mail',
                                                   Relationship_to_Company__c = 'Consumer',
                                                   Source__c = 'Web',
                                                   Letter__c = '10 Day Acknowledgement',
                                                   Template_Language__c = 'Spanish',
                                                   Confirm_Information_is_Correct__c = true,
                                                   Date_10_Day_Acknowledgement_Letter_Sent__c = System.today(),
                                                   Request_Personal_Information__c = true,
                                                   Request_to_Delete_Data__c = true ));
        }
        insert testPrivacy;        
    }
    
    @isTest 
    static void unitTest(){
        Privacy_Inquiry__c pi = [SELECT Id FROM Privacy_Inquiry__c WHERE First_Name__c='Dev0' LIMIT 1];
        pi.Template_Language__c = 'English';
        pi.Date_10_Day_Acknowledgement_Letter_Sent__c = System.today();
        update pi;
        
        List<CCPA_Language__mdt> langManager = [SELECT Id, language__c, Label, DeveloperName, MasterLabel, QualifiedApiName, 
                                                NamespacePrefix 
                                                FROM CCPA_Language__mdt];
        
        Privacy_Letter_Code_Manager__c setting = new Privacy_Letter_Code_Manager__c();
        setting.Name = '10 Day Acknowledgement@@English';
        setting.Letter_Code__c = 'PR10D';
        insert setting; 
        
        Test.StartTest(); 
        PageReference pageRef = Page.CCPA_pdfPage;
        pageRef.getparameters().put('Id', pi.id);  
        Test.setCurrentPage(pageRef);
        
        Apexpages.StandardController sc = new Apexpages.StandardController(pi);
        CCPA_pdfPageController ext = new CCPA_pdfPageController(sc);
       
        
        CCPA_EmailDocumentController obj = new CCPA_EmailDocumentController(sc);
        obj.SaveAndEmailPDF();
        obj.SavePDF();
        Test.stopTest();        
    }
    
}