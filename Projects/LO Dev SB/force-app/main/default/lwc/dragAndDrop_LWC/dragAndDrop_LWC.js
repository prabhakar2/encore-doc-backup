import { LightningElement, api, wire } from "lwc";

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class DragAndDrop_LWC extends LightningElement {
  @api recordId;

  handleUploadFinished(event) {
    const numberOfFiles = event.detail.files.length;
    this.showToast('Success',this.getSuccessMsg(numberOfFiles),'Success');
  }

  getSuccessMsg(numberOfFiles){

    return numberOfFiles+' '+(numberOfFiles > 1 ? 'Files' : 'File') + ' Uploaded Successfully.';
  }

  showToast(title,msg,variant){
    this.dispatchEvent(
      new ShowToastEvent({
        title : title,
        message : msg,
        variant : variant
      })
    );
  }
}
