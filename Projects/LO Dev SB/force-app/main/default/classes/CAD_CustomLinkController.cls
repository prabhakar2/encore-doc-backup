public class CAD_CustomLinkController {
    @AuraEnabled
    public static string fetchProfileNameofUser(){
        User usr = [SELECT Id , profile.Name FROM User WHERE Id =: userInfo.getUserId()];
        System.debug('usr.id >>>>  '+usr.ProfileId);
        profile usrProfile = [select name from profile where id=:usr.ProfileId];//'00e0B000001AB4iQAG'
        system.debug('p >>> '+usrProfile);
        return usrProfile.name;
    }
    
}