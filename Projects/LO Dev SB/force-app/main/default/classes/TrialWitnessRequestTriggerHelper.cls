public without sharing class TrialWitnessRequestTriggerHelper {
	
    public static void afterInsert(List<Trial_Witness_Request__c> newList, Map<Id, Trial_Witness_Request__c> newMap){
    	updateTrialWitnessSubmitted(newList);
    }
    
    private static void updateTrialWitnessSubmitted(List<Trial_Witness_Request__c> trialWitnessRequests){
    	Set<Id> cadIds = new Set<Id>(); 
    	for(Trial_Witness_Request__c trialWitnessRequest : trialWitnessRequests){
			cadIds.add(trialWitnessRequest.TW_Consumer_Account_Records__c);	
    	}
    	
    	List<Consumer_Account_Details__c> consumerAccountDetails = [SELECT Id, Name, (SELECT Id FROM Appeals__r LIMIT 1), 
    																	(SELECT Id FROM Settlement_Approval__r LIMIT 1),
    																	(SELECT Id FROM Business_Record_Affidavit__r LIMIT 1),
    																	(SELECT Id FROM Purchase_and_Sales_Agreement__r LIMIT 1),
    																	(SELECT Id FROM Escalated_Contested_Matters__r LIMIT 1),
    																	(SELECT Id FROM Counterclaims__r LIMIT 1),
    																	(SELECT Id FROM Discoverys__r LIMIT 1) 
    																FROM Consumer_Account_Details__c 
    																WHERE Id IN :cadIds];
		
		for(Consumer_Account_Details__c consumerAccountDetail : consumerAccountDetails){
			if(isExistingLrmmRecordExist(consumerAccountDetail)){
				consumerAccountDetail.Trial_Witness_Submitted__c = true;
			}
		}
		
		update consumerAccountDetails;    																
    }
    
    private static Boolean isExistingLrmmRecordExist(Consumer_Account_Details__c consumerAccountDetail){
    	Boolean foundLRMMRecord = false;
			if((consumerAccountDetail.Appeals__r != null && consumerAccountDetail.Appeals__r.size() > 0) 
				|| (consumerAccountDetail.Settlement_Approval__r != null && consumerAccountDetail.Settlement_Approval__r.size() > 0)
				|| (consumerAccountDetail.Business_Record_Affidavit__r != null && consumerAccountDetail.Business_Record_Affidavit__r.size() > 0)
				|| (consumerAccountDetail.Purchase_and_Sales_Agreement__r != null && consumerAccountDetail.Purchase_and_Sales_Agreement__r.size() > 0)
				|| (consumerAccountDetail.Escalated_Contested_Matters__r != null && consumerAccountDetail.Escalated_Contested_Matters__r.size() > 0)
				|| (consumerAccountDetail.Counterclaims__r != null && consumerAccountDetail.Counterclaims__r.size() > 0)
				|| (consumerAccountDetail.Discoverys__r != null && consumerAccountDetail.Discoverys__r.size() > 0)){
					foundLRMMRecord = true;
				}
		return foundLRMMRecord;				
    }
}