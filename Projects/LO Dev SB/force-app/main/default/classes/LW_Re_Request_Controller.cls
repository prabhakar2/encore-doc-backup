public class LW_Re_Request_Controller {
    public Id recordId{get;set;}
    public Trial_Witness_Request__c oldRecord{get;set;}
    public Trial_Witness_Request__c newRecord{get;set;}
    public static FINAL Map<String,String> objToCADFieldMap = new Map<String,String>{'Counterclaim__c'=>'CC_Consumer_Account_Detail_Name__c','Escalated_Contested_Matters__c'=>'ECM_Consumer_Account_Detail_Name__c','Discovery__c'=>'DY_Consumer_Account_Details__c'};
    
    public LW_Re_Request_Controller(){
        recordId = ApexPages.currentPage().getParameters().get('recordId');
        oldRecord = getData(recordId).oldRecord;
        newRecord = getData(recordId).newRecord;
    }
    
	@AuraEnabled
    public static DataWrapper getData(String recordId){
        DataWrapper dw = new DataWrapper();
        Trial_Witness_Request__c newTempRecord = new Trial_Witness_Request__c();
        List<Trial_Witness_Request__c> oldRecordList = Database.query('SELECT '+getFieldsForQuery()+',RecordType.Name FROM Trial_Witness_Request__c WHERE Id =:recordId LIMIT 1');
        if(!oldRecordList.isEmpty()){
            dw.oldRecord = oldRecordList[0];
            dw.newRecord = oldRecordList[0].clone(false,false,false,false);
            dw.newRecord.Re_Requested_Record__c = true;
            if(dw.newRecord.TW_Consumer_Account_Records__c != NULL){
                dw.newRecord.Counterclaim_Record__c =  getRelatedLRMMRecord('Counterclaim__c',dw.newRecord.TW_Consumer_Account_Records__c);
            	dw.newRecord.Escalated_Contested_Matter_Record__c = getRelatedLRMMRecord('Escalated_Contested_Matters__c',dw.newRecord.TW_Consumer_Account_Records__c);
            	dw.newRecord.Discovery_Name__c = getRelatedLRMMRecord('Discovery__c',dw.newRecord.TW_Consumer_Account_Records__c);
            }
            
            for(String blankField : label.LW_Re_Request_Blank_Fields.split(',')){
                dw.newRecord.put(blankField,NULL);
            }
        }
        return dw;
    }
    
    private static Id getRelatedLRMMRecord(String objName,Id cadId){
        String query = 'SELECT Id,Name FROM '+objName+' WHERE '+objToCADFieldMap.get(objName)+' =:cadId ORDER BY CreatedDate DESC LIMIT 1' ;
        List<Sobject> objList = Database.query(query);
        if(objList.isEmpty()){
            return NULL;
        }
        return objList[0].Id;
    }
    
    
    private static String getFieldsForQuery(){
        List<String> fieldList = new List<String>();
        Map<String, Schema.SObjectField> objFieldMap = Trial_Witness_Request__c.sObjectType.getDescribe().fields.getMap(); 
       
        for(Schema.sObjectField field : objFieldMap.values()){ 
            fieldList.add(field.getDescribe().getName());
        }
        return String.join(fieldList, ',');
    }
    
    
    @AuraEnabled
    public static String saveNewRecord(String newRecordStr,String oldRecordId){
        if(String.isBlank(newRecordStr)){
            return 'Error : Internal Data Issue.';
        }
        
        Trial_Witness_Request__c newLWRecord = (Trial_Witness_Request__c)JSON.deserialize(newRecordStr, Trial_Witness_Request__c.class);
        return saveData(newLWRecord,oldRecordId);
        
    }
    
    public PageReference SaveNew(){
        String newRecordId = saveData(newRecord,recordId);
        if(newRecordId.contains('Error')){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,newRecordId));
            return NULL;
        }
            
        return new PageReference('/'+newRecordId);
    }
    
    private static String saveData(Trial_Witness_Request__c newRecordToSave,String oldRecordId){
        Savepoint sp = Database.setSavepoint();

        try{
            insert newRecordToSave;
            
            Trial_Witness_Request__c oldRecToUpdate = new Trial_Witness_Request__c();
            oldRecToUpdate.Id = oldRecordId;
            oldRecToUpdate.Re_Request_Submitted__c = true;
            update oldRecToUpdate;

            return newRecordToSave.Id;
        }catch(Exception e){
            Database.rollback(sp);
			return 'Error : '+e.getMessage();
        }
        
    }
    
    public class DataWrapper{
        @AuraEnabled
        public Trial_Witness_Request__c oldRecord;
        @AuraEnabled
        public Trial_Witness_Request__c newRecord;
        
        public DataWrapper(){
            oldRecord = new Trial_Witness_Request__c();
            newRecord = new Trial_Witness_Request__c();
        }
    }
}