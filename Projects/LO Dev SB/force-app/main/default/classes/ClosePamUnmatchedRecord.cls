global class ClosePamUnmatchedRecord implements Database.Batchable<sObject>{

  global final Id recordTypeId ;
  global final String recordTypeName;
  global final String statusType;
  
   global ClosePamUnmatchedRecord (String recordTypeName){
  
     if(recordTypeName != null && recordTypeName!= ''){
       this.recordTypeName = recordTypeName;
       this.statusType = 'Accounts Sent To ISeries';
       this.recordTypeId = Schema.SObjectType.Process_Adherence_Monitoring__c.getRecordTypeInfosByName().get(recordTypeName.trim()).getRecordTypeId();
      }
      
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      String Query = 'select id , name,SCMDUM_No_Of_closed_Unmatched__c,SCMDUM_Total_No_Of_Unmatched__c,recordtypeid from Process_Adherence_Monitoring__c where recordtypeid =: recordTypeId AND status__c =:statusType ';
      return Database.getQueryLocator(Query);
   }

   global void execute(Database.BatchableContext BC, List<Process_Adherence_Monitoring__c> scope){
   
      
      List<Process_Adherence_Monitoring__c> pamList = new List<Process_Adherence_Monitoring__c>();
      
            for(Process_Adherence_Monitoring__c pam : scope){
                  if(pam.SCMDUM_No_Of_closed_Unmatched__c != null && pam.SCMDUM_Total_No_Of_Unmatched__c != null
                     && pam.SCMDUM_No_Of_closed_Unmatched__c == 0 && pam.SCMDUM_Total_No_Of_Unmatched__c == 0){
                       
                       pam.SCMD_No_Unmatched_This_Reporting_Period__c = true;
                       pamList.add(pam);
                       
                  }
            }
      
         if(pamList.size() > 0){
           update pamList;
         }
      
      
   
   }

   global void finish(Database.BatchableContext BC){
   
   }
}