trigger TrialWitnessRequestTrigger on Trial_Witness_Request__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
     if(trigger.isBefore){
    	
    	if(trigger.isInsert){
    	
    	}
    	
    	if(trigger.isUpdate){
    		
    	}
    }
    
    if(trigger.isAfter){
    	
    	if(trigger.isInsert){
    	
		    TrialWitnessRequestTriggerHelper.afterInsert(trigger.new, trigger.newMap);
    	}
    	
    	if(trigger.isUpdate){
    		
    	}
    	
    	if(trigger.isDelete){
    		
    	}
    	
    }
}