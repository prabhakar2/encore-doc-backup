<!--********************************************** 
* Page Name  : DataArchiveReport
* Description : Vf Page to show the number of records needs to be Archive.
* Created By  : Prabhakar Joshi
* Created Date: 1-Apr-2020
* *************************************************-->

<apex:page controller="DataArchiveReportController" showHeader="true" sidebar="false" id="pg" lightningStylesheets="true"
    applyBodyTag="false">
    <apex:slds />
    
    <style>
        .tabContent{
        border:1px solid grey;
        padding : 20px;
        
        }
        .slds-tabs_default__item{
        cursor: pointer;
        }
       
        .slds-box{
        background-color:#FFFFCC;
        width:60%;
        
        }
       
    </style>
    <apex:form id="frm">
        <!--@ Action Function to get the chart data according to selected Tab and calling from JS. @-->
        <apex:actionFunction name="getData" action="{!init}" rerender="tabContentPNL" status="sts">
            <!--@ To hold the salected Tab name and passing to controller as a parameter. @-->
            <apex:param assignTo="{!tabName}" name="tabName" value="" />
        </apex:actionFunction>
        
        <!--@ Action status to show the spinner @-->
        <apex:actionstatus id="sts">
            <div class="demo-only" style="height:6rem">
                <apex:facet name="start">
                    <div class="slds-spinner_container">
                        <div role="status" class="slds-spinner slds-spinner_medium slds-spinner_brand">
                            <span class="slds-assistive-text">Loading</span>
                            <div class="slds-spinner__dot-a"></div>
                            <div class="slds-spinner__dot-b"></div>
                        </div>
                    </div>
                </apex:facet>
            </div>
        </apex:actionstatus>
        
        
        
        <div style="text-align:center;margin:5px;">
            <apex:commandButton value="Refresh" action="{!init}" reRender="tabContentPNL" status="sts" styleClass="slds-button slds-button_brand" />
        </div>
        <apex:outputPanel style="text-align:center;padding: 10px;" id="pnl">
            <div class="slds-tabs_default">
                <ul class="slds-tabs_default__nav" role="tablist">
                    <li id="LRMM_Tab" class="slds-tabs_default__item slds-is-active" title="LRMM Tab" role="presentation">
                        <a class="slds-tabs_default__link" href="javascript:ActiveTab('LRMM_Tab','LRMM_Tab_Content');" role="tab" tabindex="0" rerender="" >LRMM</a>
                    </li>
                    <li id="CM_Tab" class="slds-tabs_default__item" title="Item Two" role="presentation">
                        <a class="slds-tabs_default__link" href="javascript:ActiveTab('CM_Tab','CM_Tab_Content');" role="tab" tabindex="-1" >Call Monitoring</a>
                    </li>
                    <li id="PAM_Tab" class="slds-tabs_default__item" title="Item Three" role="presentation">
                        <a class="slds-tabs_default__link" href="javascript:ActiveTab('PAM_Tab','PAM_Tab_Content');" role="tab" tabindex="-1">PAM</a>
                    </li>
                </ul>
                
                <div class="tabContent">
                    <apex:outputPanel id="tabContentPNL"  >
                        <apex:chart colorSet="#BCD96E,#F7DC6F,#D2B4DE,#A3E4D7,#E6B0AA ,#85C1E9,#A4F0A6,#AEB6BF" id="LRMM_chart" animate="true" data="{!chartData}"
                                    height="350" width="700"  >
                            
                            <apex:legend position="left" spacing="20" padding="10"  />
                            
                            <apex:pieSeries dataField="data" tips="false">
                                <apex:chartLabel display="rotate" />
                            </apex:pieSeries>
                            
                        </apex:chart>
                        <div style="font-weight:bold;padding:10px;">
                            <span>Total {!dataType} Archive Attachments : {!totalArchiveData}
                                <br/>
                                <br/> Total Attachments : {!totalAttachmentData}
                            </span>
                            
                        </div>
                        <div class="slds-text-align_center slds-m-around_small">
                            <apex:commandButton id="archiveBtn" value="Archive Data" title="LRMM" disabled="{!if(or(AttachmentTriggerActive == true,totalArchiveData == 0),true,false)}" onclick="archiveDataJS(this.title);return false;" styleClass="slds-button slds-button_destructive" />
                        </div>
                        <apex:outputPanel rendered="{!AttachmentTriggerActive}" >
                            <div class = " slds-m-around_small slds-box slds-box_xx-small slds-text-align_center " style="margin:auto;">
                                First, Need to Inactive 'AttachmentTrigger' from custom setting to enable the Archive button. 
                                <a href="javascript:showInstruction();" >See Instructions...</a>
                            </div>
                            
                        </apex:outputPanel>
                        <apex:outputPanel rendered="{!!AttachmentTriggerActive}" >
                            <div class = " slds-m-around_small slds-box slds-box_xx-small slds-text-align_center " style="margin:auto;">
                                Active the 'AttachmentTrigger' from custom setting after completing Data Archive process.
                                <a href="javascript:showInstruction();" >See Instructions...</a>
                            </div>
                            
                        </apex:outputPanel>
                        <div class = " slds-m-around_small slds-box slds-box_xx-small slds-text-align_center slds-hide" id="instruction" style="margin:auto;">
                            <a href="/setup/ui/listCustomSettings.apexp" target="_blank" class="slds-m-around_x-small"> Go to custom settings</a><B> > Find 'Trigger Setting' and click on Manage > Edit the 'AttachmentTrigger'
                            <br/> > &nbsp;<apex:outputText value="{!if(AttachmentTriggerActive == false,'Check','Uncheck')}"/> 
                                the Active checkbox</B>
                        </div>
                        
                        
                        
                        <div id="progressBarDiv" style="width:60%;margin:auto;" class="slds-progress-bar slds-hide slds-progress-bar_circular" aria-valuemin="0" aria-valuemax="100" aria-valuenow="5" role="progressbar">
                            <span id="progressBarValue" style="width:0%;" class="slds-progress-bar__value">
                                
                            </span>
                        </div>
                        
                        <div id="processingDiv" class="slds-hide">
                            
                        </div>
                        <div id="progressRing" class="slds-progress-ring slds-progress-ring_complete slds-m-around_small slds-hide">
                            <div class="slds-progress-ring__progress" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="100">
                                <svg viewBox="-1 -1 2 2">
                                    <path class="slds-progress-ring__path" id="slds-progress-ring-path-43" d="M 1 0 A 1 1 0 1 1 1 -2.4492935982947064e-16 L 0 0"></path>
                                </svg>
                            </div>
                            <div class="slds-progress-ring__content">
                                <span class="slds-icon_container slds-icon-utility-check" title="Complete">
                                    <svg class="slds-icon" aria-hidden="true">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#check')}" />
                                    </svg>
                                    <span class="slds-assistive-text">Complete</span>
                                </span>
                            </div>
                        </div>
                    </apex:outputPanel>
                    
                </div>
            </div>
        </apex:outputPanel>
    </apex:form>
    
    
    
    <script>
    
    /* Function the Active the tab for switch between tabs. */
    function ActiveTab(tabId,tab_contentId){
        document.getElementById(tabId).classList.add("slds-is-active");
        var allTabs = document.getElementsByClassName('slds-tabs_default__item');
        for (let i = 0; i < allTabs.length; i++) {
            if(allTabs[i].id != tabId){
                allTabs[i].classList.remove("slds-is-active");
            }
        }
        getData(tabId);
    }
    
    /* Function to start the process of Data Archive. */
    function archiveDataJS(){
        if(confirm('Are you sure to want to archive data.')){
            document.getElementById('pg:frm:archiveBtn').disabled = true;
            document.getElementById('progressBarDiv').classList.toggle('slds-hide');
            document.getElementById('processingDiv').classList.toggle('slds-hide');
            Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.DataArchiveReportController.archiveData}',
                '{!tabName}',
                function(result, event){
                    if(event.status && result){
                        var jobId = result;
                        
                        getBatchProgress(jobId);
                    }
                }
            );
        }
    }
    
    /* Function to get the status of the process of Data Archive. */
    function getBatchProgress(jobId){
        
        Visualforce.remoting.Manager.invokeAction(
            '{!$RemoteAction.DataArchiveReportController.updateProgress}',
            jobId,
            function(result, event){
                document.getElementById('progressBarValue').style.width = result.percent+'%';
                if(result.status != 'Completed'){
                    document.getElementById('processingDiv').innerHTML = result.status+'...';
                    
                    setTimeout(function(){
                        getBatchProgress(jobId);
                    }, 1000);
                }else{
                    document.getElementById('progressBarValue').classList.add('slds-progress-bar__value_success');
                    document.getElementById('processingDiv').classList.toggle('slds-hide');
                    document.getElementById('progressRing').classList.toggle('slds-hide');
                }
            }
        );
    }
    
    /* Function to render the instruction panel. */
    function showInstruction(){
        document.getElementById('instruction').classList.toggle('slds-hide');
    }
    
    </script>
</apex:page>