trigger CaseTrigger on Case (before insert) {
    
    if(Trigger.IsBefore){
        if(Trigger.isInsert){
            CaseTriggerHelper.UpdateEmailToCaseFields(Trigger.new);
        }
    }
}