/*
	* @ Trigger Name 		: 	InstallmentTrigger
 	* @ Description 		: 	Trigger on Installment__c object.
	* @ Created By			:   Prabhakar Joshi   
	* @	CreatedDate		    :   11-Dec-2019
 	* @ LastModifiedDate 	: 	25-Aug-2020 
*/

trigger InstallmentTrigger on Installment__c (before update) {
    Boolean triggerActive = Trigger_Setting__c.getValues('InstallmentTrigger') != NULL ? Trigger_Setting__c.getValues('InstallmentTrigger').Active__c : false;
    if(triggerActive){
        InstallmentTrigger_Helper handler = new InstallmentTrigger_Helper();
        if(trigger.isBefore){
            if(trigger.isUpdate){
                for(Installment__c ins : trigger.new){
                    handler.beforeUpdate(trigger.new,trigger.oldMap);
                }
            }
        }
    }
}