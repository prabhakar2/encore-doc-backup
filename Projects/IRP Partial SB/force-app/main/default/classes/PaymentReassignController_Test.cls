/*
	* @ Class Name 		    : 	    PaymentReassignController_Test
 	* @ Description 	    : 	    Test class for PaymentReassignController.
 	* @ Created By 		    : 	    Prabhakar Joshi
    * @ Created Date	    : 	    15-Oct-2020 
*/

@isTest
public class PaymentReassignController_Test {
	@isTest
    static void test1(){
        Account ac = new Account();
        ac.Name = 'test';
        insert ac;
        
        Payments__c pay =new Payments__c();
        pay.RecordTypeId = Schema.SObjectType.Payments__c.getRecordTypeInfosByName().get('Payment 20201101').getRecordTypeId();
        pay.Payment_Type__c = 'Check';
        pay.Company_Name__c = ac.Id;
        insert pay;
        
        Payments__c no_pay = new Payments__c();
        no_pay.RecordTypeId = Schema.SObjectType.Payments__c.getRecordTypeInfosByName().get('No Payment').getRecordTypeId();
        no_pay.Company_Name__c = ac.Id;
        insert no_pay;
        
        Backup__c bk = new Backup__c();
        bk.Payment__c = no_pay.Id;
        bk.Company__c = ac.Id;
        insert bk;
        
        PaymentReassignController obj = new PaymentReassignController();
        obj.currentPayId = no_pay.Id;
        obj.paymentNumber = '202010';
        obj.search();
        obj.searchPayList = new List<PaymentReassignController.PaymentWrapper>{new PaymentReassignController.PaymentWrapper(true,pay)};
        obj.selectedPayRecord = pay;
        obj.fetchBackups();
        for(PaymentReassignController.BackupWrapper bkWrp : obj.backupWrapList){
            bkWrp.isSelected = true;
        }
        obj.reassign();
    }
}