public class MPARTPDFsExtension {

    private ApexPages.StandardController controller;
    public boolean showSellers {get;set;}
    public boolean includeNYOCA {get;set;}
    public boolean includeBreakdown {get;set;}
    public Media_Performance_Relationship_Tracker__c MPART;
    public List<Media_Performance_Relationship_Tracker__c> MPARTList {get;set;}
    public List<Id> selectedMPARTList {get;set;}
    public List<SelectOption> MPARTPicklist {get;set;}
    public Media_Performance_Relationship_Tracker__c Seller1 {get;set;}
    public Media_Performance_Relationship_Tracker__c Seller2 {get;set;}
    public Media_Performance_Relationship_Tracker__c Seller3 {get;set;}
    public string id1 = 'none';
    public string id2 = 'none';
    public string id3 = 'none';
            
    public MPARTPDFSExtension(ApexPages.StandardController stdcontroller){
        this.controller=stdcontroller;
        this.MPART = (Media_Performance_Relationship_Tracker__c)stdcontroller.getRecord();
        
        //query records from same quarter and year to compare with
        MPARTList = [SELECT Id, Name, Company__r.Name, Quarter__c, Year__c, Overall_Score__c,
                     NY_OCA_Not_Applicable__c
                     FROM Media_Performance_Relationship_Tracker__c
                     WHERE Quarter__c=:this.MPART.Quarter__c AND Year__c=:this.MPART.Year__c AND Id!=:this.MPART.Id
                     ORDER BY Name];
        
        //select records to compare with
        MPARTPicklist = new List<SelectOption>();
        for(Media_Performance_Relationship_Tracker__c mpart : MPARTList){
            MPARTPicklist.add(new SelectOption(mpart.Id, mpart.Company__r.Name));
        }

    }
       
    public PageReference Submit(){
        Integer selectedNumberSellers = selectedMPARTList.size();
        //validation for the seller comparison multipicklist (select between 1 and 3 sellers)
        if(selectedNumberSellers==0)
        {
            MPART.addError('Must select at least 1 seller to compare against.');
            return null;
        }else if(selectedNumberSellers>3){
            MPART.addError('Please select 3 or fewer sellers to compare against.');
            return null;
        }else{
            //count number of sellers and get ids
            if(selectedNumberSellers>=1){
                id1 = selectedMPARTList[0];
            }
            if(selectedNumberSellers>=2){
                id2 = selectedMPARTList[1];
            }
            if(selectedNumberSellers>=3){
                id3 = selectedMPARTList[2];
            }
            //proceed with generating the pdf
            PageReference rPost = Page.MPART_PDF_Seller2;
                rPost.setRedirect(true);
                rPost.getParameters().put('id',MPART.Id);
                rPost.getParameters().put('showSellersString',string.valueOf(showSellers));
                rPost.getParameters().put('numberSellers',string.ValueOf(selectedNumberSellers));
                rPost.getParameters().put('includeNYOCA',string.valueOf(includeNYOCA));
                rPost.getParameters().put('includeBreakdown',string.ValueOf(includeBreakdown));
                rPost.getParameters().put('id1',id1);
                rPost.getParameters().put('id2',id2);
                rPost.getParameters().put('id3',id3);
            return rPost;
        }
    }

}