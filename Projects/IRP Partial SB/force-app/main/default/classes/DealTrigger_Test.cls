@isTest
public class DealTrigger_Test 
{
	
       @isTest
    static void test1()
    {
        Trigger_Setting__c ts = new Trigger_Setting__c();
        ts.Name = 'DealTrigger';
        ts.Active__c = true;
        insert ts;
        
        Account ac= new Account();
        ac.Name='test';
        ac.Governing_Law__c ='test';
        ac.Legal_Selling_Entity_1__c ='test1';
        ac.Legal_Selling_Entity_2__c = 'test2';
        insert ac;
        
        Product_Family__c pf1 = new Product_Family__c();
        pf1.Company_Name__c = ac.Id;
        insert pf1;
        
        Product_Family__c pf2 = new Product_Family__c();
        pf2.Company_Name__c = ac.Id;
        pf2.Parent_Product_Family__c = pf1.Id;
        pf2.Asset_Class__c = 'Credit Card';
        pf2.Paper_Type__c = 'CC01';
        pf2.Agency_Cycle__c = 'All';
        pf2.Product_Type__c = 'Core';
        pf2.Region__c = 'US';
        insert pf2;
        
        Deals__c dl = new Deals__c();
        dl.Name = 'test';
        dl.Company_Name__c = ac.Id;
        dl.Product_Family_New__c = pf2.Id;
        dl.Exp_of_Accts__c = 1234;
        dl.Avg_Age_at_Valuation__c = 1234;
        dl.Exp_Mkt_Clearing_Price__c = 44444;
        dl.Exp_Face_Value__c = 434343;
        dl.Bid_Status__c = 'Forecast';
        insert dl;
       
        list<Installment__c> insList = new list<Installment__c>();
        
        Installment__c ins = new Installment__c();
        ins.Deal_Name__c = dl.Id;
        ins.Face_Value__c = 212121;
        ins.Includes_Media_Amt__c = false;
        ins.of_Accts__c = 323232;
        ins.Avg_Age__c = 10;
        ins.Est_Close_Date__c = System.today().addDays(2);
        ins.Purchase_Rate__c = 5;
        insert ins;
        
        dl.Includes_Media_Amt__c = true;
        dl.Comments_1__c='test1';
        dl.Comments_2__c='test2';
        dl.Comments_3__c='test3';
        dl.Comments_4__c='test4';
        dl.Comments_5__c='test5';
        dl.Comments_6__c='test6';
        dl.Comments_7__c='test7';
        dl.Detail_Description__c='details';
        dl.Does_Transfer_Accounting_Apply__c='Yes';
        update dl;
       
    }
    
    @isTest
    static void test2()
    {
        Trigger_Setting__c ts = new Trigger_Setting__c();
        ts.Name = 'DealTrigger';
        ts.Active__c = true;
        insert ts;
        
        Account ac= new Account();
        ac.Name='test';
        ac.Governing_Law__c ='test';
        ac.Legal_Selling_Entity_1__c ='test1';
        ac.Legal_Selling_Entity_2__c = 'test2';
        insert ac;
        
        Product_Family__c pf1 = new Product_Family__c();
        pf1.Company_Name__c = ac.Id;
        insert pf1;
        
        Product_Family__c pf2 = new Product_Family__c();
        pf2.Company_Name__c = ac.Id;
        pf2.Parent_Product_Family__c = pf1.Id;
        pf2.Asset_Class__c = 'Credit Card';
        pf2.Paper_Type__c = 'CC01';
        pf2.Agency_Cycle__c = 'All';
        pf2.Product_Type__c = 'Core';
        pf2.Region__c = 'US';
        insert pf2;
        
        Deals__c dl = new Deals__c();
        dl.Name = 'test';
        dl.Company_Name__c = ac.Id;
        dl.Product_Family_New__c = pf2.Id;
        dl.Exp_of_Accts__c = 1234;
        dl.Avg_Age_at_Valuation__c = 1234;
        dl.Exp_Mkt_Clearing_Price__c = 44444;
        dl.Exp_Face_Value__c = 434343;
        dl.Bid_Status__c = 'Forecast';
        insert dl;
       
        list<Installment__c> insList = new list<Installment__c>();
        
        Installment__c ins = new Installment__c();
        ins.Deal_Name__c = dl.Id;
        ins.Face_Value__c = 212121;
        ins.Includes_Media_Amt__c = false;
        ins.of_Accts__c = 323232;
        ins.Avg_Age__c = 10;
        ins.Est_Close_Date__c = System.today().addDays(2);
        ins.Purchase_Rate__c = 5;
        insert ins;
        
        dl.Question_1_Answer__c='Language Approved';
        dl.Question_2_Answer__c='Not Approved';
        dl.Question_3_Answer__c='Language Approved';
        dl.Question_4_Answer__c='Language Approved';
        dl.Question_5_Answer__c='Not Approved';
        dl.Question_6_Answer__c='Language Approved';
        dl.Question_7_Answer__c='Language Approved';
        dl.Valuation_120_Month_Liq__c= 1;
        dl.Duration_Months__c = 2;
        dl.Bid_IRR__c=1;
        dl.Bid_Rate__c=1;
        dl.Purchase_Rate__c=1;
        dl.Post_Tax_Bid_IRR__c = 2;
        dl.Delete_Installments__c = true;
        update dl;
       
    }
       @isTest
    static void test3()
    {
        Trigger_Setting__c ts = new Trigger_Setting__c();
        ts.Name = 'DealTrigger';
        ts.Active__c = true;
        insert ts;
        
        Account ac= new Account();
        ac.Name='test';
        ac.Governing_Law__c ='test';
        ac.Legal_Selling_Entity_1__c ='test1';
        ac.Legal_Selling_Entity_2__c = 'test2';
        insert ac;
        
        Product_Family__c pf1 = new Product_Family__c();
        pf1.Company_Name__c = ac.Id;
        insert pf1;
        
        Product_Family__c pf2 = new Product_Family__c();
        pf2.Company_Name__c = ac.Id;
        pf2.Parent_Product_Family__c = pf1.Id;
        pf2.Asset_Class__c = 'Credit Card';
        pf2.Paper_Type__c = 'CC01';
        pf2.Agency_Cycle__c = 'All';
        pf2.Product_Type__c = 'Core';
        pf2.Region__c = 'US';
        insert pf2;
        
        Deals__c dl = new Deals__c();
        dl.Name = 'test';
        dl.Company_Name__c = ac.Id;
        dl.Product_Family_New__c = pf2.Id;
        dl.Exp_of_Accts__c = 1234;
        dl.Avg_Age_at_Valuation__c = 1234;
        dl.Exp_Mkt_Clearing_Price__c = 44444;
        dl.Exp_Face_Value__c = 434343;
        dl.Bid_Status__c = 'Forecast';
        insert dl;
       
        list<Installment__c> insList = new list<Installment__c>();
        
        Installment__c ins = new Installment__c();
        ins.Deal_Name__c = dl.Id;
        ins.Face_Value__c = 212121;
        ins.Includes_Media_Amt__c = false;
        ins.of_Accts__c = 323232;
        ins.Avg_Age__c = 10;
        ins.Est_Close_Date__c = System.today().addDays(2);
        ins.Purchase_Rate__c = 5;
        insert ins;
        
        dl.Audit_Provision__c='test Audit';
        dl.Bankruptcy_Time_Period__c='test bank';
        dl.Charge_Off_Date_Time_Period__c='test charge';
        dl.Deceased_Time_Period__c='deceased';
        dl.Disputed_Time_Period__c='disputed';
        dl.Duplicative_Time_Period__c='duplicate';
        dl.Fraud_Time_Period__c='fraud';
        dl.Governing_Law__c='governing';
        dl.High_Balance_Time_Period__c='high';
        dl.Litigation_Time_Period__c='litigation';
        dl.Low_Balance_Time_Period__c='low';
        dl.Material_Variation_Time_Period__c='material';
        dl.Non_Compliance_Time_Period__c='compliance';
        dl.Non_Liable_Time_Period__c='liable';
        dl.Post_ChargeOff_Interest_Fees_Time_Period__c='test charge';
        dl.Proof_of_Claim_Date_Time_Period__c='test proof';
        dl.Putbacks_Categories_Included__c ='1099C';
        dl.Putbacks_Notes_c__c='notes';
        dl.Putbacks_Reimbursement_Limit_days__c=1.0;
        dl.Putbacks_Time_Period_Days_del__c=1.0;
        dl.Putbacks_Volume_Limitations__c='volume';
        dl.Required_Documentation_Time_Period__c='document';
        dl.Required_Information_Time_Period__c='information';
        dl.Seller_Audit_Rights__c='Yes';
        dl.Servicemembers_Civil_Relief_Time_Period__c='service';
        dl.Settled_Time_Period__c='settled';
        dl.Statute_of_Limitations_Time_Period__c='statue';
        dl.X1099C_Time_Period__c='X1099';
        dl.Initial_Consent_Transfers_Review_Date__c= system.today();
        dl.Date_T_C_s_are_Received__c = system.today();
        dl.Date_Media_T_Cs_Analysis_is_Completed__c = system.today();
        dl.BD_Legal_Proposal_Review_is_Completed__c = system.today();
        dl.Media_Legal_Proposal_Review_Completed__c = system.today();
        dl.Date_BRMS_Release_is_Created__c = system.today();
        dl.Date_BRMS_Release_Approval__c = system.today();
        dl.Date_BRMS_Activity_Created__c = system.today();
        dl.Date_BRMS_Activity_Approval__c = system.today();
        update dl;
       
    }
}