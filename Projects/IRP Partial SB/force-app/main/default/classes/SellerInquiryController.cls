public with sharing class SellerInquiryController {
    public Case csRecord{
        get;
        set;
    }
    /** To get and  hold the current payment recordId from URL. */
    public String paymentId{
        get;
        set;
    }
    public SellerInquiryController() {
        paymentId = ApexPages.currentPage().getParameters().get('id');
        csRecord = new Case();
    }
}
