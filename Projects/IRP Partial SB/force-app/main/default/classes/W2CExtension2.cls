public class W2CExtension2 {

	public Case myCase {get;set;}
    public string filename {get;set;}
    public blob filebody {get;set;}

    
    Private ApexPages.StandardController controller;
    public W2CExtension2(ApexPages.StandardController controller) { 
       this.controller=controller;
    }

  	public PageReference Attach() {
        //get record ID for new case
        Case myCase = (Case) controller.getRecord();
       	System.debug(myCase);
        
        if(filename!=null)
        {
			insert new Attachment(Name=filename, Body=filebody, ParentId=myCase.Id);

            //reload page to add more attachments
            PageReference pageRef = Page.W2C_MR_Pg2;
            pageRef.setRedirect(true);
            pageRef.getParameters().put('id',controller.getID());
            return pageRef;
        }
        else {
            ApexPages.Message myMsg=new  ApexPages.Message(ApexPages.Severity.ERROR,'Please choose a file');
            ApexPages.addMessage(myMsg);
            return null;
        }

    }
    
    public PageReference Done() {
        //create new case
        controller.Save();
        //get record ID for new case
        Case myCase = (Case) controller.getRecord();
       	System.debug(myCase);
        
        //Fetching the assignment rules on case
        AssignmentRule AR = new AssignmentRule();
        AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
        //Creating the DMLOptions for "Assign using active assignment rules" checkbox
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;
        //to trigger auto-response rules // the email is sent to the address specified in SuppliedEmail
        dmlOpts.EmailHeader.triggerAutoResponseEmail = true;
        //Setting the DMLOption on Case instance
        myCase.setOptions(dmlOpts);
        update myCase;
        
        //redirect to confirmation page
        PageReference pageRef = Page.W2C_MR_Confirmation;
        pageRef.setRedirect(true);
        pageRef.getParameters().put('id',controller.getID());
        return pageRef;
    }

}