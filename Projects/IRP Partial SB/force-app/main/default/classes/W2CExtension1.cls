public class W2CExtension1 {

	public Case myCase {get;set;}
    public string filename {get;set;}
    public blob filebody {get;set;}
    Private ApexPages.StandardController controller;
    Private final Case record;
    public W2CExtension1 (ApexPages.StandardController controller) { 
       this.controller=controller;
       this.record = (Case)controller.getRecord();
    }
     
   	public PageReference Next() {
        
        if(record.Media_Document_Type__c=='Other/Multiple' && record.Description==null)
        {
            record.Description.addError('Notes are required when Other/Multiple is selected');
            return null;
        }
        else{
        //create new case
        Case newCase = new Case();
        	newCase.Auto_Response__c=true;
        	newCase.Origin='Web';
        	newCase.RecordTypeId='012500000009hSj';
        	newCase.SuppliedName=record.SuppliedName;
        	newCase.SuppliedEmail=record.SuppliedEmail;
        	newCase.Requested_Due_Date__c=record.Requested_Due_Date__c;
        	newCase.OnDemand_Ordered_Date__c=record.OnDemand_Ordered_Date__c;
        	newCase.Media_Request_Type__c=record.Media_Request_Type__c;
			newCase.Customer_Name__c=record.Customer_Name__c;
        	newCase.W2C_Account_Number_Type__c=record.W2C_Account_Number_Type__c;
        	newCase.W2C_Account_Number__c=record.W2C_Account_Number__c;
        	newCase.Company_Purchased_from__c=record.Company_Purchased_from__c;
        	newCase.Original_Account_Number__c=record.Original_Account_Number__c;
        	newCase.Document_Date__c=record.Document_Date__c;
        	newCase.Media_Document_Type__c=record.Media_Document_Type__c;
        	newCase.Description=record.Description;
        upsert newCase;
        id newId = newCase.id;
      
        //redirect to attachment page
        PageReference pageRef = Page.W2C_MR_Pg2;
        pageRef.setRedirect(true);
        pageRef.getParameters().put('id',newId);
        return pageRef;
        }

    }
    
    public PageReference Done() {
        
        if(record.Media_Document_Type__c=='Other/Multiple' && record.Description==null)
        {
            record.Description.addError('Notes are required when Other/Multiple is selected');
            return null;
        }
        else{
       //create new case
        Case newCase = new Case();
        	newCase.Auto_Response__c=true;
        	newCase.Origin='Web';
        	newCase.RecordTypeId='012500000009hSj';
        	newCase.SuppliedName=record.SuppliedName;
        	newCase.SuppliedEmail=record.SuppliedEmail;
        	newCase.Requested_Due_Date__c=record.Requested_Due_Date__c;
        	newCase.OnDemand_Ordered_Date__c=record.OnDemand_Ordered_Date__c;
        	newCase.Media_Request_Type__c=record.Media_Request_Type__c;
			newCase.Customer_Name__c=record.Customer_Name__c;
        	newCase.W2C_Account_Number_Type__c=record.W2C_Account_Number_Type__c;
        	newCase.W2C_Account_Number__c=record.W2C_Account_Number__c;
        	newCase.Company_Purchased_from__c=record.Company_Purchased_from__c;
        	newCase.Original_Account_Number__c=record.Original_Account_Number__c;
        	newCase.Document_Date__c=record.Document_Date__c;
        	newCase.Media_Document_Type__c=record.Media_Document_Type__c;
        	newCase.Description=record.Description;
        upsert newCase;
        //get record ID for new case
        id newId = newCase.id;
        
        //Fetching the assignment rules on case
        AssignmentRule AR = new AssignmentRule();
        AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
        //Creating the DMLOptions for "Assign using active assignment rules" checkbox
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;
        //to trigger auto-response rules // the email is sent to the address specified in SuppliedEmail
        dmlOpts.EmailHeader.triggerAutoResponseEmail = true;
        //Setting the DMLOption on Case instance
        newCase.setOptions(dmlOpts);
        update newCase;
        
        //Redirect to confirmation page
        PageReference pageRef = Page.W2C_MR_Confirmation;
        pageRef.setRedirect(true);
        pageRef.getParameters().put('id',controller.getID());
        return pageRef;
        }
    }
 
}