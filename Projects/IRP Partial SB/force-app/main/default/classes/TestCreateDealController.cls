/*
###########################################################################
# Created by............: Chander Chauhan
# Created Date..........: 7th November 2014
# Description...........: Test class
###########################################################################*/
@isTest
public class TestCreateDealController{

static testMethod void runTest() 
{
    Test.startTest();
    Account ac=new Account();
    ac.Name='xyz';
    insert ac;
   
   
    Forecast__c a = new Forecast__c();
    a.Forecasted_Avg_Age__c = 11;  
    a.Exp_Face_Value__c = 1;
    a.Date_File_Expected__c = Date.newInstance(2017, 1, 1);
    a.Avg_Bal__c = 1;
    a.of_Installments__c  = 11;
    a.Forecasted_Agency_Cycle__c   = 'Performing';
    a.Forecasted_Asset_Class__c   = 'Credit Card';
    a.Forecasted_Avg_Age__c   = 2;
    a.End_Date__c   =  system.today();
    a.Exp_Face_Value__c   = 1;
    a.General_Notes__c   = 'tested deal';
    a.IRR__c   = 1;
    a.Product_Type__c   = 'Utility';
    a.Start_Date__c   =  Date.newInstance(2017,1,20);
    a.Encore_Win_Rate__c=10;
    a.Company_Name__c= ac.id;
    
    insert a;
   
    PageReference p = Page.createdeal;
    p.getParameters().put('id', a.Id);
    Test.setCurrentPage(p);
    
    createdealcontroller ctrl = new createdealcontroller(new ApexPages.StandardController(a)); 
    ctrl.deal();
    Test.stopTest();
       
   list<Deals__c> b =[SELECT Id,  Avg_Age_at_Valuation__c From Deals__c where Avg_Age_at_Valuation__c =: a.Forecasted_Avg_Age__c];
   System.assertEquals(1, b.size()); 
            
    
  }
 }