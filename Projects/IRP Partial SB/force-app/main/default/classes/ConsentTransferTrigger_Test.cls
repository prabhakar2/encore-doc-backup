@isTest
public class ConsentTransferTrigger_Test 
{
	@isTest
    static void test()
    {
        Trigger_Setting__c ts = new Trigger_Setting__c();
        ts.Name = 'DealTrigger';
        ts.Active__c = true;
        insert ts;
        
        Trigger_Setting__c ts1 = new Trigger_Setting__c();
        ts1.Name = 'ConsentTransferTrigger';
        ts1.Active__c = true;
        insert ts1;
        
        Account ac= new Account();
        ac.Name='test';
        ac.Governing_Law__c ='test';
        ac.Legal_Selling_Entity_1__c ='test1';
        ac.Legal_Selling_Entity_2__c = 'test2';
        insert ac;
        
        Product_Family__c pf1 = new Product_Family__c();
        pf1.Company_Name__c = ac.Id;
        insert pf1;
        
        Product_Family__c pf2 = new Product_Family__c();
        pf2.Company_Name__c = ac.Id;
        pf2.Parent_Product_Family__c = pf1.Id;
        pf2.Asset_Class__c = 'Credit Card';
        pf2.Paper_Type__c = 'CC01';
        pf2.Agency_Cycle__c = 'All';
        pf2.Product_Type__c = 'Core';
        pf2.Region__c = 'US';
        insert pf2;
        
        Deals__c dl = new Deals__c();
        dl.Name = 'test';
        dl.Company_Name__c = ac.Id;
        dl.Product_Family_New__c = pf2.Id;
        dl.Exp_of_Accts__c = 1234;
        dl.Avg_Age_at_Valuation__c = 1234;
        dl.Exp_Mkt_Clearing_Price__c = 44444;
        dl.Exp_Face_Value__c = 434343;
        dl.Bid_Status__c = 'Forecast';
        insert dl;
        
        Installment__c ins = new Installment__c();
        ins.Deal_Name__c = dl.Id;
        ins.Face_Value__c = 212121;
        ins.Includes_Media_Amt__c = false;
        ins.of_Accts__c = 323232;
        ins.Avg_Age__c = 10;
        ins.Est_Close_Date__c = System.today().addDays(2);
        ins.Purchase_Rate__c = 5;
        insert ins;
        
        Consent_Transfer__c ct = new Consent_Transfer__c();
        ct.Deal__c = dl.Id;
        ct.Accuracy_Rep__c = 'test acc';
        ct.Additional_Numbers__c = true;
        ct.Additional_Number_s_Type__c ='Work';
        ct.Upfront_Phone_Number_s_Provided__c = true;
        ct.Upfront_Phone_Number_s_Provided_Type__c ='Home';
        ct.Phone_Data_Existence__c='yes';
        ct.Phone_Notes__c='test phone';
        ct.Phone_Validity_Code_PVC_Availability__c='yes';
        ct.Further_Information__c='test further';
        ct.Contract__c='Yes';
        ct.Practice__c='No';
        ct.Go_Forward_Confidence__c='No';
        ct.Have_Attachments_Been_Uploaded__c=true;
        insert ct;
        
        ct.Arbitration__c='Yes';
        ct.Assignment__c='Yes';
        ct.Communication__c='No';
        ct.Arbitration_Comments__c='test arbit';
        ct.Assignment_Comments__c='test assign';
        ct.Communication_Comments__c='test comm';
        ct.Date_T_C_Language_Sent_for_Legal_Review__c= system.today();
        ct.Date_Legal_Completes_T_C_Review__c = system.today();
        ct.Legal_Notes__c='test legal';
        ct.Has_Consent_Transfer_Been_Approved__c='No';
        ct.ESC_Consent_Transfer_Proposal_Review__c = system.today();
        ct.Consent_Transfer_Approved_by_ESC__c ='No';
        ct.Consent_Transfer_Deployment_Date__c = system.today();
        update ct;
    }
}