public class RunCaseAssignmentRules
{
    @InvocableMethod
    public static void CaseAssign(List<Id> CaseId)
    {
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule= true;          
            Case Cases=[SELECT Id FROM Case WHERE Case.Id in :CaseId];
            Cases.setOptions(dmo);
            update Cases;
   }
}