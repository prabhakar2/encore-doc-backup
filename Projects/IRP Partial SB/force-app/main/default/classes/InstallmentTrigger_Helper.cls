/*
	* @ Class Name 			: 	InstallmentTrigger_Helper
	* @ Description 		: 	Handler Class for InstallmentTrigger.
	* @ Created By			: 	Prabhakar Joshi
	* @ Created Date		: 	11-Dec-2019
	* @ LastModifiedDate 	: 	25-Aug-2020 
*/

public class InstallmentTrigger_Helper {
    
    /* Method for before Update event. */
    public void beforeUpdate(List<Installment__c> triggerNew,Map<Id,Installment__c> oldMap){
        this.populatePortFolioNumber(triggerNew,oldMap);
    }
    
    
    /* @ Method definition for populate the portfolio Number. */
    private void populatePortFolioNumber(List<Installment__c> triggerNew,Map<Id,Installment__c> oldMap){
        List<Installment__c> blankList = new List<Installment__c>();
        for(Installment__c ins : triggerNew){
            if(ins.Create_Pool_Port__c && ins.Portfolio__c == NULL && oldMap.get(ins.Id).get('Portfolio__c') == NULL){
                blankList.add(ins);
            } 
        }
        
        if(!blankList.isEmpty()){
            List<Installment__c> oldInsList = [SELECT Id,Name,Portfolio__c,Portfolio_No__c FROM Installment__c 
                                               WHERE Portfolio_No__c != NULL AND Portfolio__c != NULL 
                                               AND Id NOT IN :triggerNew 
                                               ORDER BY Portfolio_No__c DESC LIMIT 1];
            if(!oldInsList.isEmpty()){
                Integer count = 1;
                for(Installment__c ins : blankList){
                    ins.Portfolio_No__c = oldInsList[0].Portfolio_No__c + count;
                    ins.Portfolio__c = String.valueOf(ins.Portfolio_No__c);
                    ins.Pool__c = ins.Portfolio__c;
                    count++;
                }
            }
        }
    }
}