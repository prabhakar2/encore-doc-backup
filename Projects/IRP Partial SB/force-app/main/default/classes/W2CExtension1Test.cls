@isTest(SeeAllData=true)
public class W2CExtension1Test {
    
    @isTest static void testNextElse()
    {
        //insert new record without attachment
        Case newCase = new Case();
        	newCase.Auto_Response__c=true;
        	newCase.Origin='Web';
        	newCase.RecordTypeId='012500000009hSj';
        	newCase.SuppliedName='testNextElse';
        	newCase.SuppliedEmail='SuppliedEmail@mcmcg.com';
        	newCase.Requested_Due_Date__c=system.today()+1;
        	newCase.OnDemand_Ordered_Date__c=system.today();
        	newCase.Media_Request_Type__c='Other';
			newCase.Customer_Name__c='Customer Name';
        	newCase.W2C_Account_Number_Type__c='MCM';
        	newCase.W2C_Account_Number__c='1234567890';
        	newCase.Company_Purchased_from__c='Company Name';
        	newCase.Original_Account_Number__c='0987654321';
        	newCase.Document_Date__c=system.today();
        	newCase.Media_Document_Type__c='Other/Multiple';
        	newCase.Description='Test';
         try
            {
                ApexPages.StandardController sc = new ApexPages.StandardController(newCase);
                W2CExtension1 W2CExtension1Obj = new W2CExtension1(sc);//Instantiate the Class
                W2CExtension1Obj.Next();
             }
         catch(Exception e)
            {}
        list<Case> case1 = [SELECT Id, SuppliedName, Requested_Due_Date__c FROM Case WHERE Requested_Due_Date__c=:system.today()+1 AND SuppliedName='testNextElse'];//retrieve the record
        integer i = case1.size();
        system.assertEquals(1,i);//test that record was inserted
    }
    
    @isTest static void testDoneElse()
    {
        //insert new record without attachment
        Case newCase = new Case();
        	newCase.Auto_Response__c=false;
        	newCase.Origin='Web';
        	newCase.RecordTypeId='012500000009hSj';
        	newCase.SuppliedName='testDoneElse';
        	newCase.SuppliedEmail='SuppliedEmail@mcmcg.com';
        	newCase.Requested_Due_Date__c=system.today()+1;
        	newCase.OnDemand_Ordered_Date__c=system.today();
        	newCase.Media_Request_Type__c='Other';
			newCase.Customer_Name__c='Customer Name';
        	newCase.W2C_Account_Number_Type__c='MCM';
        	newCase.W2C_Account_Number__c='1234567890';
        	newCase.Company_Purchased_from__c='Company Name';
        	newCase.Original_Account_Number__c='0987654321';
        	newCase.Document_Date__c=system.today();
        	newCase.Media_Document_Type__c='Other/Multiple';
        	newCase.Description='Test';
         try
            {
                ApexPages.StandardController sc = new ApexPages.StandardController(newCase);
                W2CExtension1 W2CExtension1Obj = new W2CExtension1(sc);//Instantiate the Class
                W2CExtension1Obj.Done();
             }
            catch(Exception ee)
            {}
        list<Case> case1 = [SELECT Id, SuppliedName, Requested_Due_Date__c FROM Case WHERE Requested_Due_Date__c=:system.today()+1 AND SuppliedName='testDoneElse'];//retrieve the record
        integer i = case1.size();
        system.assertEquals(1,i);//test that record was inserted
    }

}