/***********************************************************
 * Class Name  : DealTrigger_Helper
 * Description : Helper class for DealTrigger
 * Created By  : Vaibhav Jain  
 * CreatedDate  : 01-July-2020
 * ******************************************************* */

public class DealTrigger_Helper 
{ 
    
    /************************ Method for updating media Amount in installment when updating *************************/   
    public void afterUpdateMediaAmt(List<Deals__c> triggerNew,Map<Id,Deals__c> oldMap)
    {   
        Set<Id> dealIds = new Set<Id>();
    List<Installment__c> instList = new List<Installment__c>();
    for (Deals__c d: triggerNew) {
        Deals__c oldDeal = oldMap.get(d.Id);
        if (d.Includes_Media_Amt__c != oldDeal.Includes_Media_Amt__c && d.Includes_Media_Amt__c == true) 
        {
            dealIds.add(d.Id);
        }
    }
   //     system.debug('after update media amt');
    for (Installment__c ins: [select Includes_Media_Amt__c,deal_name__c from Installment__c where deal_name__c in :dealIds]) 
    {
        if (ins.Includes_Media_Amt__c!= true) {
            ins.Includes_Media_Amt__c = true;
            instList.add(ins);
        }
    }
    if (!instList.isEmpty()) {
        update instList;
    }

    } 
    
    /************************ Method for updating accounting comments in installment when updating deal *************************/ 
    public void updateInstallmentComments(List<Deals__c> triggerNew,Map<Id,Deals__c> oldMap)
    {   
        Map<Id, Deals__c> changed = new Map<Id, Deals__c>();
        List<Installment__c> instList = new List<Installment__c>();
        for (Deals__c d: triggerNew) {
            Deals__c oldDeal = oldMap.get(d.Id);
            if (d.Comments_1__c  != oldDeal.Comments_1__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Comments_2__c  != oldDeal.Comments_2__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Comments_3__c  != oldDeal.Comments_3__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Comments_4__c  != oldDeal.Comments_4__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Comments_5__c  != oldDeal.Comments_5__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Comments_6__c  != oldDeal.Comments_6__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Comments_7__c  != oldDeal.Comments_7__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Detail_Description__c != oldDeal.Detail_Description__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Does_Transfer_Accounting_Apply__c  != oldDeal.Does_Transfer_Accounting_Apply__c) 
            {
                changed.put(d.Id, d);
            }  
        }
       // 	system.debug('entering after criteria check');
        for (Installment__c ins: [select Comments_1__c,Comments_2__c,Comments_3__c,Comments_4__c,Comments_5__c,
                                  Comments_6__c,Comments_7__c,Detail_Description__c,Does_Transfer_Accounting_Apply__c,
                                  deal_name__c from Installment__c where deal_name__c IN : changed.keySet()]) 
        {
            Deals__c ds = changed.get(ins.deal_name__c);
            ins.Comments_1__c = ds.Comments_1__c;
            ins.Comments_2__c = ds.Comments_2__c;
            ins.Comments_3__c = ds.Comments_3__c;
            ins.Comments_4__c = ds.Comments_4__c;
            ins.Comments_5__c = ds.Comments_5__c;
            ins.Comments_6__c = ds.Comments_6__c;
            ins.Comments_7__c = ds.Comments_7__c;
            ins.Detail_Description__c = ds.Detail_Description__c;
            ins.Does_Transfer_Accounting_Apply__c = ds.Does_Transfer_Accounting_Apply__c;
            instList.add(ins);
        }
        if (!instList.isEmpty()) {
            update instList;
        }
    }
    
    /************************ Method for updating accounting questions in installment when updating deal *************************/ 
    public void updateInstallmentQuestions(List<Deals__c> triggerNew,Map<Id,Deals__c> oldMap)
    {   
        Map<Id, Deals__c> changed = new Map<Id, Deals__c>();
        List<Installment__c> instList = new List<Installment__c>();
        for (Deals__c d: triggerNew) {
            Deals__c oldDeal = oldMap.get(d.Id);
            if (d.Question_1_Answer__c   != oldDeal.Question_1_Answer__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Question_2_Answer__c  != oldDeal.Question_2_Answer__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Question_3_Answer__c  != oldDeal.Question_3_Answer__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Question_4_Answer__c  != oldDeal.Question_4_Answer__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Question_5_Answer__c  != oldDeal.Question_5_Answer__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Question_6_Answer__c  != oldDeal.Question_6_Answer__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Question_7_Answer__c  != oldDeal.Question_7_Answer__c) 
            {
                changed.put(d.Id, d);
            }
        }
        	//system.debug('entering after criteria question check');
        for (Installment__c ins: [select Question_1_Answer__c,Question_2_Answer__c,Question_3_Answer__c,
                                  Question_4_Answer__c,Question_5_Answer__c,Question_6_Answer__c,Question_7_Answer__c,
                                  deal_name__c from Installment__c where deal_name__c IN : changed.keySet()]) 
        {
            Deals__c ds = changed.get(ins.deal_name__c);
            ins.Question_1_Answer__c = ds.Question_1_Answer__c;
            ins.Question_2_Answer__c = ds.Question_2_Answer__c;
            ins.Question_3_Answer__c = ds.Question_3_Answer__c;
            ins.Question_4_Answer__c = ds.Question_4_Answer__c;
            ins.Question_5_Answer__c = ds.Question_5_Answer__c;
            ins.Question_6_Answer__c = ds.Question_6_Answer__c;
            ins.Question_7_Answer__c = ds.Question_7_Answer__c;
            instList.add(ins);
        }
        if (!instList.isEmpty()) {
            update instList;
        }
    }
    
    /************************ Method for updating valuation details in installment when updating deal *************************/ 
    public void updateInstallmentValuationDetails(List<Deals__c> triggerNew,Map<Id,Deals__c> oldMap)
    {   
        Map<Id, Deals__c> changed = new Map<Id, Deals__c>();
        List<Installment__c> instList = new List<Installment__c>();
        
        for (Deals__c d: triggerNew) {
            Deals__c oldDeal = oldMap.get(d.Id);
            if (d.Valuation_120_Month_Liq__c   != oldDeal.Valuation_120_Month_Liq__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Duration_Months__c  != oldDeal.Duration_Months__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Bid_Multiple__c  != oldDeal.Bid_Multiple__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Bid_IRR__c  != oldDeal.Bid_IRR__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Bid_Rate__c  != oldDeal.Bid_Rate__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Purchase_Rate__c  != oldDeal.Purchase_Rate__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Post_Tax_Bid_IRR__c  != oldDeal.Post_Tax_Bid_IRR__c) 
            {
                changed.put(d.Id, d);
            }
        }
        //	system.debug('entering after criteria valuation check');
        for (Installment__c ins: [select X120_Month_Liq__c,Duration_Months__c,Expected_Multiple_at_Purchase__c,
                                  Expected_IRR_at_Purchase__c,IRR__c,Bid_Rate__c,Purchase_Rate__c,Expected_Post_Tax_IRR_at_Purchase__c,
                                  Post_Tax_IRR__c,deal_name__c from Installment__c where deal_name__c IN : changed.keySet()
                                  and Est_Close_Date__c > today]) 
        {
            Deals__c ds = changed.get(ins.deal_name__c);
            ins.X120_Month_Liq__c = ds.Valuation_120_Month_Liq__c;
            ins.Duration_Months__c = ds.Duration_Months__c;
            ins.Expected_Multiple_at_Purchase__c = ds.Bid_Multiple__c;
            ins.Expected_IRR_at_Purchase__c = ds.Bid_IRR__c;
            ins.IRR__c = ds.Bid_IRR__c;
            ins.Bid_Rate__c = ds.Bid_Rate__c;
            ins.Purchase_Rate__c = ds.Purchase_Rate__c;
            ins.Expected_Post_Tax_IRR_at_Purchase__c = ds.Post_Tax_Bid_IRR__c;
            ins.Post_Tax_IRR__c = ds.Post_Tax_Bid_IRR__c;
            instList.add(ins);
        }
        if (!instList.isEmpty()) {
            update instList;
        }
    }

	/************************ Method for updating putback details in installment when updating deal *************************/ 
    public void updateInstallmentPutbacks(List<Deals__c> triggerNew,Map<Id,Deals__c> oldMap)
    {   
        Map<Id, Deals__c> changed = new Map<Id, Deals__c>();
        List<Installment__c> instList = new List<Installment__c>();
        
        for (Deals__c d: triggerNew) {
            Deals__c oldDeal = oldMap.get(d.Id);
            if (d.Audit_Provision__c != oldDeal.Audit_Provision__c || d.Bankruptcy_Time_Period__c != oldDeal.Bankruptcy_Time_Period__c ||
               d.Charge_Off_Date_Time_Period__c != oldDeal.Charge_Off_Date_Time_Period__c || d.Deceased_Time_Period__c != oldDeal.Deceased_Time_Period__c ||
               d.Disputed_Time_Period__c != oldDeal.Disputed_Time_Period__c || d.Duplicative_Time_Period__c != oldDeal.Duplicative_Time_Period__c ||
               d.Fraud_Time_Period__c != oldDeal.Fraud_Time_Period__c ||d.Governing_Law__c != oldDeal.Governing_Law__c ||
               d.High_Balance_Time_Period__c != oldDeal.High_Balance_Time_Period__c ||d.Litigation_Time_Period__c != oldDeal.Litigation_Time_Period__c ||
               d.Low_Balance_Time_Period__c != oldDeal.Low_Balance_Time_Period__c || d.Material_Variation_Time_Period__c != oldDeal.Material_Variation_Time_Period__c ||
               d.Non_Compliance_Time_Period__c != oldDeal.Non_Compliance_Time_Period__c || d.Non_Liable_Time_Period__c != oldDeal.Non_Liable_Time_Period__c ||
               d.Post_ChargeOff_Interest_Fees_Time_Period__c != oldDeal.Post_ChargeOff_Interest_Fees_Time_Period__c ||
               d.Proof_of_Claim_Date_Time_Period__c != oldDeal.Proof_of_Claim_Date_Time_Period__c ||
               d.Putbacks_Categories_Included__c != oldDeal.Putbacks_Categories_Included__c ||
               d.Putbacks_Notes_c__c != oldDeal.Putbacks_Notes_c__c ||
               d.Putbacks_Reimbursement_Limit_days__c != oldDeal.Putbacks_Reimbursement_Limit_days__c ||
               d.Putbacks_Time_Period_Days_del__c != oldDeal.Putbacks_Time_Period_Days_del__c ||
               d.Putbacks_Volume_Limitations__c != oldDeal.Putbacks_Volume_Limitations__c ||
               d.Required_Documentation_Time_Period__c != oldDeal.Required_Documentation_Time_Period__c ||
               d.Required_Information_Time_Period__c != oldDeal.Required_Information_Time_Period__c ||
               d.Seller_Audit_Rights__c != oldDeal.Seller_Audit_Rights__c ||
               d.Servicemembers_Civil_Relief_Time_Period__c != oldDeal.Servicemembers_Civil_Relief_Time_Period__c ||
               d.Settled_Time_Period__c != oldDeal.Settled_Time_Period__c ||
               d.Statute_of_Limitations_Time_Period__c != oldDeal.Statute_of_Limitations_Time_Period__c ||
               d.X1099C_Time_Period__c != oldDeal.X1099C_Time_Period__c) 
            {
                changed.put(d.Id, d);
            }
        }
    
        for (Installment__c ins: [select id,deal_name__c from Installment__c where deal_name__c IN : changed.keySet()]) 
        {
            Deals__c ds = changed.get(ins.deal_name__c);
            ins.Audit_Provision__c = ds.Audit_Provision__c;
            ins.Bankruptcy_Time_Period__c = ds.Bankruptcy_Time_Period__c;
            ins.Charge_Off_Date_Time_Period__c = ds.Charge_Off_Date_Time_Period__c;
            ins.Deceased_Time_Period__c = ds.Deceased_Time_Period__c;
            ins.Disputed_Time_Period__c = ds.Disputed_Time_Period__c;
            ins.Duplicative_Time_Period__c = ds.Duplicative_Time_Period__c;
            ins.Fraud_Time_Period__c = ds.Fraud_Time_Period__c;
            ins.Governing_Law__c = ds.Governing_Law__c;
            ins.High_Balance_Time_Period__c = ds.High_Balance_Time_Period__c;
            ins.Litigation_Time_Period__c = ds.Litigation_Time_Period__c;
            ins.Low_Balance_Time_Period__c = ds.Low_Balance_Time_Period__c;
            ins.Material_Variation_Time_Period__c = ds.Material_Variation_Time_Period__c;
            ins.Non_Compliance_Time_Period__c = ds.Non_Compliance_Time_Period__c;
            ins.Non_Liable_Time_Period__c = ds.Non_Liable_Time_Period__c;
            ins.Post_ChargeOff_Interest_Fees_Time_Period__c = ds.Post_ChargeOff_Interest_Fees_Time_Period__c;
            ins.Proof_of_Claim_Date_Time_Period__c = ds.Proof_of_Claim_Date_Time_Period__c;
            ins.Putbacks_Categories_Included__c = ds.Putbacks_Categories_Included__c;
            ins.Putbacks_Notes__c = ds.Putbacks_Notes_c__c;
            ins.Putbacks_Reimbursement_Limit_days__c = ds.Putbacks_Reimbursement_Limit_days__c;
            ins.Putbacks_Time_Period_Days__c = ds.Putbacks_Time_Period_Days_del__c;
            ins.Putbacks_Volume_Limitations__c = ds.Putbacks_Volume_Limitations__c;
            ins.Required_Documentation_Time_Period__c = ds.Required_Documentation_Time_Period__c;
            ins.Required_Information_Time_Period__c = ds.Required_Information_Time_Period__c;
            ins.Seller_Audit_Rights__c = ds.Seller_Audit_Rights__c;
            ins.Servicemembers_Civil_Relief_Time_Period__c = ds.Servicemembers_Civil_Relief_Time_Period__c;
            ins.Settled_Time_Period__c = ds.Settled_Time_Period__c;
            ins.Statute_of_Limitations_Time_Period__c = ds.Statute_of_Limitations_Time_Period__c;
            ins.X1099C_Time_Period__c = ds.X1099C_Time_Period__c;
            instList.add(ins);
        }
        if (!instList.isEmpty()) {
            update instList;
        }
    }
    
    /************************ Method for updating SLA's in installment when updating deal *************************/ 
    public void updateInstallmentSLA(List<Deals__c> triggerNew,Map<Id,Deals__c> oldMap)
    {   
        Map<Id, Deals__c> changed = new Map<Id, Deals__c>();
        List<Installment__c> instList = new List<Installment__c>();
        
        for (Deals__c d: triggerNew) {
            Deals__c oldDeal = oldMap.get(d.Id);
            if (d.Initial_Consent_Transfers_Review_Date__c    != oldDeal.Initial_Consent_Transfers_Review_Date__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Date_T_C_s_are_Received__c   != oldDeal.Date_T_C_s_are_Received__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Date_Media_T_Cs_Analysis_is_Completed__c  != oldDeal.Date_Media_T_Cs_Analysis_is_Completed__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.BD_Legal_Proposal_Review_is_Completed__c  != oldDeal.BD_Legal_Proposal_Review_is_Completed__c ) 
            {
                changed.put(d.Id, d);
            }
            if (d.Media_Legal_Proposal_Review_Completed__c  != oldDeal.Media_Legal_Proposal_Review_Completed__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Date_BRMS_Release_is_Created__c  != oldDeal.Date_BRMS_Release_is_Created__c ) 
            {
                changed.put(d.Id, d);
            }
            if (d.Date_BRMS_Release_Approval__c  != oldDeal.Date_BRMS_Release_Approval__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Date_BRMS_Activity_Created__c  != oldDeal.Date_BRMS_Activity_Created__c ) 
            {
                changed.put(d.Id, d);
            }
            if (d.Date_BRMS_Activity_Approval__c  != oldDeal.Date_BRMS_Activity_Approval__c) 
            {
                changed.put(d.Id, d);
            }
            if (d.Consent_Transfer_Applicability_Review__c  != oldDeal.Consent_Transfer_Applicability_Review__c ) 
            {
                changed.put(d.Id, d);
            }
            if (d.Receive_First_Installment_T_Cs__c  != oldDeal.Receive_First_Installment_T_Cs__c) 
            {
                changed.put(d.Id, d);
            }
            if(d.Complete_Media_T_Cs_Analysis_By__c  != oldDeal.Complete_Media_T_Cs_Analysis_By__c )
            {
                changed.put(d.Id,d);
            }
            if(d.Complete_BD_Legal_Proposal_Review_By__c != oldDeal.Complete_BD_Legal_Proposal_Review_By__c )
            {
                changed.put(d.Id,d);
            }
            if(d.Media_Legal_Proposal_Review_By__c != oldDeal.Media_Legal_Proposal_Review_By__c)
            {
                changed.put(d.Id,d);
            }
            if(d.Create_BRMS_Release_By0__c != oldDeal.Create_BRMS_Release_By0__c )
            {
                changed.put(d.Id,d);
            }
            if(d.Approve_BRMS_Release_By__c != oldDeal.Approve_BRMS_Release_By__c)
            {
                changed.put(d.Id,d);
            }
            if(d.Create_BRMS_Activity_By0__c != oldDeal.Create_BRMS_Activity_By0__c)
            {
                changed.put(d.Id,d);
            }
            if(d.Approve_BRMS_Activity_By__c != oldDeal.Approve_BRMS_Activity_By__c)
            {
                changed.put(d.Id,d);
            }
        }
        //	system.debug('entering after criteria valuation check');
        for (Installment__c ins: [select id,deal_name__c from Installment__c where deal_name__c IN : changed.keySet()]) 
        {
            Deals__c ds = changed.get(ins.deal_name__c);
            ins.Initial_Consent_Transfer_Review_Date__c = ds.Initial_Consent_Transfers_Review_Date__c;
            ins.Date_T_Cs_are_Received__c = ds.Date_T_C_s_are_Received__c;
            ins.Date_Media_T_Cs_Analysis_is_Completed__c = ds.Date_Media_T_Cs_Analysis_is_Completed__c;
            ins.BD_Legal_Proposal_Review_Completed__c = ds.BD_Legal_Proposal_Review_is_Completed__c;
            ins.Media_Legal_Proposal_Review_Completed__c = ds.Media_Legal_Proposal_Review_Completed__c;
            ins.Date_BRMS_Release_Created__c = ds.Date_BRMS_Release_is_Created__c;
            ins.Date_BRMS_Release_Approval__c = ds.Date_BRMS_Release_Approval__c;
            ins.Date_BRMS_Activity_Created__c = ds.Date_BRMS_Activity_Created__c;
            ins.Date_BRMS_Activity_Approval__c = ds.Date_BRMS_Activity_Approval__c;
            ins.Consent_Transfer_Applicability_Review_By__c = ds.Consent_Transfer_Applicability_Review__c;
            ins.Receive_First_Installment_T_Cs_By__c = ds.Receive_First_Installment_T_Cs__c;
            ins.Complete_Media_T_Cs_Analysis_By__c = ds.Complete_Media_T_Cs_Analysis_By__c;
            ins.Complete_BD_Legal_Proposal_Review_By__c = ds.Complete_BD_Legal_Proposal_Review_By__c;
            ins.Complete_Media_Legal_Proposal_Review_By__c = ds.Media_Legal_Proposal_Review_By__c;
            ins.Create_BRMS_Release_By__c = ds.Create_BRMS_Release_By0__c;
            ins.Approve_BRMS_Release_By__c = ds.Approve_BRMS_Release_By__c;
            ins.Create_BRMS_Activity_By__c = ds.Create_BRMS_Activity_By0__c;
            ins.Approve_BRMS_Activity_By__c = ds.Approve_BRMS_Activity_By__c;
            instList.add(ins);
        }
        if (!instList.isEmpty()) {
            update instList;
        }
    }
    
    /****************************method to update deal fields on basis of company & Family*******************************/
    public void CompanyFamilyToDeal(list<Deals__c> triggerNew)
    {
     //   system.debug('entering company part');
      //  Deals__c mydeal = triggerNew[0];
      //  Deals__c de = [select id from deals__c where id =: mydeal.Id];
      //  system.debug('mydeal>>>>'+mydeal);
        list<deals__c> dealList = [select id,Governing_Law__c,Legal_Selling_Entity1__c,Legal_Selling_Entity_2__c,Market_Share_1__c,
                                   Market_Share_2__c,Agency_Cycle__c,Asset_Class__c,Product_Type1__c,Product_Type__c,region__c,
                                   Company_Name__r.governing_law__c,Company_Name__r.legal_selling_entity_1__c,Product_Family_New__r.Name,
                                  Company_Name__r.legal_selling_entity_2__c,Product_Family_New__r.agency_cycle__c,Start_Date__c,End_Date__c,
                                  Product_Family_New__r.asset_class__c,Product_Family_New__r.product_type__c,of_Installments_to_be_Created__c,
                                  Product_Family_New__r.paper_type__c,Product_Family_New__r.region__c from deals__c where id IN: triggerNew];
       // system.debug('dealList-->'+dealList);
       
        Map<Integer,String> monthNameMap=new Map<Integer, String>{1 =>'Jan', 2=>'Feb', 3=>'Mar', 4=>'Apr', 5=>'May',                         
    			6=>'Jun', 7=>'Jul', 8=>'Aug', 9=>'Sep',10=>'Oct', 11=>'Nov', 12=>'Dec'};            
        
        for(Deals__c de: dealList)
        {
            de.Governing_Law__c = de.Company_Name__r.governing_law__c;
            de.Legal_Selling_Entity1__c = de.Company_Name__r.legal_selling_entity_1__c;
            de.Legal_Selling_Entity_2__c = de.Company_Name__r.legal_selling_entity_2__c;
            de.Update_Deal_Name__c = true;
            if(de.Company_Name__r.legal_selling_entity_1__c != null && de.Company_Name__r.legal_selling_entity_2__c!= null)
            {
                de.Market_Share_1__c = 0.5;
                de.Market_Share_2__c = 0.5;
            }
            else
            {
                de.Market_Share_1__c = 1.0; 
                de.Market_Share_2__c = 0.0;
            }
            de.Agency_Cycle__c = de.Product_Family_New__r.agency_cycle__c;
            de.Asset_Class__c = de.Product_Family_New__r.asset_class__c;
            de.Product_Type1__c = de.Product_Family_New__r.product_type__c;
            de.Product_Type__c = de.Product_Family_New__r.paper_type__c;
            de.Region__c = de.Product_Family_New__r.region__c;
            if(de.Legal_Selling_Entity_2__c == null)
            {
                de.Market_Share_1__c = 1.0;
                de.Market_Share_2__c = 0.0;
            }
        }
       
        update dealList;
    }
}