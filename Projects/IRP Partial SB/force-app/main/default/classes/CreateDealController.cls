/*
###########################################################################
# Created by............: Chander Chauhan
# Created Date..........: 7th November 2014
# Description...........: This class maps fields from Forecast__c object to Deals__c object.                     
###########################################################################*/
public  class CreateDealController
{
   private id accountId{get;set;} 
   public Forecast__c  b{get;set;}
  
    public CreateDealController (ApexPages.StandardController controller) 
    {
    accountId = ApexPages.currentPage().getParameters().get('id');
    }
    
    public  pageReference  deal()
    {
     Forecast__c  b = [Select id, Num_of_Accounts__c, of_Installments__c, Forecasted_Agency_Cycle__c , Forecasted_Asset_Class__c , Forecasted_Avg_Age__c ,
                       Company_Name__c , End_Date__c , Exp_Market_Clearing_Price__c , Exp_Face_Value__c , Name , General_Notes__c , IRR__c , 
                       Product_Family_New__c , Product_Type__c , Start_Date__c, OwnerId  from Forecast__c  
                       where    id=:accountId];
     Deals__c   a = new Deals__c();
     { 
        a.OwnerId=b.OwnerId;
        a.Exp_of_Accts__c=b.Num_of_Accounts__c;
        a.Total_Installments__c=b.of_Installments__c;
        a.Agency_Cycle__c=b.Forecasted_Agency_Cycle__c;
        a.Asset_Class__c=b.Forecasted_Asset_Class__c;
        a.Avg_Age_at_Valuation__c=b.Forecasted_Avg_Age__c;
        a.Company_Name__c=b.Company_Name__c;
        a.Purchase_End_Date__c=b.End_Date__c;
        
        if(b.Exp_Market_Clearing_Price__c==null)
        {
        a.Exp_Mkt_Clearing_Price__c=0;
        }
        else
        {
        a.Exp_Mkt_Clearing_Price__c=b.Exp_Market_Clearing_Price__c;
        }
        
        a.Exp_Face_Value__c=b.Exp_Face_Value__c;
        a.Name=b.Name;
        a.General_Notes__c=b.General_Notes__c;
        a.Bid_IRR__c=b.IRR__c;
        a.Product_Family_New__c=b.Product_Family_New__c;
        a.Product_Type__c=b.Product_Type__c;
        a.Purchase_Start_Date__c=b.Start_Date__c;  
      }
      insert a;
      PageReference pageRef = new PageReference('/'+a.id);
      pageRef.setRedirect(true);
      return pageRef;
    }
}