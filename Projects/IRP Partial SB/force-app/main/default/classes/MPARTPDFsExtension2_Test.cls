@isTest
public class MPARTPDFsExtension2_Test {

    @isTest
    static void testShowSellers(){
        
        List<Account> aList = new List<Account>();
        Account a1 = new Account(Name='TestClass1');
        Account a2 = new Account(Name='TestClass2');
        Account a3 = new Account(Name='TestClass3');
        Account a4 = new Account(Name='TestClass4');
        aList.add(a1);
        aList.add(a2);
        aList.add(a3);
        aList.add(a4);
        insert aList;
        
        List<Media_Performance_Relationship_Tracker__c> mList = new List<Media_Performance_Relationship_Tracker__c>();
        Media_Performance_Relationship_Tracker__c mpart1 = new Media_Performance_Relationship_Tracker__c();
            mpart1.Company__c=a1.Id;
            mpart1.Name='TestClass1';
            mpart1.Quarter__c='H1';
            mpart1.Year__c='2019';
            mpart1.Up_Front_Media_Coverage_Score__c=1;
            mpart1.Ordered_Media_Delivery_Score__c=1;
            mpart1.Missing_Media_Reconciliation_Score__c=1;
            mpart1.Media_Packaging_Score__c=1;
            mpart1.Media_Packaging_Score__c=1;
            mpart1.Delivery_Technology_Score__c=1;
            mpart1.Synergy_Solutions_Score__c=1;
            mpart1.Engagement_Communications_Score__c=1;
            mpart1.Weekly_Fulfillment_Score__c=1;
            mpart1.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart1);
        Media_Performance_Relationship_Tracker__c mpart2 = new Media_Performance_Relationship_Tracker__c();
            mpart2.Company__c=a2.Id;
            mpart2.Name='TestClass2';
            mpart2.Quarter__c='H1';
            mpart2.Year__c='2019';
            mpart2.Up_Front_Media_Coverage_Score__c=1;
            mpart2.Ordered_Media_Delivery_Score__c=1;
            mpart2.Missing_Media_Reconciliation_Score__c=1;
            mpart2.Media_Packaging_Score__c=1;
            mpart2.Media_Packaging_Score__c=1;
            mpart2.Delivery_Technology_Score__c=1;
            mpart2.Synergy_Solutions_Score__c=1;
            mpart2.Engagement_Communications_Score__c=1;
            mpart2.Weekly_Fulfillment_Score__c=1;
            mpart2.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart2);
        Media_Performance_Relationship_Tracker__c mpart3 = new Media_Performance_Relationship_Tracker__c();
            mpart3.Company__c=a3.Id;
            mpart3.Name='TestClass3';
            mpart3.Quarter__c='H1';
            mpart3.Year__c='2019';
            mpart3.Up_Front_Media_Coverage_Score__c=1;
            mpart3.Ordered_Media_Delivery_Score__c=1;
            mpart3.Missing_Media_Reconciliation_Score__c=1;
            mpart3.Media_Packaging_Score__c=1;
            mpart3.Media_Packaging_Score__c=1;
            mpart3.Delivery_Technology_Score__c=1;
            mpart3.Synergy_Solutions_Score__c=1;
            mpart3.Engagement_Communications_Score__c=1;
            mpart3.Weekly_Fulfillment_Score__c=1;
            mpart3.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart3);
        Media_Performance_Relationship_Tracker__c mpart4 = new Media_Performance_Relationship_Tracker__c();
            mpart4.Company__c=a4.Id;
            mpart4.Name='TestClass4';
            mpart4.Quarter__c='H1';
            mpart4.Year__c='2019';
            mpart4.Up_Front_Media_Coverage_Score__c=1;
            mpart4.Ordered_Media_Delivery_Score__c=1;
            mpart4.Missing_Media_Reconciliation_Score__c=1;
            mpart4.Media_Packaging_Score__c=1;
            mpart4.Media_Packaging_Score__c=1;
            mpart4.Delivery_Technology_Score__c=1;
            mpart4.Synergy_Solutions_Score__c=1;
            mpart4.Engagement_Communications_Score__c=1;
            mpart4.Weekly_Fulfillment_Score__c=1;
            mpart4.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart4);
        insert mList;
        
        Test.StartTest();
        
            //Assign page and parameters
            PageReference testPage = Page.MPART_PDF_Seller2;
            testPage.getParameters().put('id',mpart1.Id);
            testPage.getParameters().put('showSellersString','true');
            testPage.getParameters().put('numberSellers','2');
            testPage.getParameters().put('includeNYOCA','true');
            testPage.getParameters().put('includeBreakdown','true');
            testPage.getParameters().put('id1',mpart2.id);
            testPage.getParameters().put('id2',mpart3.id);
            testPage.getParameters().put('id3','none');
            Test.setCurrentPage(testPage);
        
            //Instantiate the standard controller
            ApexPages.StandardController sc = new ApexPages.StandardController(mpart1);
            
            //Instantiate the extension
            MPARTPDFsExtension2 ext = new MPARTPDFsExtension2(sc);
        
            ext.getSellers();
            String Seller1Name = ext.Seller1Name;
            String Seller2Name = ext.Seller2Name;
            String Seller3Name = ext.Seller3Name;
            
            system.debug('>>>Test SELLER1NAME: Expected=TestClass2 Actual='+Seller1Name);
            system.debug('>>>Test SELLER2NAME: Expected=TestClass3 Actual='+Seller2Name);
            system.debug('>>>Test SELLER3NAME: Expected=null Actual='+Seller3Name);           
            system.assertEquals('TestClass2',Seller1Name);
            system.assertEquals('TestClass3',Seller2Name);
            system.assertEquals(null,Seller3Name);
            
        Test.StopTest();
    }
    
    @isTest
    static void testHideSellers(){
        
        List<Account> aList = new List<Account>();
        Account a1 = new Account(Name='TestClass1');
        Account a2 = new Account(Name='TestClass2');
        Account a3 = new Account(Name='TestClass3');
        Account a4 = new Account(Name='TestClass4');
        aList.add(a1);
        aList.add(a2);
        aList.add(a3);
        aList.add(a4);
        insert aList;
        
        List<Media_Performance_Relationship_Tracker__c> mList = new List<Media_Performance_Relationship_Tracker__c>();
        Media_Performance_Relationship_Tracker__c mpart1 = new Media_Performance_Relationship_Tracker__c();
            mpart1.Company__c=a1.Id;
            mpart1.Name='TestClass1';
            mpart1.Quarter__c='H1';
            mpart1.Year__c='2019';
            mpart1.Up_Front_Media_Coverage_Score__c=1;
            mpart1.Ordered_Media_Delivery_Score__c=1;
            mpart1.Missing_Media_Reconciliation_Score__c=1;
            mpart1.Media_Packaging_Score__c=1;
            mpart1.Media_Packaging_Score__c=1;
            mpart1.Delivery_Technology_Score__c=1;
            mpart1.Synergy_Solutions_Score__c=1;
            mpart1.Engagement_Communications_Score__c=1;
            mpart1.Weekly_Fulfillment_Score__c=1;
            mpart1.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart1);
        Media_Performance_Relationship_Tracker__c mpart2 = new Media_Performance_Relationship_Tracker__c();
            mpart2.Company__c=a2.Id;
            mpart2.Name='TestClass2';
            mpart2.Quarter__c='H1';
            mpart2.Year__c='2019';
            mpart2.Up_Front_Media_Coverage_Score__c=1;
            mpart2.Ordered_Media_Delivery_Score__c=1;
            mpart2.Missing_Media_Reconciliation_Score__c=1;
            mpart2.Media_Packaging_Score__c=1;
            mpart2.Media_Packaging_Score__c=1;
            mpart2.Delivery_Technology_Score__c=1;
            mpart2.Synergy_Solutions_Score__c=1;
            mpart2.Engagement_Communications_Score__c=1;
            mpart2.Weekly_Fulfillment_Score__c=1;
            mpart2.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart2);
        Media_Performance_Relationship_Tracker__c mpart3 = new Media_Performance_Relationship_Tracker__c();
            mpart3.Company__c=a3.Id;
            mpart3.Name='TestClass3';
            mpart3.Quarter__c='H1';
            mpart3.Year__c='2019';
            mpart3.Up_Front_Media_Coverage_Score__c=1;
            mpart3.Ordered_Media_Delivery_Score__c=1;
            mpart3.Missing_Media_Reconciliation_Score__c=1;
            mpart3.Media_Packaging_Score__c=1;
            mpart3.Media_Packaging_Score__c=1;
            mpart3.Delivery_Technology_Score__c=1;
            mpart3.Synergy_Solutions_Score__c=1;
            mpart3.Engagement_Communications_Score__c=1;
            mpart3.Weekly_Fulfillment_Score__c=1;
            mpart3.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart3);
        Media_Performance_Relationship_Tracker__c mpart4 = new Media_Performance_Relationship_Tracker__c();
            mpart4.Company__c=a4.Id;
            mpart4.Name='TestClass4';
            mpart4.Quarter__c='H1';
            mpart4.Year__c='2019';
            mpart4.Up_Front_Media_Coverage_Score__c=1;
            mpart4.Ordered_Media_Delivery_Score__c=1;
            mpart4.Missing_Media_Reconciliation_Score__c=1;
            mpart4.Media_Packaging_Score__c=1;
            mpart4.Media_Packaging_Score__c=1;
            mpart4.Delivery_Technology_Score__c=1;
            mpart4.Synergy_Solutions_Score__c=1;
            mpart4.Engagement_Communications_Score__c=1;
            mpart4.Weekly_Fulfillment_Score__c=1;
            mpart4.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart4);
        insert mList;
        
        Test.StartTest();
        
        //Assign page and parameters
        PageReference testPage = Page.MPART_PDF_Seller2;
        testPage.getParameters().put('id',mList[0].Id);
        testPage.getParameters().put('showSellersString','false');
        testPage.getParameters().put('numberSellers','3');
        testPage.getParameters().put('includeNYOCA','false');
        testPage.getParameters().put('includeBreakdown','false');
        testPage.getParameters().put('id1',mList[1].Id);
        testPage.getParameters().put('id2',mList[2].Id);
        testPage.getParameters().put('id3',mList[3].Id);
        Test.setCurrentPage(testPage);
          
        //Instantiate the standard controller
        ApexPages.StandardController sc = new ApexPages.StandardController(mList[0]);
        
        //Instantiate the extension
        MPARTPDFsExtension2 ext = new MPARTPDFsExtension2(sc);
        
        ext.getSellers();
        String Seller1Name = ext.Seller1Name;
        String Seller2Name = ext.Seller2Name;
        String Seller3Name = ext.Seller3Name;
        system.debug('>>>Test SELLER1NAME: Expected=Seller B Actual='+Seller1Name);
        system.debug('>>>Test SELLER2NAME: Expected=Seller C Actual='+Seller2Name);
        system.debug('>>>Test SELLER3NAME: Expected=Seller D Actual='+Seller3Name);           
        system.assertEquals('Seller B',Seller1Name);
        system.assertEquals('Seller C',Seller2Name);
        system.assertEquals('Seller D',Seller3Name);
        
        Test.StopTest();
     
    }
}