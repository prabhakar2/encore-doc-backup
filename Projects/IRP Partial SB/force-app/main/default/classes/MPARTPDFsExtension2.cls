public class MPARTPDFsExtension2 {

    public ApexPages.StandardController controller;
    public string showSellersString {get;set;}
    public Media_Performance_Relationship_Tracker__c MPART;
    public List<Media_Performance_Relationship_Tracker__c> MPARTList {get;set;}
    public Media_Performance_Relationship_Tracker__c Seller1 {get;set;}
    public Media_Performance_Relationship_Tracker__c Seller2 {get;set;}
    public Media_Performance_Relationship_Tracker__c Seller3 {get;set;}
    public string Seller1Name {get;set;}
    public string Seller2Name {get;set;}
    public string Seller3Name {get;set;}
    public string id1;
    public string id2;
    public string id3;

    public MPARTPDFSExtension2(ApexPages.StandardController stdcontroller){
        this.controller=stdcontroller;
        this.MPART = (Media_Performance_Relationship_Tracker__c)stdcontroller.getRecord();
        showSellersString = ApexPages.currentPage().getParameters().get('showSellersString');
        id1 = ApexPages.currentPage().getParameters().get('id1');
        id2 = ApexPages.currentPage().getParameters().get('id2');
        id3 = ApexPages.currentPage().getParameters().get('id3');
        
        MPARTList = [SELECT Id, Name, Company__r.Name, Overall_Media_Delivery_Score__c, Up_Front_Media_Coverage_Score__c,
                     Ordered_Media_Delivery_Score__c, Overall_Media_Processes_Score__c, Missing_Media_Reconciliation_Score__c,
                     Media_Packaging_Score__c, Delivery_Technology_Score__c, Overall_Media_Partnership_Score__c, 
                     Synergy_Solutions_Score__c, Engagement_Communications_Score__c, Overall_NY_OCA_Production_Score__c,
                     Weekly_Fulfillment_Score__c, Affidavit_Best_Practices_Score__c, Overall_Score__c
                     FROM Media_Performance_Relationship_Tracker__c
                     WHERE id!=:this.MPART.id AND (id=:id1 OR id=:id2 OR id=:id3)];
                     system.debug('>>>MPARTList size: '+MPARTList.size());
        getSellers();
    }
    
    public void getSellers(){
        Integer i = 1;
        for(Media_Performance_Relationship_Tracker__c mpart : MPARTList){
            if(i==1){
                Seller1 = mpart;
                if(this.showSellersString=='true'){
                    Seller1Name = mpart.Company__r.Name;
                }else{
                    Seller1Name = 'Seller B';
                }
                system.debug('>>>Seller1Name: '+Seller1Name);
            }else if(i==2){
                Seller2 = mpart;
                if(this.showSellersString=='true'){
                    Seller2Name = mpart.Company__r.Name;
                }else{
                    Seller2Name = 'Seller C';
                }
 				system.debug('>>>Seller2Name: '+Seller2Name);
            }else if(i==3){
                Seller3 = mpart;
                if(this.showSellersString=='true'){
                    Seller3Name = mpart.Company__r.Name;
                }else{
                    Seller3Name = 'Seller D';
                }
                system.debug('>>>Seller3Name: '+Seller3Name);
            }
            i++;
          
        }
        system.debug('>>>MPARTList: '+MPARTList);

    }
   
    public void queryMPARTs(){
            
    }
}