/*****************************************************
* @ Class Name      : 	DisbursementController_Test
* @ Description 	: 	Test Class for DisbursementController
* @ Created By		:   Shivangi Srivastava  
* @	CreatedDate	    :   08-Oct-2020
*****************************************************/

@isTest
public class DisbursementController_Test {
    @isTest 
    static void testMethod1(){
       Payments__c pay =new Payments__c();
        pay.RecordTypeId = Schema.SObjectType.Payments__c.getRecordTypeInfosByName().get('Payment 20201101').getRecordTypeId();
        pay.Payment_Type__c = 'Check';
        insert pay;
        
        Disbursement__c disb = new Disbursement__c();
        disb.Payment__c = pay.Id;
        insert disb;
        
        apexpages.currentpage().getparameters().put('id',pay.id);
        DisbursementController disbCntrlr = new DisbursementController();
        disbCntrlr.saveDisbursement();
        
       /* test.startTest();
        DisbursementController  disbCntrlr = new DisbursementController();
		test.stopTest();*/
        
    }

}