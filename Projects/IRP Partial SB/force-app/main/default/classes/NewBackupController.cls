/*
	* @ Class Name 		    : 	    NewBackupController
 	* @ Description 	    : 	    Controller for NewBackup VF Page.
 	* @ Created By 		    : 	    Prabhakar Joshi
    * @ Created Date	    : 	    16-Oct-2020 
*/
public with sharing class NewBackupController {
    /** To get and  hold the current payment recordId from URL. */
    public String paymentId{
        get;
        set;
    }
    /** To hold the new backup record. */
    public Backup__c bkObj{
        get;
        set;   
    }
    /** To hold the name of selected recordType. */
    public String rtName{
        get;
        set;
    }

    public Boolean showRecordPage{
        get;
        set;
    }

    /** Constructor */
    public NewBackupController() {
        showRecordPage = false;
        paymentId = ApexPages.currentPage().getParameters().get('id');
        
        Schema.RecordTypeInfo defaultRTInfo = this.getBackupDefaultRTInfo();

        if(defaultRTInfo != NULL){
            bkObj = new Backup__c();
            bkObj.Payment__c = paymentId;
            bkObj.RecordTypeId = defaultRTInfo.getRecordTypeId();
            rtName = defaultRTInfo.getName();
        }else{
            ApexPages.addMessage(new Apexpages.Message(ApexPages.severity.ERROR,'Internal RecordType Issue. Contact to System Administrator.'));
        }
    }/** End of Constructor */

    

    public List<RecordType> getRecordTypeInfo(){
        return [SELECT Id,Name,Description FROM RecordType WHERE SobjectType = 'Backup__c'];
    }

    public void continueAfterRTSelect(){
        
        String query = 'SELECT '+this.getPaymentFieldsForQuery()+' FROM Payments__c WHERE Id =:paymentId LIMIT 1';
        List<Payments__c> currentPayList = Database.query(query);

        bkObj.Company__c = currentPayList[0].Company_Name__c;
        bkObj.Has_Apollo_Accounts__c = currentPayList[0].Has_Apollo_Accounts__c;
        bkObj.Entity__c = currentPayList[0].Entity__c;
        bkObj.Few_or_All_Accounts_Not_Loaded_at_Start__c = currentPayList[0].Few_Accounts_Not_Loaded_at_Start__c;
        bkObj.Project_Reminder_Date__c = currentPayList[0].Project_Reminder_Date__c;
        if(rtName == 'Bal Adj'){
            bkObj.Balance_Adjustment_Applied_Date__c = currentPayList[0].Balance_Adjustment_Applied_Date__c;
        }
        if(rtName == 'Direct Pay'){
            bkObj.Direct_Pay_Misc_Writeoff_Amount__c = currentPayList[0].Direct_Pay_Misc_Writeoff_Amount__c;
        }
        if(rtName == 'Recall'){
            bkObj.Recall_Applied_Date__c = currentPayList[0].Recall_Applied_Date__c;
            bkObj.Putback_Applied_Date__c = currentPayList[0].Putback_Applied_Date__c;
            bkObj.Recall_Misc_Writeoff_Amount__c = currentPayList[0].Recall_Misc_Writeoff_Amount__c;
            bkObj.Putback_Misc_Writeoff_Amount__c = currentPayList[0].Putback_Misc_Writeoff_Amount__c;
        }
        showRecordPage = true;
    }

    /** Method to set the recordType Name from the recordTypeId. */
    /** Calling from recordTypeId inputField on the page. */
    public void handleRTChange(){
        rtName = Schema.SObjectType.Backup__c.getRecordTypeInfosById().get(bkObj.RecordTypeId).getName();
    }   

    public Pagereference saveNew(){
        try{
            String url = '/'+ApexPages.currentPage().getUrl().subStringAfter('/').subStringBefore('?')+'?id='+paymentId;
            insert bkObj;
            return new Pagereference(url).setRedirect(true);
        }catch(Exception e){
            Apexpages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage()));
            return NULL;
        }
    }

    /** Method definition to save the new Backup record. */
    /** Calling from save button on the page. */
    public Pagereference saveRecord(){
        try{
            insert bkObj;
            return new Pagereference('/'+paymentId);
        }catch(Exception e){
            Apexpages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage()));
            return NULL;
        }
    }/** End of 'saveRecord' method */

    
    private String getPaymentFieldsForQuery(){
        String fields = 'Id,Name,Company_Name__c,Has_Apollo_Accounts__c,Entity__c,Few_Accounts_Not_Loaded_at_Start__c,Project_Reminder_Date__c';
        if(rtName == 'Bal Adj'){
            fields += ',Balance_Adjustment_Applied_Date__c';
        }else if(rtName == 'Direct Pay'){
            fields += ',Direct_Pay_Misc_Writeoff_Amount__c';
        }else if(rtName == 'Recall'){
            fields += ',Recall_Applied_Date__c,Putback_Applied_Date__c,Recall_Misc_Writeoff_Amount__c,Putback_Misc_Writeoff_Amount__c';
        }
        return fields;
    }

    /** Method definition to get the recordType Info of default recordType of Backup__c Object. */
    /** Calling from Constructor. */
    private Schema.RecordTypeInfo getBackupDefaultRTInfo(){
        Schema.RecordTypeInfo defaultRT ;
        for(Schema.RecordTypeInfo rtInfo : Backup__c.SobjectType.getDescribe().getRecordTypeInfos()){
            if(rtInfo.isDefaultRecordTypeMapping()){
                defaultRT = rtInfo;
                break;
            }
        }
        return defaultRT;
    }/** End of 'getBackupDefaultRTInfo' method. */
}/** End of main class */
