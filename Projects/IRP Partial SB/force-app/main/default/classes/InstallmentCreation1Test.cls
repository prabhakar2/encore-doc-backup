@isTest
public class InstallmentCreation1Test 
{
	@isTest
    public static void test()
    {
        Trigger_Setting__c ts = new Trigger_Setting__c();
        ts.Name = 'DealTrigger';
        ts.Active__c = true;
        insert ts;
        
        Account acc = new Account();
        acc.name ='Test Account';
        acc.Governing_Law__c ='test law';
        acc.Legal_Selling_Entity_1__c='test legal 1';
        acc.Legal_Selling_Entity_2__c ='test legal 2';
        insert acc;
        
        Product_Family__c pf1 = new Product_Family__c();
        pf1.Company_Name__c = acc.Id;
        insert pf1;
        
        Product_Family__c pf2 = new Product_Family__c();
        pf2.Company_Name__c = acc.Id;
        pf2.Parent_Product_Family__c = pf1.Id;
        pf2.Asset_Class__c = 'Credit Card';
        pf2.Paper_Type__c = 'CC01';
        pf2.Agency_Cycle__c = 'All';
        pf2.Product_Type__c = 'Core';
        pf2.Region__c = 'US';
        insert pf2;
        
        Deals__c dl = new Deals__c();
        dl.Name = 'test';
        dl.Company_Name__c = acc.Id;
        dl.Product_Family_New__c = pf2.Id;
        dl.Avg_Age_at_Valuation__c = 1;
        dl.Bid_Status__c = 'Forecast';
        dl.Create_Installments__c = true;
        dl.Start_Date__c = date.parse('07/20/2020');
        dl.End_Date__c = date.parse('06/20/2021');
        dl.Exp_of_Accts__c = 1;
        dl.Exp_Mkt_Clearing_Price__c = 1;
        dl.Exp_Face_Value__c = 1;
        dl.Market_Share_1__c = 0.5;
        dl.Market_Share_2__c = 0.5;
        dl.Asset_Class__c ='Auto';
        dl.Broker_Fee__c = 1;
        dl.Includes_Media_Amt__c = true;
        insert dl;
        
        test.startTest();
        InstallmentCreation1.saveInstallment(dl.Id);
        test.stopTest();
    }
    
    @isTest
    public static void test1()
    {
        Trigger_Setting__c ts = new Trigger_Setting__c();
        ts.Name = 'DealTrigger';
        ts.Active__c = true;
        insert ts;
        
        Account acc = new Account();
        acc.name ='Test Account';
        acc.Governing_Law__c ='test law';
        acc.Legal_Selling_Entity_1__c='test legal 1';
        insert acc;
        
        Product_Family__c pf1 = new Product_Family__c();
        pf1.Company_Name__c = acc.Id;
        insert pf1;
        
        Product_Family__c pf2 = new Product_Family__c();
        pf2.Company_Name__c = acc.Id;
        pf2.Parent_Product_Family__c = pf1.Id;
        pf2.Asset_Class__c = 'Credit Card';
        pf2.Paper_Type__c = 'CC01';
        pf2.Agency_Cycle__c = 'All';
        pf2.Product_Type__c = 'Core';
        pf2.Region__c = 'US';
        insert pf2;
        
        Deals__c dl = new Deals__c();
        dl.Name = 'test';
        dl.Company_Name__c = acc.Id;
        dl.Product_Family_New__c = pf2.Id;
        dl.Avg_Age_at_Valuation__c = 1;
        dl.Bid_Status__c = 'Forecast';
        dl.Create_Installments__c = true;
        dl.Start_Date__c = date.parse('07/20/2020');
        dl.End_Date__c = date.parse('06/20/2021');
        dl.Exp_of_Accts__c = 1;
        dl.Exp_Mkt_Clearing_Price__c = 1;
        dl.Exp_Face_Value__c = 1;
        dl.Market_Share_1__c = 0.5;
        dl.Market_Share_2__c = 0.5;
        dl.Asset_Class__c ='Auto';
        dl.Broker_Fee__c = 1;
        dl.Includes_Media_Amt__c = true;
        insert dl;
       
        test.startTest();
        InstallmentCreation1.saveInstallment(dl.Id);
        test.stopTest();
    }
    
    @isTest
    public static void test2()
    {
        Trigger_Setting__c ts = new Trigger_Setting__c();
        ts.Name = 'DealTrigger';
        ts.Active__c = true;
        insert ts;
        
        Account acc = new Account();
        acc.name ='Test Account';
        acc.Governing_Law__c ='test law';
        acc.Legal_Selling_Entity_1__c='test legal 1';
        insert acc;
        
        Product_Family__c pf1 = new Product_Family__c();
        pf1.Company_Name__c = acc.Id;
        insert pf1;
        
        Product_Family__c pf2 = new Product_Family__c();
        pf2.Company_Name__c = acc.Id;
        pf2.Parent_Product_Family__c = pf1.Id;
        pf2.Asset_Class__c = 'Credit Card';
        pf2.Paper_Type__c = 'CC01';
        pf2.Agency_Cycle__c = 'All';
        pf2.Product_Type__c = 'Core';
        pf2.Region__c = 'US';
        insert pf2;
        
        Deals__c dl = new Deals__c();
        dl.Name = 'test';
        dl.Company_Name__c = acc.Id;
        dl.Product_Family_New__c = pf2.Id;
        dl.Avg_Age_at_Valuation__c = 1;
        dl.Bid_Status__c = 'Forecast';
        dl.Create_Installments__c = true;
        dl.Start_Date__c = date.parse('07/20/2020');
        dl.End_Date__c = date.parse('06/20/2021');
        dl.Exp_of_Accts__c = 1;
        dl.Exp_Mkt_Clearing_Price__c = 1;
        dl.Exp_Face_Value__c = 1;
        dl.Market_Share_1__c = 0.5;
        dl.Market_Share_2__c = 0.5;
        dl.Asset_Class__c ='Auto';
        dl.Broker_Fee__c = 1;
        dl.Includes_Media_Amt__c = true;
        insert dl;
        
        Installment__c ins = new Installment__c();
        ins.Deal_Name__c = dl.Id;
        ins.Face_Value__c = 212121;
        ins.Includes_Media_Amt__c = false;
        ins.of_Accts__c = 323232;
        ins.Avg_Age__c = 10;
        ins.Est_Close_Date__c = System.today().addDays(2);
        ins.Purchase_Rate__c = 5;
        insert ins;
        
        deals__c d1 =[select id,of_Installments__c,of_Months__c,of_Installments_to_be_Created__c,Legal_Selling_Entity_2__c,Bid_Status__c,
                       	Avg_Age_at_Valuation__c,Start_Date__c,Expected_Monthly_Face_Value__c,Expected_Monthly_of_Accounts__c,
                       	Market_Share_1__c,Market_Share_2__c,Asset_class__c,Broker_fee__c,Includes_Media_Amt__c,Legal_Selling_Entity1__c,
                       	Media_Cost__c from deals__c where id =: dl.Id];
       if(d1.of_Installments__c > 0 && (d1.Bid_Status__c != 'Bid Won' || d1.Bid_Status__c !='Bid Lost'))
       {test.startTest();
        InstallmentCreation1.saveInstallment(d1.Id);
        test.stopTest();}
    }
    
    @isTest
    public static void test3()
    {
        Trigger_Setting__c ts = new Trigger_Setting__c();
        ts.Name = 'DealTrigger';
        ts.Active__c = true;
        insert ts;
        
        Account acc = new Account();
        acc.name ='Test Account';
        acc.Governing_Law__c ='test law';
        acc.Legal_Selling_Entity_1__c='test legal 1';
        acc.Legal_Selling_Entity_2__c='test legal 2';
        insert acc;
        
        Product_Family__c pf1 = new Product_Family__c();
        pf1.Company_Name__c = acc.Id;
        insert pf1;
        
        Product_Family__c pf2 = new Product_Family__c();
        pf2.Company_Name__c = acc.Id;
        pf2.Parent_Product_Family__c = pf1.Id;
        pf2.Asset_Class__c = 'Credit Card';
        pf2.Paper_Type__c = 'CC01';
        pf2.Agency_Cycle__c = 'All';
        pf2.Product_Type__c = 'Core';
        pf2.Region__c = 'US';
        insert pf2;
        
        Deals__c dl = new Deals__c();
        dl.Name = 'test';
        dl.Company_Name__c = acc.Id;
        dl.Product_Family_New__c = pf2.Id;
        dl.Avg_Age_at_Valuation__c = 1;
        dl.Bid_Status__c = 'Forecast';
        dl.Create_Installments__c = true;
        dl.Start_Date__c = date.parse('07/20/2020');
        dl.End_Date__c = date.parse('06/20/2021');
        dl.Exp_of_Accts__c = 1;
        dl.Exp_Mkt_Clearing_Price__c = 1;
        dl.Exp_Face_Value__c = 1;
        dl.Market_Share_1__c = 0.5;
        dl.Market_Share_2__c = 0.5;
        dl.Asset_Class__c ='Auto';
        dl.Broker_Fee__c = 1;
        dl.Includes_Media_Amt__c = true;
        insert dl;
        
        Installment__c ins = new Installment__c();
        ins.Deal_Name__c = dl.Id;
        ins.Face_Value__c = 212121;
        ins.Includes_Media_Amt__c = false;
        ins.of_Accts__c = 323232;
        ins.Avg_Age__c = 10;
        ins.Est_Close_Date__c = System.today().addDays(2);
        ins.Purchase_Rate__c = 5;
        insert ins;
        
        deals__c d1 =[select id,of_Installments__c,of_Months__c,of_Installments_to_be_Created__c,Legal_Selling_Entity_2__c,Bid_Status__c,
                       	Avg_Age_at_Valuation__c,Start_Date__c,Expected_Monthly_Face_Value__c,Expected_Monthly_of_Accounts__c,
                       	Market_Share_1__c,Market_Share_2__c,Asset_class__c,Broker_fee__c,Includes_Media_Amt__c,Legal_Selling_Entity1__c,
                       	Media_Cost__c from deals__c where id =: dl.Id];
       if(d1.of_Installments__c > 0 && (d1.Bid_Status__c != 'Bid Won' || d1.Bid_Status__c !='Bid Lost'))
       {test.startTest();
        InstallmentCreation1.saveInstallment(d1.Id);
        test.stopTest();}
    }
}