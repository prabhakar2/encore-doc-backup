@isTest
public class CaseTriggerHelper_Test{
    
    //Positive Testing
    public static testMethod void unitTestPositive(){
        Test.startTest();
        Case caseObj1 = new Case();
        caseObj1.Description = 'Media Request Type: Account inquiry\n';
        caseObj1.Description += 'MCM Account #: 85632156\n';
        caseObj1.Description += 'Original Account #: 5465313153483455312\n';
        caseObj1.Description += 'Consumer Name: Shelly Ludwig\n';
        caseObj1.Description += 'Company Purchased From: Amazon\n';
        //caseObj1.Description += 'Document Type: Statements\n';
        caseObj1.Description += 'Document Date: 8/3/2017\n';
        caseObj1.Description += 'Other: Test\n';
        caseObj1.Origin = 'Email';

        insert caseObj1;
        
        caseObj1 = [SELECT Customer_Name__c FROM Case WHERE id =: caseObj1.id];
        system.assertEquals('Shelly Ludwig',caseObj1.Customer_Name__c);
        Test.stopTest();
    }
    
    //Negative Testing Record
    public static testMethod void unitTestNegative(){
        Test.startTest();
        
        //Positive Record
        Case caseObj1 = new Case();
        caseObj1.Description = 'Media Request Type: Account inquiry\n';
        caseObj1.Description += 'MCM Account #: 85632156\n';
        caseObj1.Description += 'Original Account #: 5465313153483455312\n';
        caseObj1.Description += 'Consumer Name: Shelly Ludwig\n';
        caseObj1.Description += 'Company Purchased From: Amazon\n';
        //caseObj1.Description += 'Document Type: Statement\n';
        caseObj1.Description += 'Document Date: 8/3/2017\n';
        caseObj1.Description += 'Other: Test\n';
        caseObj1.Origin = 'Email';
        
        //Negative Record
        Case caseObj2 = new Case();
        caseObj2.Description = 'Media Request Type: Account inquiry\n';
        caseObj2.Description += 'MCM Account #: 85632156\n';
        caseObj2.Description += 'Original Account #: 5465313153483455312\n';
        caseObj2.Description += 'Consumer Name: Shelly Ludwig\n';
        caseObj2.Description += 'Company Purchased From: Amazon\n';
        //caseObj2.Description += 'Document Type: Statements\n';
        caseObj2.Description += 'Document Date: \n';
        caseObj2.Description += 'Other: Test\n';
        caseObj2.Origin = 'Email';
        insert new List<Case>{CaseObj1,CaseObj2};
        
        caseObj1 = [SELECT Customer_Name__c FROM Case WHERE id =: caseObj1.id];
        system.assertEquals('Shelly Ludwig',caseObj1.Customer_Name__c);
        Test.stopTest();
    }
    
}