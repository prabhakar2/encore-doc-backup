public class CaseTriggerHelper{
    
    /*
    *This method updates the fields of case from the EmailToCase. We provide the template from where we get the details of fields to populate from Description field.
    */
    public static void UpdateEmailToCaseFields(List<Case> triggerList){
        Set<String> requestedDocumentPicklistValuesSet = new Set<String>();
        Schema.DescribeFieldResult requestedDocumentField = Schema.SObjectType.Case.fields.Media_Document_Type__c; 
        for(Schema.PicklistEntry picklistVal : requestedDocumentField.getPicklistValues()){
            requestedDocumentPicklistValuesSet.add(picklistVal.getValue());
        }
        Map<String,Map<String,String>> templateDetailsMap = new Map<String,Map<String,String>>();
        for(Case caseObj : triggerList){
            if(caseObj.Description == null) continue;
            for(String fieldDetails : caseObj.Description.split('\n')){
                Map<String,String> tempMap = templateDetailsMap.get(caseObj.id);
                if(tempMap == null) tempMap = new Map<String,String>();
                if(fieldDetails.containsIgnoreCase('Media Request Type:')) tempMap.put('Media_Request_Type__c',fieldDetails.substringAfter('Media Request Type: '));
                if(fieldDetails.containsIgnoreCase('MCM Account #:')) tempMap.put('MCM_Account_Number__c',fieldDetails.substringAfter('MCM Account #: '));
                if(fieldDetails.containsIgnoreCase('Original Account #:')) tempMap.put('Original_Account_Number__c',fieldDetails.substringAfter('Original Account #: '));
                if(fieldDetails.containsIgnoreCase('Consumer Name:')) tempMap.put('Customer_Name__c',fieldDetails.substringAfter('Consumer Name: '));
                if(fieldDetails.containsIgnoreCase('Company Purchased From:')) tempMap.put('Company_Purchased_from__c',fieldDetails.substringAfter('Company Purchased From: '));
                if(fieldDetails.containsIgnoreCase('Document Type:')) tempMap.put('Media_Document_Type__c',fieldDetails.substringAfter('Document Type: '));
                if(fieldDetails.containsIgnoreCase('Document Date:')) tempMap.put('Document_Date__c',fieldDetails.substringAfter('Document Date: '));
                templateDetailsMap.put(caseObj.id,tempMap);
            }
        }
        
        for(Case caseObj : triggerList){
            if(templateDetailsMap.get(caseObj.id) != null){
                Map<String,String> tempMap = templateDetailsMap.get(caseObj.id);
                for(String fieldAPIName : tempMap.keyset()){
                    if(!fieldAPIName.equalsIgnoreCase('Document_Date__c') && !fieldAPIName.equalsIgnoreCase('Media_Document_Type__c')){
                            caseObj.put(fieldAPIName,tempMap.get(fieldAPIName));
                    }else if(fieldAPIName.equalsIgnoreCase('Document_Date__c') && tempMap.get(fieldAPIName) != null 
                                && tempMap.get(fieldAPIName).trim() != '' && tempMap.get(fieldAPIName).trim().remove('/').isNumeric()){
                        caseObj.Document_Date__c = Date.parse(tempMap.get(fieldAPIName).trim());
                    }else if(fieldAPIName.equalsIgnoreCase('Media_Document_Type__c') && requestedDocumentPicklistValuesSet.contains(tempMap.get(fieldAPIName).trim())){
                        caseObj.put(fieldAPIName,tempMap.get(fieldAPIName).trim());
                    }
                }
            }
        }
    }
    
}