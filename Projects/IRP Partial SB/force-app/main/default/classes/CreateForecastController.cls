/*
###########################################################################
# Created by............: Donald Diaz
# Created Date..........: 16th May 2017
# Description...........: This class maps fields from Deals__c object to Forecast__c object.                     
###########################################################################*/
public  class CreateForecastController
{
   private id accountId{get;set;} 
   public Deals__c  b{get;set;}
  
    public CreateForecastController (ApexPages.StandardController controller) 
    {
    accountId = ApexPages.currentPage().getParameters().get('id');
    }
    
    public  pageReference  forecast()
    {
     Deals__c  b = [Select id, Winning_Bid_Rate__c, Bid_Status__c, Region__c, Actual_Avg_Bal__c, Actual_Face_Value__c, Purchase_Rate__c, Exp_of_Accts__c, Total_Installments__c, Agency_Cycle__c , Asset_Class__c , Avg_Age_at_Valuation__c , Company_Name__c , Purchase_End_Date__c , Exp_Mkt_Clearing_Price__c , Exp_Face_Value__c , Name , General_Notes__c , Bid_IRR__c , Product_Family_New__c , Product_Type__c , Purchase_Start_Date__c  from Deals__c  where    id=:accountId];
     Forecast__c   a = new Forecast__c();
     { 
        a.Avg_Bal__c=b.Actual_Avg_Bal__c;
        a.of_Installments__c=b.Total_Installments__c;
        a.Forecasted_Agency_Cycle__c=b.Agency_Cycle__c;
        a.Forecasted_Asset_Class__c=b.Asset_Class__c;
        a.Forecasted_Avg_Age__c=b.Avg_Age_at_Valuation__c;
        a.Company_Name__c=b.Company_Name__c;
        a.Region__c=b.Region__c;
        a.Stage__c='Qualified Lead';
        
        if(b.Bid_Status__c=='Bid Won')
        {
        a.Exp_Market_Clearing_Price__c=b.Purchase_Rate__c;
        }
        else if(b.Winning_Bid_Rate__c<>null && b.Winning_Bid_Rate__c<>0)
        {
        a.Exp_Market_Clearing_Price__c=b.Winning_Bid_Rate__c;
        }
        else
        {
        a.Exp_Market_Clearing_Price__c=b.Exp_Mkt_Clearing_Price__c;
        }
        
        a.Exp_Face_Value__c=b.Actual_Face_Value__c;
        a.Name=b.Name;
        a.General_Notes__c=b.General_Notes__c;
        a.IRR__c=b.Bid_IRR__c;
        a.Product_Family_New__c=b.Product_Family_New__c;
        a.Product_Type__c=b.Product_Type__c;  
      }
      insert a;
      PageReference pageRef = new PageReference('/'+a.id);
      pageRef.setRedirect(true);
      return pageRef;
    }
}