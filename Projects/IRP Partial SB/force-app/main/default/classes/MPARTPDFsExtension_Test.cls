@isTest
public class MPARTPDFsExtension_Test {

    @testSetup //test setup method - can't get it to work but leaving in code for potential to use in future
    static void testSetup(){
        
        List<Account> aList = new List<Account>();
        Account a1 = new Account(Name='TestClass1');
        Account a2 = new Account(Name='TestClass2');
        Account a3 = new Account(Name='TestClass3');
        Account a4 = new Account(Name='TestClass4');
        aList.add(a1);
        aList.add(a2);
        aList.add(a3);
        aList.add(a4);
        insert aList;
        
        List<Media_Performance_Relationship_Tracker__c> mList = new List<Media_Performance_Relationship_Tracker__c>();
        Media_Performance_Relationship_Tracker__c mpart1 = new Media_Performance_Relationship_Tracker__c();
            mpart1.Company__c=a1.Id;
            mpart1.Name='TestClass1';
            mpart1.Quarter__c='H1';
            mpart1.Year__c='2019';
            mpart1.Up_Front_Media_Coverage_Score__c=1;
            mpart1.Ordered_Media_Delivery_Score__c=1;
            mpart1.Missing_Media_Reconciliation_Score__c=1;
            mpart1.Media_Packaging_Score__c=1;
            mpart1.Media_Packaging_Score__c=1;
            mpart1.Delivery_Technology_Score__c=1;
            mpart1.Synergy_Solutions_Score__c=1;
            mpart1.Engagement_Communications_Score__c=1;
            mpart1.Weekly_Fulfillment_Score__c=1;
            mpart1.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart1);
        Media_Performance_Relationship_Tracker__c mpart2 = new Media_Performance_Relationship_Tracker__c();
            mpart2.Company__c=a2.Id;
            mpart2.Name='TestClass2';
            mpart2.Quarter__c='H1';
            mpart2.Year__c='2019';
            mpart2.Up_Front_Media_Coverage_Score__c=1;
            mpart2.Ordered_Media_Delivery_Score__c=1;
            mpart2.Missing_Media_Reconciliation_Score__c=1;
            mpart2.Media_Packaging_Score__c=1;
            mpart2.Media_Packaging_Score__c=1;
            mpart2.Delivery_Technology_Score__c=1;
            mpart2.Synergy_Solutions_Score__c=1;
            mpart2.Engagement_Communications_Score__c=1;
            mpart2.Weekly_Fulfillment_Score__c=1;
            mpart2.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart2);
        Media_Performance_Relationship_Tracker__c mpart3 = new Media_Performance_Relationship_Tracker__c();
            mpart3.Company__c=a3.Id;
            mpart3.Name='TestClass3';
            mpart3.Quarter__c='H1';
            mpart3.Year__c='2019';
            mpart3.Up_Front_Media_Coverage_Score__c=1;
            mpart3.Ordered_Media_Delivery_Score__c=1;
            mpart3.Missing_Media_Reconciliation_Score__c=1;
            mpart3.Media_Packaging_Score__c=1;
            mpart3.Media_Packaging_Score__c=1;
            mpart3.Delivery_Technology_Score__c=1;
            mpart3.Synergy_Solutions_Score__c=1;
            mpart3.Engagement_Communications_Score__c=1;
            mpart3.Weekly_Fulfillment_Score__c=1;
            mpart3.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart3);
        Media_Performance_Relationship_Tracker__c mpart4 = new Media_Performance_Relationship_Tracker__c();
            mpart4.Company__c=a4.Id;
            mpart4.Name='TestClass4';
            mpart4.Quarter__c='H1';
            mpart4.Year__c='2019';
            mpart4.Up_Front_Media_Coverage_Score__c=1;
            mpart4.Ordered_Media_Delivery_Score__c=1;
            mpart4.Missing_Media_Reconciliation_Score__c=1;
            mpart4.Media_Packaging_Score__c=1;
            mpart4.Media_Packaging_Score__c=1;
            mpart4.Delivery_Technology_Score__c=1;
            mpart4.Synergy_Solutions_Score__c=1;
            mpart4.Engagement_Communications_Score__c=1;
            mpart4.Weekly_Fulfillment_Score__c=1;
            mpart4.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart4);
        insert mList;      
    }
    
    //test positive scenario with 1 record selected for comparison and all other variables true
    static testMethod void testSubmit1(){
        
        List<Account> aList = new List<Account>();
        Account a1 = new Account(Name='TestClass1');
        Account a2 = new Account(Name='TestClass2');
        aList.add(a1);
        aList.add(a2);
        insert aList;
        
        List<Media_Performance_Relationship_Tracker__c> mList = new List<Media_Performance_Relationship_Tracker__c>();
        Media_Performance_Relationship_Tracker__c mpart1 = new Media_Performance_Relationship_Tracker__c();
            mpart1.Company__c=a1.Id;
            mpart1.Name='TestClass1';
            mpart1.Quarter__c='H1';
            mpart1.Year__c='2019';
            mpart1.Up_Front_Media_Coverage_Score__c=1;
            mpart1.Ordered_Media_Delivery_Score__c=1;
            mpart1.Missing_Media_Reconciliation_Score__c=1;
            mpart1.Media_Packaging_Score__c=1;
            mpart1.Media_Packaging_Score__c=1;
            mpart1.Delivery_Technology_Score__c=1;
            mpart1.Synergy_Solutions_Score__c=1;
            mpart1.Engagement_Communications_Score__c=1;
            mpart1.Weekly_Fulfillment_Score__c=1;
            mpart1.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart1);
        Media_Performance_Relationship_Tracker__c mpart2 = new Media_Performance_Relationship_Tracker__c();
            mpart2.Company__c=a2.Id;
            mpart2.Name='TestClass2';
            mpart2.Quarter__c='H1';
            mpart2.Year__c='2019';
            mpart2.Up_Front_Media_Coverage_Score__c=1;
            mpart2.Ordered_Media_Delivery_Score__c=1;
            mpart2.Missing_Media_Reconciliation_Score__c=1;
            mpart2.Media_Packaging_Score__c=1;
            mpart2.Media_Packaging_Score__c=1;
            mpart2.Delivery_Technology_Score__c=1;
            mpart2.Synergy_Solutions_Score__c=1;
            mpart2.Engagement_Communications_Score__c=1;
            mpart2.Weekly_Fulfillment_Score__c=1;
            mpart2.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart2);
        insert mList;  
             
        //Instantiate the standard controller
        ApexPages.StandardController sc = new ApexPages.StandardController(mpart1);
        
        //Instantiate the extension
        MPARTPDFsExtension ext = new MPARTPDFsExtension(sc);
        ext.MPART = mpart1;
        
        //select 1 record to compare against
        List<Id> testSelectIds = new List<Id>();
        testSelectIds.add(mpart2.Id);
        ext.selectedMPARTList = testSelectIds;
        
        //assign other variables
        ext.showSellers = true;
        ext.includeNYOCA = true;
        ext.includeBreakdown = true;
        
        test.StartTest();
            //execute test
            PageReference testPageResults = ext.Submit();
            //verify results
            system.assertEquals(mpart1.Id,testPageResults.getParameters().get('id'));
            system.assertEquals('true',testPageResults.getParameters().get('showSellersString'));
            system.assertEquals('1',testPageResults.getParameters().get('numberSellers'));
            system.assertEquals('true',testPageResults.getParameters().get('includeNYOCA'));
            system.assertEquals('true',testPageResults.getParameters().get('includeBreakdown'));
            system.assertEquals(mpart2.Id,testPageResults.getParameters().get('id1'));
            system.assertEquals('none',testPageResults.getParameters().get('id2'));
            system.assertEquals('none',testPageResults.getParameters().get('id3'));
        test.StopTest();
    }
    
    //test exception when select 0 records
    static testMethod void testSubmit0(){
        
        List<Account> aList = new List<Account>();
        Account a1 = new Account(Name='TestClass1');
        aList.add(a1);
        insert aList;
        
        List<Media_Performance_Relationship_Tracker__c> mList = new List<Media_Performance_Relationship_Tracker__c>();
        Media_Performance_Relationship_Tracker__c mpart1 = new Media_Performance_Relationship_Tracker__c();
            mpart1.Company__c=a1.Id;
            mpart1.Name='TestClass1';
            mpart1.Quarter__c='H1';
            mpart1.Year__c='2019';
            mpart1.Up_Front_Media_Coverage_Score__c=1;
            mpart1.Ordered_Media_Delivery_Score__c=1;
            mpart1.Missing_Media_Reconciliation_Score__c=1;
            mpart1.Media_Packaging_Score__c=1;
            mpart1.Media_Packaging_Score__c=1;
            mpart1.Delivery_Technology_Score__c=1;
            mpart1.Synergy_Solutions_Score__c=1;
            mpart1.Engagement_Communications_Score__c=1;
            mpart1.Weekly_Fulfillment_Score__c=1;
            mpart1.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart1);
        insert mList;      
        
        //Instantiate the standard controller
        ApexPages.StandardController sc = new ApexPages.StandardController(mpart1);
        
        //Instantiate the extension
        MPARTPDFsExtension ext = new MPARTPDFsExtension(sc);
        ext.MPART = mpart1;
        
        //select 0 records to compare against
        List<Id> testSelectIds = new List<Id>();
        ext.selectedMPARTList = testSelectIds;
        
        //assign remaining variables
        ext.showSellers = true;
        ext.includeNYOCA = true;
        ext.includeBreakdown = true;
        
        test.StartTest();
           //execute test
            try{
                PageReference testPageResults = ext.Submit(); 
            }
            //verify results
            catch(exception e){
                system.assert(e.getMessage().contains('Must select at least 1 seller to compare against.'));
            }
        test.StopTest();    
    }
    
    //test exception when user selects more than 3 records
    static testMethod void testSubmit4(){
        
        List<Account> aList = new List<Account>();
        Account a1 = new Account(Name='TestClass1');
        Account a2 = new Account(Name='TestClass2');
        Account a3 = new Account(Name='TestClass3');
        Account a4 = new Account(Name='TestClass4');
        aList.add(a1);
        aList.add(a2);
        aList.add(a3);
        aList.add(a4);
        insert aList;
        
        List<Media_Performance_Relationship_Tracker__c> mList = new List<Media_Performance_Relationship_Tracker__c>();
        Media_Performance_Relationship_Tracker__c mpart1 = new Media_Performance_Relationship_Tracker__c();
            mpart1.Company__c=a1.Id;
            mpart1.Name='TestClass1';
            mpart1.Quarter__c='H1';
            mpart1.Year__c='2019';
            mpart1.Up_Front_Media_Coverage_Score__c=1;
            mpart1.Ordered_Media_Delivery_Score__c=1;
            mpart1.Missing_Media_Reconciliation_Score__c=1;
            mpart1.Media_Packaging_Score__c=1;
            mpart1.Media_Packaging_Score__c=1;
            mpart1.Delivery_Technology_Score__c=1;
            mpart1.Synergy_Solutions_Score__c=1;
            mpart1.Engagement_Communications_Score__c=1;
            mpart1.Weekly_Fulfillment_Score__c=1;
            mpart1.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart1);
        Media_Performance_Relationship_Tracker__c mpart2 = new Media_Performance_Relationship_Tracker__c();
            mpart2.Company__c=a2.Id;
            mpart2.Name='TestClass2';
            mpart2.Quarter__c='H1';
            mpart2.Year__c='2019';
            mpart2.Up_Front_Media_Coverage_Score__c=1;
            mpart2.Ordered_Media_Delivery_Score__c=1;
            mpart2.Missing_Media_Reconciliation_Score__c=1;
            mpart2.Media_Packaging_Score__c=1;
            mpart2.Media_Packaging_Score__c=1;
            mpart2.Delivery_Technology_Score__c=1;
            mpart2.Synergy_Solutions_Score__c=1;
            mpart2.Engagement_Communications_Score__c=1;
            mpart2.Weekly_Fulfillment_Score__c=1;
            mpart2.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart2);
        Media_Performance_Relationship_Tracker__c mpart3 = new Media_Performance_Relationship_Tracker__c();
            mpart3.Company__c=a3.Id;
            mpart3.Name='TestClass3';
            mpart3.Quarter__c='H1';
            mpart3.Year__c='2019';
            mpart3.Up_Front_Media_Coverage_Score__c=1;
            mpart3.Ordered_Media_Delivery_Score__c=1;
            mpart3.Missing_Media_Reconciliation_Score__c=1;
            mpart3.Media_Packaging_Score__c=1;
            mpart3.Media_Packaging_Score__c=1;
            mpart3.Delivery_Technology_Score__c=1;
            mpart3.Synergy_Solutions_Score__c=1;
            mpart3.Engagement_Communications_Score__c=1;
            mpart3.Weekly_Fulfillment_Score__c=1;
            mpart3.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart3);
        Media_Performance_Relationship_Tracker__c mpart4 = new Media_Performance_Relationship_Tracker__c();
            mpart4.Company__c=a4.Id;
            mpart4.Name='TestClass4';
            mpart4.Quarter__c='H1';
            mpart4.Year__c='2019';
            mpart4.Up_Front_Media_Coverage_Score__c=1;
            mpart4.Ordered_Media_Delivery_Score__c=1;
            mpart4.Missing_Media_Reconciliation_Score__c=1;
            mpart4.Media_Packaging_Score__c=1;
            mpart4.Media_Packaging_Score__c=1;
            mpart4.Delivery_Technology_Score__c=1;
            mpart4.Synergy_Solutions_Score__c=1;
            mpart4.Engagement_Communications_Score__c=1;
            mpart4.Weekly_Fulfillment_Score__c=1;
            mpart4.Affidavit_Best_Practices_Score__c=1;
        mList.add(mpart4);
        insert mList;    
        
        //Instantiate the standard controller
        ApexPages.StandardController sc = new ApexPages.StandardController(mpart1);
        
        //Instantiate the extension
        MPARTPDFsExtension ext = new MPARTPDFsExtension(sc);
        ext.MPART = mpart1;
        
        //select 4 records to compare against
        List<Id> testSelectIds = new List<Id>();
        testSelectIds.add(mpart2.Id);
        testSelectIds.add(mpart3.Id);
        testSelectIds.add(mpart4.Id);
        testSelectIds.add(mpart1.Id);
        ext.selectedMPARTList = testSelectIds;
        
        //assign other variables
        ext.showSellers = true;
        ext.includeNYOCA = true;
        ext.includeBreakdown = true;
        
        test.StartTest();
            //execute test
            try{
                PageReference testPageResults = ext.Submit();
            }
            //verify results
            catch(exception e){
                system.assert(e.getMessage().contains('Please select 3 or fewer sellers to compare against.'));
            }
        test.StopTest();  
    }
}