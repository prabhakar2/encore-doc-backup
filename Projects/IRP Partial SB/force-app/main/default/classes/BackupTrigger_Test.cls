/*
* @ Class Name      : 	BackupTrigger_Test
* @ Description 	: 	Test Class for BackupTrigger
* @ Created By		:   Prabhakar Joshi   
* @	CreatedDate	    :   28-Sept-2020
*/

@isTest
public class BackupTrigger_Test {
    @testSetup
    static void setupMethod(){
        Trigger_Setting__c trgSetting = new Trigger_Setting__c();
        trgSetting.Name = 'BackupTrigger';
        trgSetting.Active__c = true;
        trgSetting.SObject_Type__c = 'Backup__c';
        insert trgSetting;
        
        Payments__c no_pay =new Payments__c();
        no_pay.RecordTypeId = Schema.SObjectType.Payments__c.getRecordTypeInfosByName().get('No Payment').getRecordTypeId();
        no_pay.Payment_Type__c = 'Check';
        insert no_pay;
        
        Payments__c pay =new Payments__c();
        pay.RecordTypeId = Schema.SObjectType.Payments__c.getRecordTypeInfosByName().get('Payments').getRecordTypeId();
        pay.Payment_Type__c = 'Check';
        insert pay;
        
        Backup__c bk = new Backup__c();
        bk.recordTypeId = Schema.SObjectType.Backup__c.getRecordTypeInfosByName().get('Recall').getRecordTypeId();
        bk.Payment__c = pay.Id;
        bk.No_Payment__c = false;
        insert bk;
        
    }
    
    @isTest
    static void test1(){
        List<Trigger_Setting__c> trgSetting = [SELECT Id,Name,Active__c,SObject_Type__c FROM Trigger_Setting__c LIMIT 1];
        
        List<Payments__c> payList = [SELECT Id,Name,recordTypeId,Payment_Type__c FROM Payments__c];
        List<Backup__c> bkList = [SELECT Id,Name,No_Payment__c,recordTypeId FROM Backup__c];
        System.assertEquals(1, bkList.size());
        bkList[0].No_Payment__c = true;
        update bkList;
    }
    
}