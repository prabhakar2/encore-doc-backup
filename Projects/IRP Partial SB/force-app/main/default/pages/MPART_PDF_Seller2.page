<apex:page standardController="Media_Performance_Relationship_Tracker__c" extensions="MPARTPDFsExtension2" renderAs="pdf"
           showHeader="false" applyHtmlTag="false" applyBodyTag="false">
<head>
   
    <style>
       @page {
            size: letter landscape;
            margin-left: 0.4cm;
            margin-right: 0.4cm;
            martin-bottom: 0.4cm;
            margin-top: 2.3cm;
            @top-center {
                content: element(header);
            }
        }
        table, th, td, tr {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
        }
        table, tr {
            page-break-inside: avoid;
        }
        div.header {
            position: running(header);
        }
    </style>
    
    <div class="header">
        <table>
            <tr style="font-weight: bold; vertical-align: middle;">
                <td style="border: 0px;">
                    <apex:image id="theImage" value="https://c.na89.visual.force.com/resource/1565737396000/MCMLogo" width="150"/>
                </td>
                <td style="border: 0px;">
                    <apex:outputtext value="Comparison Scorecard for {!Media_Performance_Relationship_Tracker__c.Name}" />
                </td>
            </tr>
        </table>
    </div>   

</head>
<body>

    <table style="height: 100%;">
        <thead>
           <tr>
               <th style="width: 125px; font-size: 16px;"><b>Category</b></th>
               <th style="width: 300px; font-size: 16px;"><b>Description</b></th>
               <th align="center" style="width: 75px; font-size: 16px; border-right: 3px solid black;"><b>% Weight</b></th>
               <th align="center" style="width: 75px; font-size: 16px; border-right: 1px dashed black;"><b>Score</b></th>
               <th align="center" style="width: 75px; font-size: 14px; display: {!IF($CurrentPage.parameters.numberSellers=='1'
                          ||$CurrentPage.parameters.numberSellers=='2'||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <b>{!Seller1Name}</b></th>
               <th align="center" style="width: 75px; font-size: 14px; display: {!IF($CurrentPage.parameters.numberSellers=='2'
                          ||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <b>{!Seller2Name}</b></th>
               <th align="center" style="width: 75px; font-size: 14px; display: {!IF($CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <b>{!Seller3Name}</b></th>           
           </tr>
        </thead>
        <tbody>
           <tr style="color: white; background: #133b62; font-weight:bold; font-size: 12px;">
               <td colspan="3" align="left" style="border-right: 3px solid black;">MEDIA DELIVERY ({!Media_Performance_Relationship_Tracker__c.Overall_Media_Delivery_Score_Weight__c}%)</td>
               <td align="center" style="font-size:12px; border-right: 1px dashed black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Overall_Media_Delivery_Score__c==0,"N/A",Round(Media_Performance_Relationship_Tracker__c.Overall_Media_Delivery_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='1'
                          ||$CurrentPage.parameters.numberSellers=='2'||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller1.Overall_Media_Delivery_Score__c==0,"N/A",Round(Seller1.Overall_Media_Delivery_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='2'
                          ||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller2.Overall_Media_Delivery_Score__c==0,"N/A",Round(Seller2.Overall_Media_Delivery_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller3.Overall_Media_Delivery_Score__c==0,"N/A",Round(Seller3.Overall_Media_Delivery_Score__c,1))}"/></td>
           </tr>
           <tr style="display: {!IF($CurrentPage.parameters.includeBreakdown=='true','table-row','none')}">
               <td align="left" style="font-size:11px;">Up-Front Media Coverage</td>
               <td align="left" style="font-size:11px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Up_Front_Media_Coverage_Critiera__c}"/></td>
               <td align="center" style="font-size:11px; border-right: 3px solid black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Up_Front_Media_Coverage__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Up_Front_Media_Coverage__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Up_Front_Media_Coverage__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:12px; font-weight: bold; border-right: 1px dashed black;">
                           <apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Up_Front_Media_Coverage_Score__c==0,"N/A",Round(Media_Performance_Relationship_Tracker__c.Up_Front_Media_Coverage_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='1'
                          ||$CurrentPage.parameters.numberSellers=='2'||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller1.Up_Front_Media_Coverage_Score__c==0,"N/A",Round(Seller1.Up_Front_Media_Coverage_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='2'
                          ||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller2.Up_Front_Media_Coverage_Score__c==0,"N/A",Round(Seller2.Up_Front_Media_Coverage_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller3.Up_Front_Media_Coverage_Score__c==0,"N/A",Round(Seller3.Up_Front_Media_Coverage_Score__c,1))}"/></td>
           </tr>
           <tr style="display: {!IF($CurrentPage.parameters.includeBreakdown=='true','table-row','none')}">
               <td align="left" style="font-size:11px;">Ordered Media Delivery</td>
               <td align="left" style="font-size:11px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Ordered_Media_Delivery_Criteria__c}"/></td>
               <td align="center" style="font-size:11px; border-right: 3px solid black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Ordered_Media_Delivery__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Ordered_Media_Delivery__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Ordered_Media_Delivery__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:12px; font-weight:bold; border-right: 1px dashed black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Ordered_Media_Delivery_Score__c==0,"N/A",Round(Media_Performance_Relationship_Tracker__c.Ordered_Media_Delivery_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='1'
                          ||$CurrentPage.parameters.numberSellers=='2'||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller1.Ordered_Media_Delivery_Score__c==0,"N/A",Round(Seller1.Ordered_Media_Delivery_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='2'
                          ||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller2.Ordered_Media_Delivery_Score__c==0,"N/A",Round(Seller2.Ordered_Media_Delivery_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller3.Ordered_Media_Delivery_Score__c==0,"N/A",Round(Seller3.Ordered_Media_Delivery_Score__c,1))}"/></td>
           </tr>
    
           <tr style="color: white; background: #133b62; font-weight:bold; font-size: 12px;">
               <td colspan="3" align="left" style="border-right: 3px solid black;">MEDIA PROCESSES ({!Media_Performance_Relationship_Tracker__c.Overall_Media_Processes_Score_Weight__c}%)</td>
               <td align="center" style="font-size:12px; border-right: 1px dashed black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Overall_Media_Processes_Score__c==0,"N/A",Round(Media_Performance_Relationship_Tracker__c.Overall_Media_Processes_Score__c,1))}"/></td>
                          <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='1'
                          ||$CurrentPage.parameters.numberSellers=='2'||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}"><apex:outputtext value=""/>
                          <apex:outputtext value="{!IF(Seller1.Overall_Media_Processes_Score__c==0,"N/A",Round(Seller1.Overall_Media_Processes_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='2'
                          ||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}"><apex:outputtext value=""/>
                          <apex:outputtext value="{!IF(Seller2.Overall_Media_Processes_Score__c==0,"N/A",Round(Seller2.Overall_Media_Processes_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}"><apex:outputtext value=""/>
                          <apex:outputtext value="{!IF(Seller3.Overall_Media_Processes_Score__c==0,"N/A",Round(Seller3.Overall_Media_Processes_Score__c,1))}"/></td>
           </tr>
           <tr style="display: {!IF($CurrentPage.parameters.includeBreakdown=='true','table-row','none')}">
               <td align="left" style="font-size:11px;">Missing Media Reconciliation</td>
               <td align="left" style="font-size:11px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Missing_Media_Reconciliation_Critiera__c}"/></td>
               <td align="center" style="font-size:11px; border-right: 3px solid black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Missing_Media_Reconciliation__c==0, "N/A",Media_Performance_Relationship_Tracker__c.Missing_Media_Reconciliation__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Missing_Media_Reconciliation__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:12px; font-weight:bold; border-right: 1px dashed black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Missing_Media_Reconciliation_Score__c==0,"N/A",Round(Media_Performance_Relationship_Tracker__c.Missing_Media_Reconciliation_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='1'
                          ||$CurrentPage.parameters.numberSellers=='2'||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller1.Missing_Media_Reconciliation_Score__c==0,"N/A",Round(Seller1.Missing_Media_Reconciliation_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='2'
                          ||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller2.Missing_Media_Reconciliation_Score__c==0,"N/A",Round(Seller2.Missing_Media_Reconciliation_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller3.Missing_Media_Reconciliation_Score__c==0,"N/A",Round(Seller3.Missing_Media_Reconciliation_Score__c,1))}"/></td>
           </tr>
           <tr style="display: {!IF($CurrentPage.parameters.includeBreakdown=='true','table-row','none')}">
               <td align="left" style="font-size:11px;">Media Packaging</td>
               <td align="left" style="font-size:11px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Media_Packaging_Criteria__c}"/></td>
               <td align="center" style="font-size:11px; border-right: 3px solid black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Media_Packaging__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Media_Packaging__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Media_Packaging__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:12px; font-weight:bold; border-right: 1px dashed black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Media_Packaging_Score__c==0,"N/A",Round(Media_Performance_Relationship_Tracker__c.Media_Packaging_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='1'
                          ||$CurrentPage.parameters.numberSellers=='2'||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller1.Media_Packaging_Score__c==0,"N/A",Round(Seller1.Media_Packaging_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='2'
                          ||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller2.Media_Packaging_Score__c==0,"N/A",Round(Seller2.Media_Packaging_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller3.Media_Packaging_Score__c==0,"N/A",Round(Seller3.Media_Packaging_Score__c,1))}"/></td>
           </tr>
           <tr style="display: {!IF($CurrentPage.parameters.includeBreakdown=='true','table-row','none')}">
               <td align="left" style="font-size:11px;">Delivery Technology</td>
               <td align="left" style="font-size:11px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Delivery_Technology_Criteria__c}"/></td>
               <td align="center" style="font-size:11px; border-right: 3px solid black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Delivery_Technology__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Delivery_Technology__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Delivery_Technology__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:12px; font-weight:bold; border-right: 1px dashed black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Delivery_Technology_Score__c==0,"N/A",Round(Media_Performance_Relationship_Tracker__c.Delivery_Technology_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='1'
                          ||$CurrentPage.parameters.numberSellers=='2'||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller1.Delivery_Technology_Score__c==0,"N/A",Round(Seller1.Delivery_Technology_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='2'
                          ||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller2.Delivery_Technology_Score__c==0,"N/A",Round(Seller2.Delivery_Technology_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller3.Delivery_Technology_Score__c==0,"N/A",Round(Seller3.Delivery_Technology_Score__c,1))}"/></td>
           </tr>
    
           <tr style="color: white; background: #133b62; font-weight:bold; font-size: 12px;">
               <td colspan="3" align="left" style="border-right: 3px solid black;">MEDIA PARTNERSHIP ({!Media_Performance_Relationship_Tracker__c.Overall_Media_Partnership_Score_Weight__c}%)</td>
               <td align="center" style="font-size:12px; border-right: 1px dashed black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Overall_Media_Partnership_Score__c==0,"N/A",Round(Media_Performance_Relationship_Tracker__c.Overall_Media_Partnership_Score__c,1))}"/></td>
                          <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='1'
                          ||$CurrentPage.parameters.numberSellers=='2'||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller1.Overall_Media_Partnership_Score__c==0,"N/A",Round(Seller1.Overall_Media_Partnership_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='2'
                          ||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller2.Overall_Media_Partnership_Score__c==0,"N/A",Round(Seller2.Overall_Media_Partnership_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller3.Overall_Media_Partnership_Score__c==0,"N/A",Round(Seller3.Overall_Media_Partnership_Score__c,1))}"/></td>
           </tr>
           <tr style="display: {!IF($CurrentPage.parameters.includeBreakdown=='true','table-row','none')}">
               <td align="left" style="font-size:11px;">Synergy and Solutions</td>
               <td align="left" style="font-size:11px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Synergy_Solutions_Criteria__c}"/></td>
               <td align="center" style="font-size:11px; border-right: 3px solid black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Synergy_Solutions__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Synergy_Solutions__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Synergy_Solutions__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:12px; font-weight:bold; border-right: 1px dashed black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Synergy_Solutions_Score__c==0,"N/A",Round(Media_Performance_Relationship_Tracker__c.Synergy_Solutions_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='1'
                          ||$CurrentPage.parameters.numberSellers=='2'||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller1.Synergy_Solutions_Score__c==0,"N/A",Round(Seller1.Synergy_Solutions_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='2'
                          ||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller2.Synergy_Solutions_Score__c==0,"N/A",Round(Seller2.Synergy_Solutions_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller3.Synergy_Solutions_Score__c==0,"N/A",Round(Seller3.Synergy_Solutions_Score__c,1))}"/></td>
           </tr>
           <tr style="display: {!IF($CurrentPage.parameters.includeBreakdown=='true','table-row','none')}">
               <td align="left" style="font-size:11px;">Engagement and Communications</td>
               <td align="left" style="font-size:11px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Engagement_Communications_Criteria__c}"/></td>
               <td align="center" style="font-size:11px; border-right: 3px solid black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Engagement_Communications__c==0,'N/A',Media_Performance_Relationship_Tracker__c.Engagement_Communications__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Engagement_Communications__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:12px; font-weight:bold; border-right: 1px dashed black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Engagement_Communications_Score__c==0,"N/A",Round(Media_Performance_Relationship_Tracker__c.Engagement_Communications_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='1'
                          ||$CurrentPage.parameters.numberSellers=='2'||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller1.Engagement_Communications_Score__c==0,"N/A",Round(Seller1.Engagement_Communications_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='2'
                          ||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller2.Engagement_Communications_Score__c==0,"N/A",Round(Seller2.Engagement_Communications_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller3.Engagement_Communications_Score__c==0,"N/A",Round(Seller3.Engagement_Communications_Score__c,1))}"/></td>
           </tr>
           <tr style="color: white; background: #133b62; font-weight:bold; font-size: 12px; display:{!IF($CurrentPage.parameters.includeNYOCA=='true','table-row','none')}">
               <td colspan="3" align="left" style="border-right: 3px solid black;">NY OCA PRODUCTION ({!Media_Performance_Relationship_Tracker__c.Overall_NY_OCA_Production_Score_Weight__c}%)</td>
               <td align="center" style="font-size:12px; border-right: 1px dashed black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Overall_NY_OCA_Production_Score__c==0,"N/A",Round(Media_Performance_Relationship_Tracker__c.Overall_NY_OCA_Production_Score__c,1))}"/></td>
                          <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='1'
                          ||$CurrentPage.parameters.numberSellers=='2'||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller1.Overall_NY_OCA_Production_Score__c==0,"N/A",Round(Seller1.Overall_NY_OCA_Production_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='2'
                          ||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller2.Overall_NY_OCA_Production_Score__c==0,"N/A",Round(Seller2.Overall_NY_OCA_Production_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller3.Overall_NY_OCA_Production_Score__c==0,"N/A",Round(Seller3.Overall_NY_OCA_Production_Score__c,1))}"/></td>
           </tr>
           <tr style="display:{!IF($CurrentPage.parameters.includeNYOCA=='true'&&$CurrentPage.parameters.includeBreakdown=='true','table-row','none')}">
               <td align="left" style="font-size:11px;">Weekly Fulfillment</td>
               <td align="left" style="font-size:11px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Weekly_Fulfillment_Criteria__c}"/></td>
               <td align="center" style="font-size:11px; border-right: 3px solid black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Weekly_fulfillment__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Weekly_fulfillment__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Weekly_fulfillment__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:12px; font-weight:bold; border-right: 1px dashed black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Weekly_Fulfillment_Score__c==0,"N/A",Round(Media_Performance_Relationship_Tracker__c.Weekly_Fulfillment_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='1'
                          ||$CurrentPage.parameters.numberSellers=='2'||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller1.Weekly_Fulfillment_Score__c==0,"N/A",Round(Seller1.Weekly_Fulfillment_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='2'
                          ||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller2.Weekly_Fulfillment_Score__c==0,"N/A",Round(Seller2.Weekly_Fulfillment_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller3.Weekly_Fulfillment_Score__c==0,"N/A",Round(Seller3.Weekly_Fulfillment_Score__c,1))}"/></td>
           </tr>
           <tr style="display:{!IF($CurrentPage.parameters.includeNYOCA=='true'&&$CurrentPage.parameters.includeBreakdown=='true','table-row','none')}">
               <td align="left" style="font-size:11px;">Affidavit Best Practices</td>
               <td align="left" style="font-size:11px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Affidavit_Best_Practices_Criteria__c}"/></td>
               <td align="center" style="font-size:11px; border-right: 3px solid black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Affidavit_Best_Practices__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Affidavit_Best_Practices__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Affidavit_Best_Practices__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:12px; font-weight:bold; border-right: 1px dashed black;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Affidavit_Best_Practices_Score__c==0,"N/A",Round(Media_Performance_Relationship_Tracker__c.Affidavit_Best_Practices_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='1'
                          ||$CurrentPage.parameters.numberSellers=='2'||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller1.Affidavit_Best_Practices_Score__c==0,"N/A",Round(Seller1.Affidavit_Best_Practices_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='2'
                          ||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller2.Affidavit_Best_Practices_Score__c==0,"N/A",Round(Seller2.Affidavit_Best_Practices_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller3.Affidavit_Best_Practices_Score__c==0,"N/A",Round(Seller3.Affidavit_Best_Practices_Score__c,1))}"/></td>
           </tr>
           <tr style="color: white; background: #5e9632; font-size: 16px;">
               <td colspan="3" align="left" valign="top" style="border-right: 3px solid black;"><b>Overall</b></td>
               <td align="center" valign="top" style="border-right: 1px dashed black;"><b><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Overall_Score__c==0,"N/A",Round(Media_Performance_Relationship_Tracker__c.Overall_Score__c,1))}"/></b></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='1'
                          ||$CurrentPage.parameters.numberSellers=='2'||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller1.Overall_Score__c==0,"N/A",Round(Seller1.Overall_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='2'
                          ||$CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller2.Overall_Score__c==0,"N/A",Round(Seller2.Overall_Score__c,1))}"/></td>
               <td align="center" style="font-size:11px; display: {!IF($CurrentPage.parameters.numberSellers=='3',
                          'table-col','none')}">
                          <apex:outputtext value="{!IF(Seller3.Overall_Score__c==0,"N/A",Round(Seller3.Overall_Score__c,1))}"/></td>
           </tr>
        </tbody>
    </table>

    
</body>

</apex:page>