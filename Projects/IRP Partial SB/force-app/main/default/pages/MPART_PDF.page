<apex:page standardController="Media_Performance_Relationship_Tracker__c" renderAs="pdf"
           showHeader="false" applyHtmlTag="false" applyBodyTag="false">
<head>
    <style>
        @page {
            size: letter landscape;
            margin-left: 0.4cm;
            margin-right: 0.4cm;
            martin-bottom: 0.4cm;
            margin-top: 2.3cm;
            @top-center {
                content: element(header);
            }
        }
        table, th, td, tr {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
        }
        table, tr {
            page-break-inside: avoid;
        }
        div.header {
            position: running(header);
        }
    </style> 
    <div class="header">
        <table>
            <tr style="font-weight: bold; vertical-align: middle;">
                <td style="border: 0px;">
                    <apex:image id="theImage" value="https://c.cs27.visual.force.com/resource/1565737124000/MCMLogo" width="150"/>
                </td>
                <td style="border: 0px;">
                    <apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Company__r.Name} MPART - {!Media_Performance_Relationship_Tracker__c.Quarter__c} - {!Media_Performance_Relationship_Tracker__c.Year__c}"/>
                </td>
            </tr>
        </table>
    </div>   
</head>
<body>
    <table style="width: 100%; height: 100%;">
        <thead>
           <tr>
               <th style="width: 10%; font-size: 14px;"><b>Category</b></th>
               <th style="width: 17%; font-size: 14px;"><b>Description</b></th>
               <th style="width: 8%; font-size: 14px;"><b>% Weight</b></th>
               <th style="width: 5%; font-size: 14px;"><b>Score</b></th>
               <th style="width: 60%; font-size: 14px;"><b>Comments</b></th>
           </tr>
        </thead>
        <tbody>
           <tr style="color: white; background: #133b62; font-weight:bold; font-size: 12px;">
               <td colspan="3" align="left">MEDIA DELIVERY ({!Media_Performance_Relationship_Tracker__c.Overall_Media_Delivery_Score_Weight__c}%)</td>
               <td align="center"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Overall_Media_Delivery_Score__c}"/></td>
               <td></td>
           </tr>
           <tr>
               <td align="left" style="font-size:11px;">Up-Front Media Coverage</td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Up_Front_Media_Coverage_Critiera__c}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Up_Front_Media_Coverage__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Up_Front_Media_Coverage__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Up_Front_Media_Coverage__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Up_Front_Media_Coverage_Score__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Up_Front_Media_Coverage_Score__c)}"/></td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Up_Front_Media_Coverage_Comments__c}"/></td>
           </tr>
           <tr>
               <td align="left" style="font-size:11px;">Ordered Media Delivery</td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Ordered_Media_Delivery_Criteria__c}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Ordered_Media_Delivery__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Ordered_Media_Delivery__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Ordered_Media_Delivery__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Ordered_Media_Delivery_Score__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Ordered_Media_Delivery_Score__c)}"/></td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Ordered_Media_Delivery_Comments__c}"/></td>
           </tr>
    
           <tr style="color: white; background: #133b62; font-weight:bold; font-size: 12px;">
               <td colspan="3" align="left">MEDIA PROCESSES ({!Media_Performance_Relationship_Tracker__c.Overall_Media_Processes_Score_Weight__c}%)</td>
               <td align="center"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Overall_Media_Processes_Score__c}"/></td>
               <td></td>
           </tr>
           <tr>
               <td align="left" style="font-size:11px;">Missing Media Reconciliation</td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Missing_Media_Reconciliation_Critiera__c}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Missing_Media_Reconciliation__c==0, "N/A",Media_Performance_Relationship_Tracker__c.Missing_Media_Reconciliation__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Missing_Media_Reconciliation__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Missing_Media_Reconciliation_Score__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Missing_Media_Reconciliation_Score__c)}"/></td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Missing_Media_Reconciliation_Comments__c}"/></td>
           </tr>
           <tr>
               <td align="left" style="font-size:11px;">Media Packaging</td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Media_Packaging_Criteria__c}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Media_Packaging__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Media_Packaging__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Media_Packaging__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Media_Packaging_Score__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Media_Packaging_Score__c)}"/></td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Media_Packaging_Comments__c}"/></td>
           </tr>
           <tr>
               <td align="left" style="font-size:11px;">Delivery Technology</td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Delivery_Technology_Criteria__c}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Delivery_Technology__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Delivery_Technology__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Delivery_Technology__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Delivery_Technology_Score__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Delivery_Technology_Score__c)}"/></td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Delivery_Technology_Comments__c}"/></td>
           </tr>
    
           <tr style="color: white; background: #133b62; font-weight:bold; font-size: 12px;">
               <td colspan="3" align="left">MEDIA PARTNERSHIP ({!Media_Performance_Relationship_Tracker__c.Overall_Media_Partnership_Score_Weight__c}%)</td>
               <td align="center"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Overall_Media_Partnership_Score__c}"/></td>
               <td></td>
           </tr>
           <tr>
               <td align="left" style="font-size:11px;">Synergy and Solutions</td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Synergy_Solutions_Criteria__c}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Synergy_Solutions__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Synergy_Solutions__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Synergy_Solutions__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Synergy_Solutions_Score__c==00,"N/A",Media_Performance_Relationship_Tracker__c.Synergy_Solutions_Score__c)}"/></td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Synergy_Solutions_Comments__c}"/></td>
           </tr>
           <tr>
               <td align="left" style="font-size:11px;">Engagement and Communications</td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Engagement_Communications_Criteria__c}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Engagement_Communications__c==0,'N/A',Media_Performance_Relationship_Tracker__c.Engagement_Communications__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Engagement_Communications__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Engagement_Communications_Score__c==0,'N/A',Media_Performance_Relationship_Tracker__c.Engagement_Communications_Score__c)}"/></td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Engagement_Communications_Comments__c}"/></td>
           </tr>
           <tr style="color: white; background: #133b62; font-weight:bold; font-size: 12px; display:{!IF(Media_Performance_Relationship_Tracker__c.NY_OCA_Not_Applicable__c==FALSE,'table-row','none')}">
               <td colspan="3" align="left">NY OCA PRODUCTION ({!Media_Performance_Relationship_Tracker__c.Overall_NY_OCA_Production_Score_Weight__c}%)</td>
               <td align="center"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Overall_NY_OCA_Production_Score__c}"/></td>
               <td></td>
           </tr>
           <tr style="display:{!IF(Media_Performance_Relationship_Tracker__c.NY_OCA_Not_Applicable__c==FALSE,'table-row','none')}">
               <td align="left" style="font-size:11px;">Weekly Fulfillment</td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Weekly_Fulfillment_Criteria__c}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Weekly_fulfillment__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Weekly_fulfillment__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Weekly_fulfillment__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Weekly_Fulfillment_Score__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Weekly_Fulfillment_Score__c)}"/></td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Weekly_Fulfillment_Comments__c}"/></td>
           </tr>
           <tr style="display:{!IF(Media_Performance_Relationship_Tracker__c.NY_OCA_Not_Applicable__c==FALSE,'table-row','none')}">
               <td align="left" style="font-size:11px;">Affidavit Best Practices</td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Affidavit_Best_Practices_Criteria__c}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Affidavit_Best_Practices__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Affidavit_Best_Practices__c)}"/><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Affidavit_Best_Practices__c!=0, '%','')}"/></td>
               <td align="center" style="font-size:11px;"><apex:outputtext value="{!IF(Media_Performance_Relationship_Tracker__c.Affidavit_Best_Practices_Score__c==0,"N/A",Media_Performance_Relationship_Tracker__c.Affidavit_Best_Practices_Score__c)}"/></td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Affidavit_Best_Practices_Comments__c}"/></td>
           </tr>
           <tr style="color: white; background: #5e9632; font-size: 14px;">
               <td colspan="3" align="left" valign="top"><b>Overall</b></td>
               <td align="center" valign="top"><b><apex:outputtext value="{!Round(Media_Performance_Relationship_Tracker__c.Overall_Score__c,1)}"/></b></td>
               <td align="left" style="font-size:10px; word-wrap: break-word;"><b><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Overall_Grading_Scale__c}" style="font-size:14px;"/></b><br/><apex:outputtext value="{!Media_Performance_Relationship_Tracker__c.Summary__c}"/></td>
           </tr>
        </tbody>
    </table>
</body>

</apex:page>