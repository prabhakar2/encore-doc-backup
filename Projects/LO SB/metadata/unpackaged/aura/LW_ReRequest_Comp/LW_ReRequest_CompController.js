({
    /* @ Method to load the data initially. */
	doInit : function(component, event, helper) {
        var action = component.get("c.getData");
        action.setParams({"recordId":component.get("v.recordId")});
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            if(res){
                if(res.oldRecord){
                    component.set("v.oldLWRecord",res.oldRecord); 
                    component.set("v.newLWRecord",res.newRecord);
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    /* @ Method to hide the spinner from the component. */
    /* @ Calling on load of the record-edit-form. */
    handleLoad : function(component, event, helper) {
        component.set("v.showSpinner",false);
	},
    
    /* @ Method to close the modal. */
    /* @ Calling from cancel button on the component. */
    handleCancel : function(component, event, helper) {
        helper.closeModal();
	},
    
    /* @ Method to save the re-requested record. */
    /* @ Calling from Save button in the component. */
    handleSave : function(component, event, helper) {
        var action = component.get("c.saveNewRecord");
        action.setParams({
            "newRecordStr":JSON.stringify(component.get("v.newLWRecord")),
            "oldRecordId" : component.get("v.recordId")
        });
        action.setCallback(this,function(result){
            var res = result.getReturnValue();
            if(res && !res.includes('Error')){
                helper.closeModal();
                helper.showToast("success","Success","Record Created Successfully.");
                window.open("/"+res,"_blank");
                //helper.navigateToURL("/"+res);
            }else if(res.includes('Error')){
                 component.set("v.errorMsg",res);
            }
        });
        $A.enqueueAction(action);
	}
})