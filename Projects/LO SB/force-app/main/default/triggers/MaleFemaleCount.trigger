trigger MaleFemaleCount on Contact (after insert,after update,after delete) {
    Set<Id> accIdSet = new Set<Id>();
    for(Contact con : (trigger.isdelete) ? trigger.old : trigger.new){
        if(con.AccountId != null && con.Gender__c != null && con.Salary__c != null)
            accIdSet.add(con.AccountId);
    }
    Map<Id,Account>  accMap = new Map<Id,Account>();
    List<Contact> conList = [SELECT Id,Gender__c,Salary__c,AccountId FROM Contact WHERE AccountId IN :accIdSet];
    if(accIdSet.size() > 0 && conList.size() > 0){
        for(Contact con : conList){
            Account acc = new Account(No_of_Males__c = 0,No_of_Female__c = 0,Grade_1_Contacts__c=0,Grade_2_Contacts__c=0,Grade_3_Contacts__c = 0);
            if(accMap.containsKey(con.AccountId))
                acc = accMap.get(con.AccountId);
            else
                acc.Id = con.AccountId;
            
            if(con.Gender__c == 'Male')
                acc.No_of_Males__c ++;
            else
                acc.No_of_Female__c++;
            
            if(con.Salary__c <= 20000)
                acc.Grade_3_Contacts__c++;
            else if(con.Salary__c >20000 && con.Salary__c<=50000)
                acc.Grade_2_Contacts__c++;
            else if(con.Salary__c > 50000)
                acc.Grade_1_Contacts__c++;
            accMap.put(acc.Id, acc);
        }
    }else if(accIdSet.size() > 0 && conList.size() == 0){
        for(Id accId : accIdSet){
            Account acc = new Account();
            acc.Id = accId;
            acc.No_of_Males__c = 0;
            acc.No_of_Female__c = 0;
            acc.Grade_1_Contacts__c = 0;
            acc.Grade_2_Contacts__c = 0;
            acc.Grade_3_Contacts__c = 0;
            accMap.put(acc.Id, acc);
        }
    }
    if(accMap.values().size() > 0)
        update accMap.values();
    
    
    /*
    Map<Id,Account>  accMap = new Map<Id,Account>();
    for(AggregateResult agg : [SELECT COUNT(Id) totalRec,AccountId accId,Gender__c gen
                               FROM Contact 
                               WHERE AccountId IN :accIdSet
                               GROUP BY AccountId,Gender__c]){
                                   
                                   Account acc = new Account(No_of_Males__c = 0,No_of_Female__c = 0);
                                   
                                   if(accMap.containsKey((Id)agg.get('accId'))){
                                       acc = accMap.get((Id)agg.get('accId'));
                                       if((String)agg.get('gen') == 'Male'){
                                           acc.No_of_Males__c = (Integer)agg.get('totalRec');
                                       }else if((String)agg.get('gen') == 'Female'){
                                           acc.No_of_Female__c = (Integer)agg.get('totalRec');
                                       }
                                   }else{
                                       acc.Id = (Id)agg.get('accId');
                                       if((String)agg.get('gen') == 'Male'){
                                           acc.No_of_Males__c = (Integer)agg.get('totalRec');
                                       }else if((String)agg.get('gen') == 'Female'){
                                           acc.No_of_Female__c = (Integer)agg.get('totalRec');
                                       }
                                   }
                                   accMap.put(acc.Id, acc);
                               }
    if(accMap.values().size() > 0)
        update accMap.values();*/
}