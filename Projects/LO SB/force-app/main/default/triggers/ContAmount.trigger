trigger ContAmount on Contact (after insert , after update) {
    Set<id> accId=new Set<id>();
    for(Contact con:trigger.new){
        accId.add(con.AccountId);
    }
    try{
        System.debug('@@@@@@@@-------->InTry');
        if(accId.size()>0){
            List<Account> accList=[select id,amount__c,(select id,amount__c from Contacts) from Account where id in : accId];
            if(accList.size() > 0){
                for(Account acc : accList){
                    acc.Amount__c=0;
                    for(Contact con:acc.contacts){
                        if(con.Amount__c!=null){
                            acc.Amount__c+=con.Amount__c;
                        }
                    }
                }
            }
            update accList;
        }
    } 
    catch(Exception e){
        System.debug('@@@@@@----------->'+e);
    }
}