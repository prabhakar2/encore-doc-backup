trigger contactTask on Contact__c (after insert, after update) {
	List<task> tasklist=new List<task>();
    ///////------in case of Inactive----
    Integer delTask;
    List<task> lt=[select id,subject from task where whatid in : trigger.new];
   
    for(Contact__c con:trigger.new){
        if(trigger.isInsert){
       	 if(con.status__c=='Active'){
            Task t1=new Task(subject='Call',whatId=con.Id);
            taskList.add(t1);
       	    }
  	    }
        if(trigger.isUpdate){
        if(con.status__c=='Active'){
            Task t1=new Task(subject='Call',whatId=con.Id);
            taskList.add(t1);
    	    }
            else if(con.Status__c=='Inactive'){
                if(lt.size()>0)
                     delTask=1;
            }
    	}
    }
    if(lt.size()<1)
   	    insert taskList;
    if(delTask==1)
        delete lt;
}