trigger ContactPhoneUpdate on Contact (after update) {
    if(CheckRecursion.runSet()){
        String phoneNumber ='';
        Set<id> conIdSet =  new Set<id>();
        for(Contact con : trigger.new){
            conIdSet.add(con.id);
            if(con.Phone!=trigger.oldMap.get(con.id).Phone && con.Phone!= NULL){
                phoneNumber = con.Phone;
                break;
            }
        }
        if(phoneNumber != NULL){
            List<Contact> conList = [SELECT id FROM Contact WHERE id Not IN :conIdSet ];
            for(contact con : conList){
                con.Phone = phoneNumber;
            }
            update conList;
        }
    }
}