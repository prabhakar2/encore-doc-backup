trigger OnChangingStatus on Contact__c (after update) {
   /* List<Task> taskList=new List<Task>();
    for(Contact__c conNew:trigger.new){
        for(Contact__c conOld:trigger.old){
            if(conNew.Id==conOld.id && conNew.status__c!=conOld.status__c){
                Task t1=new Task(subject='call',whatid=conNew.Id);
                taskList.add(t1);
            }
        }
    }
    if(taskList.size()>0)
    insert taskList;
    */
    List<task> taskList=new List<task>();
    for(Contact__c con:trigger.new){
        if( con.status__c!=trigger.oldMap.get(con.Id).status__c){
            System.debug('Contact details'+' '+con);
            Task t1=new Task(subject='call',whatid=con.Id);
                taskList.add(t1);
            System.debug('new Task'+' '+t1);
        }
    }
     if(taskList.size()>0)
    insert taskList;
}