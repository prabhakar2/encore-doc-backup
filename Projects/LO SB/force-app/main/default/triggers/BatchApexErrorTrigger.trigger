trigger BatchApexErrorTrigger on BatchApexErrorEvent (after insert) {
    List<BatchLeadConvertErrors__c> lcErrorList = new List<BatchLeadConvertErrors__c>();
    for(BatchApexErrorEvent obj : trigger.new){
        BatchLeadConvertErrors__c lcError = new BatchLeadConvertErrors__c();
        lcError.AsyncApexJobId__c = obj.AsyncApexJobId;
        lcError.records__c = obj.JobScope;
        lcError.StackTrace__c = obj.StackTrace; 
        lcErrorList.add(lcError);
    }
 
    if(lcErrorList.size() > 0){
        insert lcErrorList;
    }
       
    
}