({
    /* @ Method to close the record creation modal. */
	closeModal : function() {
		$A.get("e.force:closeQuickAction").fire();
	},
    
	/* @ Method to show the toast message. */
    showToast : function(type,title,msg){
        $A.get("e.force:showToast").setParams({"type" : type,"title" : title,"message":msg}).fire();
    }
})