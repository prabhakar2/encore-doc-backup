<!--********************************************** 
* Component Name	: LinkedAccountAddCompApp
* Description		: page to create new linked CAD record used in Vf page named as Linked_TrialPage 
* Created By  		: Shivangi Srivastava  
* Created Date		: 7-08-2020
* *************************************************-->
<aura:application extends="ltng:outApp" access="GLOBAL">
    <aura:dependency resource="c:LinkedAccountAddComp"/>
</aura:application>