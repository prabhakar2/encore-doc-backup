({
    loadPage : function(component){
        var recType = component.get("v.SelectedRecTypeName");
        var action=component.get("c.fetchMasterCadRecords");
        action.setParams({
            MasterId : component.get("v.recordId"),
            selectedRecTypeId : recType
        });
        action.setCallback(this, function(response){
            var state=response.getState();
            if(state=='SUCCESS'){
                console.log(response.getReturnValue());
                component.set("v.CADRecords",response.getReturnValue());
                var cad = component.get("v.CADRecords");
            }
            else if (status === "ERROR") {
                alert("Error: " + errorMessage);
            }
        });
        $A.enqueueAction(action); 
    },
    
    showToast : function(type) {
        document.getElementById(type).classList.toggle('slds-hide');
        window.setTimeout(function(){
            document.getElementById(type).classList.toggle('slds-hide');
        }, 3000);
        //var toastEvent = $A.get("e.force:showToast").setParams({"type": type,"title": title,"message": msg}).fire();
    },
    
    navigateToBaseRecord : function(newRecordId){
        window.location = '/lightning/r/Consumer_Account_Details__c/'+newRecordId+'/view'; 
    },
    
    fetchCADRecordTypes: function(component) {
        var action = component.get("c.getListOfRecordType");
        action.setCallback(this, function(actionResult) {
            var result = actionResult.getReturnValue();
            var temArr = [];
            for(var key in result){
                temArr.push({'value':key,'label':result[key]});
            }
            component.set("v.recordTypes", temArr);
         });
        $A.enqueueAction(action);
    }
})