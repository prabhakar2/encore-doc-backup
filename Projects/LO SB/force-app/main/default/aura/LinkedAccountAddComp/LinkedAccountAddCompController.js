({
    doInit : function(component, event, helper) {
        var url = window.location.href;
        var id = url.substring(url.lastIndexOf('=') + 1);
        component.set("v.recordId",id);
        helper.loadPage(component);
     },
    
    handleOnLoad : function(component, event, helper){
        component.set("v.showSpinner",false);
    },
    
    handleCancel : function(component, event, helper){
        var currentId = component.get("v.recordId");
        helper.navigateToBaseRecord(currentId);
    },
    
    handleSave : function(component,event,helper){
        component.set("v.btnName","");
    },
    
    handleSaveNew : function(component,event,helper){
        component.set("v.btnName","saveNew");
    },
    handleOnSuccess : function(component, event, helper){
        
        var newRecord = event.getParams();
        var newRecordId = newRecord.response.id; 
        if(newRecordId){
            helper.showToast('SuccessToast');
            if(component.get("v.btnName") == "saveNew"){
                component.set("v.mainComp",false);
                helper.fetchCADRecordTypes(component); 
            }else{
                component.set("v.btnName","");
                helper.navigateToBaseRecord(newRecordId);
            }
        }
    },
    createNewCADRecord : function (component, event, helper) {
        var selectedRT = component.get("v.SelectedRecTypeName");
        

        if(selectedRT) {
            var recTypes = component.get("v.recordTypes");
            var map = {};
            for(var key in recTypes){
                map[recTypes[key].value] = recTypes[key].label;
            }
            
            if(map[selectedRT] == "CAD: MCM Linked Master CAD Account Details"){
                component.set("v.linkedMasterSelected",true);
            }              
            component.set("v.mainComp",true);
            
            helper.loadPage(component);
        }else{
            helper.showToast('ErrorToast');
        } 
    },
    
})