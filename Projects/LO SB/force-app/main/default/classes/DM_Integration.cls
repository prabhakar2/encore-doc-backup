/***********************************************************************************
 * Class Name : DM_Integration
 * Description: This class is to do the callout with DM server API.
 * Created By : Prabhakar Joshi
 * Created Date:22-11-2019
 * *******************************************************************************/
public class DM_Integration {
    
    public static void doCallOut(String fromDate){
        Map<String,DM_API_Credentials__c> apiCreds = DM_API_Credentials__c.getAll();
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        String endPointURL = apiCreds.get('API 1').Endpoint_URL__c;
        
        if(fromDate != NULL && String.isNotBlank(fromDate)){
            endPointURL +='?fromDate='+fromDate;
        }
        req.setEndpoint(endPointURL);
        req.setMethod('GET');
        req.setHeader('Content-Type','application/json');
        req.setHeader('mcm-appid',apiCreds.get('API 1').mcm_appid__c);
        req.setHeader('mcm-userid', apiCreds.get('API 1').mcm_userid__c);
        req.setHeader('correlation-id', apiCreds.get('API 1').correlation_id__c);
        req.setHeader('apikey', apiCreds.get('API 1').API_Key__c);
        req.setHeader('keysecret', apiCreds.get('API 1').Key_Secret__c);
        req.setTimeout(60000);
        
        
        
        //Blob headerVal = Blob.valueOf(apiKey+':'+keysecret);
        //String header = 'BASIC '+EncodingUtil.base64Encode(headerVal);
        //req.setHeader('Authorization', header);
        
        HttpResponse res = http.send(req) ; // ;
        //HttpResponse res = new HttpResponse() ;
        
        //String resp = getFakeResponse();
        //res.setBody(resp);
        System.debug('>>>>>>>>body>>'+res.getBody());
        if(res.getBody() != NULL && String.isNotBlank(res.getBody())){
            
            DM_Integration_Helper.processJSONData(res.getBody());
            
        }
    }
    
    /******************************** Fake Response ***********************************************/
    
    private static String getFakeResponse(){
        
        String res = '{"status": {"code": 200, "message": "Success","uniqueErrorId": null,"messageCode": null},';
        res += '"data": [{"consumerAccountId": 192109,"consumerAccountIdentifierAgencyId": 300194506,"originalAccountNumber": "444796237474645",';
        res += '"reactiveFlag": null,"proactiveFlag": "No","missingMedia": null,"oaldVerificationStatus": null,';
        res += '"mediaLegalElgibility": "Yes","reactiveMissingDocTypes": "","oaldModifiedDate": null,"oaldModifiedBy": "",';
        res += '"proactiveMissingDocTypes": "((((((chrgoff OR chrgoffev) OR chrgoffactev) OR chrgoffsupev) OR lastblstmtev)';
        res += 'OR lastbillstmt) AND ((((((((((((lastpmt OR lastpmtev) OR lastpyprchev) OR pyprchstmtev) OR pystmtev)';
        res += ' OR chrgoffactev) OR lastprchev) OR lastbalxfrev) OR lastcshadvev) OR prchstmtev) OR cshadvstmtev) OR balxfrstmtev) OR lastactivity))",';
        res += '"lastUpdateDate": "2019-11-21T15:45:29","proactiveRemediationStatus": null,"reactiveRemediationStatus": null,';
        res += '"originalCreditorName": null,"sellerName": null,"firmID": "CA137","state": "CA","suitTheoryDate": "2019-06-05T12:00:00" },';
        res += '{"consumerAccountId": 480945,"consumerAccountIdentifierAgencyId": 300516712,"originalAccountNumber": "6019183466069556",';
        res += '"reactiveFlag": null,"proactiveFlag": "Yes","missingMedia": null,"oaldVerificationStatus": null,"mediaLegalElgibility": "Yes",';
        res += '"reactiveMissingDocTypes": "","oaldModifiedDate": null,"oaldModifiedBy": "","proactiveMissingDocTypes": null,';
        res += '"lastUpdateDate": "2019-11-21T15:45:29","proactiveRemediationStatus": null,"reactiveRemediationStatus": null,';
        res += '"originalCreditorName": null,"sellerName": null,"firmID": "AL4","state": "NY","suitTheoryDate": "2019-06-05T12:00:00"},';
        res += '{"consumerAccountId": 480947,"consumerAccountIdentifierAgencyId": 300516714,"originalAccountNumber": "6019170348547305",';
        res += '"reactiveFlag": null,"proactiveFlag": "No","missingMedia": null,"oaldVerificationStatus": null,"mediaLegalElgibility": "No",';
        res += '"reactiveMissingDocTypes": null,"oaldModifiedDate": null,"oaldModifiedBy": null,"proactiveMissingDocTypes": null,';
        res += '"lastUpdateDate": "2019-10-16T03:24:07","proactiveRemediationStatus": null,"reactiveRemediationStatus": null,';
        res += '"originalCreditorName": null,"sellerName": null,"firmID": "GA28","state": "NY","suitTheoryDate": "2019-01-05T12:00:00"},';
        res += '{"consumerAccountId": 480948,"consumerAccountIdentifierAgencyId": 300516715,"originalAccountNumber": "6019183471252908",';
        res += '"reactiveFlag": "No","proactiveFlag": "No","missingMedia": null,"oaldVerificationStatus": null,"mediaLegalElgibility": "Yes",';
        res += '"reactiveMissingDocTypes": "Reactive_Missing_doctype : (crdhldragrmt)","oaldModifiedDate": null,"oaldModifiedBy": "",';
        res += '"proactiveMissingDocTypes": "(crdhldragrmt)","lastUpdateDate": "2019-12-7T15:45:34","proactiveRemediationStatus": null,';
        res += '"reactiveRemediationStatus": null,"originalCreditorName": null,"sellerName": null,"firmID": "CA137","state": null,';
        res += '"suitTheoryDate": "2019-05-05T12:00:00"}],"custom": null,"timestamp": "2019-11-25T06:48:07.265Z"}';
        return res;
    }
    
    /***********************************************************************************************************/
}