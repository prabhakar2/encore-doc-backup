public class ListAccountOnSearchCtrl {
    public List<Account> listAccount{get;set;}
    public String searchText{get;set;}
    
    public ListAccountOnSearchCtrl(){
        listAccount=new List<Account>();
        listAccount=[SELECT name FROM Account];
    }
    
    public void searchAccount(){
        if(searchText!=null){
            listAccount.clear();
        	listAccount=[SELECT name FROM Account WHERE name LIKE :'%'+searchText+'%'];
            if(listAccount==null){
                ApexPages.addMessage(new ApexPages.message(apexPages.Severity.INFO,'No Result Found'));
            }
        }
        
    }
}