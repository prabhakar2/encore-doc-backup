public class countryController{

    public String selectedState { get; set; }
    public String selectedCountryCode {get; set;}
    public List<SelectOption> CountryCodeList=new List<SelectOption>();
    public List<SelectOption> StateCodeList{get;set;}
    
    public  List<SelectOption> getCountryCodes(){
        if(countryCodeList.isEmpty()){
            countryCodeList=new List<SelectOption>();
            List<Countries__c> allCountries = new List<Countries__c>([SELECT  Name,CountryCode__c FROM    Countries__c]);
            allCountries.sort();
            if (allCountries.size() > 0){
                
                for(Countries__c country : allCountries )
                    countryCodeList.add( new SelectOption( country.CountryCode__c, country.Name ) );
            
            }
            else
                countryCodeList.add(new SelectOption('--Select--', '--Select--'));
        
       } 
     
        return countryCodeList;
    }
    
    
    public void populateState()
    {
        List<States__c> stateList=new List<States__c>([select StateCode__c,Country_Code__c from states__c where Country_Code__c=:selectedCountryCode]);
        StateCodeList  = new List<SelectOption>();
        if(stateList.size() > 0){
            stateList.sort();
            for(States__c state : stateList){
                StateCodeList.add( new SelectOption(state.StateCode__c,state.StateCode__c) );
            }  
        }
    }
}