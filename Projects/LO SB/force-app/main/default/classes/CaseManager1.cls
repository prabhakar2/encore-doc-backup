@RestResource(urlMapping='/newcases/*')
global class CaseManager1
{
	  /*  @HttpGet
    global static Case getCaseById() {
        RestRequest request = RestContext.request;
      
      String caseId = request.requestURI.substring(
          request.requestURI.lastIndexOf('/')+1);
       
        Case result =  [SELECT CaseNumber,Subject,Status,Origin,Priority
                        FROM Case
                        WHERE Id = :caseId];
        system.debug(result);
        return result;
    }*/
   
    
    @HttpGet
    global static List<Case> getAllCases() {
        RestRequest request = RestContext.request;
      
     // String caseId = request.requestURI.substring(
      //    request.requestURI.lastIndexOf('/')+1);
       
        List<Case> result =  [SELECT CaseNumber,Subject,Status,Origin,Priority
                        FROM Case ];
        system.debug(result);
        return result;
    }
   
    @HttpPost
   global static ID createCase( String status, String origin) {
        Case thisCase = new Case(
           
            Status=status,
            Origin=origin
           );
        insert thisCase;
        return thisCase.Id;
    }  
 
    @HttpDelete
    global static void deleteCase() {
        RestRequest request = RestContext.request;
        String caseId = request.requestURI.substring(
            request.requestURI.lastIndexOf('/')+1);
        Case thisCase = [SELECT Id FROM Case WHERE Id = :caseId];
        delete thisCase;
    }    
    
    @HttpPatch
    global static id updateCase(String subject,String status, String origin)
    {
         RestRequest request = RestContext.request;
        String caseId = request.requestURI.substring(request.requestURI.lastIndexOf('/')+1);
         Case mycase = [SELECT subject,Status, origin from Case WHERE Id =:caseId];
		
        mycase.Subject=subject;
         mycase.status = status;
         mycase.Origin= origin;
        
        update mycase;
  		return mycase.Id;
	
         
    }
 
}