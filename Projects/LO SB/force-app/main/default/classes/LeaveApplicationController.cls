public class LeaveApplicationController{
    public list<Holidays__c> holidayList{get;set;}
    public Leave_Application__c leave{get;set;}
    
    public LeaveApplicationController(){
        leave=new Leave_Application__c();
    }
    public pageReference save(){
        insert leave;
        return new pagereference('/'+leave.id);
    }
    
    public pageReference savenew(){
    insert leave;
    leave=new Leave_Application__c();
    return page.LeaveApplicationPage;
    }
    public void showHoli(){
        holidayList=[select name,date__c from Holidays__c ];
    }
    
}