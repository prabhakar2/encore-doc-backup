public class CalculateArea extends NewArea
{
     public override double rectangleArea(double l,double b)
     {
         return(l*b);
     }
     public override double hexagonArea(double l)
     {
         return((3*1.73*l*l)/2);
     }
    public double squareArea(double l)
    {
        return(l*l);
    }
    
}