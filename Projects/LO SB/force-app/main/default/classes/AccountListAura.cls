public class AccountListAura {
    @auraEnabled
    public static list<Account> showAccList(){
        list<Account> listAcc=[SELECT id,Name FROM Account LIMIT 10];
        if(listAcc.size()>0){
            return listAcc;
        }else
            return null;
    }
}