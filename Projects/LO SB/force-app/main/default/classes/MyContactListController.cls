public class MyContactListController {
@auraEnabled 
    public static list<Contact> getContacts(){
        return [select id,name,email,phone from contact];
    }
}