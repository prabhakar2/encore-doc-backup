public class CreateContact{
    public list<contact> listCon{get;set;}
    public id accId;
    public Account acc;
    Integer Count=0;
    public Integer check{get;set;}
   
    
    public CreateContact(){
    
        set<date> holiday=new set<date>();
        for(Holidays__c hd:[select date__c from Holidays__c])
            holiday.add(hd.date__c);
        
        check=0;
        listCon=new list<contact>();
        
        accId=ApexPages.currentPage().getParameters().get('Id');
        system.debug('acccc--->>'+acc);
        acc=[select Day_of_week__c,Start_Date__c,End_Date__c from account where id=:accId];
      	
        String[] str=acc.Day_of_week__c.split(';');
        set<string> daySet=new set<string>();
        for(String s:str)
            daySet.add(s);
        Date dat=acc.Start_Date__c;
        while(count<10){
            Datetime dt = DateTime.newInstance(dat, Time.newInstance(0, 0, 0, 0));
            String dayOfWeek=dt.format('EEEE');
            if(daySet.contains(dayOfWeek)){
               if(holiday.contains(dat)){
                   dat=dat.addDays(1);
                   continue;
               }
               contact con1=new contact();
               con1.date__c=dat;
               con1.day__c=dayOfWeek;
               con1.accountId=accId;
               listCon.add(con1);
               count++;
           }
           dat=dat.addDays(1);
        }
        acc.End_Date__c =dat.addDays(-1);
  }
   public pageReference Save(){
        check=1;
        insert listCon;
        update acc;
        return null;
    }
   
}