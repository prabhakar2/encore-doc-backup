public class SpeakerControllerExtension {
    public blob picture{get;set;}
    public string errorMessage{get;set;}
    public Integer counter{get;set;}
    private final speaker__c speaker;
    private ApexPages.StandardController stdcontroller;
    public SpeakerControllerExtension(ApexPages.StandardController stdController){
        this.speaker=(speaker__c)stdController.getRecord();
        this.stdController=stdController;
        counter=0;
    }
    public void increment(){
        counter++;
    }
    public pageReference Save(){
        errorMessage='';
        try{
            upsert speaker;
            if(picture!=null){
                Attachment att=new Attachment();
                att.Body=picture;
                att.Name='speaker_'+speaker.id + '.jpg';
                att.ParentId=speaker.Id;
                att.ContentType='application/jpg';
                insert att;
                speaker.Picture_Path__c='/servlet/servlet.FileDownload?file='+ attachment.id;
                    update speaker;
            }
            return new ApexPages.StandardController(speaker).view();
        }
        catch(system.Exception ex){
            errormessage=ex.getMessage();
            return null;
        }
    }
}