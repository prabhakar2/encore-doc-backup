public class deleteMultipleContacts {
    
    //------------CREATING A LIST OF TYPE WRAPPER CLASS---------
    //WRAPPER CLASS WRAPS THE CONTACT DATA WITH A DEDICATED VF PAGE CHECKBOX
    public List<Wrap> ContactList{get;set;}
    
    //-----WRAPPING CONTACTS WITH CHECKBOX--------
    public List<wrap> getContacts(){
        if(contactList==Null){
            contactList = new List<wrap>();
            for(Contact__c con:[Select id,name,first_name__c,account__c,phone__c from contact__c])
                contactList.add(new wrap(con));
        }   
        return contactList;
    }
   
    //----------DELETE BUTTON ACTION----------------
    public pageReference deleteMultiple(){
        List<Contact__c> selectedContact = new List<Contact__c>();
        
 		//-------ADDING SELECTED CONTACTS TO A NEW CONTACT LIST
        for(wrap w1:contactList){
            if(w1.check==TRUE){
                selectedContact.add(w1.con);
            }
        }
        delete selectedContact;
        selectedContact.clear();
        contactList=Null;
        return null;
    }
    
    public class Wrap {
    public contact__c con{get;set;}
    public boolean check{get;set;}
    
    public wrap(Contact__c c){
        con=c;
        check=false;
    }

}
    
}